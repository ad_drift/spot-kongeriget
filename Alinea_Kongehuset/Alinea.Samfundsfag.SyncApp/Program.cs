﻿using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.SyncApp.Properties;
using CoreM41.Common;
using CoreM41.Diagnostics.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Alinea.Samfundsfag.SyncApp
{
	class Program
	{
		static void Main(string[] args)
		{
			string connectionString = Config.Instance.ConnectionString;
			SamfundsfagUmbracoDataContext dataContext = new SamfundsfagUmbracoDataContext(connectionString);
			bool databaseExists = false;

			int monthsOfInactivityAllowed = 1;
			if (args.Length > 0)
			{
				if (!Int32.TryParse(args[0], out monthsOfInactivityAllowed))
					monthsOfInactivityAllowed = 1;
			}

			try
			{
				databaseExists = dataContext.DatabaseExists();
				dataContext.Connection.Open();
			}
			catch (Exception databaseTestException)
			{
				Logger.LogError(new Exception("No database connection. Aborting...", databaseTestException));
				return;
			}

			SchoolClassService schoolService = new SchoolClassService(dataContext);
			StringBuilder stringBuilder = new StringBuilder();
			Stopwatch stopwatch = new Stopwatch();
			TimeSpan totalElapsedTime = new TimeSpan();
			List<School> failedSchools = new List<School>();
			List<School> schools;

			try
			{
				schools = schoolService.GetAllActiveSchools(Settings.Default.HoursBeforeResynchronization, monthsOfInactivityAllowed);
			}
			catch (Exception loadSchoolsException)
			{
				Logger.LogError(loadSchoolsException);
				return;
			}

			int amountOfSchools = schools.Count;
			string message = "Started synchronization of " + amountOfSchools + " school(s)." + Environment.NewLine;
			stringBuilder.AppendLine(message);
			Console.WriteLine(message);
			Logger.LogVerbose(message);

			for (int index = 0; index < amountOfSchools; index++)
			{
				int currentSchool = index + 1;
				School school = schools[index];

				message = "Synchronizing school " + currentSchool + " of " + amountOfSchools + ": " + (school.Name + " (" + school.UniInstituionId + ")").PadRight(40, '.') + ". ";
				stringBuilder.Append(message);
				Console.Write(message);

				stopwatch.Start();
				SchoolClassService schoolService2 = new SchoolClassService(new SamfundsfagUmbracoDataContext(connectionString));
				bool isSynched = schoolService2.SyncSchool(school.UniInstituionId, true);

				if (!isSynched)
				{
					SchoolClassService schoolService3 = new SchoolClassService(new SamfundsfagUmbracoDataContext(connectionString));
					isSynched = schoolService3.SyncSchool(school.UniInstituionId, true);

					if (!isSynched)
					{
						failedSchools.Add(school);
					}

					message = "(resync) ";
					stringBuilder.Append(message);
					Console.Write(message);
				}
				
				stopwatch.Stop();

				TimeSpan elapsedTime = stopwatch.Elapsed;
				totalElapsedTime = totalElapsedTime.Add(elapsedTime);
				stopwatch.Reset();

				message = elapsedTime.ToString(@"hh\:mm\:ss") + Environment.NewLine;
				stringBuilder.Append(message);
				Console.Write(message);
			}

			if (failedSchools.Count > 0)
			{
				message = Environment.NewLine + "Failed to sync schools:";

				foreach (School school in failedSchools)
				{
					message += Environment.NewLine + "\t" + school.Name + " (" + school.UniInstituionId + ")";
				}

				stringBuilder.AppendLine(message);
				Console.WriteLine(message);
			}

			message = Environment.NewLine + "Done. Total elapsed time " + totalElapsedTime.ToString(@"hh\:mm\:ss");
			stringBuilder.AppendLine(message);
			Console.WriteLine(message);
			Logger.LogInfo(stringBuilder.ToString());

			if (Config.Instance.Environment == CoreM41.Common.Enums.Environment.Local)
			{
				Console.ReadKey();
			}
		}
	}
}
