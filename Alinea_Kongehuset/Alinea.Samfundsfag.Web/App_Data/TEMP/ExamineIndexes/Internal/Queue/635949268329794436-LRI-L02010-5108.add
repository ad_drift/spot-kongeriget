<?xml version="1.0" encoding="utf-16"?>
<dictionary>
  <item>
    <key>
      <string>abstract</string>
    </key>
    <value><![CDATA[
Karim Arfaoui fik 253 personlige stemmer ved kommunalvalget,
hvilket rækker til en plads i Roskilde Byråd. Ved den efterfølgende
fordeling af opgaver blev han valgt som politisk ordfører, dvs. at
han skal diskutere politik på vegne af partiet. Han fik også en
plads i økonomiudvalget, som er ansvarlige for kommunens
økonomi.
]]></value>
  </item>
  <item>
    <key>
      <string>content</string>
    </key>
    <value><![CDATA[
Karim Arfaoui fik 253 personlige stemmer og er valgt
ind i Roskilde Byråd. Her har han fået posten som politisk ordfører
og en plads i økonomiudvalget.

 Kommunalvalget er overstået, stemmerne er talt op, og Karim
Arfaoui kan glæde sig over resultatet. Med 253 personlige stemmer
er han nemlig valgt til byrådet i Roskilde. Det er lidt færre
stemmer, end han oprindeligt regnede med at skulle have for at være
sikker, men socialdemokraterne fik et godt valg i Roskilde, så det
viste sig at være rigeligt. Faktisk var han den af de 13 valgte
socialdemokrater, der fik syvende-flest stemmer.

 – Jeg havde sat næsen op efter at blive blandt de første ti, så
det er dejligt at være nummer syv. Jeg er faktisk overrasket over,
hvor påvirket jeg blev, da jeg fik resultatet. Jeg fik helt
gåsehud, da mit navn blev læst op blandt dem, der var valgt, siger
Karim.

 

Konstituering

Da stemmerne var talt op, mødtes alle de valgte for at
konstituere sig. Et byråd har en masse forskellige udvalg, som
arbejder med alt fra økonomi til sportspladser og parker, og hvert
parti har nogle pladser i alle udvalgene. De har også nogle
forskellige ordførerskaber. Det er særlige ansvarsområder, som de
forskellige byrådsmedlemmer får at arbejde med. Man kalder det "at
konstituere sig", når man fordeler udvalgsposter og
ordførerskaber.

Karim fik den vigtige post som politisk ordfører og en plads i
det magtfulde økonomiudvalg, som styrer kommunens økonomi og skal
godkende alle beslutninger, der tages i byrådet. Den eneste anden
socialdemokrat i det udvalg er borgmesteren, Joy Mogensen.

 – Vi fik resultaterne kl. 15.30 dagen efter valget, og kl. 18.30
var der konstitueringsmøde om, hvilke poster vi skulle have. Jeg
blev politisk ordfører, som er den, der skal gå ud at forsvare
borgmesteren og vores politik, og så kom jeg med i økonomiudvalget.
Det er nogle drømmeposter, og jeg er sindssygt glad, fortæller
han.

Glad, men træt

Udover at være glad er Karim også overordentligt træt. Dels har
han gennem et halvt år kørt en valgkamp, hvor alle weekender er
brugt på at tage ud og møde vælgere, og dels har de seneste dage
budt på ekstra meget arbejde og nogle meget sene aftener.

Allerede da det tirsdag aften var afgjort, hvor mange pladser
hvert parti havde fået, gik forhandlingerne i gang om, hvem der
skulle være borgmester, og hvordan andre magtfulde poster skulle
fordeles mellem partierne.

– Vi fandt jo ud af, hvor mange stemmer de forskellige partier
fik tirsdag aften, og så gik forhandlingerne i gang. Det foregik
indtil kl. 3 om natten, så den var over 4, inden jeg gik i seng.
Næste dag var vi oppe at tælle stemmerne sammen og så til
konstitueringsmøde om aftenen. Nu glæder jeg mig til at slappe lidt
af sammen med familien og få lidt ro på, slutter Karim.

 Som alle andre nyvalgte kandidater begynder Karim arbejdet i
byrådet 1. januar.

 

&nbsp;
]]></value>
  </item>
  <item>
    <key>
      <string>copyright</string>
    </key>
    <value><![CDATA[Jakob Horn Møller]]></value>
  </item>
  <item>
    <key>
      <string>fact</string>
    </key>
    <value><![CDATA[5114]]></value>
  </item>
  <item>
    <key>
      <string>includeInBottomNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>includeInTopNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>largeImage</string>
    </key>
    <value><![CDATA[5112]]></value>
  </item>
  <item>
    <key>
      <string>linkText</string>
    </key>
    <value><![CDATA[Læs mere]]></value>
  </item>
  <item>
    <key>
      <string>linkToFirstChild</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>menuTitle</string>
    </key>
    <value><![CDATA[Karim blev valgt]]></value>
  </item>
  <item>
    <key>
      <string>poll</string>
    </key>
    <value><![CDATA[5115]]></value>
  </item>
  <item>
    <key>
      <string>promotionText</string>
    </key>
    <value><![CDATA[Karim fik 253 personlige stemmer, hvilket rækker til en plads i Roskilde Byråd]]></value>
  </item>
  <item>
    <key>
      <string>showAsPromotion</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>showOnNewsOverview</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>showOnStudent</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>smallImage</string>
    </key>
    <value><![CDATA[5111]]></value>
  </item>
  <item>
    <key>
      <string>tags</string>
    </key>
    <value><![CDATA[kommunalvalg,valgkamp,byråd,konstituering,personlige stemmer]]></value>
  </item>
  <item>
    <key>
      <string>text</string>
    </key>
    <value><![CDATA[Karim Arfaoui fik 253 personlige stemmer ved kommunalvalget, hvilket rækker til en plads i Roskilde Byråd. Ved den efterfølgende fordeling af opgaver blev han valgt som politisk ordfører, dvs. at han skal diskutere politik på vegne af partiet. Han fik også en plads i økonomiudvalget, som er ansvarlige for kommunens økonomi.]]></value>
  </item>
  <item>
    <key>
      <string>title</string>
    </key>
    <value><![CDATA[Karim blev valgt]]></value>
  </item>
  <item>
    <key>
      <string>umbracoNaviHide</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>id</string>
    </key>
    <value><![CDATA[5108]]></value>
  </item>
  <item>
    <key>
      <string>nodeName</string>
    </key>
    <value><![CDATA[Karim blev valgt]]></value>
  </item>
  <item>
    <key>
      <string>updateDate</string>
    </key>
    <value><![CDATA[2014-06-23T11:10:02]]></value>
  </item>
  <item>
    <key>
      <string>writerName</string>
    </key>
    <value><![CDATA[admin]]></value>
  </item>
  <item>
    <key>
      <string>path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5108]]></value>
  </item>
  <item>
    <key>
      <string>nodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>parentID</string>
    </key>
    <value><![CDATA[1174]]></value>
  </item>
  <item>
    <key>
      <string>__Path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5108]]></value>
  </item>
  <item>
    <key>
      <string>__NodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>__NodeId</string>
    </key>
    <value><![CDATA[5108]]></value>
  </item>
  <item>
    <key>
      <string>__IndexType</string>
    </key>
    <value><![CDATA[content]]></value>
  </item>
</dictionary>