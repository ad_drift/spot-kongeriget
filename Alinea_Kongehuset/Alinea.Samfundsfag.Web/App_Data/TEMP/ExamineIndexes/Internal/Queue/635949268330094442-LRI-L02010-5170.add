<?xml version="1.0" encoding="utf-16"?>
<dictionary>
  <item>
    <key>
      <string>abstract</string>
    </key>
    <value><![CDATA[
Blasfemiparagraffen forbyder alle at håne religioner. Det er
næsten 70 år siden, at nogen er blevet dømt efter den, men
alligevel foregår der i øjeblikket en debat om, hvorvidt den skal
afskaffes. Debatten handler meget om, hvorvidt man vil sende et
signal om, at ytringsfrihed er vigtigere end religioner, eller om
man vil vise, at man respekterer folks religion.
]]></value>
  </item>
  <item>
    <key>
      <string>content</string>
    </key>
    <value><![CDATA[
Blasfemiparagraffen forbyder at håne religioner.
Selvom den ikke er brugt i 70 år, er der debat om dens afskaffelse.
Debatten handler om ytringsfrihed og respekt for andres
religioner.

 Blasfemiparagraffen er en paragraf, altså et afsnit, i den danske
lovgivning, som forbyder at håne eller spotte religioner. Gør man
det alligevel, giver loven mulighed for, at man bliver idømt op til
fire måneders fængsel. Der er dog ikke nogen, der er blevet dømt
efter den siden 1946, hvor et par fik en bøde for at klæde sig ud
som præster til et karneval og døbe en dukke.

Vil afskaffe den

I øjeblikket er der debat om at afskaffe paragraffen. Det
skyldes ikke mindst, at ligestillings- og kirkeminister, Manu
Sareen, har foreslået at afskaffe den, fordi han mener, at hensynet
til ytringsfrihed må veje tungere end hensynet til religion. Han
har også begrundet det med, at han finder det mærkeligt, at der er
forbud mod at håne religioner, men intet forbud mod for eksempel at
håne den danske grundlov.

 

&nbsp;
 Manu Sareen får opbakning til sit synspunkt fra flere sider.
Liberal Alliance har længe ønsket blasfemiparagraffen ophævet.
Menneskerettighedsorganisationer bakker også op, fordi de mener, at
paragraffen krænker ytringsfriheden. Dansk Folkeparti støtter også
afskaffelsen. Partiet har tidligere støttet karikaturtegneres ret
til at tegne profeten Muhammed, hvilket muslimer betragter som
blasfemisk, og partiet ønsker at fjerne al tvivl om, hvorvidt det
er lovligt.

Modstand i befolkningen

Forslaget om at afskaffe paragraffen er dog mindre populært i
befolkningen. Undersøgelser viser, at to ud af tre er imod en
afskaffelse af paragraffen. Det mest almindelige argument for at
beholde paragraffen er, at den alligevel aldrig bliver brugt, men
er et godt signal om, at man respekterer hinandens
religioner.

 Andre argumenterer med, at der stadig er rig mulighed for at
kritisere religion, selvom vi har en blasfemiparagraf. Man må
nemlig gerne kritisere, så længe man ikke håner og spotter. Det
sidste mener de ikke, at der kommer noget godt eller konstruktivt
ud af, men at det i stedet bidrager til fjendskab og manglende
forståelse mellem folk.

Racismeparagraffen

En anden paragraf, som nogle ønsker afskaffet, er
racismeparagraffen. Det er en paragraf, som ligner
blasfemiparagraffen, men den forbyder trusler, hån og nedværdigelse
af folk på baggrund af deres nationalitet eller hudfarve.
]]></value>
  </item>
  <item>
    <key>
      <string>copyright</string>
    </key>
    <value><![CDATA[Jakob Horn Møller]]></value>
  </item>
  <item>
    <key>
      <string>fact</string>
    </key>
    <value><![CDATA[5177]]></value>
  </item>
  <item>
    <key>
      <string>includeInBottomNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>includeInTopNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>largeImage</string>
    </key>
    <value><![CDATA[5173]]></value>
  </item>
  <item>
    <key>
      <string>linkText</string>
    </key>
    <value><![CDATA[Læs mere]]></value>
  </item>
  <item>
    <key>
      <string>linkToFirstChild</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>menuTitle</string>
    </key>
    <value><![CDATA[Blasfemiparagraffen]]></value>
  </item>
  <item>
    <key>
      <string>poll</string>
    </key>
    <value><![CDATA[5179]]></value>
  </item>
  <item>
    <key>
      <string>promotionText</string>
    </key>
    <value><![CDATA[Blasfemiparagraffen forbyder at håne religioner. Selvom den ikke er brugt i 70 år, er der debat om dens afskaffelse.]]></value>
  </item>
  <item>
    <key>
      <string>showAsPromotion</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>showOnNewsOverview</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>showOnStudent</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>smallImage</string>
    </key>
    <value><![CDATA[5172]]></value>
  </item>
  <item>
    <key>
      <string>tags</string>
    </key>
    <value><![CDATA[blasfemiparagraffen,ytringsfrihed]]></value>
  </item>
  <item>
    <key>
      <string>text</string>
    </key>
    <value><![CDATA[Blasfemiparagraffen forbyder alle at håne religioner. Det er næsten 70 år siden, at nogen er blevet dømt efter den, men alligevel foregår der i øjeblikket en debat om, hvorvidt den skal afskaffes. Debatten handler meget om, hvorvidt man vil sende et signal om, at ytringsfrihed er vigtigere end religioner, eller om man vil vise, at man respekterer folks religion. ]]></value>
  </item>
  <item>
    <key>
      <string>title</string>
    </key>
    <value><![CDATA[Blasfemiparagraffen]]></value>
  </item>
  <item>
    <key>
      <string>umbracoNaviHide</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>id</string>
    </key>
    <value><![CDATA[5170]]></value>
  </item>
  <item>
    <key>
      <string>nodeName</string>
    </key>
    <value><![CDATA[Blasfemiparagraffen]]></value>
  </item>
  <item>
    <key>
      <string>updateDate</string>
    </key>
    <value><![CDATA[2014-06-23T11:10:04]]></value>
  </item>
  <item>
    <key>
      <string>writerName</string>
    </key>
    <value><![CDATA[admin]]></value>
  </item>
  <item>
    <key>
      <string>path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5170]]></value>
  </item>
  <item>
    <key>
      <string>nodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>parentID</string>
    </key>
    <value><![CDATA[1174]]></value>
  </item>
  <item>
    <key>
      <string>__Path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5170]]></value>
  </item>
  <item>
    <key>
      <string>__NodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>__NodeId</string>
    </key>
    <value><![CDATA[5170]]></value>
  </item>
  <item>
    <key>
      <string>__IndexType</string>
    </key>
    <value><![CDATA[content]]></value>
  </item>
</dictionary>