<?xml version="1.0" encoding="utf-16"?>
<dictionary>
  <item>
    <key>
      <string>abstract</string>
    </key>
    <value><![CDATA[
Som alle andre sorte sydafrikanere blev Nelson Mandela
undertrykt af det racistiske, hvide apartheidstyre gennem årtier.
Han sad 27 år i fængsel for sin rolle i modstandskampen mod styret.
I 1994 blev han præsident i Sydafrika og i stedet for at tage hævn
over sine undertrykkere, var han med til at bringe fred og
forsoning til landet.
]]></value>
  </item>
  <item>
    <key>
      <string>content</string>
    </key>
    <value><![CDATA[
Nelson Mandela blev undertrykt og sad i fængsel en
stor del af sit liv. Alligevel endte han som præsident i Sydafrika,
hvor han søgte fred og forsoning med sine undertrykkere i stedet
for hævn.&nbsp;

 Verdens ledere strømmede til Sydafrika for at hylde og mindes
Nelson Mandela, der døde d. 5. december 2013, 95 år gammel. Det er
der god grund til, for gennem sit lange liv fik Mandela sat
betydningsfulde aftryk i verden og selvfølgelig især i sit eget
land, Sydafrika.



 

Apartheid

Da Nelson Mandela var ung, var Sydafrika styret af hvide
mennesker, især efterkommere af tilflyttere fra Holland. De førte
en såkaldt apartheid-politik, som gik ud på at adskille sorte og
hvide mennesker. Det hvide mindretal levede godt, mens det store
sorte flertal levede i fattigdom og ikke havde adgang til de samme
goder som de hvide.

 Som ung jurastuderende, blev Mandela aktiv i modstanden mod
apartheidstyret og medlem af partiet ANC. I første omgang var hans
modstand fredelig, men i 1961 blev han leder af ANC's væbnede fløj.
Det skete efter en erkendelse af, at man ikke kom nogen vegne med
fredelige metoder over for det stædige, hvide apartheidstyre.

27 år i fængsel

Tilværelsen som aktiv leder af ANC's væbnede fløj blev dog kort,
for allerede året efter blev Mandela anholdt og idømt fængsel på
livstid for sin rolle i den væbnede modstand mod styret. Han
tilbragte de næste 27 år i fængsel, men blev benådet og løsladt
igen i 1990. Det skete efter vedvarende pres mod styret, både
internationalt og fra den sydafrikanske befolkning.&nbsp;

 Efter sin løsladelse blev Nelson Mandela leder af ANC og begyndte
at forhandle med styret og den nye præsident F.W. de Klerk om at
stoppe apartheidpolitikken og gøre ANC, som var blevet gjort
ulovligt af styret, lovligt igen.

 

Præsident

Begge dele lykkedes, og i 1994 stillede Nelson Mandela op som
præsident i Sydafrika og vandt en overvældende sejr. Mange
forventede, at Mandela som Sydafrikas første sorte præsident, ville
tage hævn over de mange års apartheidstyre og sine egne mange år i
fængsel ved at undertrykke de hvide sydafrikanere, men det skete
ikke.

 Tværtimod gik han målrettet efter et Sydafrika, hvor alle har lige
rettigheder og lever i fred og fordragelighed med hinanden, og
derfor er han i eftertiden blevet et symbol på frihed, fred og
forsoning. Nelson Mandela var præsident i Sydafrika i fem år. Alle
sydafrikanske præsidenter siden ham har også været sorte og
medlemmer af ANC.&nbsp;
]]></value>
  </item>
  <item>
    <key>
      <string>copyright</string>
    </key>
    <value><![CDATA[Jakob Horn Møller]]></value>
  </item>
  <item>
    <key>
      <string>includeInBottomNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>includeInTopNavigation</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>largeImage</string>
    </key>
    <value><![CDATA[5146]]></value>
  </item>
  <item>
    <key>
      <string>linkText</string>
    </key>
    <value><![CDATA[Læs mere]]></value>
  </item>
  <item>
    <key>
      <string>linkToFirstChild</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>menuTitle</string>
    </key>
    <value><![CDATA[Nelson Mandela]]></value>
  </item>
  <item>
    <key>
      <string>poll</string>
    </key>
    <value><![CDATA[5150]]></value>
  </item>
  <item>
    <key>
      <string>promotionText</string>
    </key>
    <value><![CDATA[Nelson Mandela sad i fængsel det meste af sit liv. Alligevel endte han som præsident i Sydafrika, hvor han søgte fred og forsoning med sine undertrykkere i stedet for hævn. ]]></value>
  </item>
  <item>
    <key>
      <string>showAsPromotion</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>showOnNewsOverview</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>showOnStudent</string>
    </key>
    <value><![CDATA[1]]></value>
  </item>
  <item>
    <key>
      <string>smallImage</string>
    </key>
    <value><![CDATA[5145]]></value>
  </item>
  <item>
    <key>
      <string>tags</string>
    </key>
    <value><![CDATA[Sydafrika,apartheid,Nelson Mandela,Mandela,forsoning,fred,leder]]></value>
  </item>
  <item>
    <key>
      <string>text</string>
    </key>
    <value><![CDATA[Nelson Mandela blev undertrykt og sad i fængsel en stor del af sit liv. Alligevel endte han som præsident i Sydafrika, hvor han søgte fred og forsoning med sine undertrykkere i stedet for hævn. ]]></value>
  </item>
  <item>
    <key>
      <string>title</string>
    </key>
    <value><![CDATA[Nelson Mandela]]></value>
  </item>
  <item>
    <key>
      <string>umbracoNaviHide</string>
    </key>
    <value><![CDATA[0]]></value>
  </item>
  <item>
    <key>
      <string>id</string>
    </key>
    <value><![CDATA[5142]]></value>
  </item>
  <item>
    <key>
      <string>nodeName</string>
    </key>
    <value><![CDATA[Nelson Mandela]]></value>
  </item>
  <item>
    <key>
      <string>updateDate</string>
    </key>
    <value><![CDATA[2014-06-23T11:10:03]]></value>
  </item>
  <item>
    <key>
      <string>writerName</string>
    </key>
    <value><![CDATA[admin]]></value>
  </item>
  <item>
    <key>
      <string>path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5142]]></value>
  </item>
  <item>
    <key>
      <string>nodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>parentID</string>
    </key>
    <value><![CDATA[1174]]></value>
  </item>
  <item>
    <key>
      <string>__Path</string>
    </key>
    <value><![CDATA[-1,1110,1136,1174,5142]]></value>
  </item>
  <item>
    <key>
      <string>__NodeTypeAlias</string>
    </key>
    <value><![CDATA[NewsArticle]]></value>
  </item>
  <item>
    <key>
      <string>__NodeId</string>
    </key>
    <value><![CDATA[5142]]></value>
  </item>
  <item>
    <key>
      <string>__IndexType</string>
    </key>
    <value><![CDATA[content]]></value>
  </item>
</dictionary>