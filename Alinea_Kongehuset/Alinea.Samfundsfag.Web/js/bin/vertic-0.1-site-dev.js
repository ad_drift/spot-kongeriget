﻿/*
* Vertic JS - Site functional wrapper
* http://labs.vertic.com
*
* Copyright 2012, Vertic A/S
*
* Date: Tue Jan 31 12:00:00 2012 +0200
*/
(function (_v, jQuery, Modernizr) {
	// Hook in to Vertic lib or create if unavailable
	_v = typeof _v !== 'undefined' ? _v : typeof window._v !== 'undefined' ? window._v : new Object();

	// Hook in to 3rd party libs and make "normal" variable names available
	var jQuery = typeof jQuery !== 'undefined' ? jQuery : typeof window.jQuery !== 'undefined' ? window.jQuery : false;
	var $ = jQuery;
	var Modernizr = typeof Modernizr !== 'undefined' ? jQuery : typeof window.Modernizr !== 'undefined' ? window.Modernizr : false;

	// Site init function - added to Vertic lib to make available in global scope without cluttering the namespace
	_v.init = function () {
		// Plugins
		//  Lettering.js - letteringjs.com
		(function (b) { function c(a, e, c, d) { var e = a.text().split(e), f = ""; e.length && (b(e).each(function (a, b) { f += '<span class="' + c + (a + 1) + '">' + b + "</span>" + d }), a.empty().append(f)) } var d = { init: function () { return this.each(function () { c(b(this), "", "char", "") }) }, words: function () { return this.each(function () { c(b(this), " ", "word", " ") }) }, lines: function () { return this.each(function () { c(b(this).children("br").replaceWith("eefec303079ad17405c889e092e105b0").end(), "eefec303079ad17405c889e092e105b0", "line", "") }) } }; b.fn.lettering = function (a) { if (a && d[a]) return d[a].apply(this, [].slice.call(arguments, 1)); if ("letters" === a || !a) return d.init.apply(this, [].slice.call(arguments, 0)); b.error("Method " + a + " does not exist on jQuery.lettering"); return this } })(jQuery);
		// jQuery SWFObject v1.1.1 MIT/GPL @jon_neal-  http://jquery.thewikies.com/swfobject
		(function (f, h, i) { function k(a, c) { var b = (a[0] || 0) - (c[0] || 0); return b > 0 || !b && a.length > 0 && k(a.slice(1), c.slice(1)) } function l(a) { if (typeof a != g) return a; var c = [], b = ""; for (var d in a) { b = typeof a[d] == g ? l(a[d]) : [d, m ? encodeURI(a[d]) : a[d]].join("="); c.push(b) } return c.join("&") } function n(a) { var c = []; for (var b in a) a[b] && c.push([b, '="', a[b], '"'].join("")); return c.join(" ") } function o(a) { var c = []; for (var b in a) c.push(['<param name="', b, '" value="', l(a[b]), '" />'].join("")); return c.join("") } var g = "object", m = true; try { var j = i.description || function () { return (new i("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version") } () } catch (p) { j = "Unavailable" } var e = j.match(/\d+/g) || [0]; f[h] = { available: e[0] > 0, activeX: i && !i.name, version: { original: j, array: e, string: e.join("."), major: parseInt(e[0], 10) || 0, minor: parseInt(e[1], 10) || 0, release: parseInt(e[2], 10) || 0 }, hasVersion: function (a) { a = /string|number/.test(typeof a) ? a.toString().split(".") : /object/.test(typeof a) ? [a.major, a.minor] : a || [0, 0]; return k(e, a) }, encodeParams: true, expressInstall: "expressInstall.swf", expressInstallIsActive: false, create: function (a) { if (!a.swf || this.expressInstallIsActive || !this.available && !a.hasVersionFail) return false; if (!this.hasVersion(a.hasVersion || 1)) { this.expressInstallIsActive = true; if (typeof a.hasVersionFail == "function") if (!a.hasVersionFail.apply(a)) return false; a = { swf: a.expressInstall || this.expressInstall, height: 137, width: 214, flashvars: { MMredirectURL: location.href, MMplayerType: this.activeX ? "ActiveX" : "PlugIn", MMdoctitle: document.title.slice(0, 47) + " - Flash Player Installation"}} } attrs = { data: a.swf, type: "application/x-shockwave-flash", id: a.id || "flash_" + Math.floor(Math.random() * 999999999), width: a.width || 320, height: a.height || 180, style: a.style || "" }; m = typeof a.useEncode !== "undefined" ? a.useEncode : this.encodeParams; a.movie = a.swf; a.wmode = a.wmode || "opaque"; delete a.fallback; delete a.hasVersion; delete a.hasVersionFail; delete a.height; delete a.id; delete a.swf; delete a.useEncode; delete a.width; var c = document.createElement("div"); c.innerHTML = ["<object ", n(attrs), ">", o(a), "</object>"].join(""); return c.firstChild } }; f.fn[h] = function (a) { var c = this.find(g).andSelf().filter(g); /string|object/.test(typeof a) && this.each(function () { var b = f(this), d; a = typeof a == g ? a : { swf: a }; a.fallback = this; if (d = f[h].create(a)) { b.children().remove(); b.html(d) } }); typeof a == "function" && c.each(function () { var b = this; b.jsInteractionTimeoutMs = b.jsInteractionTimeoutMs || 0; if (b.jsInteractionTimeoutMs < 660) b.clientWidth || b.clientHeight ? a.call(b) : setTimeout(function () { f(b)[h](a) }, b.jsInteractionTimeoutMs + 66) }); return c } })(jQuery, "flash", navigator.plugins["Shockwave Flash"] || window.ActiveXObject);
		// FancyBox - jQuery Plugin - Examples and documentation at: http://fancybox.net
		(function (b) { var m, t, u, f, D, j, E, n, z, A, q = 0, e = {}, o = [], p = 0, d = {}, l = [], G = null, v = new Image, J = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, W = /[^\.]\.(swf)\s*$/i, K, L = 1, y = 0, s = "", r, i, h = false, B = b.extend(b("<div/>")[0], { prop: 0 }), M = b.browser.msie && b.browser.version < 7 && !window.XMLHttpRequest, N = function () { t.hide(); v.onerror = v.onload = null; G && G.abort(); m.empty() }, O = function () { if (false === e.onError(o, q, e)) { t.hide(); h = false } else { e.titleShow = false; e.width = "auto"; e.height = "auto"; m.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>'); F() } }, I = function () { var a = o[q], c, g, k, C, P, w; N(); e = b.extend({}, b.fn.fancybox.defaults, typeof b(a).data("fancybox") == "undefined" ? e : b(a).data("fancybox")); w = e.onStart(o, q, e); if (w === false) h = false; else { if (typeof w == "object") e = b.extend(e, w); k = e.title || (a.nodeName ? b(a).attr("title") : a.title) || ""; if (a.nodeName && !e.orig) e.orig = b(a).children("img:first").length ? b(a).children("img:first") : b(a); if (k === "" && e.orig && e.titleFromAlt) k = e.orig.attr("alt"); c = e.href || (a.nodeName ? b(a).attr("href") : a.href) || null; if (/^(?:javascript)/i.test(c) || c == "#") c = null; if (e.type) { g = e.type; if (!c) c = e.content } else if (e.content) g = "html"; else if (c) g = c.match(J) ? "image" : c.match(W) ? "swf" : b(a).hasClass("iframe") ? "iframe" : c.indexOf("#") === 0 ? "inline" : "ajax"; if (g) { if (g == "inline") { a = c.substr(c.indexOf("#")); g = b(a).length > 0 ? "inline" : "ajax" } e.type = g; e.href = c; e.title = k; if (e.autoDimensions) if (e.type == "html" || e.type == "inline" || e.type == "ajax") { e.width = "auto"; e.height = "auto" } else e.autoDimensions = false; if (e.modal) { e.overlayShow = true; e.hideOnOverlayClick = false; e.hideOnContentClick = false; e.enableEscapeButton = false; e.showCloseButton = false } e.padding = parseInt(e.padding, 10); e.margin = parseInt(e.margin, 10); m.css("padding", e.padding + e.margin); b(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () { b(this).replaceWith(j.children()) }); switch (g) { case "html": m.html(e.content); F(); break; case "inline": if (b(a).parent().is("#fancybox-content") === true) { h = false; break } b('<div class="fancybox-inline-tmp" />').hide().insertBefore(b(a)).bind("fancybox-cleanup", function () { b(this).replaceWith(j.children()) }).bind("fancybox-cancel", function () { b(this).replaceWith(m.children()) }); b(a).appendTo(m); F(); break; case "image": h = false; b.fancybox.showActivity(); v = new Image; v.onerror = function () { O() }; v.onload = function () { h = true; v.onerror = v.onload = null; e.width = v.width; e.height = v.height; b("<img />").attr({ id: "fancybox-img", src: v.src, alt: e.title }).appendTo(m); Q() }; v.src = c; break; case "swf": e.scrolling = "no"; C = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + e.width + '" height="' + e.height + '"><param name="movie" value="' + c + '"></param>'; P = ""; b.each(e.swf, function (x, H) { C += '<param name="' + x + '" value="' + H + '"></param>'; P += " " + x + '="' + H + '"' }); C += '<embed src="' + c + '" type="application/x-shockwave-flash" width="' + e.width + '" height="' + e.height + '"' + P + "></embed></object>"; m.html(C); F(); break; case "ajax": h = false; b.fancybox.showActivity(); e.ajax.win = e.ajax.success; G = b.ajax(b.extend({}, e.ajax, { url: c, data: e.ajax.data || {}, error: function (x) { x.status > 0 && O() }, success: function (x, H, R) { if ((typeof R == "object" ? R : G).status == 200) { if (typeof e.ajax.win == "function") { w = e.ajax.win(c, x, H, R); if (w === false) { t.hide(); return } else if (typeof w == "string" || typeof w == "object") x = w } m.html(x); F() } } })); break; case "iframe": Q() } } else O() } }, F = function () { var a = e.width, c = e.height; a = a.toString().indexOf("%") > -1 ? parseInt((b(window).width() - e.margin * 2) * parseFloat(a) / 100, 10) + "px" : a == "auto" ? "auto" : a + "px"; c = c.toString().indexOf("%") > -1 ? parseInt((b(window).height() - e.margin * 2) * parseFloat(c) / 100, 10) + "px" : c == "auto" ? "auto" : c + "px"; m.wrapInner('<div style="width:' + a + ";height:" + c + ";overflow: " + (e.scrolling == "auto" ? "auto" : e.scrolling == "yes" ? "scroll" : "hidden") + ';position:relative;"></div>'); e.width = m.width(); e.height = m.height(); Q() }, Q = function () { var a, c; t.hide(); if (f.is(":visible") && false === d.onCleanup(l, p, d)) { b.event.trigger("fancybox-cancel"); h = false } else { h = true; b(j.add(u)).unbind(); b(window).unbind("resize.fb scroll.fb"); b(document).unbind("keydown.fb"); f.is(":visible") && d.titlePosition !== "outside" && f.css("height", f.height()); l = o; p = q; d = e; if (d.overlayShow) { u.css({ "background-color": d.overlayColor, opacity: d.overlayOpacity, cursor: d.hideOnOverlayClick ? "pointer" : "auto", height: b(document).height() }); if (!u.is(":visible")) { M && b("select:not(#fancybox-tmp select)").filter(function () { return this.style.visibility !== "hidden" }).css({ visibility: "hidden" }).one("fancybox-cleanup", function () { this.style.visibility = "inherit" }); u.show() } } else u.hide(); i = X(); s = d.title || ""; y = 0; n.empty().removeAttr("style").removeClass(); if (d.titleShow !== false) { if (b.isFunction(d.titleFormat)) a = d.titleFormat(s, l, p, d); else a = s && s.length ? d.titlePosition == "float" ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + s + '</td><td id="fancybox-title-float-right"></td></tr></table>' : '<div id="fancybox-title-' + d.titlePosition + '">' + s + "</div>" : false; s = a; if (!(!s || s === "")) { n.addClass("fancybox-title-" + d.titlePosition).html(s).appendTo("body").show(); switch (d.titlePosition) { case "inside": n.css({ width: i.width - d.padding * 2, marginLeft: d.padding, marginRight: d.padding }); y = n.outerHeight(true); n.appendTo(D); i.height += y; break; case "over": n.css({ marginLeft: d.padding, width: i.width - d.padding * 2, bottom: d.padding }).appendTo(D); break; case "float": n.css("left", parseInt((n.width() - i.width - 40) / 2, 10) * -1).appendTo(f); break; default: n.css({ width: i.width - d.padding * 2, paddingLeft: d.padding, paddingRight: d.padding }).appendTo(f) } } } n.hide(); if (f.is(":visible")) { b(E.add(z).add(A)).hide(); a = f.position(); r = { top: a.top, left: a.left, width: f.width(), height: f.height() }; c = r.width == i.width && r.height == i.height; j.fadeTo(d.changeFade, 0.3, function () { var g = function () { j.html(m.contents()).fadeTo(d.changeFade, 1, S) }; b.event.trigger("fancybox-change"); j.empty().removeAttr("filter").css({ "border-width": d.padding, width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }); if (c) g(); else { B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.changeSpeed, easing: d.easingChange, step: T, complete: g }) } }) } else { f.removeAttr("style"); j.css("border-width", d.padding); if (d.transitionIn == "elastic") { r = V(); j.html(m.contents()); f.show(); if (d.opacity) i.opacity = 0; B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.speedIn, easing: d.easingIn, step: T, complete: S }) } else { d.titlePosition == "inside" && y > 0 && n.show(); j.css({ width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }).html(m.contents()); f.css(i).fadeIn(d.transitionIn == "none" ? 0 : d.speedIn, S) } } } }, Y = function () { if (d.enableEscapeButton || d.enableKeyboardNav) b(document).bind("keydown.fb", function (a) { if (a.keyCode == 27 && d.enableEscapeButton) { a.preventDefault(); b.fancybox.close() } else if ((a.keyCode == 37 || a.keyCode == 39) && d.enableKeyboardNav && a.target.tagName !== "INPUT" && a.target.tagName !== "TEXTAREA" && a.target.tagName !== "SELECT") { a.preventDefault(); b.fancybox[a.keyCode == 37 ? "prev" : "next"]() } }); if (d.showNavArrows) { if (d.cyclic && l.length > 1 || p !== 0) z.show(); if (d.cyclic && l.length > 1 || p != l.length - 1) A.show() } else { z.hide(); A.hide() } }, S = function () { if (!b.support.opacity) { j.get(0).style.removeAttribute("filter"); f.get(0).style.removeAttribute("filter") } e.autoDimensions && j.css("height", "auto"); f.css("height", "auto"); s && s.length && n.show(); d.showCloseButton && E.show(); Y(); d.hideOnContentClick && j.bind("click", b.fancybox.close); d.hideOnOverlayClick && u.bind("click", b.fancybox.close); b(window).bind("resize.fb", b.fancybox.resize); d.centerOnScroll && b(window).bind("scroll.fb", b.fancybox.center); if (d.type == "iframe") b('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (b.browser.msie ? 'allowtransparency="true""' : "") + ' scrolling="' + e.scrolling + '" src="' + d.href + '"></iframe>').appendTo(j); f.show(); h = false; b.fancybox.center(); d.onComplete(l, p, d); var a, c; if (l.length - 1 > p) { a = l[p + 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } } if (p > 0) { a = l[p - 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } } }, T = function (a) { var c = { width: parseInt(r.width + (i.width - r.width) * a, 10), height: parseInt(r.height + (i.height - r.height) * a, 10), top: parseInt(r.top + (i.top - r.top) * a, 10), left: parseInt(r.left + (i.left - r.left) * a, 10) }; if (typeof i.opacity !== "undefined") c.opacity = a < 0.5 ? 0.5 : a; f.css(c); j.css({ width: c.width - d.padding * 2, height: c.height - y * a - d.padding * 2 }) }, U = function () { return [b(window).width() - d.margin * 2, b(window).height() - d.margin * 2, b(document).scrollLeft() + d.margin, b(document).scrollTop() + d.margin] }, X = function () { var a = U(), c = {}, g = d.autoScale, k = d.padding * 2; c.width = d.width.toString().indexOf("%") > -1 ? parseInt(a[0] * parseFloat(d.width) / 100, 10) : d.width + k; c.height = d.height.toString().indexOf("%") > -1 ? parseInt(a[1] * parseFloat(d.height) / 100, 10) : d.height + k; if (g && (c.width > a[0] || c.height > a[1])) if (e.type == "image" || e.type == "swf") { g = d.width / d.height; if (c.width > a[0]) { c.width = a[0]; c.height = parseInt((c.width - k) / g + k, 10) } if (c.height > a[1]) { c.height = a[1]; c.width = parseInt((c.height - k) * g + k, 10) } } else { c.width = Math.min(c.width, a[0]); c.height = Math.min(c.height, a[1]) } c.top = parseInt(Math.max(a[3] - 20, a[3] + (a[1] - c.height - 40) * 0.5), 10); c.left = parseInt(Math.max(a[2] - 20, a[2] + (a[0] - c.width - 40) * 0.5), 10); return c }, V = function () { var a = e.orig ? b(e.orig) : false, c = {}; if (a && a.length) { c = a.offset(); c.top += parseInt(a.css("paddingTop"), 10) || 0; c.left += parseInt(a.css("paddingLeft"), 10) || 0; c.top += parseInt(a.css("border-top-width"), 10) || 0; c.left += parseInt(a.css("border-left-width"), 10) || 0; c.width = a.width(); c.height = a.height(); c = { width: c.width + d.padding * 2, height: c.height + d.padding * 2, top: c.top - d.padding - 20, left: c.left - d.padding - 20} } else { a = U(); c = { width: d.padding * 2, height: d.padding * 2, top: parseInt(a[3] + a[1] * 0.5, 10), left: parseInt(a[2] + a[0] * 0.5, 10)} } return c }, Z = function () { if (t.is(":visible")) { b("div", t).css("top", L * -40 + "px"); L = (L + 1) % 12 } else clearInterval(K) }; b.fn.fancybox = function (a) { if (!b(this).length) return this; b(this).data("fancybox", b.extend({}, a, b.metadata ? b(this).metadata() : {})).unbind("click.fb").bind("click.fb", function (c) { c.preventDefault(); if (!h) { h = true; b(this).blur(); o = []; q = 0; c = b(this).attr("rel") || ""; if (!c || c == "" || c === "nofollow") o.push(this); else { o = b("a[rel=" + c + "], area[rel=" + c + "]"); q = o.index(this) } I() } }); return this }; b.fancybox = function (a, c) { var g; if (!h) { h = true; g = typeof c !== "undefined" ? c : {}; o = []; q = parseInt(g.index, 10) || 0; if (b.isArray(a)) { for (var k = 0, C = a.length; k < C; k++) if (typeof a[k] == "object") b(a[k]).data("fancybox", b.extend({}, g, a[k])); else a[k] = b({}).data("fancybox", b.extend({ content: a[k] }, g)); o = jQuery.merge(o, a) } else { if (typeof a == "object") b(a).data("fancybox", b.extend({}, g, a)); else a = b({}).data("fancybox", b.extend({ content: a }, g)); o.push(a) } if (q > o.length || q < 0) q = 0; I() } }; b.fancybox.showActivity = function () { clearInterval(K); t.show(); K = setInterval(Z, 66) }; b.fancybox.hideActivity = function () { t.hide() }; b.fancybox.next = function () { return b.fancybox.pos(p + 1) }; b.fancybox.prev = function () { return b.fancybox.pos(p - 1) }; b.fancybox.pos = function (a) { if (!h) { a = parseInt(a); o = l; if (a > -1 && a < l.length) { q = a; I() } else if (d.cyclic && l.length > 1) { q = a >= l.length ? 0 : l.length - 1; I() } } }; b.fancybox.cancel = function () { if (!h) { h = true; b.event.trigger("fancybox-cancel"); N(); e.onCancel(o, q, e); h = false } }; b.fancybox.close = function () { function a() { u.fadeOut("fast"); n.empty().hide(); f.hide(); b.event.trigger("fancybox-cleanup"); j.empty(); d.onClosed(l, p, d); l = e = []; p = q = 0; d = e = {}; h = false } if (!(h || f.is(":hidden"))) { h = true; if (d && false === d.onCleanup(l, p, d)) h = false; else { N(); b(E.add(z).add(A)).hide(); b(j.add(u)).unbind(); b(window).unbind("resize.fb scroll.fb"); b(document).unbind("keydown.fb"); j.find("iframe").attr("src", M && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank"); d.titlePosition !== "inside" && n.empty(); f.stop(); if (d.transitionOut == "elastic") { r = V(); var c = f.position(); i = { top: c.top, left: c.left, width: f.width(), height: f.height() }; if (d.opacity) i.opacity = 1; n.empty().hide(); B.prop = 1; b(B).animate({ prop: 0 }, { duration: d.speedOut, easing: d.easingOut, step: T, complete: a }) } else f.fadeOut(d.transitionOut == "none" ? 0 : d.speedOut, a) } } }; b.fancybox.resize = function () { u.is(":visible") && u.css("height", b(document).height()); b.fancybox.center(true) }; b.fancybox.center = function (a) { var c, g; if (!h) { g = a === true ? 1 : 0; c = U(); !g && (f.width() > c[0] || f.height() > c[1]) || f.stop().animate({ top: parseInt(Math.max(c[3] - 20, c[3] + (c[1] - j.height() - 40) * 0.5 - d.padding)), left: parseInt(Math.max(c[2] - 20, c[2] + (c[0] - j.width() - 40) * 0.5 - d.padding)) }, typeof a == "number" ? a : 200) } }; b.fancybox.init = function () { if (!b("#fancybox-wrap").length) { b("body").append(m = b('<div id="fancybox-tmp"></div>'), t = b('<div id="fancybox-loading"><div></div></div>'), u = b('<div id="fancybox-overlay"></div>'), f = b('<div id="fancybox-wrap"></div>')); D = b('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(f); D.append(j = b('<div id="fancybox-content"></div>'), E = b('<a id="fancybox-close"></a>'), n = b('<div id="fancybox-title"></div>'), z = b('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), A = b('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')); E.click(b.fancybox.close); t.click(b.fancybox.cancel); z.click(function (a) { a.preventDefault(); b.fancybox.prev() }); A.click(function (a) { a.preventDefault(); b.fancybox.next() }); b.fn.mousewheel && f.bind("mousewheel.fb", function (a, c) { if (h) a.preventDefault(); else if (b(a.target).get(0).clientHeight == 0 || b(a.target).get(0).scrollHeight === b(a.target).get(0).clientHeight) { a.preventDefault(); b.fancybox[c > 0 ? "prev" : "next"]() } }); b.support.opacity || f.addClass("fancybox-ie"); if (M) { t.addClass("fancybox-ie6"); f.addClass("fancybox-ie6"); b('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(D) } } }; b.fn.fancybox.defaults = { padding: 30, margin: 40, opacity: false, modal: false, cyclic: false, scrolling: "auto", width: 560, height: 340, autoScale: true, autoDimensions: true, centerOnScroll: false, ajax: {}, swf: { wmode: "transparent" }, hideOnOverlayClick: true, hideOnContentClick: false, overlayShow: true, overlayOpacity: 0.7, overlayColor: "#000", titleShow: true, titlePosition: "float", titleFormat: null, titleFromAlt: false, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing", easingOut: "swing", showCloseButton: true, showNavArrows: true, enableEscapeButton: true, enableKeyboardNav: true, onStart: function () { }, onCancel: function () { }, onComplete: function () { }, onCleanup: function () { }, onClosed: function () { }, onError: function () { } }; b(document).ready(function () { b.fancybox.init() }) })(jQuery);
		// Debounced resize listener - http://paulirish.com/2009/throttled-smartresize-jquery-event-handler/
		(function (h, b) { var g = function (a, b, d) { var c; return function () { var e = this, f = arguments; c ? clearTimeout(c) : d && a.apply(e, f); c = setTimeout(function () { d || a.apply(e, f); c = null }, b || 100) } }; jQuery.fn[b] = function (a) { return a ? this.bind("resize", g(a)) : this.trigger(b) } })(jQuery, "smartresize");
		// Custom dropdowns - ported from simpleFormUI - https://bitbucket.org/laustdeleuran/simpleformui/
		(function (c) { c.fn.simpleFormUI = function (k) { var d = { cssClass: "simpleFormUI", inheritClasses: !0, inlineStyles: !0, keyboardSupport: !0, hideStyles: { visibility: "hidden", position: "absolute", left: "-9999em", outline: "none none"} }; k && c.extend(d, k); var i = d.cssClass ? "." + d.cssClass : "", e = { selectOption: function (a, b, e, d) { if (c(a).attr("id")) { var a = c(a), b = a.closest(b + ".select"), f = a.attr("id").split("select-")[1], f = b.find('option[value="' + f + '"]'), d = "undefined" === typeof d ? !0 : d; b.find("li").removeClass("selected"); a.addClass("selected"); b.find("option").removeAttr("selected"); f.attr("selected", "selected"); d && f.parent().trigger("change"); e.text(f.text()).removeAttr("class").attr("class", a.attr("data-cssclass")) } }, showList: function (a, b) { a.offset().top + b.outerHeight() + a.outerHeight() > c(window).height() + c(window).scrollTop() ? b.css({ top: "auto", bottom: "110%" }).slideDown(175) : b.css({ top: "110%", bottom: "auto" }).slideDown(175); a.css("z-index", 1E4).addClass('open'); c("html").bind("click", { container: a, list: b }, e.htmlHideList) }, htmlHideList: function (a) { e.hideList(a.data.container, a.data.list); c("html").unbind("click", e.htmlHideList) }, hideList: function (a, b) { b.css("display", "none"); a.css("z-index", 1).removeClass('open'); c("html").unbind("click", e.htmlHideList) }, toggleList: function (a, b) { "block" == b.css("display") ? e.hideList(a, b) : e.showList(a, b) }, hideOtherLists: function (a) { c("." + d.cssClass + ".select").find("ul").each(function () { this !== a && c(this).hide() }) } }; return this.each(function () { if (c(this).is("select")) { var a = c(this), b = a.clone(), g = c('<div class="' + d.cssClass + ' select"' + (d.inlineStyles ? ' style="position:relative;"' : "") + "></div>").append(b), h = c("<ul" + (d.inlineStyles ? ' style="display:none;position:absolute;top:110%;left:0;z-index:9000;margin:0;"' : "") + "></ul>").appendTo(g), f = c("<div></div>").appendTo(g), j = c('<a href="javascript:void(0);"' + (d.inlineStyles ? ' style="display:block;overflow:hidden;"' : "") + ">Default</a>").appendTo(f); "" != b.attr("class") && d.inheritClasses && (g.addClass(b.attr("class")), g.attr("data-cssclass", b.attr("class"))); d.inlineStyles && b.css(d.hideStyles); b.find("option").each(function () { var a = d.inlineStyles ? ' style="list-style:none; margin:0;"' : "", b = c(this), a = c("<li" + a + '><a href="#">' + b.text() + "</a></li>"); "" != b.attr("class") && (a.attr("class", b.attr("class")), a.attr("data-cssclass", b.attr("class"))); a.attr("id", "select-" + c(this).attr("value")); a.click(function (a) { a.preventDefault(); e.selectOption(this, i, j); f.click() }); h.append(a); this.selected && e.selectOption(a, i, j, !1) }); a.replaceWith(g); f.click(function () { e.hideOtherLists(h[0]); e.toggleList(g, h) }); f.keydown(function (a) { var b = a.keyCode; if (38 == b || 40 == b || 27 == b) a.preventDefault(), a = h.find("li.selected"), a[0] ? 38 == b ? e.selectOption(a.prev("li"), i, j) : 40 == b ? e.selectOption(a.next("li"), i, j) : 27 == b && e.hideList(h) : h.children().first().click() }); g.click(function (a) { a.stopPropagation() }) } }) } })(jQuery);
		// Image Preload - v1.2 - https://github.com/farinspace/jquery.imgpreload 
		if ("undefined" != typeof jQuery) { (function (a) { a.imgpreload = function (b, c) { c = a.extend({}, a.fn.imgpreload.defaults, c instanceof Function ? { all: c} : c); if ("string" == typeof b) { b = [b] } var d = []; var e = b.length; for (var f = 0; f < e; f++) { var g = new Image; a(g).bind("load error", function (b) { d.push(this); a.data(this, "loaded", "error" == b.type ? false : true); if (c.each instanceof Function) { c.each.call(this) } if (d.length >= e && c.all instanceof Function) { c.all.call(d) } }); g.src = b[f] } }; a.fn.imgpreload = function (b) { var c = []; this.each(function () { c.push(a(this).attr("src")) }); a.imgpreload(c, b); return this }; a.fn.imgpreload.defaults = { each: null, all: null} })(jQuery) };
		// hitTestObject - http://upshots.org/javascript/jquery-hittestobject
		$.fn.hitTestObject = function (obj) {
			var bounds = this.offset();
			bounds.right = bounds.left + this.outerWidth();
			bounds.bottom = bounds.top + this.outerHeight();
			var compare = obj.offset();
			compare.right = compare.left + obj.outerWidth();
			compare.bottom = compare.top + obj.outerHeight();
			return (!(compare.right < bounds.left ||
					compare.left > bounds.right ||
					compare.bottom < bounds.top ||
					compare.top > bounds.bottom));
		};
		// Reverse // http://forum.jquery.com/topic/jquery-reverse-a-collection-of-jquery-elements
		$.fn.reverse = [].reverse;
		// :before pseudo selector test.
		// By @laustdeleuran
		// Please see http://reference.sitepoint.com/css/pseudoelement-before for inconsistencies
		Modernizr.addTest('before', function () {
			var hasBefore,
						rules = ['#modernizr-before{visibility:hidden;}', '#modernizr-before:before{content:"modernizr-before"}'],
						head = document.getElementsByTagName('head')[0] || (function () {
							return document.documentElement.appendChild(document.createElement('head'));
						} ()),
						root = document.body || (function () {
							return document.documentElement.appendChild(document.createElement('body'));
						} ()),
						before = document.createElement('div'),
						style = document.createElement('style');

			style.type = "text/css";
			if (style.styleSheet) { style.styleSheet.cssText = rules.join(''); }
			else { style.appendChild(document.createTextNode(rules.join(''))); }
			head.appendChild(style);

			before.id = "modernizr-before";
			root.appendChild(before);
			hasBefore = before.offsetHeight > 0,

				head.removeChild(style);
			root.removeChild(before);

			return hasBefore;
		});


		// Behaviour
		// Set up project handles
		_v.alinea = new Object();
		var $doc = $(document),
			$win = $(window);

		// Set up local log
		_v.alinea.log = new _v.utils.log({ meta: false });
		var log = function (arg) { _v.alinea.log.write(arg); };

		// Set up touch event handling
		var touchClick = Modernizr.touch ? 'click' : 'click',
			touchUp = Modernizr.touch ? 'touchend' : 'mouseup',
			touchDown = Modernizr.touch ? 'touchstart' : 'mousedown',
			touchMove = Modernizr.touch ? 'touchmove' : 'mousemove';

		// Bridge mobile safari label click
		$('label').on('click', function () { });

		// Convert time function
		var convertTime = function (ms) {
			var two = function (x) { return ((x > 9) ? "" : "0") + x },
					three = function (x) { return ((x > 99) ? "" : "0") + ((x > 9) ? "" : "0") + x };
			var sec = Math.floor(ms / 1000);
			ms = ms % 1000;
			var min = Math.floor(sec / 60);
			sec = sec % 60;
			var hr = Math.floor(min / 60);
			min = min % 60;
			var day = Math.floor(hr / 60);
			hr = hr % 60;
			return { ms: three(ms), s: two(sec), m: two(min), h: two(hr), d: day };
		}

		// Array filtering object method
		if (!Array.prototype.filter) {
			Array.prototype.filter = function (fun /*, thisp*/) {
				var len = this.length;
				if (typeof fun != "function") throw new TypeError();

				var res = new Array();
				var thisp = arguments[1];
				for (var i = 0; i < len; i++) {
					if (i in this) {
						var val = this[i]; // in case fun mutates this
						if (fun.call(thisp, val, i, this)) res.push(val);
					}
				}

				return res;
			};
		}

		/**
		* Rich Media embedding
		* Utilizes Video-JS for video and SoundManager 2 for audio.
		* Both uses the HTML5 API with flash fallbacks with a similar JS API.
		* For ultimate fallback, markup is build around a link and a placeholder image.
		**/
		var initMedia = function ($mediaItems) {

			
			if (typeof _v.alinea.players !== 'object') _v.alinea.players = new Object();
			$mediaItems.each(function (i) {
				// Determine type
				var $container = $(this), type = false;
				if ($container.hasClass('vid')) type = 'video';
				if ($container.hasClass('aud')) type = 'audio';
				if (!type) {
					_v.err('Unrecognized media type on ".media" element.')
					return false;
				}

				// Get data
				var id = type + '-' + i + '-' + Math.round(Math.random() * 10000),
					$fallback = $container.children('.fallback'),
					src = $container.data('src').split(',');
				$container.addClass('enhanced');

				// Build new markup
				switch (type) {
					case 'video': // Video
						if (typeof _v.alinea.players.video !== 'object') _v.alinea.players.video = new Array();

						// Get extra data
						var width = $container.data('width'),
							height = $container.data('height'),
							poster = $fallback.children('img').attr('src'),
							source = '',
							$chapters = $container.find('.chapters'),
							$info = $container.find('.info'),
							player = false;

						// Set up info
						if ($info[0]) {
							var $infoItems = $info.find('.item'), infoIsShown = false;
							$info.addClass('enhanced').css({
								width: width,
								height: height,
								'margin-left': -Math.round(width / 2)
							});

							// Build navigation
							var $infoNav = $('<ul class="info-nav" />').css({
								width: width,
								'margin-left': -Math.round(width / 2)
							});
							$infoItems.each(function () {
								var $infoItem = $(this),
									$infoNavItem = $('<li>' + $infoItem.find('h1,h2,h3,h4').first().text() + '</li>');
								$infoNavItem.bind(touchClick, function () {
									var $t = $(this);
									if ($t.hasClass('active')) {
										$info.hide();
										$infoItems.hide();
										$(this).removeClass('active');
										infoIsShown = false;
									} else {
										$info.show();
										$infoItem.toggle().siblings('.item').hide();
										$(this).addClass('active').siblings().removeClass('active');
										if (player) player.pause();
										infoIsShown = true;
									}
								});
								$infoNavItem.appendTo($infoNav);
							});
							$infoNav.insertBefore($info);

							// Show/hide navigation
							if (!Modernizr.touch) {
								$infoNav.hide();
								var infoNavTimer = 0,
									showInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 1 }, 250);
									},
									hideInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										if (!infoIsShown) {
											infoNavTimer = setTimeout(function () {
												$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 0 }, 750, function () { $infoNav.css({ 'display': 'none' }); });
											}, 500);
										}
									};
								$container.delegate('.video-js', 'mouseenter', showInfoNav).delegate('.video-js', 'mouseleave', hideInfoNav);
								$infoNav.hover(showInfoNav, hideInfoNav);
							}

							// Add close button
							$infoItems.each(function () {
								var $infoItem = $(this),
								$close = $('<a href="#close" class="close">Close</a>').prependTo($infoItem);
								$close.bind('click', function (e) {
									e.preventDefault();
									$info.hide();
									$infoItems.hide();
									$infoNav.children().removeClass('active');
									infoIsShown = false;
								});
							});
						}

						// Set up chapters
						if ($chapters[0]) {
							$chapters.width(width);
							$container.addClass('has-chapters');

							// Set handles
							var $cNext = $chapters.find('.next'),
								$cPrev = $chapters.find('.prev'),
								$cScroll = $chapters.find('.scroll'),
								$cList = $chapters.find('ul'),
								$cItems = $cList.find('li'),
								$cLinks = $cList.find('a'),
								cItemWidth = $cItems.outerWidth(true),
								step = 0,
								shown = Math.floor($cScroll.width() / cItemWidth);

							// Set list width
							$cList.width(cItemWidth * $cItems.length);

							// Set prev/next buttons
							var cShowHidePrevNext = function () {
								var $btns = $cPrev.add($cNext);
								$btns.removeClass('hidden');
								$btns.stop().animate({ 'opacity': 1 }, 250);
								if (step == $cItems.length - shown) {
									$cNext.stop().animate({ 'opacity': 0 }, 250).addClass('hidden');
								} else if (step == 0) {
									$cPrev.stop().animate({ 'opacity': 0 }, 250).addClass('hidden');
								}
							};
							$cNext.bind('click', function (e) {
								e.preventDefault();
								if (step < $cItems.length - shown) {
									step++;
									$cList.animate({ 'margin-left': -step * cItemWidth }, 500);
								}
								cShowHidePrevNext();
							});
							$cPrev.bind('click', function (e) {
								e.preventDefault();
								if (step > 0) {
									step--;
									$cList.animate({ 'margin-left': -step * cItemWidth }, 500);
								}
								cShowHidePrevNext();
							});
							cShowHidePrevNext();

							// Set link clicks
							$cLinks.bind(touchClick, function (e) {
								e.preventDefault();
								var ms = $(this).attr('href').replace('#', '');
								if (player) {
									player.pause().currentTime(ms / 1000).pause().play();
								} else {
									log('Player not yet ready or not found (' + player + ' ' + typeof player + ').');
								}
							});
						}

						// Set dimensions
						$container.css({
							//'min-width': width,
							height: $chapters[0] ? height + 100 : height
						});

						// Build source links
						$.each(src, function (i) {
							var type = src[i].indexOf('.mp4') > -1 ? 'video/mp4' : src[i].indexOf('.webm') > -1 ? 'video/webm' : src[i].indexOf('.ogv') > -1 || src[i].indexOf('.oga') > -1 || src[i].indexOf('.ogg') > -1 ? 'video/ogv' : 'video';
							source = source + '<source src="' + src[i] + '" type="' + type + '" />';
						});

						// Load video
						var loadVideo = function (id, $container) {
							// Insert new markup
							$fallback.remove();
							var $video = $('<video id="' + id + '" class="video-js vjs-default-skin" controls preload="auto" width="' + width + '" height="' + height + '" poster="' + poster + '">' + source + '</video>').prependTo($container), 
							$caption = $container.find('.caption');
							$chapters.insertAfter($video);
							
							if ($caption.length && $container.next().is('footer')) {
								$container.css('margin-bottom', parseInt($caption.height()) + 50);
							}


							// Init video-js
							var path = typeof basePath === 'string' ? basePath + 'js/video-js/video-js.swf' : '../js/video-js/video-js.swf';
							player = _V_(id, { flash: { swf: path }, techOrder: ["flash", "html5"] });
							player.addEvent('ended', function () {
								setTimeout(function () {
									$container.find('.vjs-loading-spinner').hide();
									$container.find('.vjs-poster').show();
								}, 100);
							});

							if ($.browser.msie && parseInt($.browser.version) > 9) {
								var $banner = $container.closest('.banner');
								$banner.children('img').height($banner.height());
							}

							// Make player publicly available
							$container.data('player', player);
							_v.alinea.players.video.push(player);

							// Support banner box
							var $bannerBox = $container.find('.box');
							if ($bannerBox[0]) {
								setTimeout(function () {
									try { player.controlBar.fadeIn(); } catch (err) { _v.err(err.toString()); };
								}, 500);
								$bannerBox.bind(touchClick, function () {
									player.play();
								});
								player.addEvent('play', function () {
									$bannerBox.fadeOut();
								});
								player.addEvent('pause', function () {
									$bannerBox.fadeIn();
								});
							}
						};

						if (typeof _V_ === "function") {
							loadVideo(id, $container);
						} else {
							_v.utils.loadRemoteScript('/js/video-js/video.min.js', function () {
								loadVideo(id, $container);
							});
						}
						break;
					case 'audio': // Audio
						if (typeof _v.alinea.players.audio !== 'object') _v.alinea.players.audio = new Array();

						// Get source
						var src = $fallback.attr('href');
						// Build UI
						var $ui = $('<div class="sm-ui" id="' + id + '" />'),
							$play = $('<a href="' + src + '" class="sm-play" target="_blank">Play</a>').appendTo($ui),
							$timeLapsed = $('<div class="sm-time-lapsed">00:00</div>').appendTo($ui)
						$progress = $('<div class="sm-progress" />').appendTo($ui),
							$progressBar = $('<div class="sm-bar" />').appendTo($progress),
							$progressLoaded = $('<div class="sm-loaded" />').appendTo($progress),
							$progressPlayed = $('<div class="sm-lapsed" />').appendTo($progress),
							$progressHandle = $('<div class="sm-handle" />').appendTo($progress),
							$timeLeft = $('<div class="sm-time-left">-00:00</div>').appendTo($ui),
							$muteControl = $('<div class="sm-mute-control" />').appendTo($ui),
							$volume = $('<div class="sm-volume" />').appendTo($ui),
							$volumeBar = $('<div class="sm-bar" />').appendTo($volume),
							$volumeLevel = $('<div class="sm-level" />').appendTo($volume),
							$volumeHandle = $('<div class="sm-handle" />').appendTo($volume);

						// Load sound
						var loadSound = function () {
							// Insert new markup
							$fallback.remove();
							$ui.appendTo($container);
							// Init SM2
							soundManager.url = '/js/soundmanager-2/';
							soundManager.waitForWindowLoad = true;
							soundManager.reboot();
							soundManager.onready(function () {
								// Load sound
								var sound = soundManager.createSound({
									id: id,
									url: src,
									whileloading: updateUi
								});

								// Make player publicly available
								$container.data('player', sound);
								_v.alinea.players.audio.push(player);

								// Set up functions
								var loop = 0,
										updateUi = function () {
											// Stop stacking of loop
											clearTimeout(loop);

											// Update UI
											updateVolume();
											updateTimers();
											updateProgress();
											if (sound.playState > 0 && !sound.paused) {
												if (!$ui.hasClass('playing')) $ui.addClass('playing');
											} else {
												if ($ui.hasClass('playing')) $ui.removeClass('playing');
											}

											// Loop
											if (!sound.paused && sound.playState > 0) loop = setTimeout(updateUi, 100);
										},
										togglePlayback = function () {
											if (sound.playState > 0) {
												sound.togglePause();
											} else {
												sound.play();
											}
											updateUi();
										},
										updateVolume = function () {
											$volumeLevel.height(sound.volume + '%');
											$volumeHandle.css('top', (100 - sound.volume) + '%');
											console.log("updateVolume", $ui, $ui.attr("id"));
											$ui.removeClass('vol-0 vol-1 vol-2 vol-3').addClass('vol-' + Math.round(sound.volume / 30));
										},
										updateTimers = function () {
											if (sound.duration) {
												var lapsed = convertTime(sound.position),
												left = convertTime(sound.duration - sound.position),
												lapsedText = '',
												leftText = '';

												if (parseInt(lapsed.h) > 0) lapsedText += lapsed.h + ':';
												lapsedText += lapsed.m + ':' + lapsed.s;
												if (parseInt(left.h) > 0) leftText += left.h + ':';
												leftText += left.m + ':' + left.s;

												$timeLapsed.text(lapsedText);
												$timeLeft.text('-' + leftText);
											}
										},
										updateProgress = function () {
											if (sound.duration) {
												var progress = Math.round(sound.position / sound.duration * 100),
												loaded = Math.round(sound.bytesLoaded / sound.bytesTotal * 100);
												$progressPlayed.width(progress + '%');
												$progressHandle.css('left', progress + '%');
												$progressLoaded.width(loaded + '%');
											}
										};

								// Bind events
								// Play click
								$play.bind(touchClick, function (e) {
									e.preventDefault();
									togglePlayback();
								});
								// Progress click
								$progressPlayed.add($progressLoaded).bind(touchClick, function (e) {
									if (sound.duration) {
										var position = e.pageX,
												offset = $(this).offset().left,
												width = $progress.width(),
												pct = Math.round((position - offset) / width * 100);
										sound.setPosition(sound.duration * pct / 100);
										updateUi();
										sound.resume();
									}
								});
								// Progress handle drag
								var listenProgressDrag = function (e) {
									if (sound.duration) {
										sound.pause();
										var position = e.pageX,
												offset = $progress.offset().left,
												width = $progress.width(),
												pct = Math.round((position - offset) / width * 100),
												loadedPct = Math.round($progressLoaded.width() / $progress.width() * 100);
										pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
										if (pct > loadedPct) pct = loadedPct;
										sound.setPosition(sound.duration * pct / 100);
										updateUi();
									}
								},
									isProgressDragging = false;
								$progressHandle.bind(touchDown, function (e) {
									isProgressDragging = true;
									e.stopPropagation();
									sound.pause();
									updateUi();
									$ui.bind(touchMove, listenProgressDrag);
								})
								$ui.bind(touchUp, function (e) {
									if (isProgressDragging) {
										$ui.unbind(touchMove, listenProgressDrag);
										sound.resume();
										updateUi();
										isProgressDragging = false;
									}
								});
								// Volume click
								$volumeBar.add($volumeLevel).bind(touchClick, function (e) {
									var position = e.pageY,
											offset = $volume.offset().top,
											width = $volume.height(),
											pct = 100 - Math.round((position - offset) / width * 100);
									pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
									soundManager.setVolume(id, pct);
									updateUi();
								});
								// Volume handle drag
								var listenVolumeDrag = function (e) {
									var position = e.pageY,
											offset = $volume.offset().top,
											width = $volume.height(),
											pct = 100 - Math.round((position - offset) / width * 100);
									pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
									soundManager.setVolume(id, pct);
									updateUi();
								},
										isVolumeDragging = false;
								$volumeHandle.bind(touchDown, function (e) {
									isVolumeDragging = true;
									e.stopPropagation();
									updateUi();
									$ui.bind(touchMove, listenVolumeDrag);
								})
								$ui.bind(touchUp, function (e) {
									if (isVolumeDragging) {
										$ui.unbind(touchMove, listenVolumeDrag);
										updateUi();
										isDragging = false;
									}
								});
								// Mute control click
								var showVolume = function () {
									$volume.show();
								},
										hideVolume = function () {
											$volume.hide();
										},
										toggleVolume = function () {
											$volume.toggle();
										};
								hideVolume();
								if (Modernizr.touch) {
									$muteControl.bind(touchClick, function () {
										toggleVolume();
									});
								} else {
									var lastVolume = 100,
											lazyHover = 0;
									$muteControl.bind(touchClick, function () {
										var newVolume = sound.volume > 0 ? 0 : lastVolume;
										lastVolume = sound.volume;
										soundManager.setVolume(id, newVolume);
										updateUi();
									});
									$muteControl.add($volume).hover(function () {
										showVolume();
										clearTimeout(lazyHover);
									}, function () {
										clearTimeout(lazyHover);
										lazyHover = setTimeout(function () {
											hideVolume();
										}, 1000);
									});
								}

							});
						};
						if (typeof soundManager === "object") {
							loadSound(id, $container);
						} else {
							_v.utils.loadRemoteScript('/js/soundmanager-2/soundmanager2-nodebug-jsmin.js', function () {
								loadSound(id, $container);
							});
						}
						break;
				}
			});
		};
		// DOM ready
		$doc.ready(function () {

			/**
			* Header functionality
			*
			* Header
			**/
			// Site nav dropdowns
			$('.site-header nav>select').simpleFormUI({ cssClass: 'custom-dropdown' });

			// Message box functionality
			$('.messages').each(function () {
				// Disable functionality on preview
				if ($(this).hasClass('form-example')) return false;

				// Find common elements and make open close functionality
				var $msgBox = $('.messages').addClass('closed'),
					$msgBtn = $('.button.get-messages'),
					openMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if ($msgBox.hasClass('closed')) $msgBox.removeClass('closed');
					},
					closeMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if (!$msgBox.hasClass('closed')) $msgBox.addClass('closed');
					},
					toggleMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if ($msgBox.hasClass('closed')) {
							$msgBox.removeClass('closed');
						} else {
							$msgBox.addClass('closed');
						}
					};

				// Break on missing elements
				if (!$msgBox[0]) {
					log('Unexpected error trying to create message box - required elements not present.');
					return false;
				}
				// Align box offset
				var alignOffset = function () {
					if ($msgBtn[0]) {
						var btnOffset = $msgBtn.offset(),
							btnDim = { width: $msgBtn.outerWidth(), height: $msgBtn.outerHeight() };
						$msgBox.css({ top: btnOffset.top + btnDim.height + 2, left: btnOffset.left + btnDim.width / 2 });
					}
				}
				$win.smartresize(alignOffset).load(alignOffset);
				alignOffset();

				// Show articles and switch between them
				var $articles = $msgBox.find('article'),
						$paging = $msgBox.find('.paging'),
						$next = $paging.find('.next'),
						$prev = $paging.find('.prev'),
						$title = $paging.find('p'),
						updateTitle = function ($el) {
							var s = 0;
							$articles.each(function (i, e) {
								s += 1;
								if (e == $el[0]) return false;
							});
							$title.text(s + ' af ' + $articles.length);
							$('.button.get-messages .notification.new').text($articles.length);
						};

				// Update title and set first one active
				updateTitle($articles.first().addClass('active'));

				// Create and bind functionality for prev/next buttons
				$next.bind(touchClick, function (e) {
					e.preventDefault();
					var $next = $articles.filter('.active').next();
					$articles.removeClass('active');
					if ($next.is('article')) {
						$next.addClass('active');
					} else {
						$next = $articles.first().addClass('active');
					}
					updateTitle($next);
				});
				$prev.bind(touchClick, function (e) {
					e.preventDefault();
					var $prev = $articles.filter('.active').prev();
					$articles.removeClass('active');
					if ($prev.is('article')) {
						$prev.addClass('active');
					} else {
						$prev = $articles.last().addClass('active');
					}
					updateTitle($prev);
				});

				// Bind close/open events
				$msgBtn.bind(touchClick, toggleMsgBox);
				$('<a href="#close" class="close">Luk</a>').bind(touchClick, closeMsgBox).appendTo($msgBox);
			});

			// Search form
			$('.search-form').each(function () {
				var $container = $(this),
					$input = $container.find('input[type="text"],input[type="search"]').attr('autocomplete', 'off'),
					$btn = $container.find('input[type="submit"],input[type="button"]'),
					$form = $input.closest('form'),
					$kh = $('<ul class="keywords" />').appendTo($container).hide(),
					timeout = 0;

				// Get keywords
				var keywords = $container.data('keywords'),
					service = $container.data('serviceurl'),
					data = $container.data('servicedata'),
					feedback = function (arg) { return log(arg); };

				// Format prefilled keywords
				if (keywords) {
					keywords = keywords.replace(/'/g, '"');
					try { keywords = eval(keywords); } catch (err) { }
					keywords = typeof keywords === 'object' ? keywords : new Array;
				} else {
					keywords = new Array();
				}

				// Get service keywords
				if (service) {
					$container.addClass('loading');

					// Format prefilled data
					data = data.replace(/'/g, '"');
					try { data = eval('(' + data + ')'); } catch (err) { }
					data = typeof data === 'object' ? data : {};

					// Perform service call
					_v.services.request(service, data, function (resp) {
						$container.removeClass('loading');
						if (resp.Outcome.Validated) {
							keywords = resp.Response.Keywords;
						} else if (resp.Outcome.Error) {
							feedback(resp.Outcome.Error);
						} else {
							_v.err(this, 'Unexpected error', 'Unexpected format returned.');
						}
					});
				}

				// Lookup keywords
				var curVal = false, hideKh = function () { $kh.empty().hide(); };
				$container.hover(function () {
					$container.addClass('hover');
				}, function () {
					$container.removeClass('hover');
				});
				var once = false
				$input.bind('keyup', function (e) {
					if (e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 38 || e.keyCode == 40) return;
					clearTimeout(timeout);
					var val = $input.val().replace(/^\s+/g, "");
					if (val.length > 0) {
						curVal = val;
						setTimeout(function () {
							var filterCriteria = function (e, i, a) {
								return (e.toLowerCase().indexOf(val.toLowerCase()) == 0) && e.length > val.length;
							},
							filteredKeywords = keywords.filter(filterCriteria);

							// Show keywords
							if (filteredKeywords.length > 0) {
								$kh.empty().show();
								$.each(filteredKeywords, function (i, e) {
									var $item = $('<li>' + e + '</li>');
									$item.appendTo($kh).hover(function () {
										$kh.children().removeClass('active');
										$item.addClass('active');
									}, function () {
										$item.removeClass('active');
									});
									$item.bind(touchClick, function () {
										$input.val($item.text());
										hideKh();
									});
								});
							} else {
								hideKh();
							}
						}, 50);
					} else if (val.length == 0) {
						hideKh();
					}
				}).bind('keydown', function (e) {
					var $active = $kh.find('.active');
					if ((e.keyCode == 13 || e.keyCode == 32) && $active.length) { // 13:enter, 32:space
						$input.val($active.text());
					} else if ((e.keyCode == 38 || e.keyCode == 40) && $kh.children().length) { // 38:up arrow, 40:down arrow
						var $next = e.keyCode == 38 ? $active.prev() : $active.next();
						if (!$next.length) {
							var $next = e.keyCode == 38 ? $kh.children().last() : $kh.children().first();
						}
						$next.addClass('active').siblings().removeClass('active');
					} else if (e.keyCode == 27) {
						hideKh();
					}
					if (e.keyCode == 13 && $.browser.msie && $.browser.version <= 8) {
						$btn.click();
					}
				}).bind('blur', function () {
					clearTimeout(timeout);
					if (!$container.hasClass('hover')) {
						hideKh();
					}
				});
			});

			/**
			* Banner formatting and functionality
			*
			* Banner
			**/
			$('.banner').each(function () {
				var $banner = $(this);

				// Banner box in video banner
				if ($banner.hasClass('video')) {
					$banner.find('.media .box').each(function () {
						var $t = $(this);
						$t.css('margin-left', -Math.round($t.outerWidth() / 2));
					});
				}

				// Video in cut banner
				if ($banner.hasClass('cut')) {
					(function () {
						var $wrapper = $banner.find('.wrapper'),
						$bannerBox = $banner.add($wrapper),
						$btn = $banner.find('.die'),
						$vid = $banner.find('.media.vid').addClass('hidden'),
						$box = $banner.find('.box');

						// Set up button
						var btnText = $btn.text(), btnAltText = $btn.data('alttext');
						$win.load(function () {
							$btn.bind(touchClick, function (e) {
								e.preventDefault();
								if ($btn.hasClass('active')) {
									$btn.removeClass('active').text(btnText);
									hideVideo();
									try { $vid.data('player').pause(); } catch (err) { log('Unable to catch media player (' + err.toString() + ')'); }
								} else {
									$btn.addClass('active').text(btnAltText);
									showVideo();
									try { $vid.data('player').pause(); } catch (err) { log('Unable to catch media player (' + err.toString() + ')'); }
								}
							});
						});

						// Set up hide/show functions
						var vidHeight = $vid.outerHeight(),
						orgHeight = $bannerBox.height(),
						isSmaller = vidHeight > orgHeight,
						getDimensions = function () {
							vidHeight = $vid.outerHeight();
							isSmaller = vidHeight > orgHeight;
						},
						showVideo = function () {
							getDimensions();
							$bannerBox.stop().animate(
								{ 'height': isSmaller ? vidHeight : orgHeight },
								isSmaller ? 250 : 0,
								function () {
									$box.stop().fadeOut(250);
									$vid.removeClass('hidden').fadeIn(250);
								}
							);
						},
						hideVideo = function () {
							getDimensions();
							$vid.stop().fadeOut(250, function () {
								$vid.addClass('hidden');
								$bannerBox.stop().animate({ 'height': orgHeight }, isSmaller ? 250 : 0);
								$box.stop().fadeIn(250);
							});
						};
					})();
				}
			});

			/**
			* Content formatting and functionality - Most of which is wrapped in a function to allow reinit with lightbox
			*
			* Content
			**/
			// Reinit available function
			var formatContent = function (fc) {
				var $fc = $(fc);

				// Bridge nth-child selector
				$('.method-list>li:nth-child(2n+1)').addClass('nth-child-2n-plus-1');
				$('.subject > ul > li:nth-child(odd)').addClass('nth-child-odd');
				$('.subject-list li:nth-child(2n+1)').addClass('nth-child-2n-plus-1');
				$('nav.dropdown li:nth-child(3n+1)').addClass('nth-child-3n-plus-1');
				$('.media-gallery ul li:nth-child(4n+1)').addClass('nth-child-4n-plus-1');

				// Alerts
				$fc.find('a.alert').click(function (e) {
					var text = $(this).data('alert');
					text = text.length > 0 ? text : 'Er du sikker?';
					if (!confirm(text)) e.preventDefault();
				});

				// Iframes in articles
				$fc.find('.entry iframe').each(function () {
					var $iframe, $wrap;
					$iframe = $(this);
					$wrap = $('<div class="iframe"></div>');

					if ($iframe.attr('src').indexOf('youtube') > -1) {
						$wrap.addClass('youtube');
					}
					$iframe.wrap($wrap);
				});

				// Slideshow
				$fc.find('.slideshow').each(function () {
					var $container = $(this).addClass('enhanced'),
						$list = $container.find('ul'),
						$items = $list.children(),
						curSlide = false,
						getSlide = function (i) {
							if (i < 0 || i >= $items.length) return false;
							$items.filter('.active').removeClass('active');
							curSlide = i;
							$pageText.text((i + 1) + ' af ' + $items.length);
							return $($items[i]).addClass('active');
						},
						getNextSlide = function () {
							var nextSlide = curSlide + 1 >= $items.length ? 0 : curSlide + 1;
							getSlide(nextSlide);
						},
						getPrevSlide = function () {
							var nextSlide = curSlide - 1 < 0 ? $items.length - 1 : curSlide - 1;
							getSlide(nextSlide);
						},
						$paging = $('<div class="paging" />'),
						$pageText = $('<p />').appendTo($paging);

					// Add next/prev
					$('<a href="#prev" class="button prev">&larr;</a>').bind(touchClick, function (e) {
						e.preventDefault();
						getPrevSlide();
					}).appendTo($paging);
					$('<a href="#next" class="button next">&rarr;</a>').bind(touchClick, function (e) {
						e.preventDefault();
						getNextSlide();
					}).appendTo($paging);

					// Insert paging
					$paging.appendTo($container);

					// Hide / show entries
					$items.each(function () {
						var $item = $(this),
							$entry = $item.find('.entry');

						if ($entry[0]) {
							$entry.append('<span class="arrow" />');
							$entry.find('a').bind(touchClick, function (e) {
								e.stopPropagation();
							});
							$entry.bind(touchClick, function (e) {
								e.stopPropagation();
								$entry.toggleClass('closed');
							});
						}
					});

					// Set first active
					getSlide(0);
				});

				// Timeline canvas
				$fc.find('.tl-canvas').each(function () {
					var $canvas = $(this).addClass('enhanced'),
						$list = $canvas.find('.tl-items>ol'),
						$items = $list.children(),
						$heading = $canvas.children('.tl-heading,.tl-desc'),
						toggleHeading = function () {
							if (!$items.filter('.active')[0]) {
								$heading.stop().fadeIn(250);
							} else {
								$heading.stop().fadeOut(250);
							}
						};

					// Insert labels, close and listen for click event
					$items.each(function (i, el) {
						var $item = $(this),
							$article = $item.find('.tl-article');

						//S Set up position
						$item.css({ 'left': $item.data('position') });

						// Insert labels
						var labelText = $item.data('label');
						if (labelText) $('<span class="tl-jsline"></span><span class="tl-jslabel">' + labelText + '</span>').appendTo($item);

						// Insert close
						$('<a href="#close" class="tl-jsclose">Luk</a>').prependTo($article.find('.entry')).bind(touchClick, function (e) {
							e.preventDefault();
							$items.removeClass('active')
							toggleHeading();
						});

						// Insert arrow
						$('<span class="tl-arrow"></span>').appendTo($item);

						// Stop propagation
						$item.find('.tl-article').bind(touchClick, function (e) {
							e.stopPropagation();
						});

						// Bind event
						$item.bind(touchClick, function (e) {
							e.preventDefault();
							$item.toggleClass('active').siblings().removeClass('active');
							toggleHeading();

							// Set alignment
							$item.removeClass('right');
							var cl = $canvas.offset().left,
								il = $article.offset().left;
							if (il - cl > $article.width() * 1.2) {
								$item.addClass('right');
							}
						});
					});

					// Set offset on labels
					var $labels = $items.find('.tl-jslabel');
					$items.reverse().each(function () {
						var $item = $(this),
							$label = $item.find('.tl-jslabel'),
							hit = false;
						if ($label[0]) {
							$labels.each(function (i, e) {
								if ($label[0] == $labels[i]) return;
								if ($label.hitTestObject($(this))) hit = true;
							});
							if (hit) $item.addClass('offset');
						} else {
							return false;
						}
					}).reverse();
				});

				// Form 
				$fc.find('.form').each(function () {
					var $form = $(this);

					// Dropdowns
					$form.find('select').simpleFormUI({ cssClass: 'custom-dropdown' });

					// Preview
					$form.find('a.preview').each(function () {
						var $link = $(this),
							$target = $($link.attr('href')),
							$container = $target.parent('.form-preview');

						if ($target[0] && $container[0]) {
							var $html = $('html'),
								pos = $link.position(),
								height = $link.outerHeight();

							$link.click(function (e) {
								e.preventDefault();
								e.stopPropagation();

								$container.css({ 'top': pos.top + height, 'left': pos.left });

								$target.css({ 'display': 'block' }).siblings().css({ 'display': 'none' });

								$html.bind('click', function () {
									$target.css({ 'display': 'none' });
								});
							});
						}
					});
				});

				// Goal table
				$fc.find('.entry table.goals').each(function () {
					var $table = $(this),
					$inputs = $table.find('input[type="checkbox"], input[type="radio"]');

					$inputs.each(function () {
						var $input = $(this);
						if (this.checked) {
							$input.parent().addClass('checked');
						} else {
							$input.parent().addClass('unchecked');
						}
					});
				});

				// Form checkboxes
				$fc.find('.form .checkbox,.form .radio').each(function () {
					var $container = $(this),
						$inputs = $container.find('input'),
						$iContainers = $inputs.parent();

					$inputs.change(function () {
						$iContainers.removeClass('checked');
						$inputs.each(function () {
							if (this.checked) {
								$(this).parent().addClass('checked');
							}
						});
					}).trigger('change');
				});

				// Header typographic alignment
				var headerAlignment = function () {
					$fc.find('.entry.article').find('h2').each(function () {
						var $header = $(this);
						$header.css('margin-bottom', -$header.height());
					});
				};
				$win.smartresize(headerAlignment).load(headerAlignment);
				headerAlignment();

				// Abbreviation speech bubble
				$fc.find('.entry abbr').each(function () {
					var $abbr = $(this).addClass('abbr hidden'),
							$bubble = $('<span class="abbr-bubble"><span class="top"></span><span class="inner">' + $abbr.attr('title') + '</span><span class="bottom"></span></span>').appendTo($abbr),
							hideBubble = function () { return !$abbr.hasClass('hidden') ? $abbr.addClass('hidden') : false; },
							showBubble = function () { return $abbr.hasClass('hidden') ? $abbr.removeClass('hidden') : false; },
							toggleBubble = function () { return $abbr.toggleClass('hidden'); };

					$abbr.removeAttr('title');
					if (Modernizr.touch) {
						$abbr.bind(touchClick, function (e) {
							e.stopPropagation();
							toggleBubble();
						}).closest('.entry').bind(touchClick, function () {
							hideBubble();
						});
					} else {
						$abbr.hover(function () {
							showBubble();
						}, function () {
							hideBubble();
						});
					}
				});

				// Assignments auto-fold
				$fc.find('.content-box.assignment').each(function () {
					var $ass = $(this).removeClass('hidden').height('auto'),
						init = function () {
							if ($ass.outerHeight() - 200 > 50) {
								$ass.addClass('hidden');
								var $shade = $ass.find('.shade')[0] ? $ass.find('.shade') : $('<span class="shade"></span>').appendTo($ass),
								$link = $ass.find('.show')[0] ? $ass.find('.show') : $('<a href="#show" class="show">Vis hele opgaven &darr;</a>').appendTo($ass),
								origHeight = $ass.height();
								$ass.height($ass.find('h4').outerHeight() + 110);
								$link.bind(touchClick, function (e) {
									e.preventDefault();
									$ass.animate({ height: origHeight }, 500, function () {
										$ass.height('auto');
									});
									if ($.browser.msie && parseInt($.browser.version) < 9) {
										$link.add($shade).remove();
									} else {
										$link.add($shade).fadeOut(500, function () {
											$(this).remove();
										});
									}
								});
							}
						};
					init();
				});

				// Poll - radio buttons
				$fc.find('.content-box.opinion .options').each(function () {
					if ($(this).closest('.content-box.opinion').hasClass('form-example')) return false;

					var $container = $(this),
						$inputs = $container.find('input'),
						$labels = $container.find('label'),
						$btn = $container.siblings('.button[data-service]');

					if ($inputs[0]) {
						// Bind change event
						var init = function () {
							$labels.removeClass('checked');
							$inputs.each(function () {
								if (this.checked) $(this).next('label').addClass('checked');
								if (this.disabled) $(this).next('label').addClass('disabled');
							});
						};
						$inputs.change(init);
						init();

						// Bind submit
						$btn.bind(touchClick, function (e) {
							e.preventDefault();
							var $t = $(this),
								service = $t.data('service'),
								hasAnswer = false,
								$feedback = $container.parent().find('.feedback')[0] ? $container.parent().find('.feedback') : $('<p class="feedback"></p>').insertAfter($container),
								feedback = function (str, cls) {
									$feedback.hide().text(str).removeClass('success error').addClass(cls).fadeIn(250);
								};
							try {
								var data = eval('(' + $t.data('parameters') + ')');
							} catch (err) {
								var data = new Object();
							}
							data = typeof data !== 'object' ? new Object() : data;
							$feedback.hide();
							if ($t.hasClass('disabled')) {
								feedback('Du kan kun svare én gang på hver afstemning', 'error');
								return false;
							}
							$container.addClass('loading');
							if (service) {
								$inputs.each(function () {
									if (this.checked) {
										hasAnswer = true;
										data[$(this).attr('name')] = $(this).val();
									}
								});
								if (hasAnswer) {
									_v.services.request(service, data, function (resp) {
										$container.removeClass('loading');
										if (resp.Outcome.Validated) {
											var items = resp.Response.Responses;
											$.each(items, function (i, v) {
												var $item = $inputs.attr('disabled', 'disabled').filter('#' + v.InputId.toString()).parent(),
														$bar = $('<span class="bar" style="width:0%"></span>').appendTo($item).attr('title', v.Percent + '%').animate({ width: '50%' }, 500),
														$val = $('<span class="val" style="width:0%"></span>').appendTo($bar).animate({ width: v.Percent + '%' }, 500);
												//$btn.add($container).addClass('disabled');
												$btn.remove();
												//init();
											});
										} else if (resp.Outcome.Error) {
											feedback(resp.Outcome.Error);
										} else {
											_v.err(this, 'Unexpected error', 'Unexpected format returned.');
										}
									});
								} else {
									$container.removeClass('loading');
									feedback('Du skal først vælge dit svar', 'error');
								}
							} else {
								$container.removeClass('loading');
								_v.err(this, 'Unexpected error', 'No service string found, even though attribute is present');
							}
						});

						if ($container.data('auto-post')) {
							var input = $('#' + $container.data('input-id'));
							// .click on label doesn't work in ie7/8
							input.parent().find('label').addClass('checked');
							input.parent().find('input').prop('checked', 'checked');
							$btn.click();
						}
					}
				});

				// Navigational dropdown
				$fc.find('nav.dropdown').each(function () {
					var $container = $(this),
						$link = $container.children('a'),
						$list = $container.children('ul'),
						toggleList = function () {
							if ($list.hasClass('active')) {
								hideList();
							} else {
								showList();
							}
						},
						showList = function () {
							$list.hide().addClass('active').slideDown(250);
						},
						hideList = function () {
							$list.slideUp(250, function () {
								$list.removeClass('active').show()
							});
						},
						lazyTimer = 0;

					// Bind events depending on touch device
					if (Modernizr.touch) {
						$link.bind('click', function (e) {
							e.preventDefault();
							toggleList();
						});
					} else {
						$container.hover(function () {
							clearTimeout(lazyTimer);
							if (!$list.hasClass('active')) showList();
						}, function () {
							clearTimeout(lazyTimer);
							lazyTimer = setTimeout(function () {
								hideList();
							}, 750);
						});
					}

					// Align items 
					var $collection = $(),
							maxHeight = 0,
							s = 0, t = 2;
					$list.addClass('active').find('a').each(function (i, e) {
						if (s > t) {
							s = 0;
							$collection.height(maxHeight);
							maxHeight = 0;
							$collection = $();
						}
						s++;
						var $t = $(this),
								th = $t.height();
						$collection = $collection.add($t);
						maxHeight = maxHeight < th ? th : maxHeight;
					});
					$list.removeClass('active');
				});

				// Subject list 
				$fc.find('.subject-list').each(function () {
					var $container = $(this),
						$areaLinks = $container.find('li nav a'),
						$shortTexts = $container.find('.short-texts').children();

					// Bind buttons
					$areaLinks.bind(touchClick, function (e) {
						var $link = $(this),
							$item = $link.closest('li'),
							$itemLink = $item.children('a'),
							itemPos = $item.position(),
							itemDim = { w: $itemLink.width(), h: $item.height(), ml: parseInt($item.css('margin-left')) + parseInt($item.css('padding-left')) },
							$shortText = $shortTexts.filter('#' + $link.data('shorttext'));

						if ($shortText[0]) {
							e.preventDefault();
							$shortTexts.removeClass('active');
							$shortText.addClass('active').css({ 'top': itemPos.top, 'left': Math.round(itemPos.left + itemDim.ml), 'width': itemDim.w, 'height': itemDim.h - $link.height() - 1 });
						}
					});

					// Add close link
					$('<a href="#close" class="close">Luk</a>').appendTo($shortTexts).bind(touchClick, function (e) {
						e.preventDefault();
						$shortTexts.removeClass('active');
					});
				});

				// Content box news
				$fc.find('.content-box.news').each(function () {
					var $container = $(this),
							$items = $container.find('article'),
							itemHeight;

					// Show first
					$items.first().addClass('show');

					// Paging
					$items.each(function (i, e) {
						var $item = $(this),
								$paging = $item.find('.paging'),
								$pagingTitle = $paging.find('p'),
								$nextBtn = $paging.find('.next'),
								$prevBtn = $paging.find(' .prev');

						// Correct text
						$pagingTitle.text((i + 1) + ' af ' + $items.length);

						// Bind click events
						$nextBtn.bind(touchClick, function (e) {
							e.preventDefault();
							var $get = $item.next();
							$get = $get[0] ? $get : $items.first();
							$get.siblings('.show').removeClass('show').addClass('hide').fadeOut(250, function () {
								$get.siblings('.hide').removeClass('hide').show();
							});
							$get.addClass('show');
						});
						$prevBtn.bind(touchClick, function (e) {
							e.preventDefault();
							var $get = $item.prev();
							$get = $get[0] ? $get : $items.last();
							$get.siblings('.show').removeClass('show').addClass('hide').fadeOut(250, function () {
								$get.siblings('.hide').removeClass('hide').show();
							});
							$get.addClass('show');
						});
						var resizeItems = function () {
							itemHeight = 0;
							$items.each(function () {
								var $item = $(this),
								height = parseInt($item.outerHeight());

								$item.removeAttr('style');

								itemHeight = itemHeight < height ? height : itemHeight ;
							}).each(function () {
								var $item = $(this),
								$entry = $item.find('.entry').first(),
								paddingBottom,
								margin = itemHeight - parseInt($item.outerHeight());

								if (margin > 0) {
									paddingBottom = parseInt($entry.css('padding-bottom'));
									$entry.css('padding-bottom', paddingBottom + margin);
								}
							});
						};
						if ($items.length === 1) {
							$items.find('.paging').hide();
						} else {
							resizeItems();
							$(window).smartresize(resizeItems).load(resizeItems);

						}
					});
				});

				// Lettering
				//$('.entry.article h2').lettering();

				// Polyfills
				if (!Modernizr.before) {
					$('.entry.article h2').prepend('<span class="before"></span>');
				}
				



				return $fc;
			};
			formatContent(document);

			// Init media and audio
			initMedia($('.media'));

			// Media gallery
			$('.media-gallery').each(function () {
				var $container = $(this),
					$mediaAdditional = $container.find('.additional-subjects'),
					$showAll = $container.find('.show-all');
				$mediaAdditional.toggle(0);

				$showAll.bind(touchClick, function (e) {
					e.preventDefault();
					var alttext = $showAll.data('alttext');
					$showAll.data('alttext', $showAll.text());
					$showAll.text(alttext);
					$mediaAdditional.slideToggle(300);
				});

				var $mediaLists = $container.find('.media-list');
				$mediaLists.each(function () {
					var $mediaItems = $(this).find('li');
					$mediaItems.each(function (i, e) {
						var $li = $(this),
							$item = $li.find('.item'),
							type = $item.hasClass('media-video') ? 'video' : 'image',
							$link = $item.find('figure a, .figure a'),
							$download = $item.find('.download'),
							title = $item.find('h3').text(),
							href = $link.attr('href'),
							openLb;

						// Build content
						var $content = $('<div class="media-content" />'),
							$footer = $('<div class="footer" />').appendTo($content),
							$dl = $download.clone().appendTo($footer),
							$close = $('<a href="#close" class="close">Close</a>').appendTo($footer).hide(),
							$paging = $('<div class="paging"><p>' + (i + 1) + ' af ' + $mediaItems.length + '</p></div>').appendTo($footer),
							$prev = $('<a href="#prev" class="button prev">&larr;</a>').appendTo($paging),
							$next = $('<a href="#next" class="button next">&rarr;</a>').appendTo($paging),
							$media = $();

						// Build media
						switch (type) {
							case 'video':
								var vidSrc = href,
									vidWidth = $item.data('videowidth'),
									vidHeight = $item.data('videoheight'),
									vidImg = $link.find('img').attr('src');
								vidWidth = vidWidth ? vidWidth : 600;
								vidHeight = vidHeight ? vidHeight : 450;
								$media = $('<div class="media vid" data-src="' + vidSrc + '" data-width="' + vidWidth + '" data-height="' + vidHeight + '" style="width:' + vidWidth + 'px;height:' + vidHeight + 'px;"><a href="/alinea/samfundsfag/html/media/video.mp4" class="fallback" target="_blank"><img src="' + vidImg + '" alt="Video" style="width:' + vidWidth + 'px;height:' + vidHeight + 'px;" /></a></div>').prependTo($content);
								break;
							case 'image':
								$media = $('<img class="image" src="' + href + '" alt="' + title + '" />').prependTo($content);
								$media.load(function () { $media.data('load', true); });
								break;
						}

						// Set up open function
						openLb = function () {
							$.fancybox({
								content: $content,
								padding: 0,
								onStart: function () {
									//$('#fancybox-close').hide();
								},
								onComplete: function () {
									//$('#fancybox-close').hide();

									// Reinit standard content
									var $fbc = formatContent('#fancybox-content');

									// Reinit rich media
									initMedia($fbc.find('.media'));

									// Reinit back button
									$fbc.find('.button.back').bind(touchClick, function (e) {
										e.preventDefault();
										$.fancybox.close();
									});

									// Reinit print button
									$fbc.find('.print').bind(touchClick, function (e) {
										e.preventDefault();
										var pw = window.open(href, "printWindow");
										$(pw).load(function () {
											pw.print();
											pw.close();
										});
									});

									// Set up interactions
									$close.bind(touchClick, function (e) {
										e.preventDefault();
										$.fancybox.close();
									});
									$prev.bind(touchClick, function (e) {
										e.preventDefault();
										var $next = $li.prev();
										$next = $next[0] ? $next : $mediaItems.last();
										$next.find('figure a').trigger(touchClick);
									});
									$next.bind(touchClick, function (e) {
										e.preventDefault();
										var $next = $li.next();
										$next = $next[0] ? $next : $mediaItems.first();
										$next.find('figure a').trigger(touchClick);
									});
								}
							});
						};

						// Attach link event
						$link.bind(touchClick, function (e) {
							e.preventDefault();

							// Show fancybox
							if (type == "image" && !$media.data('load')) {
								$.imgpreload([href], {
									all: function () {
										openLb();
									}
								});
							} else {
								openLb();
							}
						});
					});
				});
			});

			// Print
			$('.print').bind(touchClick, function (e) {
				var height, width, maxWidth, resized;

				e.preventDefault();

				maxWidth = 960;
				resized = false;
				height = $win.height();
				width = $win.width();

				if ($.browser.msie && width > maxWidth) {
					resized = true;
					window.resizeTo(maxWidth, height);
					setTimeout(function () {
						window.print();
					}, 250);
				} else {
					window.print();
				}
			});

			// Back buttons
			$('.button.back').bind(touchClick, function (e) {
				e.preventDefault();
				history.back();
			});

			// Content iframes
			$('.lb-content').each(function () {
				var $link = $(this),
				href = $link.attr('href');

				$link.bind(touchClick, function (e) {
					e.preventDefault();

					var $content = $('<div>');
					window.c = $content;
					$link.addClass('loading');

					var $youtubes = $('.youtube');
					$youtubes.addClass('is-hidden');

					$content.load(href + ' .content', function (rt, ts, xhr) {
						$link.removeClass('loading');
						if (ts == 'success' || ts == 'notmodified') {
							// Open fancybox
							$.fancybox({
								content: $content,
								onComplete: function () {
									// Reinit standard content
									var $fbc = formatContent('#fancybox-content');

									// Reinit rich media
									initMedia($fbc.find('.media').not('.loaded'));

									// Reinit back button
									$fbc.find('.button.back').bind(touchClick, function (e) {
										e.preventDefault();
										$.fancybox.close();
									});

									// Reinit print button
									$fbc.find('.print').bind(touchClick, function (e) {
										e.preventDefault();
										var pw = window.open(href, "printWindow");
										$(pw).load(function () {
											pw.print();
											pw.close();
										});
									});
								},
								onClosed: function () {
									$youtubes.removeClass('is-hidden');
								}
							});

						} else {
							log('Error getting href in attempt to open content lightbox (' + ts + ').');
						}
					});
				});
			});

			// Images in iframes
			$('.lb-image').fancybox({
				'padding': 0
			});

			// Broken links highlighting
			var getQuerystring = function getQuerystring(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regexS = "[\\?&]" + name + "=([^&#]*)";
				var regex = new RegExp(regexS);
				var results = regex.exec(window.location.search);
				if (results == null)
					return "";
				else
					return decodeURIComponent(results[1].replace(/\+/g, " "));
			}

			if (getQuerystring('brokenlink').length > 0) {
				var brokenLink = getQuerystring('brokenlink');
				$('a[href="' + brokenLink + '"]').css('border', '5px solid #f00');
				$('img[src="' + brokenLink + '"]').css('border', '5px solid #f00');
			}


			// IE7 hacks
			if ($.browser.msie && parseInt($.browser.version) < 8) {
				// Danger: Very, very ugly hack ahead
				$win.load(function () {
					var $flickr = $('.content-box, .paging .button, .options');
					setTimeout(function () {
						$flickr.css({ 'display': 'none' });
						setTimeout(function () {
							$flickr.css({ 'display': 'block' });
						}, 20);
					}, 20);
				});
			}


			
		});
	};
	_v.init();
})();