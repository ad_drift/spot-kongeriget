module.exports = function(grunt) {

	'use strict';

	// Project configuration.
	grunt.initConfig({
		// Metadata
		pkg: grunt.file.readJSON('package.json'),
		banner: '/*!\n' +
			'* <%= pkg.name || pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
			'* <%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
			'* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>; Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n' +
			'*/\n',
		// Task configuration.

		less: {
			options: {
				file: 'screen',
				sourceMap: true,
				sourceMapFilename: '<%= pkg.config.css.dist + less.options.file %>.map',
				sourceMapURL: '<%= less.options.file %>.map'
			},
			development: {
				files: {
					'<%= pkg.config.css.dist + less.options.file %>.css': '<%= pkg.config.css.src + less.options.file %>.less'
				}
			},
			production: {
				options: {
					cleancss: true,
					sourceMap: true
				},
				files: {
					'<%= pkg.config.css.dist + less.options.file %>.min.css': '<%= pkg.config.css.src + less.options.file %>.less'
				}
			}
		},
		concat: {
			options: {
				banner: '<%= banner %>',
				stripBanners: true
			},
			plugins: {
				src: ['<%= pkg.config.js.src %>plugins/*.js'],
				dest: '<%= pkg.config.js.dist %>jquery.plugins.js'
			}
		},
		uglify: {
			options: {
				beautify: {
					ascii_only: true
				}
			},
			library: {
				src: '<%= pkg.config.js.src %>jquery.js',
				dest: '<%= pkg.config.js.src %>min/jquery.js'
			},
			plugins: {
				options: {
					banner: '<%= banner %>',
					sourceMap: "<%= pkg.config.js.src %>min/jquery.plugins.map"
				},
				src: '<%= concat.plugins.dest %>',
				dest: '<%= pkg.config.js.src %>min/jquery.plugins.js'
			},
			script: {
				options: {
					sourceMap: "<%= pkg.config.js.src %>min/<%= pkg.title %>.func.map"
				},
				src: '<%= pkg.config.js.src + pkg.title %>.func.js',
				dest: '<%= pkg.config.js.src %>min/<%= pkg.title %>.func.js'
			}
		},
		watch: {
			less: {
				files: '<%= pkg.config.css.src %>**/*.less',
				tasks: ['less']
			},
			scripts: {
				files: '<%= pkg.config.js.src + pkg.title %>.func.js',
				tasks: ['uglify:script']
			}
		},
		/*@@*/
		copy: {
			build: {
				files: [
					{src: ['<%= pkg.main %>'], dest: 'distro/'},
					{src: ['package.json'], dest: 'distro/'},
					{src: ['README.md'], dest: 'distro/'}
				]
			},
			scripts: {
				files: [{expand: true, src: ['<%= pkg.config.js.src %>/rammevaerk.func.js'], dest: '<%= pkg.config.js.src %>',  rename: function(dest, src) {
					return src.replace("rammevaerk", '<%= pkg.title %>');
				}}]
			}
		},
		clean: {
			library: ["rammevaerk.build.zip"],
			scripts: ["<%= pkg.config.js.src %>/min", "<%= pkg.config.js.src %>/rammevaerk.func.js"]
		},
		zip: {
			distro: {
				src: ['<%= pkg.config.js.src %>**', '<%= pkg.config.css.src %>**', '<%= pkg.config.css.dist %>/img/**', '<%= pkg.config.html.src %>**'],
				dest: 'distro/rammevaerk.build.zip',
				compression: 'DEFLATE'
			}
		},

		/*@@*/
		connect: {
			server: {
				options: {
					port: 8080,
					root:'.',
					middleware: function(connect, options) {
						var dirOpts = {
							icons: true
						};
						return [
							connect.favicon(),
							connect['static'](options.base),
							connect.directory(options.base, dirOpts)
						];
					}
				}
			}
		}
	});

	// Libraries
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
/*@@*/
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-zip');
/*@@*/
	// Tasks
	grunt.registerTask('default', ['less', 'concat' ,'uglify']);
	grunt.registerTask('js', ['concat', 'uglify']);
	grunt.registerTask('css', ['less']);
	grunt.registerTask('server', ['connect', 'watch']);
/*@@*/
	grunt.registerTask('dist', ['zip', 'copy:build']);
	
	grunt.registerTask('init', function(){
		grunt.task.run('copy:scripts');
		grunt.task.run('clean');
		grunt.task.run('replace');
		grunt.task.run('less');
		grunt.task.run('js');
	});

	grunt.registerTask('replace', function(){
		var file = grunt.file.read(grunt.config.data.pkg.main, 'utf8').replace(/.\*@@([\s\S]*?)@@\*./g, '');
		grunt.file.write(grunt.config.data.pkg.main, file, 'utf8');
	});
/*@@*/
};