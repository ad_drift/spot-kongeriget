<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
	
<xsl:variable name="getLogin" select="$currentPage/ancestor-or-self::*/Homepage/Login"/>

<xsl:template name="getLogin">

	<xsl:choose>
		<xsl:when test="samfundsfag:IsLoggedInUserStudent()">
			<a class="button discreet" href="/elev/">
				Gå til emnerne
			</a>
		</xsl:when>
		<xsl:when test="samfundsfag:IsLoggedInUserTeacher()">
			<a class="button discreet" href="/laerer/">
				Gå til emnerne
			</a>
		</xsl:when>
		<xsl:otherwise>
			<a class="button discreet large" href="http://www.alinea.dk/salgssupport/proveabb.aspx?produkt=Kongeriget" target="_blank">
				Bestil prøveabonnement
			</a>
		</xsl:otherwise>
	</xsl:choose>
	
	

</xsl:template>

<xsl:template name="getLoginCommonHomepage">

  <xsl:choose>
    <xsl:when test="samfundsfag:IsLoggedInUserStudent()">
      <a class="button discreet" href="/elev/">
        Gå til emnerne
      </a>
    </xsl:when>
    <xsl:when test="samfundsfag:IsLoggedInUserTeacher()">
      <a class="button discreet" href="/laerer/">
        Gå til emnerne
      </a>
    </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>