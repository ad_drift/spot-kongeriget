<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt"/>
	
<xsl:param name="currentPage"/>

<xsl:template match="/">

	<article class="content wrapper" role="main">
		<div class="heading">
			<h1>
				<xsl:value-of select="$currentPage/title"/>
			</h1>
			<a href="#print" class="print">Print</a>
		</div>
		<div class="method article entry grid-2 grid-first">
			<xsl:value-of select="samfundsfag:ConvertAbbreviationTags($currentPage/content)" disable-output-escaping="yes"/>
		</div>
		<footer class="method">
			<xsl:call-template name="back-button">
				<xsl:with-param name="parentId" select="$currentPage/@parentID"></xsl:with-param>
			</xsl:call-template>
		</footer>
	</article>

</xsl:template>

</xsl:stylesheet>