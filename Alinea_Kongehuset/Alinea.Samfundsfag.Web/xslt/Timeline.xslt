<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">

	<xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

	<xsl:include href="Templates.xslt" />
	
	<xsl:param name="currentPage"/>

	<xsl:variable name="timeline" select="$currentPage/Timeline"/>
  <xsl:variable name="pickedyear" select="$timeline/startYearOnTimeline"/>

	<xsl:template match="/">
		<xsl:if test="$timeline">
			<section class="grid-3 timeline">
				<div class="heading">
					<h2>
						<xsl:value-of select="$timeline/headline"/>
					</h2>
				</div>
				<figure class="tl-canvas enhanced">
					<h3 class="tl-heading" style="display: block; ">
						<!--<xsl:value-of select="$timeline/title"/>-->
					</h3>
					<p class="tl-desc" style="display: block; ">Klik på et af punkterne for at læse mere</p>
					<div class="tl-items">
						<ol>
							<xsl:for-each select="$timeline/* [@isDoc]">
								<li>
                  <xsl:attribute name="class">tl-item
                    <xsl:if test="$pickedyear = current()/@id">
                      active
                    </xsl:if>
                  </xsl:attribute>
									<xsl:attribute name="data-position"><xsl:value-of select="current()/edit"/>%</xsl:attribute>
									<xsl:attribute name="data-label"><xsl:value-of select="current()/label"/></xsl:attribute>
									<div class="tl-article">
										<xsl:call-template name="renderImage">
											<xsl:with-param name="imageId" select="current()/image" />
										</xsl:call-template>
										<div class="entry">
											<h4>
												<span class="tl-label"><xsl:value-of select="current()/label"/></span>
												<xsl:if test="current()/title != ''">
													<xsl:value-of select="' - '"/>
													<xsl:value-of select="current()/title"/>
												</xsl:if>
											</h4>
											<p>
												<xsl:value-of select="current()/description"/>
											</p>
											<xsl:if test="current()/readMore != '' and current()/readMoreLink != ''">
												<a href="{umbraco.library:NiceUrl(current()/readMoreLink)}" class="tl-readmore" target="_blank">
													<xsl:value-of select="current()/readMore" />
												</a>
											</xsl:if>
										</div>
									</div>
								</li>
							</xsl:for-each>
						</ol>
					</div>
				</figure>
			</section>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>