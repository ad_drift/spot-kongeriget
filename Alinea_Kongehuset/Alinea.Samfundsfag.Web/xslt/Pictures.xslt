<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="templates.xslt"/>
  
<xsl:param name="currentPage"/>

<xsl:variable name="querystringTopicId" select="umbraco.library:RequestQueryString('emne')"/>
	
<xsl:template match="/">
	
	<xsl:choose>
		<xsl:when test="$querystringTopicId != ''">
			<xsl:call-template name="showTopicPictures">
				<xsl:with-param name="topicId" select="$querystringTopicId"></xsl:with-param>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="showAllTopicPictures"></xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>

	<footer class="entry media-footer">
		<xsl:if test="$querystringTopicId != ''">
			<a href="#images" class="button significant show-all" data-alttext="Skjul igen">Vis alle emner</a>
		</xsl:if>
		<xsl:call-template name="back-button">
			<xsl:with-param name="parentId" select="$currentPage/@parentID"/>
		</xsl:call-template>
	</footer>

</xsl:template>
	
	<xsl:template name="showTopicPictures">
		<xsl:param name="topicId"/>

		<xsl:variable name="mediaTopic" select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Pictures/MediaTopic [topicLink=$topicId][1]"></xsl:variable>
		<xsl:variable name="topic" select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Topic [@id=$topicId]"></xsl:variable>
		
		<div class="main-subject media-list">
			<h2>
				<xsl:value-of select="$topic/title"/>
			</h2>
			<ul>
				<xsl:for-each select="$mediaTopic/* [@isDoc]">
					<xsl:call-template name="showPicture">
						<xsl:with-param name="item" select="current()"></xsl:with-param>
					</xsl:call-template>
				</xsl:for-each>
			</ul>
		</div>
   
		<div class="additional-subjects">
			<xsl:for-each select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Pictures/MediaTopic">

				<xsl:if test="string($mediaTopic) = '' or @id != $mediaTopic/@id">
					<xsl:variable name="additionalTopic" select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Topic [@id=current()/topicLink]"></xsl:variable>
					
					<div class="additional-subject media-list">
						<h2>
							<xsl:value-of select="$additionalTopic/title"/>

							<xsl:if test="count(current()/* [@isDoc]) &gt; 4">
								<a class="media-list-show-all" href="#">
									<xsl:attribute name="onclick">
										<xsl:text>
											if($(this).text() == 'Vis alle') 
											{ 
												$(this).closest('.media-gallery').find('.media-list').each(function()
												{
													$(this).find('ul li:gt(3)').slideUp(); 
												});
												$('.media-list-show-all').text('Vis alle');

												$(this).closest('.media-list').find('ul li:gt(3)').slideDown(); 
												$(this).text('Skjul igen');
											} 
											else
											{
												$(this).closest('.media-list').find('ul li:gt(3)').slideUp(); 
												$(this).text('Vis alle');
											}
											return false;
										</xsl:text>
									</xsl:attribute>
									<xsl:text>Vis alle</xsl:text>
								</a>
							</xsl:if>
						</h2>
						<ul>
							<xsl:for-each select="current()/* [@isDoc]">
								<xsl:call-template name="showPicture">
									<xsl:with-param name="item" select="current()"></xsl:with-param>
									<xsl:with-param name="hidden" select="position() &gt; 4" />
								</xsl:call-template>
							</xsl:for-each>
						</ul>
					</div>
				</xsl:if>
			</xsl:for-each>
		</div>
		
	</xsl:template>

	<xsl:template name="showAllTopicPictures">

		<xsl:variable name="mediaTopics" select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Pictures/MediaTopic"></xsl:variable>

		<xsl:for-each select="$mediaTopics">
			<xsl:variable name="topic" select="$currentPage/ancestor-or-self::Homepage/StudentMaster/Topic [@id=current()/topicLink]"></xsl:variable>
			<xsl:variable name="mediaTopic" select="current()"/>

			<xsl:if test="count($mediaTopic/* [@isDoc]) &gt; 0">
				<div class="main-subject media-list">
					<h2>
						<xsl:value-of select="$topic/title"/>

						<xsl:if test="count($mediaTopic/* [@isDoc]) &gt; 4">
							<a class="media-list-show-all" href="#">
								<xsl:attribute name="onclick">
									<xsl:text>
									if($(this).text() == 'Vis alle') 
									{ 
										$(this).closest('.media-gallery').find('.media-list').each(function()
										{
											$(this).find('ul li:gt(3)').slideUp(); 
										});
										$('.media-list-show-all').text('Vis alle');

										$(this).closest('.media-list').find('ul li:gt(3)').slideDown(); 
										$(this).text('Skjul igen');
									} 
									else
									{
										$(this).closest('.media-list').find('ul li:gt(3)').slideUp(); 
										$(this).text('Vis alle');
									}
									return false;
								</xsl:text>
								</xsl:attribute>
								<xsl:text>Vis alle</xsl:text>
							</a>
						</xsl:if>
					</h2>
					<ul>
						<xsl:for-each select="$mediaTopic/* [@isDoc]">
							<xsl:call-template name="showPicture">
								<xsl:with-param name="item" select="current()" />
								<xsl:with-param name="hidden" select="position() &gt; 4" />
							</xsl:call-template>
						</xsl:for-each>
					</ul>
				</div>
			</xsl:if>
			
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="showPicture">
		<xsl:param name="item" />
		<xsl:param name="hidden" />
		
		<xsl:variable name="isImage" select="name() = 'Image'" />
		<xsl:variable name="thumbnail" select="umbraco.library:GetMedia($item/thumbnail,0)"/>

		<li>
			<xsl:if test="$hidden">
				<xsl:attribute name="style">
					<xsl:value-of select="'display:none;'"/>
				</xsl:attribute>
			</xsl:if>
			<div class="item entry content-box">
				<xsl:attribute name="class">
					<xsl:if test="$isImage">
						<xsl:text>item entry content-box media-image</xsl:text>
					</xsl:if>
					<xsl:if test="$isImage = ''">
						<xsl:text>item entry content-box media-video</xsl:text>
					</xsl:if>
				</xsl:attribute>
				<figure>
					<a>
						<xsl:attribute name="href">
							<xsl:if test="$isImage">
                <xsl:variable name="image" select="umbraco.library:GetMedia($item/image,0)"/>
                <xsl:value-of select="samfundsfag:ResolveUrl($image/umbracoFile)"/>
              </xsl:if>
							<xsl:if test="$isImage = ''">
								<xsl:variable name="video" select="umbraco.library:GetMedia($item/video,0)"></xsl:variable>
								<xsl:value-of select="$video/umbracoFile"/>
							</xsl:if>
						</xsl:attribute>
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="samfundsfag:ResolveUrl($thumbnail/umbracoFile)"/>
							</xsl:attribute>
							<xsl:attribute name="alt">
								<xsl:value-of select="$thumbnail/@nodeName"/>
							</xsl:attribute>
						</img>
					</a>
				</figure>
				<h3>
					<xsl:value-of select="$item/title"/>
				</h3>
				<footer>
					<a class="download">
						<xsl:attribute name="href">
							<xsl:if test="$isImage">
								<xsl:variable name="image" select="umbraco.library:GetMedia($item/image,0)"/>
								<xsl:value-of select="'/download.ashx?file='"/>
								<xsl:value-of select="umbraco.library:UrlEncode($image/umbracoFile)"/>
							</xsl:if>
							<xsl:if test="$isImage = ''">
								<xsl:variable name="video" select="umbraco.library:GetMedia($item/video,0)" />
								<xsl:value-of select="'/download.ashx?file='"/>
								<xsl:value-of select="umbraco.library:UrlEncode($video/umbracoFile)"/>
							</xsl:if>
						</xsl:attribute>
						Hent 
					</a>
				</footer>
			</div>
		</li>
	</xsl:template>
	
</xsl:stylesheet>