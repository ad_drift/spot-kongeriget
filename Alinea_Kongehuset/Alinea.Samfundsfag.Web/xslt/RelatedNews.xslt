<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;">
	<!ENTITY rarr "&#x2192;">
	<!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt" />

<xsl:param name="currentPage"/>

<xsl:template match="/">
	
	<div class="heading">
		<h2>Perspektiv</h2>
	</div>
	<section class="content-box important simple opinion news">
		<xsl:for-each select="$currentPage/relatedNews/links/link">
			
			<xsl:variable name="this" select="umbraco.library:GetXmlNodeById(current()/@link)"/>
			<xsl:call-template name="news">
				<xsl:with-param name="this" select="$this"></xsl:with-param>
				<xsl:with-param name="isPromotion" select="'false'"></xsl:with-param>
				<xsl:with-param name="topicId" select="$currentPage/@id"></xsl:with-param>
			</xsl:call-template>

		</xsl:for-each>
	</section>

</xsl:template>

</xsl:stylesheet>