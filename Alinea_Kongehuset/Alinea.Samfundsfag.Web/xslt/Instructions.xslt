<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

	<div class="subject-list">
		<ul>
			<xsl:for-each select="$currentPage/Instruction">
				<li>
					<div>
						<hgroup>
							<h3>
								<xsl:value-of select="current()/title"/>
							</h3>
							<h4>
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
									</xsl:attribute>
									Se lærervejledning
								</a>
							</h4>
						</hgroup>
						<xsl:if test="current()/image != ''">
							<xsl:variable name="image" select="umbraco.library:GetMedia(current()/image, 0)"/>
							<img width="400" height="300">
								<xsl:attribute name="src">
									<xsl:value-of select="samfundsfag:ResolveUrl($image/umbracoFile)"/>
								</xsl:attribute>
								<xsl:attribute name="alt">
									<xsl:value-of select="$image/@nodeName"/>
								</xsl:attribute>
							</img>
						</xsl:if>
						<!--<xsl:if test="count(current()/Presentation) &gt; 0">
							<div class="subject-list-links">
								<xsl:for-each select="current()/Presentation">
									<p>
										<a>
											<xsl:attribute name="href">
												<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
											</xsl:attribute>
											<xsl:value-of select="current()/menuTitle"/>
										</a>
									</p>
								</xsl:for-each>
							</div>
						</xsl:if>-->
					</div>
				</li>
			</xsl:for-each>
		</ul>
	</div>

</xsl:template>

</xsl:stylesheet>