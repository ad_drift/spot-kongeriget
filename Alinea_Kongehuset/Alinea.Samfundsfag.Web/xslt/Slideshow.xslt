<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="templates.xslt"/>
	
<xsl:param name="currentPage"/>

<xsl:variable name="slideshow" select="umbraco.library:GetXmlNodeById(/macro/slideshowId)"/>

<xsl:template match="/">

	<div class="slideshow">
		<ul>
			<xsl:for-each select="$slideshow/Slide">
				<li>
					<xsl:call-template name="renderImage">
						<xsl:with-param name="imageId" select="current()/image"/>
						<xsl:with-param name="popupImageId" select="current()/popupImage"/>
					</xsl:call-template>
					<div class="entry">
						<h5>
							<xsl:value-of select="current()/title"/>
						</h5>
						<p>
							<xsl:value-of select="current()/content"/>
						</p>
						<small>
							<xsl:value-of select="current()/cite"/>
						</small>
					</div>
				</li>
			</xsl:for-each>
		</ul>
	</div>

</xsl:template>

</xsl:stylesheet>