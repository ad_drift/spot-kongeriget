<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

<ul>
	<xsl:for-each select="$currentPage/Topic">
		<li>
      <xsl:attribute name="class">
        <xsl:value-of select="umbraco.library:GetPreValueAsString(current()/topicFilter)"/>
      </xsl:attribute>
			<a>
				<xsl:attribute name="href">
					<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
				</xsl:attribute>
				<hgroup>
					<h3>
						<xsl:value-of select="current()/title"/>
					</h3>
					<h4>
						<xsl:value-of select="current()/undertitle"/>
					</h4>
				</hgroup>
				<xsl:variable name="image" select="umbraco.library:GetMedia(current()/image, 0)"/>
				<img width="400" height="300">
					<xsl:attribute name="src">
						<xsl:value-of select="$image/umbracoFile"/>
					</xsl:attribute>
					<xsl:attribute name="alt">
						<xsl:value-of select="$image/@nodeName"/>
					</xsl:attribute>
				</img>
			</a>
			<nav>
        <xsl:variable name="economics" select="current()/economicsPerspective"/>
        <xsl:variable name="politics" select="current()/politicsPerspective"/>
        <xsl:variable name="culture" select="current()/culturePerspective"/>
				<!--<a href="#oekonomi-url" data-shorttext="oekonomi">-->
        <div class="nav-item">
          
					<xsl:attribute name="style">
						width:<xsl:value-of select="current()/economicsPerspective"/>%
					</xsl:attribute>
          <xsl:choose>
            <xsl:when test="number($economics) &gt; number($politics) and number($economics) &gt; number($culture)">
              <span>HISTORIE</span>
            </xsl:when>
            <xsl:when test="number($economics) = number($politics) and number($economics) &gt; number($culture)">
              <span>HISTORIE</span>
            </xsl:when>
            <xsl:when test="number($economics) = number($culture) and number($economics) &gt; number($politics)">
              <span>HISTORIE</span>
            </xsl:when>
            <xsl:when test="number($economics) = number($culture) and number($economics) = number($politics)">
              <span>HISTORIE</span>
            </xsl:when>
            <xsl:otherwise>
              <span>H</span>
            </xsl:otherwise>
          </xsl:choose>

        </div>
        <!--<a href="#politik-url" data-shorttext="politik">-->
        <div class="nav-item">
					<xsl:attribute name="style">
						width:<xsl:value-of select="current()/politicsPerspective"/>%
					</xsl:attribute>
          <xsl:choose>
            <xsl:when test="number($politics) &gt; number($culture) and number($politics) &gt; number($economics)">
              <span>SAMFUNDSFAG</span>
            </xsl:when>
            <xsl:when test="number($politics) = number($culture) and number($politics) &gt; number($economics)">
              <span>SAMFUNDSFAG</span>
            </xsl:when>
            <xsl:when test="number($politics) = number($economics) and number($politics) &gt; number($culture)">
              <span>SAMFUNDSFAG</span>
            </xsl:when>
            <xsl:when test="number($politics) = number($economics) and number($politics) = number($culture)">
              <span>SAMFUNDSFAG</span>
            </xsl:when>
            <xsl:otherwise>
              <span>S</span>
            </xsl:otherwise>
          </xsl:choose>				
				</div>
        <!--<a href="#kultur-url" data-shorttext="kultur">-->
        <div class="nav-item">
					<xsl:attribute name="style">
						width:<xsl:value-of select="current()/culturePerspective"/>%
					</xsl:attribute>
          <xsl:choose>
            <xsl:when test="number($culture) &gt; number($politics) and number($culture) &gt; number($economics)">
              <span>KRISTENDOMSKUNDSKAB</span>
            </xsl:when>
            <xsl:when test="number($culture) = number($economics) and number($culture) &gt; number($politics)">
              <span>KRISTENDOMSKUNDSKAB</span>
            </xsl:when>
            <xsl:when test="number($culture) = number($politics) and number($culture) &gt; number($economics)">
              <span>KRISTENDOMSKUNDSKAB</span>
            </xsl:when>
            <xsl:when test="number($culture) = number($politics) and number($culture) = number($economics)">
              <span>KRISTENDOMSKUNDSKAB</span>
            </xsl:when>
            <xsl:otherwise>
              <span>K</span>
            </xsl:otherwise>
          </xsl:choose>
				</div>
			</nav>
		</li>
	</xsl:for-each>
</ul>

</xsl:template>

</xsl:stylesheet>