<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:include href="templates.xslt"/>
	
<xsl:template match="/">

			<ul>
				
				<xsl:for-each select="$currentPage/ancestor-or-self::*/StudentMaster/SingleCol [@nodeName='Spotlisten']/NewsArticle [showOnNewsOverview = '1']">
          
          <xsl:sort select="position()" data-type="number" order="descending"/>
					<li>
						<div class="content-box">
							<figure>
								<img>
									<xsl:variable name="image" select="umbraco.library:GetMedia(current()/largeImage, 0)"/>
									<xsl:attribute name="src">
										<xsl:value-of select="$image/umbracoFile"/>
									</xsl:attribute>
									<xsl:attribute name="alt">
										<xsl:value-of select="$image/@nodeName"/>
									</xsl:attribute>
								</img>
								<h2>
									<xsl:value-of select="current()/title"/>
								</h2>
							</figure>
							<div class="entry">
								<p>
									<xsl:value-of select="current()/text"/>
								</p>
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
									</xsl:attribute>
									Læs mere
								</a>
								<h5>
									Nøgleord: <xsl:value-of select="umbraco.library:Replace(current()/tags, ',', ', ')"/>
								</h5>
							</div>
						</div>
					</li>

				</xsl:for-each>
				
			</ul>

	<footer class="entry media-footer">
		<xsl:call-template name="back-button">
			<xsl:with-param name="parentId" select="$currentPage/@parentID"/>
		</xsl:call-template>
	</footer>

</xsl:template>

</xsl:stylesheet>