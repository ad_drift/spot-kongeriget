﻿using System.IO;
using System.Web;
using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.services
{
	public class UserArticleTimelineItemDelete : IHttpHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var timelineItemId = 0;
			int.TryParse(context.Request.QueryString["id"], out timelineItemId);

			var articleId = 0;
			int.TryParse(context.Request.QueryString["articleid"], out articleId);

			if (timelineItemId > 0)
			{
				using (var service = new UserArticleTimelineItemService())
				{
					service.DeleteUserArticleTimelineItem(timelineItemId, SessionController.CurrentUserId);
					var existingImg = new FileInfo(context.Server.MapPath("/media/UserArticles/" + articleId + "/timelineimage" + timelineItemId + ".jpg"));
					if (existingImg.Exists)
					{
						existingImg.Delete();
					}
				}
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

		public bool IsReusable { get { return false; } }
	}
}