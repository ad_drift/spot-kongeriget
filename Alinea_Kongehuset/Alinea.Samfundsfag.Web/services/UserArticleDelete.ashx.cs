﻿using System.Web;
using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.services
{
	public class UserArticleDelete : IHttpHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var articleId = 0;
			int.TryParse(context.Request.QueryString["id"], out articleId);
			
			if (articleId > 0)
			{
				using (var uas = new UserArticleService())
				{
					uas.DeleteUserArticle(articleId, SessionController.CurrentUserId);
				}
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

		public bool IsReusable { get { return false; } }
	}
}