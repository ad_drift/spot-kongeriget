﻿using System.Web;
using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.services
{
	public class UserArticleNewComment : IHttpHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var articleId = 0;
			int.TryParse(context.Request.QueryString["articleId"], out articleId);
			
			var isTeacherFeedback = false;
			if(SessionController.IsCurrentUserTeacher)
				bool.TryParse(context.Request.QueryString["isTeacherFeedback"], out isTeacherFeedback);

			if (articleId > 0)
			{
				using (var uacs = new UserArticleCommentService())
				{
					uacs.CreateComment(articleId, SessionController.CurrentUserId, context.Request.QueryString["comment"], isTeacherFeedback);
				}
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

		public bool IsReusable { get { return false; } }
	}
}