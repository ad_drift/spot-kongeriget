﻿using Alinea.Samfundsfag.BusinessLogic.Models.Json;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.Services.Handlers;
using CoreM41.Web.Services.Generic.Enums;
using StackExchange.Profiling;

namespace Alinea.Samfundsfag.Web.services
{
	public class Polls : ServiceHandlerBase<PollsRequest, PollsResponse>
	{
		public Polls()
		{
			using (MiniProfiler.Current.Step("Polls.Contructor"))
			{
				RequestContentType = ContentType.Json;
				ResponseContentType = ContentType.Json;
				this.LowerCaseVariables = false;
			}
		}

		public override void Process()
		{
			using (MiniProfiler.Current.Step("Polls.Process"))
			{
				var pollService = new PollService();
				Response = pollService.HandlePollVote(Request);    
			}
		}
	}
}