﻿using Alinea.Samfundsfag.BusinessLogic.Models.Json;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.Services.Handlers;
using CoreM41.Web.Services.Generic.Enums;

namespace Alinea.Samfundsfag.Web.services
{
	public class Tags : ServiceHandlerBase<TagsRequest, TagsResponse>
	{
		public Tags()
		{
			RequestContentType = ContentType.QueryString;
			ResponseContentType = ContentType.Json;
			this.LowerCaseVariables = false;
		}

		public override void Process()
		{
            Response.Keywords = SearchService.SearchTags(Request.SearchString, Request.SkipCache);
		}
	}
}