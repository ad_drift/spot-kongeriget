﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Alinea.Samfundsfag.BusinessLogic.Services;
using System.Web.SessionState;

namespace Alinea.Samfundsfag.Web.services
{
    /// <summary>
    /// Summary description for Crawler
    /// </summary>
    public class Crawler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (CrawlerService.IsRequestCrawler())
            {
                CrawlerService.RunCrawler(CrawlerService.CrawlUrls());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}