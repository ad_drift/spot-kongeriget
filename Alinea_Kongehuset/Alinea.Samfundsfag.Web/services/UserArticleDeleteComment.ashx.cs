﻿using System.Web;
using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.services
{
	public class UserArticleDeleteComment : IHttpHandler, IReadOnlySessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var commentId = 0;
			int.TryParse(context.Request.QueryString["commentId"], out commentId);

			if (commentId > 0)
			{
				using (var uas = new UserArticleCommentService())
				{
					uas.DeleteComment(commentId, SessionController.CurrentUserId);
				}
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

		public bool IsReusable { get { return false; } }
	}
}