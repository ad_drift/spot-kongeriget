﻿using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Models.Json;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.Services.Handlers;
using CoreM41.Web.Services.Generic.Enums;

namespace Alinea.Samfundsfag.Web.services
{
	public class UserArticleNotifications : ServiceHandlerBase<UserArticleNotificationsRequest, UserArticleNotificationsResponse>, IReadOnlySessionState
	{
		public UserArticleNotifications()
		{
			RequestContentType = ContentType.QueryString;
			ResponseContentType = ContentType.Json;
			this.LowerCaseVariables = false;
		}

		public override void Process()
		{
			using (var uacs = new UserArticleCommentService())
			{
				using (var uas = new UserArticleService())
				{

					if (SessionController.HasUserClass)
					{
						Response.NewComments = uacs.GetUnreadCommentsCount(SessionController.CurrentUserClass.ClassId, SessionController.CurrentUserId);

						if (SessionController.IsCurrentUserTeacher)
							Response.NewArticles = uas.GetUnseenArticlesCount(SessionController.CurrentUserClass.ClassId, SessionController.CurrentUserId);
					}
				}
			}
		}
	}
}