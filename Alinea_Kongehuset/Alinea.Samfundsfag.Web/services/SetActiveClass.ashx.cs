﻿using System.Web;
using System.Web.SessionState;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.services
{
	public class SetActiveClass : IHttpHandler, IRequiresSessionState
	{
		public void ProcessRequest(HttpContext context)
		{
			var classId = 0;
			int.TryParse(context.Request.QueryString["id"], out classId);

			using (var scs = new SchoolClassService())
			{
				SessionController.SetActiveUserClass(scs.GetClass(classId));
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

		public bool IsReusable { get { return false; } }
	}
}