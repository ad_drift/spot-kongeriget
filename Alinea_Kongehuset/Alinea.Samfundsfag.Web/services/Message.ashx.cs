﻿using Alinea.Samfundsfag.BusinessLogic.Models.Json;
using Alinea.Samfundsfag.BusinessLogic.Services.Handlers;
using CoreM41.Web.Services.Generic.Enums;

namespace Alinea.Samfundsfag.Web.services
{
	public class Message : ServiceHandlerBase<MessageRequest, MessageResponse>
	{
		public Message()
		{
			RequestContentType = ContentType.QueryString;
			ResponseContentType = ContentType.Json;
			this.LowerCaseVariables = false;
		}

		public override void Process()
		{
			Response.UnreadMessages = BoardMessageServiceInstance.MessageRead(Request.MessageId, Request.UserId, Request.ClassId);
		}
	}
}