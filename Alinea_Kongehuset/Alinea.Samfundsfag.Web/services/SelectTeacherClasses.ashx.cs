﻿using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;

namespace Alinea.Samfundsfag.Web.services
{
	public class SelectTeacherClasses : IHttpHandler, IRequiresSessionState
	{
		public bool IsReusable { get { return false; } }

		public void ProcessRequest(HttpContext context)
		{
			if (SessionController.IsCurrentUserTeacher)
			{
				var classIds = new List<int>();
				foreach (var strId in (context.Request.QueryString["classIds"] ?? "").Split(','))
				{
					var id = 0;
					if (int.TryParse(strId, out id))
					{
						classIds.Add(id);
					}
				}

				using (var scs = new TeacherClassService())
				{
					classIds.RemoveAll(id => id == 0);
					scs.SetSelectedClassIds(SessionController.CurrentUserId, classIds);
				}
			}

			var returnUrl = (!string.IsNullOrEmpty(context.Request.QueryString["returnUrl"]) ? context.Request.QueryString["returnUrl"] : context.Request.UrlReferrer.PathAndQuery);
			context.Response.Redirect(returnUrl);
		}

	}
}