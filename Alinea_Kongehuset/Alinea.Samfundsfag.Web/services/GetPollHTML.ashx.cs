﻿using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Utilities;
using Futuristic.Umbraco;
using System;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Xml.Linq;

namespace Alinea.Samfundsfag.Web.services
{
	public class GetPollHTML : IHttpHandler, IRequiresSessionState
	{
		public bool IsReusable { get { return false; } }

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			context.Response.AppendHeader("Cache-Control", "no-cache");
			
			var xml = XElement.Parse(context.Request.Form["xml"]);

			var pollId = xml.Attribute("id").Value;

            var quizBool = Int32.Parse(context.Request["quizBool"]);

            var correctAnswer = context.Request["correctAnswer"];

			var hasAnswered = XsltHelper.HasUserAlreadyAnswered(pollId) == "true";
			var answerId = XsltHelper.GetUserAnsweredInputId(pollId);
				
			var html = new StringBuilder();

            if (quizBool == 1)
                html.AppendLine(@"<ul class=""options quiz"">");
            else
                html.AppendLine(@"<ul class=""options"">");
            

			foreach (var answer in xml.Descendants("Answer"))
			{
                var localAnswerId = answer.Attribute("id").Value;
                if (correctAnswer == localAnswerId)
                {
                    html.AppendLine(@"<li class=""correctAnswer"">");
                }
                else {
                    html.AppendLine(@"<li>");
                }
                
				if (!hasAnswered)
					html.AppendLine(string.Format(@"<input type=""radio"" name=""AnswerId"" id=""answer{0}"" value=""{0}"" />", answer.Attribute("id").Value));
				else
					html.AppendLine(string.Format(@"<input type=""radio"" name=""AnswerId"" id=""answer{0}"" value=""{0}"" disabled=""disabled"" />", answer.Attribute("id").Value));

				var labelClass = (hasAnswered ? "disabled" : string.Empty) + (answer.Attribute("id").Value == answerId ? " checked" : "");
				html.AppendLine(string.Format(@"<label for=""answer{0}"" class=""{1}"">{2}</label>", answer.Attribute("id").Value, labelClass, answer.Attribute("nodeName").Value));

				if (hasAnswered)
				{
					var percent = XsltHelper.GetAnsweredInputPercent(pollId, answer.Attribute("id").Value);
					html.AppendLine(string.Format(@"<span class=""bar"" style=""width:50%;"" title=""{0}%"">", percent));
					html.AppendLine(string.Format(@"<span class=""val"" style=""width:{0}%""></span>", percent));
					html.AppendLine(@"</span>");
				}
				html.AppendLine(@"</li>");
			}

			html.AppendLine(@"</ul>");

			if (!hasAnswered)
			{
				html.AppendLine(string.Format(@"<a href=""#answer"" class=""button discreet"" data-service=""/services/polls.ashx"" data-parameters=""{{Id:'{0}'}}"">Stem</a>", pollId));
			}

			context.Response.Write(html.ToString());
		}
	}
}