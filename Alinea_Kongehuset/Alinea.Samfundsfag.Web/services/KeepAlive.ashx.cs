﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alinea.Samfundsfag.Web.services
{
	public class KeepAlive : IHttpHandler
	{
		public bool IsReusable { get { return false; } }

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			context.Response.Write("You are still alive!");
		}
	}
}