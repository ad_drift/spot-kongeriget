﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.UI;
using CoreM41.Diagnostics.Logging;
using Alinea.Samfundsfag.BusinessLogic.Controller;

namespace Alinea.Samfundsfag.Web.usercontrols
{
    public partial class ShowMessages : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsUserLoggedIn && SessionController.HasUserClass)
            {
                if (!IsPostBack)
                {
                    DataBind();
                }
            }
        }

		public override void DataBind()
		{
			rptMessages.DataSource = BoardMessageService.GetStudentsBoardMessages(CurrentClassId, CurrentUserId);
			rptMessages.DataBind();

			if (IsUserLoggedIn && SessionController.HasUserClass)
			{
				hdnClassId.Value = CurrentClassId.ToString();
				hdnUserId.Value = CurrentUserId.ToString();
			}
		}
    }
}