﻿using System.Web;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Common.Enums;
using Alinea.Samfundsfag.BusinessLogic.UI;
using Alinea.Samfundsfag.BusinessLogic.Utilities;
using CoreM41.Web.Utilities;
using System;
using CoreM41.Common.Enums;
using CoreM41.Common;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class SearchBox : UserControlBase
	{
		private static object cacheLock = new object();

		protected string Search
		{
			get
			{
				return UmbracoHelper.GetDictionaryValue(Constants.DictionaryKeys.Search);
			}
		}

		protected string SearchUrl
		{
			get
			{
                if (IsTeacherPage)
                {
                    return UmbracoHelper.TeacherSearchPage.NiceUrl;
                }
                else
                {				
                    return UmbracoHelper.StudentSearchPage.NiceUrl;
                }
			}
		}

		protected string SearchString
		{
			get
			{
				return HttpUtility.HtmlEncode(PageUtility.GetQueryStringVariable<QueryStringVariables>(QueryStringVariables.SearchString));
			}
		}

        protected void Page_Load(object sender, EventArgs e)
        {
			if (IsTeacherPage || (!IsTeacherPage && !IsStudentPage && !XsltHelper.IsLoggedInUserStudentOrTeacherAsStudent()))
			{
				this.Visible = false;
			}

            if (!IsPostBack)
            {
                txtSearch.Text = HttpUtility.HtmlDecode(SearchString);
                txtSearch.Attributes.Add(Const.Html.Placeholder, Search);
                btnSearch.Text = Search; 
            }
        }

        protected void OnBtnSearchClick(object sender, EventArgs e)
        {
            RedirectFormat("{0}?{1}={2}", SearchUrl, QueryStringVariables.SearchString.GetAttribute<QueryStringAttribute>().ShortQueryStringName, HttpUtility.UrlEncode(txtSearch.Text));
        }
	}
}