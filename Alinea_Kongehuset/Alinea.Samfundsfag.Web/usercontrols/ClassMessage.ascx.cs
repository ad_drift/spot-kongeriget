﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.UI;
using CoreM41.Common;
using Models = Alinea.Samfundsfag.BusinessLogic.Models;
using System.Web;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.usercontrols
{
    public partial class ClassMessage : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitiateControls();
            }
            ResetControls();
        }

        protected void OnRptControlMessagesItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case Const.Commands.Delete:
                    BoardMessageService.DeleteMessage(e.CommandArgument.ToString());
                    RenderMessages();
                    break;
                case Const.Commands.Edit:
                    int messageId;
                    int.TryParse(e.CommandArgument.ToString(), out messageId);
                    if (messageId > 0)
                    {
                        BoardMessage boardMessage = BoardMessageService.GetBoardMessage(messageId);
                        //check check boxes
                        foreach (RepeaterItem item in rptClasses.Items)
                        {
                            CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                            HiddenField hdnClassId = item.FindControl("hdnClassId") as HiddenField;
                            
                            int classId = 0;
                            int.TryParse(hdnClassId.Value, out classId);
                            if (classId > 0)
                            {
                                BoardMessageClass boardMessageClass = BoardMessageService.GetBoardMessageClass(classId, boardMessage.BoardMessageId);
								if (boardMessage.BoardMessageClasses.Contains(boardMessageClass))
								{
									cbClass.Checked = true;
								}
								else
								{
									cbClass.Checked = false;
								}
                            }
                        }
                        //set controls values
                        txtMessage.Text = boardMessage.Text;
                        ddlStatus.SelectedIndex = Convert.ToInt32(boardMessage.IsPublished);
                        hdnMessageId.Value = boardMessage.BoardMessageId.ToString();
                        lblCreateMessageHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.EditMessageTitle).Value;

						if (boardMessage.MediaFileId.HasValue)
						{
							Models.BoardMessage boardMessageModel = BoardMessageService.GetBoardMessageModel(boardMessage.BoardMessageId, CurrentUserId);
							lblFileName.Text = boardMessageModel.DownloadName;
							hdnFileId.Value = boardMessage.MediaFileId.ToString();
							pnlFileExists.Visible = true;
						}
						else
						{
							lblFileName.Text = "";
							hdnFileId.Value = "";
							pnlFileExists.Visible = false;
						}

                        //set id
                        hdnMessageId.Value = e.CommandArgument.ToString();
						pnlNewMessage.Visible = true;
						lnkNewMessage.Visible = false;
                    }
                    break;
            }
        }

        protected void OnBtnSubmitClick(object sender, EventArgs e)
        {
			Page.Validate();
            if (Page.IsValid)
            {
                int boardMessageId = 0;
                int.TryParse(hdnMessageId.Value, out boardMessageId);

                bool validRadioButtons = false;
                List<int> selectedClasses = new List<int>();
                foreach (RepeaterItem item in rptClasses.Items)
                {
                    CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                    HiddenField hdnClassId = item.FindControl("hdnClassId") as HiddenField;
                    if (cbClass.Checked)
                    {
                        int classId = 0;
                        int.TryParse(hdnClassId.Value, out classId);
                        if (classId > 0)
                        {
                            selectedClasses.Add(classId);
                            validRadioButtons = true;
                        }
                    }
                }
                if (validRadioButtons)
                {
                    bool isPublished = ddlStatus.SelectedValue.Equals("1");

					int fileId = 0;
					int.TryParse(hdnFileId.Value, out fileId);

                    if (cbDeleteFile.Checked)
                    {
                        if (fileId > 0)
                        {
                            BoardMessageService.DeleteFile(fileId, boardMessageId);
                        }
                    }

					BoardMessageService.DeleteMessage(Convert.ToString(boardMessageId), false);
                    boardMessageId = BoardMessageService.UpsertBoardMessage(null, selectedClasses, txtMessage.Text, isPublished, fupFile);

					if (fileId > 0 && !cbDeleteFile.Checked && !fupFile.HasFile)
	                {
						BoardMessageService.RelateBoardMessageToFile(boardMessageId, fileId);
	                }

                    lblCreateMessageFeedback.Visible = true;
                    lblCreateMessageFeedback.CssClass += " success";
					lblCreateMessageFeedback.Text = "Din besked er nu gemt.";

                    RenderMessages();
					ResetData();
					lnkNewMessage.Visible = true;
                    pnlNewMessage.Visible = false;
                }
                else
                {
                    lblRadioButtonFeedback.Visible = true;
                    return;
                }
            }
        }

        protected void OnLnkNewMessageClick(object sender, EventArgs e)
        {
            ResetData();
            lblCreateMessageHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.CreateMessageTitle).Value;
            pnlNewMessage.Visible = true;
			lnkNewMessage.Visible = false;
        }

        private void ResetData()
        {
            pnlFileExists.Visible = false;
            txtMessage.Text = string.Empty;
            ddlStatus.SelectedIndex = 0;
            hdnMessageId.Value = null;
            foreach (RepeaterItem item in rptClasses.Items)
            {
                CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                cbClass.Checked = false;
            }
            lblCreateMessageHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.CreateMessageTitle).Value;
	        hdnFileId.Value = null;
	        cbDeleteFile.Checked = false;
        }

        private void InitiateControls()
        {
            RenderMessages();

			var classes = SchoolClassService.GetClassesForUser(SessionController.CurrentUserId).ToList();
			using (var tcs = new TeacherClassService())
			{
				classes.RemoveAll(c => !tcs.GetSelectedClassIds(SessionController.CurrentUserId).Contains(c.ClassId));
			}

			rptClasses.DataSource = classes;
            rptClasses.DataBind();

            lblCreateMessageHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.CreateMessageTitle).Value;
            lblCreateMessageFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.SuccesMessage).Value;
            lblRadioButtonFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.SelectClassError).Value;
            rfvMessage.ErrorMessage = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.MessageError).Value;
        }

        private void RenderMessages()
        {
            List<Models.BoardMessage> boardMessages = BoardMessageService.GetTeachersBoardMessages(SessionController.CurrentUserId);

            rptControlMessages.DataSource = boardMessages;
            rptControlMessages.DataBind();
            rptViewMessages.DataSource = boardMessages;
            rptViewMessages.DataBind();
        }

        private void ResetControls()
        {
            lblRadioButtonFeedback.Visible = false;
            lblCreateMessageFeedback.Visible = false;
        }
    }
}