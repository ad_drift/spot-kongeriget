﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchBox.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.SearchBox" %>

<asp:Panel DefaultButton="btnSearch" runat="server">

	<div class="search-form" data-serviceurl="<%= ResolveUrl("~/services/tags.ashx") %>" data-servicedata="{id:''}">
		<asp:TextBox ID="txtSearch" runat="server" type="text" />
		<asp:Button ID="btnSearch" runat="server" UseSubmitBehavior="true" OnClick="OnBtnSearchClick" />
	</div>

</asp:Panel>