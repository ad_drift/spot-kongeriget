﻿using Alinea.Samfundsfag.BusinessLogic.UI;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	/// <summary>
	/// Macro/user control to check for login on the various pages that requires login.
	/// Simply insert the macro and set the RequiredUserAccess property.
	/// </summary>
	public partial class RequiresLogin : UserControlBaseRequiresUniLogin
	{
	}
}