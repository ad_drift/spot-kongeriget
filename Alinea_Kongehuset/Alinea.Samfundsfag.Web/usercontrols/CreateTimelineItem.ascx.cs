﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Models;
using umbraco;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class CreateTimelineItem : System.Web.UI.UserControl
	{
		public List<UserArticleTimelineItem> TimelineItems { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			RepeaterTimelineItems.DataSource = TimelineItems;
			RepeaterTimelineItems.DataBind();

			if (Page.IsPostBack)
			{
				
			}
		}
	}
}