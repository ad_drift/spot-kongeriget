﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResult.ascx.cs"
	Inherits="Alinea.Samfundsfag.Web.usercontrols.SearchResult" %>
<article class="content wrapper search-result" role="main">
	<header class="entry">
		<div class="wrapper">
			<div class="grid-2">
				<h1>
					<%= Title %>
				</h1>
			</div>
		</div>
	</header>

	<div class="entry grid-3">
		<h4>
			<asp:Literal ID="litYouSearchedFor" runat="server" />
			&nbsp;
			<span>
				<asp:Literal ID="litSearchString" runat="server" />
			</span>
		</h4>

    <asp:Repeater ID="rptSearchResults" runat="server">
    <HeaderTemplate>
      <ul>
    </HeaderTemplate>
    <ItemTemplate>
			<li>
				<div class="search entry">
					<h5></h5>
					<div class="group">
						<h2>
							<%# Eval("Title") %>
							<span><%# Eval("NodeType") %></span>
						</h2>
						<div>
							<%# Eval("ContentAbstract") %>
							<a href="<%# Eval("Url") %>"><%# ReadMore %></a>
						</div>
					</div>
				</div>
			</li>    
    </ItemTemplate>
    <FooterTemplate>
      </ul>
    </FooterTemplate>
    </asp:Repeater>

    <asp:PlaceHolder ID="plhNoSearchResults" runat="server" Visible="false">
		<p>
		  <asp:Literal ID="litNoSearchResults" runat="server" />
		</p>
    </asp:PlaceHolder>

	<footer>
		<a href="javascript:history.back(-1)" class="button discreet">
			<asp:Literal ID="litBack" runat="server"/>
		</a>
	</footer>
	</div>
		
</article>
