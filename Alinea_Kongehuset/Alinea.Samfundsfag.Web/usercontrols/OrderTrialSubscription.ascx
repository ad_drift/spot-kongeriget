﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderTrialSubscription.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.OrderTrialSubscription" %>
<div class="entry form">
    <h2><asp:Literal runat="server" ID="litHeaderline" /></h2>
    <asp:PlaceHolder ID="plhFormFields" runat="server" Visible ="true">
        <p><asp:Literal runat="server" ID="litDescription" /></p>
      <span class="hr"></span>
      <style type="text/css">
        .entry table,
        .entry table tr,
        .entry table td {
          border: none;
          margin:0;
          padding:0 10px;
        }
        .uc {
          margin:10px 0;
        }
        table {
          width:100%;
        }
        .entry.form input {
          width:100%;
        }
      </style>
    
      <table>
        <tr>
          <td><asp:Label ID="lblName" runat="server" AssociatedControlID="txtName" Text="* Navn" /></td>
          <td><asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" Text="* Email:" /></td>
        </tr>
        <tr>
          <td><div class="input"><asp:TextBox ID="txtName" runat="server" /></div>
            <asp:RequiredFieldValidator ID="valReqTxtName" runat="server" ControlToValidate="txtName" ValidationGroup="OrderTrialSub" ErrorMessage="Navn skal udfyldes" Display="None" />
              </td>
          <td><div class="input"><asp:TextBox ID="txtEmail" runat="server" /></div>
            <asp:RegularExpressionValidator  ID="valEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="OrderTrialSub" ErrorMessage="Email ikke korrekt" Display="None" />
            <asp:RequiredFieldValidator ID="valReqTxtEmail" runat="server" ControlToValidate="txtEmail" ValidationGroup="OrderTrialSub" ErrorMessage="Email skal udfyldes" Display="None" />
          </td>
        </tr>
        <tr>
          <td><asp:Label ID="lblInstitutionName" runat="server" AssociatedControlID="txtInstitutionName" Text="* Institutionsnavn:" />
          </td>
          <td><asp:Label ID="lblInstitutionNumber" runat="server" AssociatedControlID="txtInstitutionNumber" Text="Institutionsnr:" /></td>
        </tr>
        <tr>
          <td><div class="input"><asp:TextBox ID="txtInstitutionName" runat="server" /></div>
              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInstitutionName" ValidationGroup="OrderTrialSub" ErrorMessage="Institutionsnavn skal udfyldes" Display="None" />
          </td>
          <td><div class="input"><asp:TextBox ID="txtInstitutionNumber" runat="server" /></div></td>
        </tr>
        <tr>
          <td><asp:Label ID="lblClassList" runat="server" AssociatedControlID="txtClassList" Text="Klasser der ønskes åbent for:" /></td>
          <td></td>
        </tr>
        <tr>
          <td><div class="input"><asp:TextBox ID="txtClassList" runat="server" /></div></td>
          <td></td>
        </tr>
        <tr>
          <td>
              <asp:LinkButton ID="btnsubmit" runat="server" CssClass="button uc" Text="Bestil prøveabonnement" OnClick="btnsubmit_Click" ValidationGroup="OrderTrialSub" />
          </td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2">* Skal udfyldes </td>
            <asp:ValidationSummary runat="server" ID="valSummary" ValidationGroup="OrderTrialSub" />
        </tr>
      </table>
    </asp:PlaceHolder>
    <asp:Panel ID="pnlSuccesMessage" runat="server" CssClass="message" Visible ="false">
        <p><asp:Literal runat="server" ID="litSuccessMessage" /></p>
        <a href="" onclick="window.parent.jQuery.fancybox.close();">Luk bestilling</a>
    </asp:Panel>
    <asp:Panel ID="pnlErrorMessage" runat="server" CssClass="message error" Visible ="false">
        <asp:Literal runat="server" ID="litErrorMessage" />
    </asp:Panel>
    </div>