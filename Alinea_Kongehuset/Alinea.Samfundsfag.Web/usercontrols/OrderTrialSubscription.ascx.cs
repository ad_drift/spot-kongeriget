﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using Alinea.Samfundsfag.BusinessLogic.Utilities;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class OrderTrialSubscription : System.Web.UI.UserControl
	{
		public string RecipientEmail
		{
			get { return (string)ViewState["RecipientEmail"] ?? string.Empty; }
			set { ViewState["RecipientEmail"] = value; }
		}

		public string EmailSubject
		{
			get { return (string)ViewState["EmailSubject"] ?? string.Empty; }
			set { ViewState["EmailSubject"] = value; }
		}

		public string EmailBody
		{
			get { return (string)ViewState["EmailBody"] ?? string.Empty; }
			set { ViewState["EmailBody"] = value; }
		}

		public string Headline { get { return litHeaderline.Text; } set { litHeaderline.Text = value; } }

		public string Description { get { return litDescription.Text; } set { litDescription.Text = value; } }

		public string SuccessMessage
		{
			get { return (string)ViewState["SuccessMessage"] ?? string.Empty; }
			set { ViewState["SuccessMessage"] = value; }
		}

		public string ErrorMessage
		{
			get { return (string)ViewState["ErrorMessage"] ?? string.Empty; }
			set { ViewState["ErrorMessage"] = value; }
		}

		protected override void OnInit(EventArgs e)
		{
			valEmail.ValidationExpression = EmailHelper.EmailReqExValidation;

			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnsubmit_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			string name = txtName.Text;
			string email = txtEmail.Text;
			string institutionName = txtInstitutionName.Text;
			string institutionNumber = txtInstitutionNumber.Text;
			string classList = txtClassList.Text;

			Dictionary<string, string> values = new Dictionary<string, string>();
			values.Add("name", name);
			values.Add("email", email);
			values.Add("institutionName", institutionName);
			values.Add("institutionNumber", institutionNumber);
			values.Add("classList", classList);

			//// The template fields
			//// ##name##
			//// ##email##
			//// ##institutionName##
			//// ##institutionNumber##
			//// ##classList##

			if (!EmailHelper.SendMail(RecipientEmail, email, EmailSubject, EmailBody, values))
			{
				// Error
				pnlSuccesMessage.Visible = false;
				pnlErrorMessage.Visible = true;
				litErrorMessage.Text = ErrorMessage;
				return;
			}

			// Succes
			pnlErrorMessage.Visible = false;
			pnlSuccesMessage.Visible = true;
			litSuccessMessage.Text = SuccessMessage;
			plhFormFields.Visible = false;

			// Empty fields
			txtName.Text = string.Empty;
			txtEmail.Text = string.Empty;
			txtClassList.Text = string.Empty;
			txtInstitutionNumber.Text = string.Empty;
			txtInstitutionName.Text = string.Empty;
		}
	}
}