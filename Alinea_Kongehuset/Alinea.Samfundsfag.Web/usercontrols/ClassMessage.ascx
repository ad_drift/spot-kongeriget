﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassMessage.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.ClassMessage" %>

	<div class="content wrapper" role="main">
		<section class="entry form">
			<div class="heading">
				<h1>Beskeder til klassen</h1>
			</div>
			<h2>Oversigt over oprettede beskeder</h2>
			<table class="edit">
				<thead>
					<tr>
						<th>Besked</th>
						<th>Dato</th>
						<th>Klasse</th>
						<th>Status</th>
						<th>Rediger</th>
						<th>Preview</th>
						<th>Slet</th>
					</tr>
				</thead>
				<tbody>
					<asp:Repeater ID="rptControlMessages" OnItemCommand="OnRptControlMessagesItemCommand" runat="server">
						<ItemTemplate>
							<tr>
								<td><%# Eval("Preview") %>&hellip;</td>
								<td><time><%# Eval("Date") %></time></td>
								<td><%# Eval("ClassNames") %></td>
								<td><%# Eval("Status") %></td>
								<td><asp:LinkButton CommandName="Edit" CommandArgument='<%# Eval("BoardMessageId") %>' CssClass="edit" ID="lnbEdit" runat="server">Rediger</asp:LinkButton></td>
								<td><a href="#preview-<%# Eval("BoardMessageId") %>" class="preview">Preview</a></td>
								<td><asp:LinkButton CommandName="Delete" CommandArgument='<%# Eval("BoardMessageId") %>' CssClass="delete alert" data-alert="Er du sikker på at du vil slette denne besked?" ID="lnbDelete" runat="server">Slet?</asp:LinkButton></td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</tbody>
			</table>
			<asp:LinkButton ID="lnkNewMessage" CssClass="add button significant" Text="Skriv besked" OnClick="OnLnkNewMessageClick" runat="server" />
			<section class="form-preview">
				<asp:Repeater ID="rptViewMessages" runat="server">
					<ItemTemplate>
						<section class="messages form-example" id="preview-<%# Eval("BoardMessageId") %>">
							<article>
								<header class="vcard">
									<time><%# Eval("Date") %></time>
									af
									<cite><%# Eval("TeacherName") %></cite>
								</header>
								<div class="entry">
									<%# Eval("Text") %>
								</div>
								<asp:Panel ID="pnlDownload" Visible='<%# Eval("DownloadAvailable") %>' runat="server">
									<footer><strong>Download</strong>
										<ul>
											<li><a target="_blank" href="<%# Eval("DownloadLink") %>"><%# Eval("DownloadName") %></a></li>
										</ul>
									</footer>
								</asp:Panel>
							</article>
						</section>
					</ItemTemplate>
				</asp:Repeater>
			</section>
			<asp:HiddenField ID="hdnMessageId" runat="server" />
			<asp:Panel ID="pnlNewMessage" Visible="false" runat="server">
				<section id="form-message" class="form-edit">
				<h2 class="grid-1">
					<asp:Label ID="lblCreateMessageHeadline" runat="server"/>
				</h2>
				<div class="grid-2">
					<fieldset class="checkbox">
						<legend>1) Vælg klasse</legend>
						<ul class="class-selector">
							<asp:Repeater ID="rptClasses" runat="server">
								<ItemTemplate>
									<li>
										<asp:CheckBox ID="cbClass" runat="server" />
										<label for="<%# ((RepeaterItem)Container).FindControl("cbClass").ClientID %>"><%# Eval("Name")%></label>
										<asp:HiddenField ID="hdnClassId" Value='<%# Eval("ClassId") %>' runat="server" />
									</li>
								</ItemTemplate>
							</asp:Repeater>
						</ul>
						<asp:label ID="lblRadioButtonFeedback" CssClass="feedback error" runat="server" />
						<div class="class-picker-error feedback error" style="display:none;"><%=lblRadioButtonFeedback.Text %></div>
					</fieldset>
					<fieldset class="class-message">
						<legend>2) Skriv besked</legend>
						<asp:TextBox ID="txtMessage" Placeholder="Skriv din besked her..." TextMode="MultiLine" ValidationGroup="form" runat="server"></asp:TextBox>
						<asp:RequiredFieldValidator ID="rfvMessage" EnableClientScript="false" CssClass="feedback error" Display="Dynamic" ControlToValidate="txtMessage" ValidationGroup="form" runat="server"/>
						<div class="class-message-error feedback error" style="display:none;"><%=rfvMessage.ErrorMessage %></div>
					</fieldset>
					<fieldset>
						<legend>3) Vedhæft en fil</legend>
						<asp:FileUpload ID="fupFile" runat="server" />
						<asp:Panel Visible="false" ID="pnlFileExists" runat="server">
							Eksisterende: <asp:Label ID="lblFileName" Text="Lalalala.doc" runat="server" />
							<br />
							<asp:Label ID="lblDeleteFile" Text="Slet?" AssociatedControlID="cbDeleteFile" runat="server"/>
							<asp:CheckBox ID="cbDeleteFile" runat="server" />
							<asp:HiddenField ID="hdnFileId" runat="server" />
						</asp:Panel>
					</fieldset>
					<fieldset>
						<legend>4) Vælg status</legend>
						<asp:DropDownList ID="ddlStatus" DataTextField="" DataValueField="" runat="server">
							<asp:ListItem Text="Under udarbejdelse" Value="0"/>
							<asp:ListItem Text="Publiceret" Value="1"/>
						</asp:DropDownList>
					</fieldset>
					<asp:Button OnClick="OnBtnSubmitClick" ID="btnSubmit" CssClass="button significant" ValidationGroup="form" Text="Gem besked" runat="server" OnClientClick="return ValidateForm();" />
				</div>
			</section>
			<script type="text/javascript">
				$(function ()
				{
					$('html, body').animate({
						scrollTop: $('#form-message').offset().top
					}, 0);
				});
			</script>
			</asp:Panel>
			<asp:Label ID="lblCreateMessageFeedback" CssClass="feedback" runat="server"/>
			<footer>
				<a href="#back" class="button discreet back">Tilbage</a>
			</footer>
		</section>
	</div>

<script type="text/javascript">
	function ValidateForm()
	{
		var valid = true;
		if ($('.class-selector input:checked').length == 0)
		{
			valid = false;
			$('.class-picker-error').show();
		}
		else
			$('.class-picker-error').hide();

		if ($('.class-message textarea').val() == '')
		{
			valid = false;
			$('.class-message-error').show();
		}
		else
			$('.class-message-error').hide();

		return valid;
	}
</script>
