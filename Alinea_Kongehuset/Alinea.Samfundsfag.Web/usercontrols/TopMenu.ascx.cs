﻿using System;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.UI;
using Alinea.Samfundsfag.BusinessLogic.Utilities;
using CoreM41.Diagnostics.Logging;
using Futuristic.Umbraco;
using System.Linq;
using umbraco.NodeFactory;
using Alinea.Core.UniLogin;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class TopMenu : UserControlBase
	{
		protected string linkMyPage = "#";
		protected string linkMyPageText = "";

		protected string linkArticleList = "#";
		protected string linkArticleListText = "";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				InitiateItems();
			}
			IsLoggedIn();
		}

		protected void OnDdlTeacherLoginAsSelectedIndexChanged(object sender, EventArgs e)
		{
			DropDownList ddlTeacherLoginAs = sender as DropDownList;
			int classId = 0;

			if (int.TryParse(ddlTeacherLoginAs.SelectedValue, out classId))
			{
				SessionController.SetActiveUserClass(SchoolClassService.GetClass(classId));
				Response.Redirect(UmbracoHelper.StudentPage.NiceUrl);
			}
		}

		protected void OnDdlTeacherLoginAsDataBound(object sender, EventArgs e)
		{
			(sender as DropDownList).Items.Insert(0, new ListItem("Vælg...", "0"));
		}

		protected void OnLnbTeacherSectionClick(object sender, EventArgs e)
		{
			//remove class - redirect
			SessionController.SetActiveUserClass(null);
			Response.Redirect(UmbracoHelper.TeacherPage.NiceUrl);
		}

		private void IsLoggedIn()
		{
			//check if user class exists or teacher is logged in
			//set menu
			if (IsUserLoggedIn ) // todo: mini, delete this SSO bypass.
			{
				HideDefaultMenu();

				if (!UniLoginController.AccessDenied) // todo: mini delete this SSO bypass.
				{
					if (HasUserClass)
					{
						SetStudentMenu(0);
						if (IsUserLoggedInTeacher)
						{
							lnbTeacherSection.Visible = true;
						}
					}
					else if (IsUserLoggedInTeacher)
					{
						SetTeacherMenu();
					}
				}
			}
		}
		private void SetTeacherMenu()
		{
			mcrSearchBox.Visible = true;

			if (!IsPostBack && SessionController.CurrentUserId != 0)
			{
				var allClasses = SchoolClassService.GetClassesForUser(SessionController.CurrentUserId).ToList();
				var selectedClasses = (from c in allClasses select c).ToList();

				using (var tcs = new TeacherClassService())
				{
					selectedClasses.RemoveAll(c => !tcs.GetSelectedClassIds(SessionController.CurrentUserId).Contains(c.ClassId));
				}

				if (selectedClasses.Any() && Request.QueryString["changeClasses"] != "true")
				{
					ddlTeacherLoginAs.DataSource = selectedClasses;
					ddlTeacherLoginAs.DataBind();

					lblTeacherLoginAs.Text = Constants.Teacher.SeeSiteAsStudent;
					lblTeacherLoginAs.Visible = true;
					ddlTeacherLoginAs.Visible = true;
				}
				else
				{
					ddlSelectTeacherClasses.DataSource = allClasses;
					ddlSelectTeacherClasses.DataBind();

					foreach (ListItem item in ddlSelectTeacherClasses.Items)
					{
						if (selectedClasses.Exists(c => c.ClassId == Convert.ToInt32(item.Value)))
						{
							item.Attributes["data-selected"] = "true";
						}
					}

					lblSelectTeacherClasses.Text = Constants.Teacher.SelectTeacherClasses;
					lblSelectTeacherClasses.Visible = true;
					ddlSelectTeacherClasses.Visible = true;

					if (Request.QueryString["changeClasses"] == "true")
					{
						phAutoOpenSelectTeacherClases.Visible = true;
					}
				}			
			}

			if (Request.Url.AbsolutePath.Equals("/"))
				lnkTeacherTopics.Visible = true;
		}

		private void SetStudentMenu(int depth)
		{
			try
			{
				/*using (var service = new BoardMessageService())
				{
					int numberOfBoardMessages = service.CountGetStudentsBoardMessages(CurrentClassId);
					litStudentMessages.Text = numberOfBoardMessages.ToStringInvariant();
				}*/

				mcrSearchBox.Visible = true;

				lnkMessages.Visible = true;


				var myPageTarget = null as Node;
				if (SessionController.IsCurrentUserTeacher)
					myPageTarget = NodeHelper.GetNodeByAlias(Constants.NodeDocumentTypes.UserArticleList);
				else
					myPageTarget = NodeHelper.GetNodeByAlias(Constants.NodeDocumentTypes.UserArticleListMyPage);

				var linkArticleListTarget = NodeHelper.GetNodeByAlias(Constants.NodeDocumentTypes.UserArticleList);

				if (myPageTarget != null && linkArticleListTarget != null)
				{
					linkMyPageText = myPageTarget.GetNavigationName();
					linkMyPage = myPageTarget.NiceUrl;

					linkArticleListText = linkArticleListTarget.GetNavigationName();
					linkArticleList = linkArticleListTarget.NiceUrl;

					phMyPage.Visible = true;
				}

				if (SessionController.IsCurrentUserTeacher)
				{
					ddlTeacherLoginAs.Visible = true;
					if (!IsPostBack && SessionController.CurrentUserId != 0)
					{
						var classes = SchoolClassService.GetClassesForUser(SessionController.CurrentUserId).ToList();						
						using (var tcs = new TeacherClassService())
						{
							classes.RemoveAll(c => !tcs.GetSelectedClassIds(SessionController.CurrentUserId).Contains(c.ClassId));
						}

						ddlTeacherLoginAs.DataSource = classes;
						ddlTeacherLoginAs.DataBind();
						ddlTeacherLoginAs.SelectedValue = Convert.ToString(SessionController.CurrentUserClass.ClassId);						
					}
				}

				if (Request.Url.AbsolutePath.Equals("/"))
					lnkStudentTopics.Visible = true;
				
				if (BoardMessageService.CountGetStudentsBoardMessages(CurrentClassId) == 0)
					lnkMessages.Visible = false;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				if (depth < 3)
				{
					SetStudentMenu(depth++);
				}
			}
		}

		private void HideDefaultMenu()
		{
			lnkLogin.Visible = false;
			lnbTeacherSection.Visible = false;
		}

		private void InitiateItems()
		{
			var newsNode = Node.GetNodeByXpath(string.Format("/{0}/{1}/{2}/{3} [@nodeName='{4}']",
															  Constants.NodeDocumentTypes.Root,
															  Constants.NodeDocumentTypes.Homepage,
															  Constants.NodeDocumentTypes.StudentMaster,
															  Constants.NodeDocumentTypes.SingleColumn,
															  Constants.NodeNames.News));

			lnkLogin.NavigateUrl = "/login";
		}
	}
}