﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTagManager.ascx.cs" Inherits="TagManagerDashBoard.UserControl.ucTagManager" %>
<%@ Register Namespace="umbraco.uicontrols" Assembly="controls" TagPrefix="umb" %>

<link href="/umbraco_client/propertypane/style.css" rel="stylesheet" />

<div class="dashboardWrapper">
    <asp:Label ID="error" runat="server" Visible="false" ForeColor="Red"></asp:Label>
    <br />
    
    <asp:PlaceHolder ID="NoResultPlaceHolder" runat="server">
        <p>No results found</p>
        <br />
        <asp:Label Text="Tag name" runat="server" AssociatedControlID="txtNewTag"></asp:Label>
        <asp:TextBox ID="txtNewTag" Text="" runat="server"></asp:TextBox>
        &nbsp;&nbsp;<asp:Label runat="server" Text="Group" AssociatedControlID="txtNewGroup"></asp:Label>
        <asp:TextBox ID="txtNewGroup" runat="server"></asp:TextBox>
        <br />
        <asp:Button ID="addNew" Text="Add new" runat="server" OnClick="AddNew_Click" />
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="ResultPlaceHolder" runat="server">
        <div class="propertyDiv">
			<asp:GridView ID="OverviewGrid" runat="server" AutoGenerateColumns="false" DataKeyNames="id" OnRowEditing="OverviewGrid_RowEditing"
				OnRowUpdating="OverviewGrid_RowUpdating" OnRowDeleting="OverviewGrid_RowDeleting"
				OnRowCommand="OverviewGrid_RowCommand" OnRowCancelingEdit="OverviewGrid_RowCancelingEdit"
				ShowFooter="true" CellPadding="4" ForeColor="#333333" GridLines="None">
				<AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
				<Columns>
					<asp:TemplateField>
						<HeaderTemplate>
							<h2>ID</h2>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblId" runat="server" Text='<%# Eval("Id")%>'></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:Label ID="lblEditId" runat="server" Text='<%# Eval("Id")%>'></asp:Label>
						</EditItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<HeaderTemplate>
							<h2>Tag</h2>
							<%--<asp:TextBox ID="txtNewTag" runat="server"></asp:TextBox>--%>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblTag" runat="server" Text='<%# Eval("TagCaption")%>'></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:HiddenField ID="hdnTag" runat="server" Value='<%# Eval("TagCaption")%>'></asp:HiddenField>
							<asp:TextBox ID="txtTag" runat="server" Text='<%# Eval("TagCaption")%>'></asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<HeaderTemplate>
							<h2>Group</h2>
							<%--<asp:DropDownList ID="ddlNewGroup" runat="server" OnLoad="ddlGroup_OnLoad"></asp:DropDownList>--%>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Label ID="lblGroup" runat="server" Text='<%# Eval("Group")%>'></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:HiddenField ID="hdnGroup" runat="server" Value='<%# Eval("Group")%>'></asp:HiddenField>
							<asp:DropDownList ID="ddlGroup" runat="server" OnLoad="ddlGroup_OnLoad">
							</asp:DropDownList>
						</EditItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<HeaderTemplate>
							<h2>&nbsp;</h2>
							<%--<asp:LinkButton ID="btnAddNew" runat="server" CausesValidation="False" CommandName="AddNew"
								Text="Add new"></asp:LinkButton>--%>
						</HeaderTemplate>
						<EditItemTemplate>
							<asp:LinkButton ID="btnUpdate" runat="server" CausesValidation="False" CommandName="Update"
								Text="Save"></asp:LinkButton>
							<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
								Text="Cancel"></asp:LinkButton>
						</EditItemTemplate>
						<ItemTemplate>
							<asp:LinkButton ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
								Text="Update"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField>
						<HeaderTemplate>
							<h2>&nbsp;</h2>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
								OnClientClick='<%# Eval("TagCaption", "return confirm(\"Delete the tag {0}?\");") %>'
								Text="Delete" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
				<EditRowStyle BackColor="#999999"></EditRowStyle>

				<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></FooterStyle>

				<HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"></HeaderStyle>

				<PagerStyle HorizontalAlign="Center" BackColor="#284775" ForeColor="White"></PagerStyle>

				<RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>

				<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>

				<SortedAscendingCellStyle BackColor="#E9E7E2"></SortedAscendingCellStyle>

				<SortedAscendingHeaderStyle BackColor="#506C8C"></SortedAscendingHeaderStyle>

				<SortedDescendingCellStyle BackColor="#FFFDF8"></SortedDescendingCellStyle>

				<SortedDescendingHeaderStyle BackColor="#6F8DAE"></SortedDescendingHeaderStyle>
			</asp:GridView>
        </div>
    </asp:PlaceHolder>
</div>