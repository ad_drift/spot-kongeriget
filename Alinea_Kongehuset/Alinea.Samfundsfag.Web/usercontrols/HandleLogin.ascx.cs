﻿using System;
using Alinea.Samfundsfag.BusinessLogic.UI;
using Alinea.Samfundsfag.BusinessLogic.Utilities;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class HandleLogin : UserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
        {
            //redirect to elev or teacher depending on who logged in
            if (IsUserLoggedIn)
            {
                if (IsUserLoggedInStudent)
                {
                    //response to student
                    Response.Redirect(UmbracoHelper.StudentPage.NiceUrl);
                }
                else if (IsUserLoggedInTeacher)
                {
                    //reponse to teacher
                    Response.Redirect(UmbracoHelper.TeacherPage.NiceUrl);
                }
            }
        }
	}
}