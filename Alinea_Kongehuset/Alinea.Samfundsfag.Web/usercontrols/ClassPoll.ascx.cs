﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Common;
using CoreM41.Web.UI;
using AlineaUI = Alinea.Samfundsfag.BusinessLogic.UI;
using CoreM41.Common;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.Web.usercontrols
{
    public partial class ClassPoll : AlineaUI.UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
            ResetControls();
        }

        private void ResetControls()
        {
            lblClassFeedback.Visible = false;
            lblTopicFeedback.Visible = false;
            lblCreatePollFeedback.Visible = false;
            lblAnswerFeedback.Visible = false;
			lblCorrectAnswerFeedback.Visible = false;
			lblHeadlineFeedback.Visible = false;
        }

        private void RemoveInputData()
        {
            foreach (RepeaterItem item in rptClasses.Items)
            {
                CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                cbClass.Checked = false;
            }
            foreach (RepeaterItem item in rptTopics.Items)
            {
                GroupRadioButton rbTopic = item.FindControl("rbTopic") as GroupRadioButton;
                rbTopic.Checked = false;
            }
            foreach (RepeaterItem item in rptAnswers.Items)
            {
                TextBox txtAnswer = item.FindControl("txtAnswer") as TextBox;
                txtAnswer.Text = string.Empty;
            }
            txtQuestion.Text = string.Empty;

            ddlStatus.SelectedIndex = 0;
        }

        private void Initialize()
        {
            rptControlPolls.DataSource = PollService.GetTeacherPolls(CurrentUserId);
            rptControlPolls.DataBind();

            rptViewPolls.DataSource = PollService.GetTeacherPolls(CurrentUserId);
            rptViewPolls.DataBind();
			
			var classes = SchoolClassService.GetClassesForUser(SessionController.CurrentUserId).ToList();
			using (var tcs = new TeacherClassService())
			{
				classes.RemoveAll(c => !tcs.GetSelectedClassIds(SessionController.CurrentUserId).Contains(c.ClassId));
			}

			rptClasses.DataSource = classes;
            rptClasses.DataBind();

            rptTopics.DataSource = PollService.GetPollTopics();
            rptTopics.DataBind();

            rptAnswers.DataSource = new int[]{0, 1, 2, 3};
            rptAnswers.DataBind();

            lblCreatePollHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.CreatePollTitle).Value;
            lblCreatePollFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.SuccesMessage).Value;
            
            lblClassFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.SelectClassError).Value;
            lblTopicFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.SelectTopicError).Value;
            rfvTxtQuestion.ErrorMessage = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.QuestionError).Value;
            lblAnswerFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.AnswerError).Value;
			lblCorrectAnswerFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.CorrectAnswerError).Value;

			lblHeadlineFeedback.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.HeadlineError).Value;
        }

        protected void OnBtnSubmitClick(object sender, EventArgs e)
        {
			Page.Validate();
            if (Page.IsValid)
            {
				bool isQuiz = cbQuiz.Checked;
				bool validHeadline = true;
				string headline = isQuiz && !string.IsNullOrWhiteSpace(txtHeadline.Text) ? txtHeadline.Text : null;
				if (isQuiz)
				{
					validHeadline = !string.IsNullOrWhiteSpace(headline);
				}

                bool validClassRadioButtons = false;
                List<int> selectedClasses = new List<int>();
                foreach (RepeaterItem item in rptClasses.Items)
                {
                    CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                    HiddenField hdnClassId = item.FindControl("hdnClassId") as HiddenField;
                    if (cbClass.Checked)
                    {
                        int classId = 0;
                        int.TryParse(hdnClassId.Value, out classId);
                        if (classId > 0)
                        {
                            selectedClasses.Add(classId);
                            validClassRadioButtons = true;
                        }
                    }
                }
                bool validTopicRadioButtons = false;
                int selectedTopic = 0;
                foreach (RepeaterItem item in rptTopics.Items)
                {
                    GroupRadioButton rbTopic = item.FindControl("rbTopic") as GroupRadioButton;
                    HiddenField hdnTopicId = item.FindControl("hdnTopicId") as HiddenField;
                    if (rbTopic.Checked)
                    {
                        int topicId = 0;
                        int.TryParse(hdnTopicId.Value, out topicId);
                        if (topicId > 0)
                        {
                            selectedTopic = topicId;
                            validTopicRadioButtons = true;
                        }
                    }
                }

                bool validAnswers = false;
				List<Tuple<int, string, bool>> answers = new List<Tuple<int, string, bool>>();
                foreach (RepeaterItem item in rptAnswers.Items)
                {
                    TextBox txtAnswer = item.FindControl("txtAnswer") as TextBox;
                    HiddenField hdnAnswerId = item.FindControl("hdnAnswerId") as HiddenField;
					GroupRadioButton rbCorrectAnswer = item.FindControl("rbCorrectAnswer") as GroupRadioButton;

                    if (txtAnswer.Text != string.Empty)
                    {
                        int answerId = 0;
                        if (hdnAnswerId.Value != string.Empty)
                        {
                            int.TryParse(hdnAnswerId.Value, out answerId);
                        }
						bool isCorrect = false;
						if (isQuiz)
						{
							isCorrect = rbCorrectAnswer.Checked;
						}
						Tuple<int, string, bool> answerTuple = new Tuple<int, string, bool>(answerId, txtAnswer.Text, isCorrect);
                        answers.Add(answerTuple);
                    }
                }
                validAnswers = answers.Count >= 2;
				bool validCorrectAnswers = true;
				if (isQuiz)
				{
					validCorrectAnswers = answers.Any(c => c.Item3);
				}

				if (validHeadline && validClassRadioButtons && validTopicRadioButtons && validAnswers && validCorrectAnswers)
                {
                    int pollId = 0;
                    int.TryParse(hdnPollId.Value, out pollId);

                    bool isPublished = ddlStatus.SelectedValue.Equals("1");

                    PollService.UpsertPoll(pollId, selectedClasses, selectedTopic, txtQuestion.Text, answers, isPublished, isQuiz, headline);

                    lblCreatePollFeedback.Visible = true;

                    RedirectFormat(Request.Url.ToString());
                }
                else
                {
					if (!validHeadline)
					{
						lblHeadlineFeedback.Visible = true;
					}
                    if (!validClassRadioButtons)
                    {
                        lblClassFeedback.Visible = true;
                    }
                    if (!validTopicRadioButtons)
                    {
                        lblTopicFeedback.Visible = true;
                    }
                    if (!validAnswers)
                    {
                        lblAnswerFeedback.Visible = true;
                    }
					if (!validCorrectAnswers)
					{
						lblCorrectAnswerFeedback.Visible = true;
					}
                    return;
                }
            }
        }

        protected void OnRptControlPollsItemCommand(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case Const.Commands.Delete:
                    PollService.DeletePoll(e.CommandArgument.ToString());
                    Initialize();
                    break;
                case Const.Commands.Edit:
                    int pollId;
                    int.TryParse(e.CommandArgument.ToString(), out pollId);
                    if (pollId > 0)
                    {
                        Poll poll = PollService.GetPoll(pollId);
                        //set all values
						cbQuiz.Checked = poll.IsQuiz;
						txtHeadline.Text = poll.Headline;

                        foreach (RepeaterItem item in rptClasses.Items)
                        {
                            CheckBox cbClass = item.FindControl("cbClass") as CheckBox;
                            HiddenField hdnClassId = item.FindControl("hdnClassId") as HiddenField;

                            int classId = 0;
                            int.TryParse(hdnClassId.Value, out classId);
                            if (classId > 0)
	                        {
								if (poll.PollClasses.Contains(PollService.GetPollClass(classId, pollId)))
								{
									cbClass.Checked = true;
								}
								else
								{
									cbClass.Checked = false;
								}
	                        }
                        }

                        foreach (RepeaterItem item in rptTopics.Items)
                        {
                            GroupRadioButton rbTopic = item.FindControl("rbTopic") as GroupRadioButton;
                            HiddenField hdnTopicId = item.FindControl("hdnTopicId") as HiddenField;

                            int topicId = 0;
                            int.TryParse(hdnTopicId.Value, out topicId);
                            if (topicId > 0)
                            {
								if (poll.CategoryId == topicId)
								{
									rbTopic.Checked = true;
								}
								else
								{
									rbTopic.Checked = false;
								}
                            }
                        }

                        int index = 0;
                        foreach (RepeaterItem item in rptAnswers.Items)
                        {
                            TextBox txtAnswer = item.FindControl("txtAnswer") as TextBox;
                            HiddenField hdnAnswerId = item.FindControl("hdnAnswerId") as HiddenField;
							GroupRadioButton rbCorrectAnswer = item.FindControl("rbCorrectAnswer") as GroupRadioButton;
							if (poll.PollAnswers.Count() - 1 >= index)
							{
								txtAnswer.Text = poll.PollAnswers[index].Answer;
								hdnAnswerId.Value = poll.PollAnswers[index].PollAnswerId.ToString();
								rbCorrectAnswer.Checked = poll.PollAnswers[index].IsCorrect;
							}
							else
							{
								txtAnswer.Text = "";
								hdnAnswerId.Value = "";
							}

                            index++;
                        }

						
                        txtQuestion.Text = poll.Question;
                        hdnPollId.Value = poll.PollId.ToString();
                        ddlStatus.SelectedIndex = Convert.ToInt32(poll.IsPublished);
                        lblCreatePollHeadline.Text = CurrentPage.GetProperty(Constants.NodePropertyNames.FormPages.EditPollTitle).Value;
						lnkNewPoll.Visible = false;
						pnlNewPoll.Visible = true;
                    }
                    break;
            }
        }

        protected void OnLnkNewPollClick(object sender, EventArgs e)
        {
            pnlNewPoll.Visible = true;
			lnkNewPoll.Visible = false;
        }
    }
}