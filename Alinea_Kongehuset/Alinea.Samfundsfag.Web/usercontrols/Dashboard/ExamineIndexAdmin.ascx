﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExamineIndexAdmin.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.Dashboard.ExamineIndexAdmin" %>

<asp:Repeater ID="rptIndexManager" runat="server">
  <HeaderTemplate>Index manager<br /></HeaderTemplate>
  <ItemTemplate>
      <%#Eval("Name")%>&nbsp;<asp:Button ID="btnRebuild" CommandArgument='<%#Eval("Name")%>' Text="Rebuild" runat="server" OnClick="OnBtnRebuildClick" />
  </ItemTemplate>
  <SeparatorTemplate><br /></SeparatorTemplate>
</asp:Repeater>
<asp:Literal ID="litResult" runat="server" Visible="false" />