﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Examine;

namespace Alinea.Samfundsfag.Web.usercontrols.Dashboard
{
	public partial class ExamineIndexAdmin : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				rptIndexManager.DataSource = ExamineManager.Instance.IndexProviderCollection;
				rptIndexManager.DataBind();
			}
		}

		protected void OnBtnRebuildClick(Object sender, EventArgs e)
		{
			Button clickedButton = (Button)sender;
			string indexToRebuild = clickedButton.CommandArgument;
			ExamineManager.Instance.IndexProviderCollection[indexToRebuild].RebuildIndex();
			litResult.Text = string.Format("Index {0} added to index queue", indexToRebuild);
			litResult.Visible = true;
		}
	}
}