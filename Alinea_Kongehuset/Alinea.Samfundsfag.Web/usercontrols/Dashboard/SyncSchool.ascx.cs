﻿using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Alinea.Samfundsfag.Web.usercontrols.Dashboard
{
	public partial class SyncSchool : System.Web.UI.UserControl
	{
		private const string FeedbackNoInput = "Indtast venligst et instutitionsnavn.";
		private const string FeedbackNotFound = "Instutitionen blev ikke fundet.";
		private const string FeedbackError = "Ukendt fejl. Instutitionen blev ikke synkroniseret.";
		private const string FeedbackSuccess = "Instutitionen blev synkroniseret.";

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void OnBtnSyncSchoolClick(object sender, EventArgs e)
		{
			string uniInstId = txtSchool.Text.Trim();

			litFeedback.Text = string.Empty;
			txtSchool.Text = string.Empty;

			if (string.IsNullOrEmpty(uniInstId))
			{
				litFeedback.Text = FeedbackNoInput;
				return;
			}

			SchoolClassService schoolService = new SchoolClassService();
			School school = schoolService.GetSchool(uniInstId);

			if (school == null)
			{
				litFeedback.Text = FeedbackNotFound;
				return;
			}
			else
			{
				bool success = schoolService.SyncSchool(uniInstId, true);

				if (success)
				{
					litFeedback.Text = FeedbackSuccess;
					return;
				}
				else
				{
					litFeedback.Text = FeedbackError;
					return;
				}
			}
		}
	}

}