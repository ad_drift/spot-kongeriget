﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;
using CoreM41.Web.UI;

namespace Alinea.Samfundsfag.Web.usercontrols.Dashboard
{
	public partial class BrokenLinks : UserControlBaseWithDataContext<SamfundsfagUmbracoDataContext>
	{
		public string CrawlerToken = Constants.Crawler.Token;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				repBrokenLinks.DataBind(DataContext.CrawlBrokenLinks);
				lblLatestCrawl.Text = string.Concat(Constants.Crawler.LatestCrawlText, CrawlerService.LatestCrawling.ToString());
			}
		}

		protected void OnBtnForceUpdateClick(object sender, EventArgs e)
		{
			if (CrawlerService.LatestCrawling.AddHours(3) < DateTime.Now)
			{
				lblStatus.ForeColor = Color.Green;
				lblStatus.Text = "Opdateringen er begyndt, der kan gå et par timer før du ser alle døde links.";
				CrawlerService.RunCrawler(CrawlerService.CrawlUrls());
			}
			else
			{
				lblStatus.Text = "Du skal vente mindst 3 timer efter den seneste opdatering";
				lblStatus.ForeColor = Color.Red;
			}
		}
	}
}