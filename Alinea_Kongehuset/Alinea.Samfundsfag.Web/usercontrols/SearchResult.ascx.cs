﻿using System;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Common.Enums;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.UI;
using Alinea.Samfundsfag.BusinessLogic.Utilities;
using CoreM41.Web.Utilities;
using System.Linq;
using System.Text.RegularExpressions;
using umbraco.cms.businesslogic.Tags;

namespace Alinea.Samfundsfag.Web.usercontrols
{
	public partial class SearchResult : UserControlBase
	{
		private string readMore;

		protected string ReadMore
		{
			get
			{
				if (readMore.IsNull())
				{
					readMore = UmbracoHelper.GetProperty(Constants.Fields.ReadMore).Value;
				}
				return readMore;
			}
		}

		private string title;

		protected string Title
		{
			get
			{
				if (title.IsNull())
				{
					title = UmbracoHelper.GetProperty(Constants.Fields.Title).Value;
				}
				return title;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				string searchString = PageUtility.GetQueryStringVariable<QueryStringVariables>(QueryStringVariables.SearchString);
				litSearchString.Text = searchString;
				litYouSearchedFor.Text = UmbracoHelper.GetProperty(Constants.Fields.YouSearchedFor).Value;
				litBack.Text = UmbracoHelper.GetProperty(Constants.Fields.Back).Value;

				var searchResult = SearchService.SearchSite(searchString, IsTeacherPage);

				if (searchResult.Length > 0)
				{
					searchResult = searchResult
						.OrderByDescending(s => Tag.GetTags(s.NodeId).Count(t => t.TagCaption.ToLower() == searchString.ToLower()))
						.ThenByDescending(s => Regex.Matches(s.Title.ToLower(), searchString.ToLower()).Count)
						.ThenByDescending(s => Regex.Matches(s.Content.ToLower(), searchString.ToLower()).Count)
						.ToArray();
					
					rptSearchResults.DataSource = searchResult;
					rptSearchResults.DataBind();
				}
				else
				{
					plhNoSearchResults.Visible = true;
					litNoSearchResults.Text = UmbracoHelper.GetProperty(Constants.Fields.NoSearchResults).Value;
				}
			}
		}
	}
}