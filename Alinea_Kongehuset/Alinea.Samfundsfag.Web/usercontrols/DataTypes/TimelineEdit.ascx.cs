﻿using System;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Services.Umbraco;
using Alinea.Samfundsfag.BusinessLogic.UI;
using CoreM41.Common;

namespace Alinea.Samfundsfag.Web.usercontrols.DataTypes
{
	public partial class TimelineEdit : DataTypeUserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				rptTimelineItems.DataBind(TimelineService.GetSiblingTimelineEntries(NodeId));
				hdnPosition.Value = TimelineService.GetTimelineEntry(NodeId).Position;				
			}

			if (IsPostBack)
			{
				// Save the value directly from the form variables
				hdnPosition.Value = Request.Form[hdnPosition.UniqueID];
				value = hdnPosition.Value;
			}

			htmlListItemTimelineEntry.Attributes[Const.Css.Style] = string.Concat("left:", hdnPosition.Value, "%");
		}
	}
}