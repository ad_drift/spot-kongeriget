﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelineEdit.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.DataTypes.TimelineEdit" %>

<%@ Register TagPrefix="uc" TagName="TimelineDataTypeShared" Src="~/usercontrols/DataTypes/TimelineDataTypeShared.ascx" %>

<div class="alinea-custom">
	<div class="alinea-tl-canvas alinea-tl-edit">
		<div class="alinea-tl-items">
			<ol>
        <asp:Repeater ID="rptTimelineItems" runat="server">
					<ItemTemplate>
						<li class="alinea-tl-item" data-position="<%# Eval("Position") %>%" data-label="<%# Eval("Label") %>"></li>	
					</ItemTemplate>
        </asp:Repeater>
				<li class="alinea-tl-item active" style="" id="htmlListItemTimelineEntry" runat="server"></li>
			</ol>
		</div>
    <asp:HiddenField ID="hdnPosition" runat="server" />
	</div>
</div>

<uc:TimelineDataTypeShared runat="server" />

