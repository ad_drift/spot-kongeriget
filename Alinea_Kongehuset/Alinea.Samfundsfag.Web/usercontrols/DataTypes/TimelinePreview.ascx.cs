﻿using System;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.UI;
using Alinea.Samfundsfag.BusinessLogic.Services.Umbraco;

namespace Alinea.Samfundsfag.Web.usercontrols.DataTypes
{
	public partial class TimelinePreview : DataTypeUserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                rptTimelinePreview.DataBind(TimelineService.GetChildTimelineEntries(NodeId));
            }
		}
	}
}