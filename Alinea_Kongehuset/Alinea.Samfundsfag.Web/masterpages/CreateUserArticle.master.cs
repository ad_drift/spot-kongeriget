﻿using System;
using System.Collections.Generic;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Models;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Futuristic.Umbraco;
using System.Linq;
using umbraco.NodeFactory;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Subject = Alinea.Samfundsfag.BusinessLogic.DataAccess.Subject;
using UserArticleTimelineItem = Alinea.Samfundsfag.BusinessLogic.Models.UserArticleTimelineItem;

namespace Alinea.Samfundsfag.Web.masterpages
{
	public partial class CreateUserArticle : System.Web.UI.MasterPage
	{
		#region Event wireup
		override protected void OnInit(EventArgs e)
		{
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		protected Alinea.Samfundsfag.BusinessLogic.Models.UserArticle article = null;

		protected Node myPage = NodeHelper.GetNodeByAlias(Constants.NodeDocumentTypes.UserArticleListMyPage);

		protected string title = string.Empty;
		protected string introduction = string.Empty;

		protected string text1 = string.Empty;
		protected int text1SortOrder = 1;
		protected string text2 = string.Empty;
		protected int text2SortOrder = 2;

		protected string image1Text = string.Empty;
		protected int image1SortOrder = 3;
		protected string image2Text = string.Empty;
		protected int image2SortOrder = 4;
		protected string image3Text = string.Empty;
		protected int image3SortOrder = 5;

		protected string video1YouTubeUrl = string.Empty;
		protected string video1YouTubeVideoId = string.Empty;
		protected int video1SortOrder = 6;
		protected string audio1Text = string.Empty;
		protected int audio1SortOrder = 7;

		protected string timelineHeadline = string.Empty;
		protected int timelineActiveItem = 0;

		protected bool isPublished = false;

		protected List<User> classMates = new List<User>();
		protected List<int> coAuhtorIds = new List<int>();

		protected List<Subject> subjects = new List<Subject>();
		protected List<int> subjectIds = new List<int>();

		protected void Page_Load(object sender, EventArgs e)
		{
			var cur = Node.GetCurrent();

			var articleId = 0;
            var assignmentId = 0;
			int.TryParse(Request.QueryString["id"], out articleId);
            int.TryParse(Request.QueryString["assignmentId"], out assignmentId);

			if (articleId != 0)
			{
				using (var uas = new UserArticleService())
				{
					article = uas.GetUserArticle(articleId);
					if (article == null || article.IsDeleted || article.CreatedBy != SessionController.CurrentUserId)
					{
						Response.Redirect(cur.NiceUrl);
					}
					else
					{
						title = article.Title;
						introduction = article.Introduction;

						text1 = article.Text1;
						text1SortOrder = article.Text1SortOrder;
						text2 = article.Text2;
						text2SortOrder = article.Text2SortOrder;

						image1Text = article.Image1Text;
						image1SortOrder = article.Image1SortOrder;
						image2Text = article.Image2Text;
						image2SortOrder = article.Image2SortOrder;
						image3Text = article.Image3Text;
						image3SortOrder = article.Image3SortOrder;

						video1YouTubeUrl = article.Video1YouTubeUrl;
						video1YouTubeVideoId = article.Video1YouTubeVideoId;
						video1SortOrder = article.Video1SortOrder;
						audio1Text = article.Audio1Text;
						audio1SortOrder = article.Audio1SortOrder;

						timelineHeadline = article.TimelineHeadline;
						timelineActiveItem = article.TimelineActiveItem;

						isPublished = article.IsPublished;

						coAuhtorIds = article.CoAuthorIds;
						subjectIds = article.SubjectIds;
					}
				}
			}
			else
			{
				// Save on create, and redirect imidiatly to edit-mode.
				articleId = SaveUserArticle(articleId);
                RedirectToEditUserArticle(articleId, assignmentId);
			}

			// Load co-authors
			var us = new UserService();
			classMates = us.GetStudentsForClass(SessionController.CurrentUserClass.ClassId).ToList();
			classMates.RemoveAll(u => u.UserId == SessionController.CurrentUserId);

			// Load subjects
			var ss = new SubjectService();
			subjects = ss.GetSubjects().ToList();
//			subjects.Add("Historie", "Historie");
//			subjects.Add("Samfundsfag", "Samfundsfag");
//			subjects.Add("Kristendomskundskab", "Kristendomskundskab");

			// Just save the article
			if (Request.Form["formaction"] == "save user article")
			{
				articleId = SaveUserArticle(articleId);
                RedirectToUserArticle(articleId, assignmentId);
                //RedirectToEditUserArticle(articleId, assignmentId);
			}

			// Save changes and redirect to preview
			if (Request.Form["formaction"] == "view user article")
			{
				articleId = SaveUserArticle(articleId);
                RedirectToUserArticle(articleId, assignmentId);
			}

			// Save changes, publish article and redirect to the article
			if (Request.Form["formaction"] == "publish user article")
			{
				articleId = SaveUserArticle(articleId);
				PublishUserArticle(articleId);
                RedirectToUserArticle(articleId, assignmentId);
			}

			if (Request.Form["formaction"] == "create timeline item")
			{
				if (article.TimelineItems.Count < 10)
				{
					article.TimelineItems.Add(new UserArticleTimelineItem());
				}
				articleId = SaveUserArticle(articleId);
				RedirectToEditUserArticle(articleId, assignmentId);
			}
		}

		private int GetTimelineItemsPosition(int current, int total)
		{
			if (total == 1)
			{
				return 50;
			}

			if (current == 0)
			{
				return 0;
			}

			if (current + 1 == total)
			{
				return 100;
			}

			if (total > 2)
			{
				var offset = 100m/(total - 1);
				return (int)Math.Round(current * offset, 0, MidpointRounding.AwayFromZero);
			}
			
			return 0;
		}

		private int SaveUserArticle(int articleId)
		{
            var timelineItems = new List<BusinessLogic.Models.UserArticleTimelineItem>();
            if (article != null)
            {
			    for (var i = 0; i < article.TimelineItems.Count; i++)
			    {
				    timelineItems.Add(new BusinessLogic.Models.UserArticleTimelineItem
				    {
					    Id = int.Parse(Request.Form["timeline-item" + i] ?? "0"),
					    Label = Request.Form["timeline-item-label" + i] ?? string.Empty,
					    Title = Request.Form["timeline-item-title" + i] ?? string.Empty,
					    Description = Request.Form["timeline-item-description" + i] ?? string.Empty,
					    Position = int.Parse(Request.Form["timeline-item-sortorder" + i] ?? "101")
				    });
			    }

			    timelineItems = timelineItems.OrderBy(c => c.Position).ThenBy(c => c.Id).ToList();
			    for (var i = 0; i < timelineItems.Count; i++)
			    {
				    timelineItems[i].Position = GetTimelineItemsPosition(i, timelineItems.Count);
                }
            }

			using (var uac = new UserArticleService())
			{
				var assignmentId = Request.QueryString["assignmentid"];
				articleId = uac.SaveUserArticle(
									Request.Form["title"] ?? string.Empty, Request.Form["introtext"] ?? string.Empty,
									Request.Form["text1"] ?? string.Empty, int.Parse(Request.Form["text1-sortorder"] ?? "1"),
									Request.Form["text2"] ?? string.Empty, int.Parse(Request.Form["text2-sortorder"] ?? "2"),
									Request.Form["image1-note"] ?? string.Empty, int.Parse(Request.Form["image1-sortorder"] ?? "3"),
									Request.Form["image2-note"] ?? string.Empty, int.Parse(Request.Form["image2-sortorder"] ?? "4"),
									Request.Form["image3-note"] ?? string.Empty, int.Parse(Request.Form["image3-sortorder"] ?? "5"),
									Request.Form["youtube"] ?? string.Empty, int.Parse(Request.Form["youtube-sortorder"] ?? "6"),
									Request.Form["audio-note"] ?? string.Empty, int.Parse(Request.Form["audio-sortorder"] ?? "7"),
									Request.Form["timeline-headline"] ?? string.Empty, int.Parse(Request.Form["timeline-active-item"] ?? "0"),
									int.Parse(string.IsNullOrEmpty(assignmentId) ? "0" : assignmentId),
									Request.Form["authorlist"] != null ? Request.Form["authorlist"].Split(',').Select(id => int.Parse(id)).ToList() : new List<int>(),
									Request.Form["subjectlist"] != null ? Request.Form["subjectlist"].Split(',').Select(id => int.Parse(id)).ToList() : new List<int>(),
									SessionController.CurrentUserId, SessionController.CurrentUserClass.ClassId,
									timelineItems,
									articleId);
			}

			SaveImage("list-image", "/media/UserArticles/" + articleId + "/cover.jpg");
			SaveImage("image1", "/media/UserArticles/" + articleId + "/image1.jpg");
			SaveImage("image2", "/media/UserArticles/" + articleId + "/image2.jpg");
			SaveImage("image3", "/media/UserArticles/" + articleId + "/image3.jpg");
			SaveAudio("audio", "/media/UserArticles/" + articleId + "/audio1.mp3");

			for (var i = 0; i < timelineItems.Count; i++)
			{
				SaveImage("timeline-image" + i, "/media/UserArticles/" + articleId + "/timelineimage" + timelineItems[i].Id + ".jpg");
			}

			return articleId;
		}

		private void SaveImage(string formName, string imageName)
		{
			var deleteThumbs = false;

			// Delete by user request
			if (Request.Form[formName + "-delete"] == "true")
			{
				deleteThumbs = true;

				var existingImg = new FileInfo(Server.MapPath(imageName));
				if (existingImg.Exists)
					existingImg.Delete();
			}

			// Upload new picture
			var file = Request.Files[formName];
			if (file != null && file.ContentLength > 0)
			{
				deleteThumbs = true;

				try
				{
					var dir = new DirectoryInfo(Server.MapPath(imageName.Substring(0, imageName.LastIndexOf('/') + 1)));
					if (!dir.Exists)
						dir.Create();

					using (var img = Image.FromStream(file.InputStream))
					{
						img.Save(Server.MapPath(imageName), ImageFormat.Jpeg);
					}
				}
				catch (Exception)
				{ }
			}

			// Delete cached thumbnail files
			if (deleteThumbs)
			{
				var thumbDir = new DirectoryInfo(Server.MapPath("/media/thumbs/"));
				if (thumbDir.Exists)
				{
					foreach (var thumb in thumbDir.GetFiles(imageName.ToLower().Trim('/').Split('.').First().Replace("/", "_") + "_*"))
						thumb.Delete();
				}
			}
		}

		private void SaveAudio(string formName, string fileName)
		{
			if (Request.Form[formName + "-delete"] == "true")
			{
				var existingImg = new FileInfo(fileName);
				if (existingImg.Exists)
					existingImg.Delete();
			}

			var file = Request.Files[formName];
			if(file != null && file.ContentLength > 0 && file.FileName.ToLower().EndsWith(".mp3"))
			{
				try
				{
					var dir = new DirectoryInfo(Server.MapPath(fileName.Substring(0, fileName.LastIndexOf('/') + 1)));
					if (!dir.Exists)
						dir.Create();

					file.SaveAs(Server.MapPath(fileName));
				}
				catch (Exception)
				{ }
			}
		}

		private void PublishUserArticle(int articleId)
		{
			using (var uac = new UserArticleService())
			{
				uac.PublishUserArticle(articleId, SessionController.CurrentUserId);
			}
		}

        private void RedirectToUserArticle(int articleId, int assignmentId)
		{
			if (articleId > 0)
			{
				var articleNode = NodeHelper.GetNodeByAlias(Constants.NodeDocumentTypes.UserArticle);
				if (articleNode != null)
				{
                    if (assignmentId > 0)
                    {
                        Response.Redirect(articleNode.NiceUrl + "?id=" + articleId + "&ref=edit" + "&assignmentId=" + assignmentId);
                    }
                    else {
                        Response.Redirect(articleNode.NiceUrl + "?id=" + articleId + "&ref=edit");
                    }
                }
			}
		}

        private void RedirectToEditUserArticle(int articleId, int assignmentId)
		{
            if (articleId > 0)
            {
                if (assignmentId > 0)
                {

                    Response.Redirect(Node.GetCurrent().NiceUrl + "?id=" + articleId + "&scrollpos=" + Request.Form["scrollpos"] + "&assignmentId=" + assignmentId);
                }
                else {
                    Response.Redirect(Node.GetCurrent().NiceUrl + "?id=" + articleId + "&scrollpos=" + Request.Form["scrollpos"]);
                
                }
            }
            
		}
	}
}