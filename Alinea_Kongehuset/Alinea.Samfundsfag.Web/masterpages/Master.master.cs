﻿using System;
using System.Web.UI;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Common;
using umbraco.NodeFactory;
using System.Linq;

namespace Alinea.Samfundsfag.Web.masterpages
{
	public partial class Master : MasterPage
	{
		protected bool useRazorBreadcrumbs = false;

        //protected string userName = "[navn]";
        protected string userName = string.Empty;
        protected string institutionName = string.Empty;
        protected string className = string.Empty;
        protected string userInfo = string.Empty;

		protected string googleAnalyticsId = string.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			var cur = Node.GetCurrent();
			if (cur.NodeTypeAlias == Constants.NodeDocumentTypes.UserArticle ||
				cur.NodeTypeAlias == Constants.NodeDocumentTypes.UserArticleList ||
				cur.NodeTypeAlias == Constants.NodeDocumentTypes.UserArticleListMyPage ||
				cur.NodeTypeAlias == Constants.NodeDocumentTypes.CreateUserArticle)
			{
				useRazorBreadcrumbs = true;
			}

			var root = new Node(Convert.ToInt32(cur.Path.Split(',')[1]));
			if (root != null)
			{
				var prop = root.GetProperty("googleAnalyticsId");
				if (prop != null)
				{
					googleAnalyticsId = prop.Value;
				}
			}

			// Force upperlevel on chrome detected (tells .net that this browser actually has the same/better capabilities as IE6) - fixes postbacks on chrome for iOS...
			// WTF?? Die Webforms, just die!
			if (Request.ServerVariables["HTTP_USER_AGENT"].ToLower().Contains("crios/"))
			{
				Page.ClientTarget = "uplevel";
			}

			try
			{
				// todo: mini
				if (UniLoginController.User != null)
				{
                    userInfo =	userName = UniLoginController.User.Name;

					institutionName = UniLoginController.User.PrimaryInstitutionName;
                    
                    if (UniLoginController.IsLoggedInUserStudent && UniLoginController.User.Classes != null && UniLoginController.User.Classes.Any())
                    {
                        className = UniLoginController.User.Classes[0].Name;

                        if (!string.IsNullOrEmpty(className)){
                            userInfo = userInfo + ", " + className;
                        }
                    }
                    if (!string.IsNullOrEmpty(institutionName))
                    {
                        userInfo = userInfo + ", " + institutionName;
                    }
                    
				}
				//userName = "loui192n";
				//institutionName = "A12012";

			}
			catch (NotSupportedException)
			{
			}			
		}
	}
}