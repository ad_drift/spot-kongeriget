﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Alinea.Samfundsfag.Web.masterpages
{
    public partial class AllArticles : System.Web.UI.MasterPage
    {
        #region Event wireup
        override protected void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);
        }
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}