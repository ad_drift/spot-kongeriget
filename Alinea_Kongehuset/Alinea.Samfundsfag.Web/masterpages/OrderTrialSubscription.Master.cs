﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.NodeFactory;

namespace Alinea.Samfundsfag.Web.masterpages
{
	public partial class OrderTrialSubscription : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
                var currentNode = umbraco.NodeFactory.Node.GetCurrent();

                uiOrder.RecipientEmail = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.RecipientEmail).Value;
                uiOrder.EmailSubject = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.EmailSubject).Value;
                uiOrder.EmailBody = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.EmailBody).Value;
                
                uiOrder.Headline = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.Headline).Value;
                uiOrder.Description = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.Description).Value;
                uiOrder.SuccessMessage = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.SuccessMessage).Value;
                uiOrder.ErrorMessage = currentNode.GetProperty(Alinea.Samfundsfag.BusinessLogic.Common.Constants.OrderTrialSubscription.ErrorMessage).Value;

			}
		}
	}
}