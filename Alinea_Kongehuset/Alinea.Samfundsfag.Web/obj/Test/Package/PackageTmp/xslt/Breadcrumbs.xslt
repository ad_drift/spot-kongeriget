<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:variable name="minLevel" select="1"/>

<xsl:template match="/">

<xsl:if test="$currentPage/@level &gt; $minLevel">
	<ol class="breadcrumb">

		<!-- Only show Homepage link in Breadcrumb for the about page (hardcoded node id) -->
		<xsl:if test="$currentPage/ancestor-or-self::* [@id = '1124']">
			<li>
				<a>
					<xsl:attribute name="href">
						<xsl:choose>
							<xsl:when test="(samfundsfag:IsInTeacherSection() != 'true' and samfundsfag:IsInStudentSection() != 'true' and samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent())">
								<xsl:value-of select="'/elev/'"/>
							</xsl:when>
							<xsl:when test="(samfundsfag:IsInTeacherSection() != 'true' and samfundsfag:IsInStudentSection() != 'true' and samfundsfag:IsLoggedInUserTeacher())">
								<xsl:value-of select="'/laerer/'"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'/'"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:value-of select="'Forside'"/>
				</a>
			</li>
		</xsl:if>

		<xsl:variable name="navParentId" select="umbraco.library:RequestQueryString('emne')"/>

		<xsl:choose>
			<xsl:when test="string($navParentId) != ''">
				<xsl:call-template name="renderBreadcrumb">
					<xsl:with-param name="nodeId" select="$navParentId" />					
				</xsl:call-template>

				<!-- print currentpage -->
				<li>
					<a class="active" href="{umbraco.library:NiceUrl($currentPage/@id)}">
						<xsl:choose>
							<xsl:when test="string($currentPage/title) != ''">
								<xsl:value-of select="$currentPage/title"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$currentPage/menuTitle"/>
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="renderBreadcrumb">
					<xsl:with-param name="nodeId" select="$currentPage/@id" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
	</ol>
</xsl:if>

</xsl:template>

	<xsl:template name="renderBreadcrumb">
		<xsl:param name="nodeId" />

		<xsl:variable name="pageNode" select="umbraco.library:GetXmlNodeById($nodeId)"/>

		<xsl:for-each select="$pageNode/ancestor-or-self::* [@level &gt; $minLevel and string(umbracoNaviHide) != '1']">
			<!--TOPIC: 1205
			SUBTOPIC: 1211
			ANGLE: 1209-->
			<xsl:choose>
				<xsl:when test="$currentPage[@nodeType='1209']">
					<xsl:choose>
						<xsl:when test="current()[@nodeType='1205']">
							<xsl:variable name="child" select="umbraco.library:GetXmlNodeById($currentPage/@parentID)"/>
							<xsl:if test="string(menuTitle) != '' or string(title) != ''">
								<li>
									<a href="{umbraco.library:NiceUrl(@id)}">
										<xsl:if test="./@id = $currentPage/@id">
											<xsl:attribute name="class">
												<xsl:value-of select="'active'"/>
											</xsl:attribute>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="string(menuTitle) != ''">
												<xsl:value-of select="menuTitle"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="title"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</li>
							</xsl:if>
						</xsl:when>
						<xsl:when test="current()[@nodeType='1211']">
							<!--Do nothing - posted with topic-->
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="string(menuTitle) != '' or string(title) != ''">
								<li>
									<a href="{umbraco.library:NiceUrl(@id)}">
										<xsl:if test="./@id = $currentPage/@id">
											<xsl:attribute name="class">
												<xsl:value-of select="'active'"/>
											</xsl:attribute>
										</xsl:if>
										<xsl:choose>
											<xsl:when test="string(menuTitle) != ''">
												<xsl:value-of select="menuTitle"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="title"/>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</li>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>

					<xsl:if test="./@id = $nodeId or ./@level = ($minLevel + 1) or samfundsfag:IsLoggedInUserTeacher() or samfundsfag:IsLoggedInUserStudent() or samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent()">
						<li>
							<a href="{umbraco.library:NiceUrl(@id)}">

								<xsl:if test="./@id = $currentPage/@id">
									<xsl:attribute name="class">
										<xsl:value-of select="'active'"/>
									</xsl:attribute>
								</xsl:if>
							
								<xsl:attribute name="href">
									<xsl:choose>
										<xsl:when test="@level &gt; 2 or (samfundsfag:IsInTeacherSection() ='true' and samfundsfag:IsLoggedInUserTeacher()) or (samfundsfag:IsInStudentSection() = 'true' and samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent())">
											<!-- Vis elev/lærer forside -->
											<xsl:value-of select="umbraco.library:NiceUrl(@id)"/>
										</xsl:when>
										<xsl:otherwise>
											<!-- Vis rigtig forside til ikke-indloggede brugere -->
											<xsl:value-of select="umbraco.library:NiceUrl($currentPage/ancestor-or-self::* [@level=1]/@id)"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>

								<xsl:choose>
									<xsl:when test="string(menuTitle) != ''">
										<xsl:value-of select="menuTitle"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="title"/>
									</xsl:otherwise>
								</xsl:choose>
							</a>
						</li>
					</xsl:if>
					
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>

		
	</xsl:template>
	
</xsl:stylesheet>