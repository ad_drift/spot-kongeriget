<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#x00A0;">
	<!ENTITY rarr "&#x2192;">
	<!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">

	<xsl:output method="xml" omit-xml-declaration="yes"/>

	<xsl:template name="classMessage">
		<xsl:param name="classMessage"/>
		
		<div class="heading">
			<h4>
				<xsl:value-of select="$classMessage/title"/>
			</h4>
		</div>
		<section class="content-box important simple cb-messages">
			<div class="entry">
				<p>
					<xsl:value-of select="$classMessage/teaserText"/>
				</p>
				<a class="button significant">
					<xsl:attribute name="href">
						<xsl:value-of select="umbraco.library:NiceUrl($classMessage/@id)"/>
					</xsl:attribute>
					Skriv besked
				</a>
			</div>
		</section>
	</xsl:template>

	<xsl:template name="classPoll">
		<xsl:param name="classPoll"/>
		
		<div class="heading">
			<h4>
				<xsl:value-of select="$classPoll/title"/>
			</h4>
		</div>
		<section class="content-box important simple cb-polls">
			<div class="entry">
				<p>
					<xsl:value-of select="$classPoll/teaserText"/>
				</p>
				<a class="button significant">
					<xsl:attribute name="href">
						<xsl:value-of select="umbraco.library:NiceUrl($classPoll/@id)"/>
					</xsl:attribute>
					Opret ny
				</a>
			</div>
		</section>
	</xsl:template>

	<xsl:template name="allPresentations">
		<xsl:param name="allPresentations"/>
    <xsl:if test="count($currentPage/*/Presentation) &gt; 0 or count($currentPage/Presentation) &gt; 0">
		<div class="heading">
			<xsl:if test="count($currentPage/Presentation) &gt; 0">
				<h4>
					<xsl:value-of select="'Prøveoplæg'"/>
				</h4>
			</xsl:if>
			<xsl:if test="count($currentPage/Presentation) = 0">
				<h4>
					<xsl:value-of select="$allPresentations/title"/>
				</h4>
			</xsl:if>
		</div>
		<section class="content-box important simple">
			<div class="entry">
				<xsl:if test="count($currentPage/Presentation) &gt; 0">
					<ul>
						<xsl:for-each select="$currentPage/Presentation">
							<li>
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
									</xsl:attribute>
									<xsl:value-of select="current()/menuTitle"/>
								</a>
							</li>
						</xsl:for-each>
					</ul>
				</xsl:if>
				<xsl:if test="count($currentPage/Presentation) = 0">
					<p>
						<xsl:value-of select="$allPresentations/teaserText"/>
					</p>
					<a class="button significant">
						<xsl:attribute name="href">
							<xsl:value-of select="umbraco.library:NiceUrl($allPresentations/@id)"/>
						</xsl:attribute>
            Se oversigt
          </a>
				</xsl:if>
			</div>
		</section>
    </xsl:if>
	</xsl:template>

</xsl:stylesheet>