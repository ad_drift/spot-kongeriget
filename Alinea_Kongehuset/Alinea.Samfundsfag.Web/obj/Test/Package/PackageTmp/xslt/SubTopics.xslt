<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">
	
	<div class="heading">
		<h3>
			<xsl:value-of select="$currentPage/title"/>
		</h3>
	</div>
	<ul>
		<xsl:for-each select="$currentPage/Subtopic">
			<li>
				<div class="entry content-box">
					<figure>
						<xsl:variable name="image" select="umbraco.library:GetMedia(current()/image, 0)"/>
						<img>
							<xsl:attribute name="src">
								<xsl:value-of select="$image/umbracoFile"/>
							</xsl:attribute>
							<xsl:attribute name="alt">
								<xsl:value-of select="$image/@nodeName"/>
							</xsl:attribute>
						</img>
						<h2>
							<xsl:value-of select="current()/title"/>
						</h2>
					</figure>
					<p>
						<xsl:value-of select="current()/description"/>
					</p>
					<xsl:if test="current()/Angle">
						<ul>
							<xsl:for-each select="current()/Angle">
								<xsl:variable name="smallImage" select="umbraco.library:GetMedia(current()/smallImage, 0)"/>
								<li>
									<a>
										<xsl:attribute name="href">
											<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
										</xsl:attribute>
									
										<xsl:if test="$smallImage != ''">
											<img>
												<xsl:attribute name="src">
													<xsl:value-of select="$smallImage/umbracoFile"/>
												</xsl:attribute>
												<xsl:attribute name="alt">
													<xsl:value-of select="$smallImage/@nodeName"/>
												</xsl:attribute>
											</img>
										</xsl:if>
										<h4>
											<xsl:value-of select="current()/title"/>
										</h4>
									</a>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:if>
				</div>
			</li>
		</xsl:for-each>
	</ul>

</xsl:template>

</xsl:stylesheet>