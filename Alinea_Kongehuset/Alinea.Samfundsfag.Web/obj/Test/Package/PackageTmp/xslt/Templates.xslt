<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#x00A0;">
  <!ENTITY rarr "&#x2192;">
  <!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">

  <xsl:output method="html" omit-xml-declaration="yes" indent="no"/>

  <xsl:template name="poll">
	<xsl:param name="pollXml"/>
	<xsl:param name="title"/>
  <xsl:param name="quizBool"/>
  <xsl:param name="correctAnswer"/>
  <xsl:param name="autoUpdate"/>
	
	<div>
    <xsl:attribute name="class">entry</xsl:attribute>
    <xsl:attribute name="data-timer">
      <xsl:if test="$autoUpdate = ''">0</xsl:if>
      <xsl:if test="$autoUpdate != ''">
        <xsl:value-of select="$autoUpdate" />
      </xsl:if>
      
    </xsl:attribute>

	  <h4 class="inside-poll">
		<xsl:value-of select="$title"/>
	  </h4>
	  <p>
		<strong>
		  <xsl:value-of select="$pollXml/question"/>
		</strong>
	  </p>

	  <div id="poll-html-{$pollXml/@id}"></div>

	  <xsl:variable name="strPollXml">
		<xsl:apply-templates select="$pollXml" mode="nodetostring" />
	  </xsl:variable>

	  <xsl:variable name="linebreaks" select='"&#xA;&#xD;"' />
	  <xsl:variable name="apos" select='"&apos;"' />
    <xsl:choose>
      <xsl:when test=" $quizBool = '1' and $correctAnswer !=''">
        <script type="text/javascript">
          $(function()
          {
          var id = '<xsl:value-of select="$pollXml/@id" />',
          xml = '<xsl:value-of select="translate(translate($strPollXml, $linebreaks, ''), $apos, '')" />',
          quizBool = '<xsl:value-of select="'1'" />',
          correctAnswer = '<xsl:value-of select="$correctAnswer" />',
          wrapperId = '#poll-html-' + id,
          timer = $(wrapperId).parents(".entry").data("timer")*1000;

          var pollServiceCall = function() {
          $.ajax({
          url: '/services/GetPollHTML.ashx?' + Math.random(),
          type: 'POST',
          data:
          {
          id: id,
          xml: xml,
          quizBool: quizBool,
          correctAnswer: correctAnswer
          },
          cache: false,
          async: false,
          success: function(html)
          {
          $(wrapperId).html(html);
          }
          });
          };

          pollServiceCall();

          $(wrapperId).find(".button.discreet").one("click", function() {

          if(timer > 0) {
          setInterval(pollServiceCall, timer);
          } else {

          setInterval(pollServiceCall, 100000000);
          
          }
          });

          });
        </script>
      </xsl:when>
      <xsl:otherwise>
        <script type="text/javascript">
          $(function()
          {
          var id = '<xsl:value-of select="$pollXml/@id" />',
          xml = '<xsl:value-of select="translate(translate($strPollXml, $linebreaks, ''), $apos, '')" />',
          quizBool = '<xsl:value-of select="'0'" />',
          correctAnswer = '<xsl:value-of select="$correctAnswer" />',
          wrapperId = '#poll-html-' + id,
          timer = $(wrapperId).parents(".entry").data("timer")*1000;

          var pollServiceCall = function() {
          $.ajax({
          url: '/services/GetPollHTML.ashx?' + Math.random(),
          type: 'POST',
          data:
          {
          id: id,
          xml: xml,
          quizBool: quizBool,
          correctAnswer: correctAnswer
          },
          cache: false,
          async: false,
          success: function(html)
          {
          $(wrapperId).html(html);
          }
          });
          };

          pollServiceCall();

          $(wrapperId).find(".button.discreet").one("click", function() {

          if(timer > 0) {
          setInterval(pollServiceCall, timer);
          } else {

          setInterval(pollServiceCall, 100000000);
          
          }
          });

          });
        </script>
      </xsl:otherwise>
    </xsl:choose>
	  
	</div>
	
  </xsl:template>

  <xsl:variable name="q">
	<xsl:text>"</xsl:text>
  </xsl:variable>
  <xsl:variable name="empty"/>

  <xsl:template match="*" mode="selfclosetag">
	<xsl:text>&lt;</xsl:text>
	<xsl:value-of select="name()"/>
	<xsl:apply-templates select="@*" mode="attribs"/>
	<xsl:text>/&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="*" mode="opentag">
	<xsl:text>&lt;</xsl:text>
	<xsl:value-of select="name()"/>
	<xsl:apply-templates select="@*" mode="attribs"/>
	<xsl:text>&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="*" mode="closetag">
	<xsl:text>&lt;/</xsl:text>
	<xsl:value-of select="name()"/>
	<xsl:text>&gt;</xsl:text>
  </xsl:template>

  <xsl:template match="* | text()" mode="nodetostring">
	<xsl:choose>
	  <xsl:when test="boolean(name())">
		<xsl:choose>
		  <!-- if element is not empty -->
		  <xsl:when test="normalize-space(.) != $empty or *">
			<xsl:apply-templates select="." mode="opentag"/>
			<xsl:apply-templates select="* | text()" mode="nodetostring"/>
			<xsl:apply-templates select="." mode="closetag"/>
		  </xsl:when>
		  <!-- assuming emty tags are self closing, e.g. <img/>, <source/>, <input/> -->
		  <xsl:otherwise>
			<xsl:apply-templates select="." mode="selfclosetag"/>
		  </xsl:otherwise>
		</xsl:choose>
	  </xsl:when>
	  <xsl:otherwise>		
		<xsl:value-of select="." />
	  </xsl:otherwise>
	</xsl:choose>
  </xsl:template>

  <xsl:template match="@*" mode="attribs">
	<xsl:if test="position() = 1">
	  <xsl:text> </xsl:text>
	</xsl:if>
	<xsl:value-of select="concat(name(), '=', $q, ., $q)"/>
	<xsl:if test="position() != last()">
	  <xsl:text> </xsl:text>
	</xsl:if>
  </xsl:template>
  
  
  
  <xsl:template name="news">
	<xsl:param name="this"></xsl:param>
	<xsl:param name="isPromotion"></xsl:param>
	<xsl:param name="topicId"></xsl:param>
	<article class="item">

	  <aside>
		<nav class="paging">
		  <p>x af x</p>
		  <a class="button prev" href="#prev">
			&larr;
		  </a>
		  <a class="button next" href="#next">
			&rarr;
		  </a>
		</nav>
	  </aside>

	  <a class="entry">
		<xsl:attribute name="href">
		  <xsl:value-of select="umbraco.library:NiceUrl($this/@id)"/>
		  <xsl:if test="string($topicId) != ''">
			<xsl:value-of select="'?emne='"/>
			<xsl:value-of select="$topicId"/>
		  </xsl:if>
		</xsl:attribute>
		<h3>
		  <xsl:choose>
			<xsl:when test="$isPromotion = 'true'">
			  <xsl:value-of select="$this/promotionTitle"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:value-of select="$this/title"/>
			</xsl:otherwise>
		  </xsl:choose>
		</h3>
		<img width="400" height="300">
		  <xsl:variable name="image" select="umbraco.library:GetMedia($this/smallImage, 0)"/>
		  <xsl:attribute name="src">
			<xsl:value-of select="samfundsfag:ResolveUrl($image/umbracoFile)"/>
		  </xsl:attribute>
		  <xsl:attribute name="alt">
			<xsl:value-of select="$image/@nodeName"/>
		  </xsl:attribute>
		</img>
		<p>
		  <xsl:value-of select="$this/promotionText"/>
		</p>
		<xsl:if test="$this/linkText != ''">
		  <p>
			<em>
			  <xsl:value-of	select="$this/linkText"/>
			</em>
		  </p>
		</xsl:if>
	  </a>

	</article>
  </xsl:template>

  <xsl:template name="fact">
	<xsl:param name="fact"/>
	<div class="entry">
	  <h4>
		<xsl:value-of select="$fact/@nodeName"/>
	  </h4>
		<xsl:value-of select="$fact/description" disable-output-escaping="yes"/>
	  <p>
		<a href="#fact">
		  <xsl:attribute name="href">
			<xsl:variable name="this" select="umbraco.library:GetXmlNodeById($fact/link)"/>
			<xsl:choose>
			  <xsl:when test="$this/link != ''">
				<xsl:value-of select="$this/link"/>
			  </xsl:when>
			  <xsl:otherwise>
				<xsl:if test="number($this/@id) = $this/@id">
				  <xsl:value-of select="umbraco.library:NiceUrl($this/@id)"/>
				</xsl:if>
			  </xsl:otherwise>
			</xsl:choose>
		  </xsl:attribute>
		  <xsl:attribute name="target">
			_blank
		  </xsl:attribute>
		  <xsl:value-of select="$fact/linkText"/>
		</a>
	  </p>
	</div>
  </xsl:template>

  <xsl:template name="relatedLinks">
	<xsl:param name="this"></xsl:param>
	<xsl:param name="link"></xsl:param>
	<xsl:param name="newwindow"></xsl:param>
	<xsl:param name="index"></xsl:param>
	<li>
	  <xsl:if test="($index - 1) mod 4 = 0">
		<xsl:attribute name="style">
		  <xsl:value-of select="'clear:left;'"/>
		</xsl:attribute>
	  </xsl:if>

	  <xsl:choose>
		<xsl:when test="$link/@type = 'external'">

		  <!-- EXTERNAL LINK -->
		  <strong>
			<xsl:value-of select="$link/@title"/>
		  </strong>
		  <a href="{$link/@link}">
			<xsl:if test="$newwindow = '1'">
			  <xsl:attribute name="target">			
				<xsl:value-of select="'_blank'"/>
			  </xsl:attribute>
			</xsl:if>
			<xsl:value-of select="'Læs mere'"/>			
		  </a>

		</xsl:when>
		<xsl:when test="$link/@type = 'internal'">

		  <!-- INTERNAL LINK -->
		  <strong>
			<xsl:value-of select="$link/@title"/>
			<!--
			<xsl:choose>
			  <xsl:when test="$this/name != ''">
				<xsl:value-of select="$this/name"/>
			  </xsl:when>
			  <xsl:otherwise>
				<xsl:value-of select="$this/title"/>
			  </xsl:otherwise>
			</xsl:choose>
			-->
		  </strong>

		  <xsl:choose>
			<xsl:when test="$this/relatedDescription != ''">
			  <xsl:value-of select="$this/relatedDescription"/>
			</xsl:when>
			<xsl:when test="$this/metaDescription != ''">
			  <xsl:value-of select="$this/metaDescription"/>
			</xsl:when>
			<xsl:otherwise>
			  <xsl:value-of select="$this/description"/>
			</xsl:otherwise>
		  </xsl:choose>

		  <a>
			<xsl:attribute name="href">
			  <xsl:choose>
				<xsl:when test="$this/link != ''">
				  <xsl:value-of select="$this/link"/>
				</xsl:when>
				<xsl:otherwise>
				  <xsl:if test="number($this/@id) = $this/@id">
					<xsl:value-of select="umbraco.library:NiceUrl($this/@id)"/>
				  </xsl:if>
				</xsl:otherwise>
			  </xsl:choose>
			</xsl:attribute>
			<xsl:if test="$newwindow = '1'">
			  <xsl:attribute name="target">
				<xsl:value-of select="'_blank'"/>
			  </xsl:attribute>
			</xsl:if>
			<xsl:value-of select="'Læs mere'"/>
		  </a>

		</xsl:when>
	  </xsl:choose>
	  
	</li>
  </xsl:template>

  <xsl:template name="back-button">
	<xsl:param name="parentId"/>
	<a class="button discreet back">
	  <xsl:attribute name="href">
		<!--<xsl:value-of select="umbraco.library:NiceUrl($parentId)"/>-->
		#back
	  </xsl:attribute>
	  Tilbage
	</a>
  </xsl:template>

  <xsl:template name="downloadLinks">
  <xsl:param name="assignmentId"></xsl:param>
  <xsl:param name="items"></xsl:param>
    
    <xsl:param name="hideHeadline"></xsl:param>

	<xsl:if test="count($items) > 0">
	  <!--
			<em>
				<xsl:if test="$hideHeadline != 'true'">
					Hent
				</xsl:if>
				&nbsp;
			</em>
			-->
    
	  <xsl:for-each select="$items">
		<xsl:variable name="this" select="umbraco.library:GetMedia(current()/@id, 0)"></xsl:variable>
		<a target="_blank" class="download">
		  <xsl:attribute name="href">
			<xsl:value-of select="'/download.ashx?file='"/>
			<xsl:value-of select="umbraco.library:UrlEncode($this/umbracoFile)"/>
		  </xsl:attribute>
		  <em>&nbsp;</em>
		  <xsl:value-of select="$this/@nodeName"/>
		</a>
	  </xsl:for-each>
	</xsl:if>
  </xsl:template>

  <xsl:template name="renderImage">
	<xsl:param name="imageId" />
	<xsl:param name="popupImageId" />

	<xsl:if test="string($imageId) != ''">

	  <xsl:variable name="image" select="umbraco.library:GetMedia(number($imageId), 'false')" />

	  <xsl:if test="string($image) != ''">
		<xsl:choose>
		  <xsl:when test="string($popupImageId) != ''">

			<xsl:variable name="popupImage" select="umbraco.library:GetMedia(number($popupImageId), 'false')" />
			<a href="{samfundsfag:ResolveUrl($popupImage/umbracoFile)}" class="lb-image">
			  <img width="{$image/umbracoWidth}" height="{$image/umbracoHeight}" src="{samfundsfag:ResolveUrl($image/umbracoFile)}">
				<xsl:attribute name="alt">
				  <xsl:value-of select="$image/alternativeText" />
				</xsl:attribute>
			  </img>
			</a>

		  </xsl:when>
		  <xsl:otherwise>

			<img width="{$image/umbracoWidth}" height="{$image/umbracoHeight}" src="{samfundsfag:ResolveUrl($image/umbracoFile)}">
			  <xsl:attribute name="alt">
				<xsl:value-of select="$image/alternativeText" />
			  </xsl:attribute>
			</img>

		  </xsl:otherwise>
		</xsl:choose>
	  </xsl:if>
	</xsl:if>
  </xsl:template>

</xsl:stylesheet>