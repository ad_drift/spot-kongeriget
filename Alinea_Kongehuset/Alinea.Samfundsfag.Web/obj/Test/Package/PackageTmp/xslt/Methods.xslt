<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt"/>
	
<xsl:param name="currentPage"/>
<xsl:variable name="methods" select="$currentPage/Method"></xsl:variable>

<xsl:template match="/">

	<xsl:variable name="recordsPerPage" select="4"/>
	<xsl:variable name="pageNumber">
		<xsl:choose>
			<xsl:when test="umbraco.library:RequestQueryString('page') &lt;= 0 or string(umbraco.library:RequestQueryString('’page')) = '' or string(umbraco.library:RequestQueryString('page')) = 'NaN'">0</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="umbraco.library:RequestQueryString('page')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<article class="content wrapper" role="main">
		<div class="heading">
			<h1>Metoder</h1>
		</div>
		<ul class="method-list">

			<xsl:for-each select="$methods">
				<li>
					<div class="content-box">
						<div class="entry">
							<h3>
								<xsl:value-of select="current()/title"/>
							</h3>
							<p>
								<xsl:value-of select="current()/description" disable-output-escaping="yes"/>
							</p>
							<p>
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
									</xsl:attribute>
									Læs mere
								</a>
							</p>

                            <xsl:variable name="items" select="current()/downloadLinks/media/item"/>
							<xsl:if test="count($items) > 0">
								<footer>
									<xsl:call-template name="downloadLinks">
										<xsl:with-param name="items" select="$items"/>
										<xsl:with-param name="hideHeadline" select="true"/>
									</xsl:call-template>
								</footer>
							</xsl:if>
						
						</div>
					</div>
				</li>
			</xsl:for-each>
			
		</ul>
		<footer>
			<xsl:call-template name="back-button">
				<xsl:with-param name="parentId" select="$currentPage/@parentID"></xsl:with-param>
			</xsl:call-template>
		</footer>
	</article>

</xsl:template>

</xsl:stylesheet>