<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="macro" select="/macro"></xsl:variable>
<xsl:variable name="image" select="$macro/Image/Image"></xsl:variable>
<xsl:variable name="popupImage" select="$macro/PopupImage/Image"></xsl:variable>
<xsl:variable name="fullSizeImageBool" select="$macro/fullSizeImage"></xsl:variable>

  <xsl:template match="/">
	<figure>
		<xsl:choose>
			<xsl:when test="$popupImage/umbracoFile">
				<a href="{$popupImage/umbracoFile}" class="lb-image">
          <img>
              <xsl:attribute name="src">
                <xsl:value-of select="$image/umbracoFile"/>
              </xsl:attribute>
            <xsl:if test="$fullSizeImageBool = 1">
              <xsl:attribute name="class">
                <xsl:value-of select="'largeimage'"/>
              </xsl:attribute>
            </xsl:if>
            </img>
				</a>
			</xsl:when>
			<xsl:otherwise>
        <img>
          <xsl:attribute name="src">
            <xsl:value-of select="$image/umbracoFile"/>
          </xsl:attribute>
          <xsl:if test="$fullSizeImageBool = 1">
            <xsl:attribute name="class">
              <xsl:value-of select="'largeimage'"/>
            </xsl:attribute>
          </xsl:if>
        </img>
			</xsl:otherwise>
		</xsl:choose>
		<figcaption>
			<xsl:value-of select="$macro/CiteText" />
			<cite><xsl:value-of select="$macro/Cite" /></cite>
		</figcaption>
	</figure>

</xsl:template>

</xsl:stylesheet>