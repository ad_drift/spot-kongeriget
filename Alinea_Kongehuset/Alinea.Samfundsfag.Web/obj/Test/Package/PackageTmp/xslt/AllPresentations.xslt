<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:include href="Templates.xslt"/>
	
<xsl:template match="/">

	<article class="content wrapper" role="main">
		<div class="heading">
			<h1>
				<xsl:value-of select="$currentPage/title"/>
			</h1>
		</div>
		<ul class="method-list">
			<xsl:for-each select="$currentPage/../Instruction">
				<li>
					<div class="content-box">
						<xsl:if test="current()/image != ''">
							<xsl:variable name="image" select="umbraco.library:GetMedia(current()/image, 0)"/>
							<img src="http://placehold.it/200x200" alt="">
								<xsl:attribute name="src">
									<xsl:value-of select="$image/umbracoFile"/>
								</xsl:attribute>
								<xsl:attribute name="alt">
									<xsl:value-of select="$image/@nodeName"/>
								</xsl:attribute>
							</img>
						</xsl:if>
						<div class="entry">
							<h3>
								<xsl:value-of select="current()/title"/>
							</h3>
							<ul>
								<xsl:for-each select="current()/Presentation">
									<li>
										<a href="#test">
											<xsl:attribute name="href">
												<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
											</xsl:attribute>
											<xsl:value-of select="current()/menuTitle"/>
										</a>
									</li>
								</xsl:for-each>
							</ul>
						</div>
					</div>
				</li>
			</xsl:for-each>
		</ul>
		<footer>
			<xsl:call-template name="back-button">
				<xsl:with-param name="parentId" select="$currentPage/@parentID"/>
			</xsl:call-template>
		</footer>
	</article>

</xsl:template>

</xsl:stylesheet>