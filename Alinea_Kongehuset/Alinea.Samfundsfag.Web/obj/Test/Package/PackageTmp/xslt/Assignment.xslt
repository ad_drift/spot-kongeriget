<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>
	
<xsl:include href="Templates.xslt" />
<xsl:param name="currentPage"/>
 <xsl:param name="assignmentIdUrl" select="umbraco.library:RequestQueryString('assignmentId')"/>


<xsl:template match="/">
  <xsl:choose>
    <xsl:when test="macro/AssignmentId !=''">
      <xsl:variable name="assignmentId" select="macro/AssignmentId"/>
      <xsl:variable name="assignment" select="umbraco.library:GetXmlNodeById($assignmentId)"/>
      <xsl:variable name="createArticleButtonBool" select="$assignment/createArticleButton"/>
      <section class="content-box assignment">
        <div class="entry">
          <h4>
            <xsl:value-of select="$assignment/title"/>
          </h4>
          <xsl:value-of select="umbraco.library:RenderMacroContent($assignment/content, $assignment/@id)" disable-output-escaping="yes"/>
          <xsl:if test="$createArticleButtonBool = 1">
            <a href="/elev/min-side/opret-artikel/?assignmentId={$assignmentId}" class="button significant">Opret nyt indlæg</a>
          </xsl:if>
          <xsl:if test="$assignment/downloadLinks/media/item/@title != ''">

            <footer>
              <xsl:call-template name="downloadLinks">
                <xsl:with-param name="assignmentId" select="$assignmentId"></xsl:with-param>
                <xsl:with-param name="items" select="$assignment/downloadLinks/media/item"></xsl:with-param>
                
              </xsl:call-template>
            </footer>

          </xsl:if>
        </div>
      </section>
    </xsl:when>
    <xsl:when test="$assignmentIdUrl !=''">
      <xsl:variable name="assignmentId" select="$assignmentIdUrl"/>
      <xsl:variable name="assignment" select="umbraco.library:GetXmlNodeById($assignmentIdUrl)"/>
      <section class="content-box assignment">
        <div class="entry">
          <h4>
            <xsl:value-of select="$assignment/title"/>
          </h4>
          <xsl:value-of select="umbraco.library:RenderMacroContent($assignment/content, $assignment/@id)" disable-output-escaping="yes"/>
          <xsl:if test="$assignment/downloadLinks/media/item/@title != ''">

            <footer>
              <xsl:call-template name="downloadLinks">
                <xsl:with-param name="createArticleButtonBool" select="0"></xsl:with-param>
                <xsl:with-param name="items" select="$assignment/downloadLinks/media/item"></xsl:with-param>
                <xsl:with-param name="createArticleBool" select="$assignment/createArticleButton"></xsl:with-param>
              </xsl:call-template>
            </footer>

          </xsl:if>
        </div>
      </section>
    </xsl:when>
    <xsl:otherwise>
      
    </xsl:otherwise>

  </xsl:choose>
	

</xsl:template>

</xsl:stylesheet>