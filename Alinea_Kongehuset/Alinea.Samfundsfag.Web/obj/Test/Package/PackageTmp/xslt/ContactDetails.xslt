<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="info" select="$currentPage/ancestor-or-self::*/Folder [@nodeName='Indstillinger']/ContactDetails"></xsl:variable>
	
<xsl:template match="/">

	<p>
		<strong>Kontakt Alinea</strong>
	</p>
	<div class="vcard">
		<div class="tel">
			<span class="type">
				Tlf:
			</span>
			<xsl:value-of select="$info/telephone"/>
		</div>
		<div class="tel">
			<span class="type">
				Fax:
			</span>
			<xsl:value-of select="$info/faxNumber"/>
		</div>
		E-mail:
		<a class="email">
			<xsl:attribute name="href">
				mailto:<xsl:value-of select="$info/emailAddress"/>
			</xsl:attribute>
			<xsl:value-of select="$info/emailAddress"/>
		</a>
		<br></br>
		Web:
		<a class="fn url" target="_blank">
			<xsl:attribute name="href">
				<xsl:value-of select="$info/website"/>
			</xsl:attribute>
			<xsl:value-of select="$info/website"/>
		</a>
		<br></br>
		<br></br>
		<div class="adr">
			<div class="street-address">
				<xsl:value-of select="$info/streetAddress"/>
			</div>
			<span class="region">
				<xsl:value-of select="$info/region"/>
			</span>,
			<span class="postal-code">
				<xsl:value-of select="$info/postalCode"/>
			</span>,
			<span class="country-name">
				<xsl:value-of select="$info/country"/>
			</span>
		</div>
	</div>

</xsl:template>

</xsl:stylesheet>