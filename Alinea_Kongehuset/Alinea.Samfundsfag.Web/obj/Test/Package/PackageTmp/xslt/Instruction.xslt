<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;">
													 <!ENTITY copy "&#x00A9;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:include href="Templates.xslt"/>
<xsl:include href="TeacherTemplates.xslt"/>
	
<xsl:template match="/">

	<header class="heading">
		<h1>
			Lærervejledning 
			<small>
				- 
				<xsl:value-of select="$currentPage/title"/>
			</small>
		</h1>
		<a href="#print" class="print">Print</a>
	</header>

	<div class="entry article grid-2 grid-first">
		
		<xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags($currentPage/content), $currentPage/@id)" disable-output-escaping="yes"/>
	
		<footer>
			<p>
				<xsl:value-of select="$currentPage/copyright"/>
			</p>

			<xsl:call-template name="back-button">
				<xsl:with-param name="parentId" select="$currentPage/@parentID"></xsl:with-param>
			</xsl:call-template>
		
		</footer>
	</div>

	<aside class="grid-1">
    <div class="heading">
			<xsl:if test="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesHeadline != '' and $currentPage/Objectives/@id != ''">
				<h4>
					<xsl:value-of select="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesHeadline"/>
				</h4>
			</xsl:if>
		</div>
    <xsl:if test="$currentPage/Objectives/@id != ''">
		<section class="content-box important simple">
			<div class="entry">
				<xsl:if test="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesText != ''">
					<p>
						<xsl:value-of select="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesText"/>
					</p>
				</xsl:if>
				<xsl:if test="$currentPage/Objectives/@id != ''">
					<a class="button significant lb-content">
						<xsl:attribute name="href">
							<xsl:value-of select="umbraco.library:NiceUrl($currentPage/Objectives/@id)"/>
						</xsl:attribute>
						Se trinmål
					</a>
				</xsl:if>
			</div>
		</section>
    </xsl:if>

		<xsl:variable name="allPresentations" select="$currentPage/ancestor-or-self::*/AllPresentations"/>
		<xsl:call-template name="allPresentations">
			<xsl:with-param name="allPresentations" select="$allPresentations"/>
		</xsl:call-template>

		<xsl:variable name="classMessage" select="$currentPage/ancestor-or-self::*/ClassMessage"/>
		<xsl:call-template name="classMessage">
			<xsl:with-param name="classMessage" select="$classMessage"/>
		</xsl:call-template>

		<xsl:variable name="classPoll" select="$currentPage/ancestor-or-self::*/ClassPoll"/>
		<xsl:call-template name="classPoll">
			<xsl:with-param name="classPoll" select="$classPoll"/>
		</xsl:call-template>
		
	</aside>

</xsl:template>

</xsl:stylesheet>