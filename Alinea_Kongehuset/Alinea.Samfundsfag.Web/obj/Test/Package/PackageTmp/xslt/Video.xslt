<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="macro" select="/macro"></xsl:variable>
<xsl:variable name="video" select="$macro/Video/File"></xsl:variable>
<xsl:variable name="placeholderImage" select="$macro/PlaceholderImage/Image"></xsl:variable>

	<xsl:template match="/">
		
	<div class="media vid full" data-width="590" data-height="332">
		<xsl:attribute name="data-src">
			<xsl:value-of select="$video/umbracoFile"/>
		</xsl:attribute>
		<a class="fallback" target="_blank">
			<xsl:attribute name="href">
				<xsl:value-of select="$video/umbracoFile"/>
			</xsl:attribute>
			<img alt="Video">
				<xsl:attribute name="src">
					<xsl:value-of select="$placeholderImage/umbracoFile"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$placeholderImage/@name"/>
				</xsl:attribute>
			</img>
		</a>
		<div class="video-js print-fallback vjs-default-skin">
			<img alt="Video" class="vjs-poster">
				<xsl:attribute name="src">
					<xsl:value-of select="$placeholderImage/umbracoFile"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$placeholderImage/@name"/>
				</xsl:attribute>
			</img>
		</div>
		<p class="caption">
			<xsl:value-of select="$macro/CiteText"/>
			<cite>
				Video: 
				<xsl:value-of select="$macro/Cite"/>
			</cite>
		</p>
	</div>

</xsl:template>

</xsl:stylesheet>