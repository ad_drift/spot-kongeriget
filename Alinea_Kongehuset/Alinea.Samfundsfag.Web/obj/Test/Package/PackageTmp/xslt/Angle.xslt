<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> <!ENTITY copy "&#x00A9;">
  <!ENTITY rarr "&#x2192;">
  <!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt" />

<xsl:param name="currentPage"/>
<xsl:variable name="parent" select="umbraco.library:GetXmlNodeById($currentPage/@parentID)"/>
	
<xsl:template match="/">
	
	<article class="content wrapper" role="main">
		<div class="entry article grid-2">
			<header>
				<h3>
					<xsl:value-of select="$parent/title"/>
				</h3>
				<a href="#print" class="print">Print</a>
			</header>
			<h1>
				<xsl:value-of select="$currentPage/title"/>
			</h1>

			<xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags(samfundsfag:ConvertProfileTags($currentPage/content)), $currentPage/@id)" disable-output-escaping="yes"/>

			<footer>
				<p>
					&copy;
					<xsl:value-of select="$currentPage/copyright"/>
				</p>
				
				<xsl:call-template name="back-button">
					<xsl:with-param name="parentId" select="$currentPage/parent::*/@parentID"></xsl:with-param>
				</xsl:call-template>

				<xsl:if test="$currentPage/relatedLinks/links/link">
					<section class="content-box light links">
						<div class="entry">
							<h4>
								Relevante links
							</h4>
							<ul>
								<xsl:for-each select="$currentPage/relatedLinks/links/link">
									<xsl:variable name="this" select="umbraco.library:GetXmlNodeById(current()/@link)"/>

									<xsl:call-template name="relatedLinks">
										<xsl:with-param name="this" select="$this"></xsl:with-param>
									    <xsl:with-param name="link" select="current()"></xsl:with-param>
										<xsl:with-param name="newwindow" select="current()/@newwindow"></xsl:with-param>
										<xsl:with-param name="index" select="position()"></xsl:with-param>
									</xsl:call-template>

								</xsl:for-each>
							</ul>
						</div>
					</section>
				</xsl:if>
			</footer>
		</div>
		<aside class="grid-1">
			<nav class="aspects">
				<ul>
					<xsl:for-each select="$parent/Angle">
						<xsl:if test="current()/@id != $currentPage/@id">
							<li>
								<a>
									<xsl:attribute name="href">
										<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
									</xsl:attribute>
									<xsl:value-of select="current()/title"/>
								</a>
							</li>
						</xsl:if>
            <xsl:if test="current()/@id = $currentPage/@id">
              <li class="active">
                <a href="#aspect2">
                  <xsl:attribute name="href">
                    <xsl:value-of select="umbraco.library:NiceUrl($currentPage/@id)"/>
                  </xsl:attribute>
                  <xsl:value-of select="$currentPage/title"/>
                </a>
              </li>
            </xsl:if>
					</xsl:for-each>
				</ul>
			</nav>
			<xsl:if test="$currentPage/fact != ''">
				<section class="content-box important light fact"> 
          <xsl:variable name="fact" select="umbraco.library:GetXmlNodeById($currentPage/fact)"></xsl:variable>
					<xsl:call-template name="fact">
						<xsl:with-param name="fact" select="$fact"></xsl:with-param>
          </xsl:call-template>
				</section>
			</xsl:if>

			
			<section class="content-box important simple opinion news">

        <xsl:for-each select="samfundsfag:GetRelatedUserPolls($currentPage/@id)">
          <article class="item">

            <aside>
              <nav class="paging">
                <p>X af x</p>
                <a class="button prev" href="#prev">&larr;</a>
                <a class="button next" href="#next">&rarr;</a>
              </nav>
            </aside>

            <xsl:call-template name="poll">
              <xsl:with-param name="pollXml" select="current()"/>
              <xsl:with-param name="title" select="'Klassens holdning'"/>
            </xsl:call-template>


          </article>
        </xsl:for-each>

        <xsl:for-each select="$currentPage/relatedPolls/links/link">
          <xsl:variable name="poll" select="umbraco.library:GetXmlNodeById(current()/@link)"/>
          <article class="item">

            <aside>
              <nav class="paging">
                <p>X af x</p>
                <a class="button prev" href="#prev">&larr;</a>
                <a class="button next" href="#next">&rarr;</a>
              </nav>
            </aside>

            <xsl:variable name="quizHeadline" select="$poll/headline"/>
            <xsl:variable name="quizBool" select="$poll/boolQuiz"/>
            <xsl:variable name="correctAnswer" select="$poll/correctAnswer"/>
            <xsl:choose>
              <!--checks if headline and quiz are enabled-->
              <xsl:when test ="$quizHeadline != '' and $quizBool = '1' and $correctAnswer != ''">
                <xsl:call-template name="poll">
                  <xsl:with-param name="pollXml" select="$poll" />
                  <xsl:with-param name="title" select="$poll/headline"/>
                  <xsl:with-param name="quizBool" select="$quizBool"/>
                  <xsl:with-param name="correctAnswer" select="$correctAnswer"/>
                </xsl:call-template>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="poll">
                  <xsl:with-param name="pollXml" select="$poll" />
                  <xsl:with-param name="title" select="'Din mening'"/>
                  <xsl:with-param name="quizBool" select="0"/>
                  <xsl:with-param name="correctAnswer" select="''"/>
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>


          </article>
        </xsl:for-each>
				
			</section>
		</aside>
	</article>


</xsl:template>

</xsl:stylesheet>