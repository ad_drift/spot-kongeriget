<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="templates.xslt"/>
	
<xsl:param name="currentPage"/>

<xsl:template match="/">

	<xsl:call-template name="profiles">
		<xsl:with-param name="profiles" select="$currentPage/Profile"/>
	</xsl:call-template>
	
	<footer class="entry media-footer">
		<xsl:call-template name="back-button">
			<xsl:with-param name="parentId" select="$currentPage/@parentID"/>
		</xsl:call-template>
	</footer>

</xsl:template>

	<xsl:template name="profiles">
		<xsl:param name="profiles"/>


			<div class="main-subject media-list">
				<ul>
					<xsl:for-each select="$profiles">
						<xsl:call-template name="showProfile">
							<xsl:with-param name="item" select="current()"></xsl:with-param>
						</xsl:call-template>
					</xsl:for-each>
				</ul>
			</div>
		
	</xsl:template>


	<xsl:template name="showProfile">
		<xsl:param name="item" />

		<li>
			<a>
				<xsl:attribute name="href">
					<xsl:value-of select="umbraco.library:NiceUrl($item/@id)"/>
				</xsl:attribute>
				<div class="item entry content-box media-image">
					<figure>
						<xsl:call-template name="renderImage">
							<xsl:with-param name="imageId" select="$item/smallImage"/>
						</xsl:call-template>
					</figure>
					<h3>
						<xsl:value-of select="$item/name"/>
					</h3>
				</div>
			</a>
		</li>
	</xsl:template>
	
</xsl:stylesheet>