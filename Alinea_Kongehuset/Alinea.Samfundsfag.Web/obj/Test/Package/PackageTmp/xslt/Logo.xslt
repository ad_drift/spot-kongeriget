<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
  
<xsl:template match="/">
	
<a class="logo">
	<xsl:if test="samfundsfag:IsInTeacherSection() = 'true' or (samfundsfag:IsLoggedInUserTeacher() = 'true' and samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent() != 'true' and samfundsfag:IsInStudentSection() != 'true')">
		<xsl:attribute name="class">			
			<xsl:value-of select="'logo teacher'"/>
		</xsl:attribute>
	</xsl:if>
  <xsl:attribute name="href">
	  <xsl:choose>
		  <xsl:when test="(samfundsfag:IsInTeacherSection() = 'true' and samfundsfag:IsLoggedInUserTeacher()) or (samfundsfag:IsInStudentSection() = 'true' and samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent())">
			  <xsl:value-of select="umbraco.library:NiceUrl($currentPage/ancestor-or-self::* [@level=2]/@id)"/>
		  </xsl:when>
		  <xsl:when test="(samfundsfag:IsInTeacherSection() != 'true' and samfundsfag:IsInStudentSection() != 'true' and samfundsfag:IsLoggedInUserStudentOrTeacherAsStudent())">
			  <xsl:value-of select="'/elev/'"/>
		  </xsl:when>
		  <xsl:when test="(samfundsfag:IsInTeacherSection() != 'true' and samfundsfag:IsInStudentSection() != 'true' and samfundsfag:IsLoggedInUserTeacher())">
			  <xsl:value-of select="'/laerer/'"/>
		  </xsl:when>
		  <xsl:otherwise>
			  <xsl:value-of select="umbraco.library:NiceUrl($currentPage/ancestor-or-self::* [@level=1]/@id)"/>
		  </xsl:otherwise>
	  </xsl:choose>
  </xsl:attribute>
  <em>Spot på</em>
  <h3>
    Kongeriget
  </h3>
</a>

<xsl:if test="$currentPage/@nodeType = 1209 or $currentPage/@nodeType = 1296 or $currentPage/@nodeType = 1183 or $currentPage/@nodeType = 1267 or $currentPage/@nodeType = 1310 or $currentPage/@nodeType = 1312 or $currentPage/@nodeType = 2926">
	<header class="print-only">
		<div class="print-logo">
			<img src="/images/ui/logo.png"></img>
		</div>
		<div class="print-titles">
			<h2>
				<xsl:value-of select="samfundsfag:GetBannerTitle($currentPage/@id)"/>
			</h2>
			<xsl:if test="$currentPage/@nodeType != 1310">
			  <h3>
				  <xsl:value-of select="umbraco.library:GetXmlNodeById($currentPage/@parentID)/title"/>
			  </h3>
			</xsl:if>
		</div>
	</header>
</xsl:if>

</xsl:template>

</xsl:stylesheet>

