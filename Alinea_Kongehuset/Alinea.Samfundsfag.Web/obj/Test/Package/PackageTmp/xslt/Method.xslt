<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt"/>
	
<xsl:param name="currentPage"/>

<xsl:template match="/">
	<article class="content wrapper" role="main">
		<div class="heading">
			<h1>
				<xsl:value-of select="$currentPage/title"/>
			</h1>
      <div class="subheader-wrapper">
      <xsl:choose>
        <xsl:when test="$currentPage/content !='' and $currentPage/contentHard !=''">
          <a href="#show" class="easy-button current">Let</a>
          <a href="#show" class="hard-button">Svær</a>
        </xsl:when>
        <!--<xsl:when test="$currentPage/content !=''">
          <a href="#show" class="easy-button current">Let</a>
        </xsl:when>
        <xsl:when test="$currentPage/contentHard !=''">
          <a href="#show" class="hard-button current">Svær</a>
        </xsl:when>-->
      </xsl:choose>
      </div>
      <a href="#print" class="print">Print</a>
			<!--
			<p class="dl-block">
				
				<xsl:call-template name="downloadLinks">
					<xsl:with-param name="items" select="$currentPage/downloadLinks/media/item"></xsl:with-param>
					<xsl:with-param name="hideHeadline" select="true"></xsl:with-param>
				</xsl:call-template>
				
			</p>
			-->
		</div>
		<div class="method article entry grid-2 grid-first">
      
			<!--<xsl:value-of select="samfundsfag:ConvertAbbreviationTags($currentPage/content)" disable-output-escaping="yes"/>-->
      <!--Makes it possible to render macro in content-->
      <xsl:choose>
        <xsl:when test="$currentPage/content !='' and $currentPage/contentHard !=''">
          <div class="easy-method article entry current">
            <xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags(samfundsfag:ConvertProfileTags($currentPage/content)), $currentPage/@id)" disable-output-escaping="yes"/>
          </div>
          <div class="hard-method article entry">
            <xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags(samfundsfag:ConvertProfileTags($currentPage/contentHard)), $currentPage/@id)" disable-output-escaping="yes"/>
          </div>
        </xsl:when>
        <xsl:when test="$currentPage/content !=''">
          <div class="easy-method article entry current">
            <xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags(samfundsfag:ConvertProfileTags($currentPage/content)), $currentPage/@id)" disable-output-escaping="yes"/>
          </div>
        </xsl:when>
        <xsl:when test="$currentPage/contentHard !=''">
          <div class="hard-method article entry current">
            <xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags(samfundsfag:ConvertProfileTags($currentPage/contentHard)), $currentPage/@id)" disable-output-escaping="yes"/>
          </div>
        </xsl:when>
      </xsl:choose>
		</div>

		<xsl:if test="$currentPage/image != ''">
			<div class="method article entry grid-1">
				<xsl:variable name="image" select="umbraco.library:GetMedia($currentPage/image, 0)"></xsl:variable>
				<figure>
					<img>
						<xsl:attribute name="src">
							<xsl:value-of select="samfundsfag:ResolveUrl($image/umbracoFile)"/>
						</xsl:attribute>
						<xsl:attribute name="alt">
							<xsl:value-of select="$image/@nodeName"/>
						</xsl:attribute>
					</img>
				</figure>
			</div>
		</xsl:if>


		<xsl:if test="count($currentPage/downloadLinks/media/item) &gt; 0">
			<div class="method article entry grid-1">
				<section class="content-box important light download">
					<div class="entry">
						<h4>Hent</h4>
						<ul>
							<xsl:for-each select="$currentPage/downloadLinks/media/item">
								<li>
									<a>
										<xsl:variable name="item" select="umbraco.library:GetMedia(current()/@id, 0)"/>
										<xsl:attribute name="href">
											<xsl:value-of select="'/download.ashx?file='"/>
											<xsl:value-of select="$item/umbracoFile"/>
										</xsl:attribute>
										<xsl:value-of select="$item/@nodeName"/>
									</a>
								</li>
							</xsl:for-each>
						</ul>
					</div>
				</section>
			</div>
		</xsl:if>
		
		<footer class="method">
			<xsl:call-template name="back-button">
				<xsl:with-param name="parentId" select="$currentPage/@parentID"></xsl:with-param>
			</xsl:call-template>
		</footer>
	</article>

</xsl:template>

</xsl:stylesheet>