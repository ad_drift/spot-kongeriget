<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

	<article class="content wrapper bio" role="main">
		<div class="entry">
			<div class="grid-1 grid-first">
				<figure>
					<img>
						<xsl:if test="number($currentPage/image) = $currentPage/image">
							<xsl:variable name="image" select="umbraco.library:GetMedia($currentPage/image, 0)"/>
							<xsl:attribute name="src">
								<xsl:value-of select="samfundsfag:ResolveUrl($image/umbracoFile)"/>
							</xsl:attribute>
							<xsl:attribute name="alt">
								<xsl:value-of select="$image/@nodeName"/>
							</xsl:attribute>
						</xsl:if>
					</img>
					<h3>
						<xsl:value-of select="$currentPage/name"/>
					</h3>
				</figure>
			</div>
			<div class="grid-2">
				<div class="essentials">
					<h2>
						<xsl:value-of select="$currentPage/name"/>
						<span class="birthday">
							<xsl:value-of select="$currentPage/dateOfBirth"/>
						</span>
					</h2>
					<xsl:value-of select="$currentPage/experience" disable-output-escaping="yes"/>
				</div>
				<div class="bio-col first">
					<xsl:value-of select="$currentPage/leftColumn" disable-output-escaping="yes"/>
				</div>
				<div class="bio-col">
					<xsl:value-of select="$currentPage/rightColumn" disable-output-escaping="yes"/>
				</div>
			</div>
			<footer>
				<a href="#back" class="button discreet back">Tilbage</a>
			</footer>
		</div>
	</article>

</xsl:template>

</xsl:stylesheet>