<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
	
<xsl:template match="/">

	<p>
		<strong>
			Kontaktpersoner
		</strong>
	</p>
	<xsl:for-each select="$currentPage/ancestor-or-self::*/Folder [@nodeName='Kontakter']/* [@isDoc]">
		<div class="vcard">
			<span class="fn n">
				<span class="name">
					<xsl:value-of select="current()/@nodeName"/>
				</span>
			</span>,
			<span class="position">
				<xsl:value-of select="current()/position"/>
			</span>,
			<span class="division">
				<xsl:value-of select="current()/division"/>
			</span>
			<br></br>
			<a class="email">
				<xsl:variable name="emailAddress" select="current()/emailAddress"/>
				<xsl:attribute name="href">
					mailto:<xsl:value-of select="emailAddress"/>
				</xsl:attribute>
				<xsl:value-of select="emailAddress"/>
			</a>
		</div>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>