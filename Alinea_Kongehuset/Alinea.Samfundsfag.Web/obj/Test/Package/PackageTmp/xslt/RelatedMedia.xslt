<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#x00A0;">
]>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library"
	exclude-result-prefixes="msxml umbraco.library">


  <xsl:output method="xml" omit-xml-declaration="yes" />

  <xsl:param name="currentPage"/>
  
  <!-- Input the related links property alias here -->
  <xsl:variable name="propertyAlias" select="string('relatedmedia')"/>
  
  <xsl:template match="/">

    <!-- The fun starts here -->
    <ul>
      <xsl:for-each select="$currentPage/data [@alias = $propertyAlias]/media/item">
        <li>
          <xsl:element name="a">
						<xsl:attribute name="href">
							<xsl:value-of select="umbraco.library:GetMedia(./@id,'false')/data [@alias = 'umbracoFile']"/>
						</xsl:attribute>
            <xsl:value-of select="./@title"/>
          </xsl:element>
        </li>
      </xsl:for-each>
    </ul>

  </xsl:template>

</xsl:stylesheet>