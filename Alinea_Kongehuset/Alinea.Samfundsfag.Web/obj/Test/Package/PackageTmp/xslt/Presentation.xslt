<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#x00A0;">
	<!ENTITY copy "&#x00A9;">
]>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag"
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


	<xsl:output method="xml" omit-xml-declaration="yes"/>

	<xsl:param name="currentPage"/>

	<xsl:include href="Templates.xslt"/>
	<xsl:include href="TeacherTemplates.xslt"/>

	<xsl:template match="/">

		<header class="heading">
			<h1>
				Prøveoplæg
				<small>
					-
					<xsl:value-of select="$currentPage/title"/>
				</small>
			</h1>
			<a href="#print" class="print">Print</a>
		</header>

		<div class="entry article grid-2 grid-first">

			<xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags($currentPage/content), $currentPage/@id)" disable-output-escaping="yes"/>

			<footer>
				<p>
					<xsl:value-of select="$currentPage/copyright"/>
				</p>

				<xsl:call-template name="back-button">
					<xsl:with-param name="parentId" select="$currentPage/@parentID"></xsl:with-param>
				</xsl:call-template>

			</footer>
		</div>

		<aside class="grid-1">
			<section class="content-box important light download">
				<div class="entry">
					<h4>Hent</h4>
					<ul>
						<xsl:for-each select="$currentPage/downloads/media/item">
							<li>
								<a>
									<xsl:variable name="item" select="umbraco.library:GetMedia(current()/@id, 0)"/>
									<xsl:attribute name="href">
										<xsl:value-of select="'/download.ashx?file='"/>
										<xsl:value-of select="$item/umbracoFile"/>
									</xsl:attribute>
									<xsl:value-of select="$item/@nodeName"/>
								</a>
							</li>
						</xsl:for-each>
					</ul>
				</div>
			</section>
		</aside>

	</xsl:template>

</xsl:stylesheet>