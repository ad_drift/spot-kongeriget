<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;">
	<!ENTITY rarr "&#x2192;">
	<!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt" />

<xsl:param name="currentPage"/>

<xsl:template match="/">

	<div class="heading">
		<h2>Spot</h2>
		<a>
			<xsl:attribute name="href">
				<xsl:value-of select="umbraco.library:NiceUrl($currentPage/ancestor-or-self::*/StudentMaster/SingleCol [@nodeName='Spotlisten']/@id)"/>
			</xsl:attribute>
			
			<xsl:value-of select="'Gå til Spotlisten'"/>
		</a>
	</div>
	<section class="content-box important simple opinion news">

		<xsl:for-each select="$currentPage/ancestor-or-self::*/StudentMaster/SingleCol [@nodeName='Spotlisten']/* [@isDoc and showOnStudent = '1']">
      <xsl:sort select="position()" data-type="number" order="descending"/>
			<article class="item">
				<aside>
				  <nav class="paging">
					<p>x af x</p>
					<a class="button prev" href="#prev">&larr;</a>
					<a class="button next" href="#next">&rarr;</a>
				  </nav>
				</aside>
		
				<a class="entry">
					<xsl:attribute name="href">
						<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
					</xsl:attribute>
					<h3>
						<xsl:value-of select="current()/title"/>
					</h3>
					<xsl:variable name="smallIimage" select="umbraco.library:GetMedia(current()/smallImage, 0)"/>
					<img width="400" height="300">
						<xsl:attribute name="src">
							<xsl:value-of select="$smallIimage/umbracoFile"/>
						</xsl:attribute>
						<xsl:attribute name="alt">
							<xsl:value-of select="$smallIimage/@nodeName"/>
						</xsl:attribute>
					</img>
					<p>
						<xsl:value-of select="current()/promotionText"/>
					</p>
					<xsl:if test="current()/linkText != ''">
						<p>
							<em>
								<xsl:value-of	select="current()/linkText"/>
							</em>
						</p>
					</xsl:if>
				</a>
        <xsl:if test="current()/poll != ''">
				<xsl:variable name="poll" select="umbraco.library:GetXmlNodeById(current()/poll)"/>
        <xsl:variable name="quizBool" select="$poll/boolQuiz"/>
        <xsl:variable name="autoUpdate" select="$poll/autoUpdate"/>
        <!--checks if headline and quiz are enabled-->
        <xsl:if test ="$quizBool != '1'">
				  <xsl:call-template name="poll">
					  <xsl:with-param name="pollXml" select="$poll"/>
					  <xsl:with-param name="title" select="'Din mening'"/>
            <xsl:with-param name="quizBool" select="0"/>
            <xsl:with-param name="correctAnswer" select="''"/>
            <xsl:with-param name="autoUpdate" select="$autoUpdate"/>
				  </xsl:call-template>
          </xsl:if>
        </xsl:if>
			</article>
			
		</xsl:for-each>
		
	</section>

</xsl:template>

</xsl:stylesheet>