<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:msxml="urn:schemas-microsoft-com:xslt"
  xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
  exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:template match="/">

<div class="short-texts">
	
	<xsl:for-each select="$currentPage/Folder/Perspective">
		
		<article>
			<xsl:attribute name="id">
				<xsl:value-of select="current()/@urlName"/>
			</xsl:attribute>
			<div>
				<h4>
					<xsl:value-of select="current()/title"/>
				</h4>
				<xsl:value-of select="current()/description" disable-output-escaping="yes"/>
				<p>
					<a class="lb-content">
						<xsl:attribute name="href">
							<xsl:value-of select="umbraco.library:NiceUrl(current()/@id)"/>
						</xsl:attribute>
						Læs mere
					</a>
				</p>
			</div>
		</article>

	</xsl:for-each>

</div>
	
</xsl:template>

</xsl:stylesheet>