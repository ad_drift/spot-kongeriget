<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="macro" select="/macro"></xsl:variable>
<xsl:variable name="audio" select="$macro/Audio/File"></xsl:variable>

	<xsl:template match="/">
		<div class="media aud">
			<xsl:attribute name="data-src">
				<xsl:value-of select="$audio/umbracoFile"/>
			</xsl:attribute>
			<a class="fallback" target="_blank">
				<xsl:attribute name="href">
					<xsl:value-of select="$audio/umbracoFile"/>
				</xsl:attribute>
				<img alt="Audio">
					<xsl:attribute name="src">
						<xsl:value-of select="samfundsfag:ResolveUrl('/images/content/audio-placeholder.png')"/>
					</xsl:attribute>
				</img>
			</a>
			<p class="caption">
				<xsl:value-of select="$macro/CiteText"/>
				<cite>
					Lydklip:
					<xsl:value-of select="$macro/Cite"/>
				</cite>
			</p>
		</div>

		<!--<div class="media aud" data-src="../media/audio.mp3">
			<a href="../media/audio.mp3" class="fallback" target="_blank">
				<img src="../images/content/audio-placeholder.png" alt="Audio" />
			</a>
			<p class="caption">
				Her kan du lytte til radiointerview med Joachim B. Olsen <cite>Lydklip: Danmarks Radio</cite>
			</p>
		</div>-->
</xsl:template>

</xsl:stylesheet>