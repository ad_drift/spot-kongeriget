<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>
<xsl:variable name="title" select="samfundsfag:GetBannerTitle($currentPage/@id)"/>
<xsl:variable name="introText" select="samfundsfag:GetIntroText($currentPage/@id)"/>
<xsl:variable name="bannerImage" select="umbraco.library:GetMedia(samfundsfag:GetBannerImage($currentPage/@id), 0)"/>

<xsl:variable name="bannerVideoId" select="$currentPage/bannerVideo"/>
	
	<xsl:template match="/">
		<xsl:choose>
			<xsl:when test="$bannerVideoId != ''">
				<xsl:variable name="bannerVideo" select="umbraco.library:GetMedia($bannerVideoId, 0)"/>
				<xsl:choose>
					<xsl:when test="$bannerVideo/error != ''">
						<xsl:variable name="bannerDetailedVideo" select="umbraco.library:GetXmlNodeById($bannerVideoId)"/>
						<xsl:call-template name="bannerWithDetailedVideo">
							<xsl:with-param name="detailedVideo" select="$bannerDetailedVideo"></xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="bannerWithVideo">
							<xsl:with-param name="video" select="$bannerVideo"></xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="$currentPage/video != ''">
						<xsl:call-template name="bannerVideoChapters"/>
					</xsl:when>
					<xsl:when test="$title != ''">
						<xsl:call-template name="bannerWithoutVideo"></xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="bannerSmall"></xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>

</xsl:template>

	<xsl:template name="bannerWithoutVideo">
		<section class="banner" role="banner">
			<div class="wrapper">
				<div class="box">
					<h2>
						<xsl:value-of select="$title"/>
					</h2>
					<h6>
						<xsl:value-of select="$introText"/>
					</h6>
				</div>
			</div>
			<img>
				<xsl:attribute name="src">
					<xsl:value-of select="samfundsfag:ResolveUrl($bannerImage/umbracoFile)"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$bannerImage/@nodeName"/>
				</xsl:attribute>
			</img>
		</section>
	</xsl:template>

	<xsl:template name="bannerWithVideo">
		<xsl:param name="video"/>
		<section class="banner video" role="banner">
			<div class="wrapper">
				<div class="media vid" data-width="720" data-height="370">
					<xsl:attribute name="data-src">
						<xsl:value-of select="$video/umbracoFile"/>
					</xsl:attribute>
					<a class="fallback" target="_blank">
						<xsl:attribute name="href">
							<xsl:value-of select="$video/umbracoFile"/>
						</xsl:attribute>
            <xsl:choose>
              <xsl:when test="$currentPage/videoPlaceholderImage != ''">
                <img alt="Video">
                  <xsl:variable name="placeholderImage" select="umbraco.library:GetMedia($currentPage/videoPlaceholderImage, 0)"/>
                  <xsl:attribute name="src">
                    <xsl:value-of select="$placeholderImage/umbracoFile"/>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="$placeholderImage/@nodeName"/>
                  </xsl:attribute>
                </img>
              </xsl:when>
              <xsl:otherwise>
                <img class="vjs-poster" src="/img/videolaceholder__black.jpg" alt="Video"></img>
              </xsl:otherwise>
            </xsl:choose>
					</a>
					<div class="box">
						<h2>
							<xsl:value-of select="$title"/>
						</h2>
						<h6>
							<xsl:value-of select="$introText"/>
						</h6>
					</div>
				</div>
			</div>
			<img>
				<xsl:attribute name="src">
					<xsl:value-of select="samfundsfag:ResolveUrl($bannerImage/umbracoFile)"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$bannerImage/@nodeName"/>
				</xsl:attribute>
			</img>
		</section>
	</xsl:template>

	<xsl:template name="bannerWithDetailedVideo">
		<xsl:param name="detailedVideo"></xsl:param>
    
		
		<section class="banner video cut" role="banner">
			<div class="wrapper">
				<div class="box">
					<h2>
						<xsl:value-of select="$title"/>
					</h2>
					<h6>
						<xsl:value-of select="$introText"/>
					</h6>
				</div>
				<div class="media vid" data-width="720" data-height="370">
          <xsl:choose>
            <xsl:when test="$detailedVideo/video != ''">
              <xsl:variable name="video" select="umbraco.library:GetMedia($detailedVideo/video, 0)"/>
              <xsl:attribute name="data-src">
                <xsl:value-of select="$video/umbracoFile"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="data-src">
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
					<a class="fallback" target="_blank">
            <xsl:choose>
              <xsl:when test="$detailedVideo/video != ''">
                <xsl:variable name="video" select="umbraco.library:GetMedia($detailedVideo/video, 0)"/>
                <xsl:attribute name="href">
                  <xsl:value-of select="$video/umbracoFile"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="href">
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            
            <xsl:choose>
              <xsl:when test="$currentPage/videoPlaceholderImage != ''">
                <img alt="Video">
                  <xsl:variable name="placeholderImage" select="umbraco.library:GetMedia($currentPage/videoPlaceholderImage, 0)"/>
                  <xsl:attribute name="src">
                    <xsl:value-of select="$placeholderImage/umbracoFile"/>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    <xsl:value-of select="$placeholderImage/@nodeName"/>
                  </xsl:attribute>
                </img>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$detailedVideo/video != ''">
                    <img class="vjs-poster" src="/img/videolaceholder__black.jpg" alt="Video"></img>
                  </xsl:when>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
					</a>
					<div class="info">
            <xsl:if test="$detailedVideo/debate != ''">
						  <div class="item entry">
							  <h2>Før I går i gang</h2>
							  <xsl:value-of select="samfundsfag:ConvertAbbreviationTags($detailedVideo/debate)" disable-output-escaping="yes"/>
						  </div>
            </xsl:if>
						<div class="subject item entry">
							<h2>Emnemål</h2>
							<xsl:value-of select="samfundsfag:ConvertAbbreviationTags($detailedVideo/goals)" disable-output-escaping="yes"/>
						</div>
            <xsl:if test="$detailedVideo/concepts != ''">
              <div class="item entry">
                <h2>Begreber</h2>
                <xsl:value-of select="samfundsfag:ConvertAbbreviationTags($detailedVideo/concepts)" disable-output-escaping="yes"/>
              </div>
            </xsl:if>
					</div>
				</div>
				<a href="#intro" class="die" data-alttext="Skjul intro">Se intro</a>
			</div>
			<img width="1200" height="300">
				<xsl:attribute name="src">
					<xsl:value-of select="samfundsfag:ResolveUrl($bannerImage/umbracoFile)"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$bannerImage/@nodeName"/>
				</xsl:attribute>
			</img>
		</section>
     

    
	</xsl:template>

	<xsl:template name="bannerSmall">
		<section class="banner small" role="banner">
			<img width="1200" height="300">
				<xsl:attribute name="src">
					<xsl:value-of select="samfundsfag:ResolveUrl($bannerImage/umbracoFile)"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$bannerImage/@nodeName"/>
				</xsl:attribute>
			</img>
		</section>
	</xsl:template>

	<xsl:template name="bannerVideoChapters">
		<xsl:variable name="video" select="umbraco.library:GetMedia($currentPage/video, 0)"/>
		<section class="banner video cut" role="banner">
			<div class="wrapper">
				<div class="box">
					<h2>
						<xsl:value-of select="$title"/>
					</h2>
					<h6>
						<xsl:value-of select="$introText"/>
					</h6>
				</div>
        <div class="media vid" data-width="720" data-height="370">
					<xsl:attribute name="data-src">
						<xsl:value-of select="$video/umbracoFile"/>
					</xsl:attribute>
					<a class="fallback" target="_blank">
						<xsl:attribute name="href">
							<xsl:value-of select="$video/umbracoFile"/>
						</xsl:attribute>
						<img>
							<xsl:variable name="placeholderImage" select="umbraco.library:GetMedia($currentPage/videoPlaceholderImage, 0)"/>
							<xsl:attribute name="src">
								<xsl:value-of select="$placeholderImage/umbracoFile"/>
							</xsl:attribute>
							<xsl:attribute name="alt">
								<xsl:value-of select="$placeholderImage/@nodeName"/>
							</xsl:attribute>
						</img>
					</a>
					<div class="info">
						<div class="item entry">
							<h2>
								<xsl:value-of select="$currentPage/title"/>
							</h2>
							<xsl:value-of select="umbraco.library:RenderMacroContent(samfundsfag:ConvertAbbreviationTags($currentPage/content), $currentPage/@id)" disable-output-escaping="yes"/>
						</div>
					</div>
				</div>
				<a href="#intro" class="die" data-alttext="Skjul intro">Se intro</a>
			</div>
			<img width="1200" height="300">
				<xsl:attribute name="src">
					<xsl:value-of select="samfundsfag:ResolveUrl($bannerImage/umbracoFile)"/>
				</xsl:attribute>
				<xsl:attribute name="alt">
					<xsl:value-of select="$bannerImage/@nodeName"/>
				</xsl:attribute>
			</img>
		</section>
	</xsl:template>
	
</xsl:stylesheet>