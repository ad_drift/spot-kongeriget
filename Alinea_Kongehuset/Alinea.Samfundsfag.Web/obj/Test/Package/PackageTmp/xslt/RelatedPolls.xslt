<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;">
	<!ENTITY rarr "&#x2192;">
	<!ENTITY larr "&#x2190;">
]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:include href="Templates.xslt" />

<xsl:param name="currentPage"/>

<xsl:template match="/">

	<div class="heading">
		<h2>Svar</h2>
	</div>
	<section class="content-box important simple opinion news">
		
		<xsl:for-each select="samfundsfag:GetRelatedUserPolls($currentPage/@id)">
			<article class="item">

        <aside>
          <nav class="paging">
            <p>X af x</p>
            <a class="button prev" href="#prev">&larr;</a>
            <a class="button next" href="#next">&rarr;</a>
          </nav>
        </aside>
        <xsl:variable name="quizHeadline" select="current()/headline"/>
        <xsl:variable name="quizBool" select="current()/boolQuiz"/>
        <xsl:variable name="autoUpdate" select="current()/autoUpdate"/>
        <xsl:variable name="correctAnswer" select="current()/correctAnswer"/>
        <xsl:choose>
          <!--checks if headline and quiz are enabled-->
          <xsl:when test ="$quizHeadline != '' and $quizBool = '1' and $correctAnswer != ''">
            <xsl:call-template name="poll">
              <xsl:with-param name="pollXml" select="current()" />
              <xsl:with-param name="title" select="current()/headline"/>
              <xsl:with-param name="quizBool" select="$quizBool"/>
              <xsl:with-param name="autoUpdate" select="$autoUpdate"/>
              <xsl:with-param name="correctAnswer" select="$correctAnswer"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="poll">
              <xsl:with-param name="pollXml" select="current()" />
              <xsl:with-param name="title" select="'Din mening'"/>
              <xsl:with-param name="quizBool" select="0"/>
              <xsl:with-param name="autoUpdate" select="$autoUpdate"/>
              <xsl:with-param name="correctAnswer" select="''"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
        
        <!--<xsl:call-template name="poll">
					<xsl:with-param name="pollXml" select="current()"/>
					<xsl:with-param name="title" select="'Klassens holdning'"/>
          <xsl:with-param name="quizBool" select="current()/boolQuiz"/>
          <xsl:with-param name="correctAnswer" select="current()/"/>
          <xsl:with-param name="autoUpdate" select="'Klassens holdning'"/>
				</xsl:call-template>-->

			
			</article>
		</xsl:for-each>
		
		<xsl:for-each select="$currentPage/relatedPolls/links/link">
			<xsl:variable name="poll" select="umbraco.library:GetXmlNodeById(current()/@link)"/>
			<article class="item">

        <aside>
          <nav class="paging">
            <p>X af x</p>
            <a class="button prev" href="#prev">&larr;</a>
            <a class="button next" href="#next">&rarr;</a>
          </nav>
        </aside>

        <xsl:variable name="quizHeadline" select="$poll/headline"/>
        <xsl:variable name="quizBool" select="$poll/boolQuiz"/>
        <xsl:variable name="correctAnswer" select="$poll/correctAnswer"/>
        <xsl:choose>
          <!--checks if headline and quiz are enabled-->
          <xsl:when test ="$quizHeadline != '' and $quizBool = '1' and $correctAnswer != ''">
            <xsl:call-template name="poll">
              <xsl:with-param name="pollXml" select="$poll" />
              <xsl:with-param name="title" select="$poll/headline"/>
              <xsl:with-param name="quizBool" select="$quizBool"/>
              <xsl:with-param name="correctAnswer" select="$correctAnswer"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="poll">
              <xsl:with-param name="pollXml" select="$poll" />
              <xsl:with-param name="title" select="'Din mening'"/>
              <xsl:with-param name="quizBool" select="0"/>
              <xsl:with-param name="correctAnswer" select="''"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
			
				
			</article>
		</xsl:for-each>
		
	</section>

</xsl:template>

</xsl:stylesheet>