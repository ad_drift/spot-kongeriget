<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Exslt.ExsltCommon="urn:Exslt.ExsltCommon" xmlns:Exslt.ExsltDatesAndTimes="urn:Exslt.ExsltDatesAndTimes" xmlns:Exslt.ExsltMath="urn:Exslt.ExsltMath" xmlns:Exslt.ExsltRegularExpressions="urn:Exslt.ExsltRegularExpressions" xmlns:Exslt.ExsltStrings="urn:Exslt.ExsltStrings" xmlns:Exslt.ExsltSets="urn:Exslt.ExsltSets" xmlns:samfundsfag="urn:samfundsfag" 
	exclude-result-prefixes="msxml umbraco.library Exslt.ExsltCommon Exslt.ExsltDatesAndTimes Exslt.ExsltMath Exslt.ExsltRegularExpressions Exslt.ExsltStrings Exslt.ExsltSets samfundsfag ">


<xsl:output method="html" omit-xml-declaration="yes"/>

<xsl:include href="templates.xslt"/>
	
<xsl:param name="currentPage"/>

<xsl:variable name="objectives" select="$currentPage/ancestor-or-self::*/Folder/ObjectiveSection"/>

<xsl:template match="/">
	
	<article class="content wrapper" role="main">
		<div class="entry">
			<xsl:if test="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesHeadline != ''">
				<h2>
					<xsl:value-of select="$currentPage/ancestor-or-self::*/TeacherMaster/objectivesHeadline"/>
				</h2>
			</xsl:if>
			<table class="goals" style="width:100%">
				<xsl:for-each select="$objectives">
					<thead>
						<tr>
							<th colspan="2">
								<xsl:value-of select="current()/subject"/>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th colspan="2">
								<xsl:value-of select="current()/area"/>
							</th>
						</tr>
						<xsl:for-each select="current()/* [@isDoc]">
							<tr>
								<xsl:if test="not(samfundsfag:ObjectiveIsCompleted($currentPage/@id, current()/@id))">
									<xsl:attribute name="style">
										<xsl:value-of select="'display:none;'"/>
									</xsl:attribute>
								</xsl:if>
								<td>
									<xsl:value-of select="current()/title"/>
								</td>
								<td style="width:20px;">
									<xsl:value-of select="samfundsfag:GetObjectiveTag($currentPage/@id, current()/@id)" disable-output-escaping="yes"/>
								</td>
							</tr>
						</xsl:for-each>
					</tbody>
				</xsl:for-each>
			</table>
			<a href="#" onclick="$(this).closest('.entry').find('tr').show(); $(this).remove(); return false;">Se alle</a>
		</div>
	</article>

</xsl:template>

</xsl:stylesheet>