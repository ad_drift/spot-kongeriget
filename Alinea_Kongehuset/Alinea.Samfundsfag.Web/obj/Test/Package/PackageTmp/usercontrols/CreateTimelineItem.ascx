﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateTimelineItem.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.CreateTimelineItem" %>
<%@ Register TagPrefix="uc" Namespace="Alinea.Samfundsfag.Web.usercontrols" Assembly="Alinea.Samfundsfag.Web" %>
<%-- <div class="sort-container timeline-item-container">
    <nav>
        <a href="#up" class="button up"></a>
        <a href="#down" class="button down"></a>
        <input type="hidden" class="sort-value" name="" value="" />
    </nav>
    <label for="">Label <a href="#timeline-help" class="help-toggle timeline-help-toggle"><span class="icon icon-info"></span></a></label>
    <p class="helptext timeline-help">
        <umbraco:Item ID="Item2" runat="server" Field="timelineHelp" />
    </p>
    <input type="text" name="timeline-item-label-<%= timelineItemIndex %>" placeholder="Skriv en label til dit tidslinjepunkt ..." value="<%= timelineHeadline %>" maxlength="10" />
    <label for="timeline">Overskrift <a href="#timeline-help" class="help-toggle timeline-help-toggle"><span class="icon icon-info"></span></a></label>
    <input type="text" name="timeline-item-title-<%= timelineItemIndex %>" placeholder="Skriv en overskrift til dit tidslinjepunkt ..." value="<%= timelineHeadline %>" maxlength="500" />
    <label for="timeline">Beskrivelse <a href="#timeline-help" class="help-toggle timeline-help-toggle"><span class="icon icon-info"></span></a></label>
    <textarea name="timeline-item-description-<%= timelineItemIndex %>" id="" class="rte-box rte-text"></textarea>
    <input type="file" name="" />
    <input type="hidden" id="Hidden1" name="timeline-item-<%= timelineItemIndex %>" value="<%= article.TimelineItems[timelineItemIndex].Id %>" />
</div>--%>
<div class="sort-container timeline-item-container">
    <nav>
        <a href="#up" class="button up"></a>
        <a href="#down" class="button down"></a>
        <input type="hidden" class="sort-value" name="" value="" />
    </nav>
    <asp:Repeater ID="RepeaterTimelineItems" runat="server">
        <ItemTemplate>
            <asp:TextBox ID="TextBoxLabel" placeholder="Skriv en label til dit tidslinjepunkt ..." MaxLength="10" runat="server" />

            <%# DataBinder.Eval(Container.DataItem, "Id") %>
        </ItemTemplate>
    </asp:Repeater>
</div>
