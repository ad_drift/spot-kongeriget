﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShowMessages.ascx.cs"
	Inherits="Alinea.Samfundsfag.Web.usercontrols.ShowMessages" %>
<section class="messages" id="messages" data-servicedata="{id:''}" data-serviceurl="/services/Message.ashx">
	<span class="class-id"><asp:HiddenField runat="server" ID="hdnClassId"/></span>
	<span class="user-id"><asp:HiddenField runat="server" ID="hdnUserId"/></span>
	<asp:Repeater ID="rptMessages" runat="server">
		<ItemTemplate>
			<article data-messageid="<%# Eval("BoardMessageId") %>" class="<%# Eval("CssClass") %>">
				<header class="vcard">
					<time><%# Eval("Date") %></time>
					af
					<cite><%# Eval("TeacherName") %></cite>
				</header>
				<div class="entry">
					<%# Eval("Text") %>
				</div>
				<asp:Panel ID="pnlDownload" Visible='<%# Eval("DownloadAvailable") %>' runat="server">
					<footer><strong>Download</strong>
						<ul>
							<li><a target="_blank" href="<%# Eval("DownloadLink") %>"><%# Eval("DownloadName") %></a></li>
						</ul>
					</footer>
				</asp:Panel>
			</article>
		</ItemTemplate>
	</asp:Repeater>
	<footer>
		<nav class="paging">
			<p>
				x af x
			</p>
			<a class="button prev" href="#prev">&larr;</a> <a class="button next" href="#next">
				&rarr;</a>
		</nav>
	</footer>
</section>