﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClassPoll.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.ClassPoll" %>

<div class="content wrapper" role="main">
	<section class="entry form">
		<div class="heading">
			<h1>
				Klassens quiz/afstemning
			</h1>
		</div>
		<h2>
			Oversigt over oprettede quizzer/afstemninger</h2>
		<table class="edit alternate">
			<thead>
				<tr>
					<th>
						Spørgsmål
					</th>
					<th>
						Emne
					</th>
					<th>
						Dato
					</th>
					<th>
						Klasse
					</th>
					<th>
						Status
					</th>
					<th>
						Rediger
					</th>
					<th>
						Preview
					</th>
					<th>
						Slet
					</th>
				</tr>
			</thead>
			<tbody>
				<asp:Repeater ID="rptControlPolls" OnItemCommand="OnRptControlPollsItemCommand" runat="server">
					<ItemTemplate>
						<tr>
							<td>
								<%# Eval("Preview") %>&hellip;?
							</td>
							<td>
								<%# Eval("Topic") %>
							</td>
							<td>
								<time>
									<%# Eval("Date") %>
								</time>
							</td>
							<td>
								<%# Eval("ClassNames") %>
							</td>
							<td>
								<%# Eval("Status") %>
							</td>
							<td>
								<asp:LinkButton CommandName="Edit" CommandArgument='<%# Eval("PollId") %>' CssClass="edit" ID="lnbEdit" runat="server">
									Rediger
								</asp:LinkButton>
							</td>
							<td>
								<a href="#preview-<%# Eval("PollId") %>" class="preview">
									Preview
								</a>
							</td>
							<td>
								<asp:LinkButton CommandName="Delete" CommandArgument='<%# Eval("PollId") %>' CssClass="delete alert" data-alert="Er du sikker på at du vil slette denne afstemning?" ID="lnbDelete" runat="server">
									Slet?
								</asp:LinkButton>
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</tbody>
		</table>
		<section class="form-preview">
			<asp:Repeater ID="rptViewPolls" runat="server">
				<ItemTemplate>
					<section class="content-box important light opinion form-example" id="preview-<%# Eval("PollId") %>">
						<div class="entry">
							<h4>
								Klassens quiz/afstemning
							</h4>
							<p>
								<strong>
									<%# Eval("Question") %>
								</strong>
							</p>
							<ul class="options">
								<asp:Repeater ID="rptAnswers" DataSource='<%# Eval("Answers") %>' runat="server">
									<ItemTemplate>
									<li>
										<input type="radio" name="question1" id="c<%# Eval("AnswerId")%>" />
										<label for="c<%# Eval("AnswerId")%>">
											<%# Eval("Title")%>
										</label>
									</li>
									</ItemTemplate>
								</asp:Repeater>
							</ul>
							<a href="#answer" class="button discreet" data-service="../services/poll.json" data="{id:'kralimut'}">Svar</a>
						</div>
					</section>
				</ItemTemplate>
			</asp:Repeater>
		</section>
		<%--<a href="#form-poll" class="add button significant">Opret ny afstemning</a>--%>
		<asp:LinkButton ID="lnkNewPoll" CssClass="add button significant" OnClick="OnLnkNewPollClick" runat="server">
			Opret ny
		</asp:LinkButton>
		<asp:HiddenField ID="hdnPollId" runat="server" />
		<asp:Panel ID="pnlNewPoll" Visible="false" runat="server">
			<section id="form-poll" class="form-edit">
			<h2 class="grid-1 scrollTo">
				<asp:Label ID="lblCreatePollHeadline" runat="server" />
			</h2>
			<div class="grid-2">
                <fieldset class="checkbox">
			        <legend>0) Vælg afstemning eller quiz</legend>
                    <ul class="is-quiz">
                        <li>
                            <coreM41:GroupRadioButton ID="cbPoll" GroupName="quizOrPoll" Checked="true" runat="server" />
                            <label ID="poll-btn" for="<%:cbPoll.ClientID %>">Afstemning</label>
                            <asp:HiddenField ID="HiddenField1" Value='False' runat="server" />
                        </li>
                        <li>
                            <coreM41:GroupRadioButton ID="cbQuiz" GroupName="quizOrPoll" runat="server" />
                            <label ID="quiz-btn" for="<%:cbQuiz.ClientID %>">Quiz</label>
                            <asp:HiddenField ID="hdnClassId" Value='True' runat="server" />
                        </li>
                        
                    </ul>
			    </fieldset>
                <fieldset class="quizElement">
			        <legend>Overskrift på quiz</legend>
                    <ul>
                        <li>
							<asp:TextBox placeholder="Skriv overskrift her..." ID="txtHeadline" MaxLength="20" runat="server" />
                        </li>
                    </ul>
                    <asp:Label Visible="false" CssClass="feedback error" ID="lblHeadlineFeedback" runat="server" />
			    </fieldset>
				<fieldset class="checkbox">
					<legend>1) Vælg klasse</legend>
					<ul>
						<asp:Repeater ID="rptClasses" runat="server">
							<ItemTemplate>
								<li>
									<asp:CheckBox ID="cbClass" runat="server" />
									<label for="<%# ((RepeaterItem)Container).FindControl("cbClass").ClientID %>">
										<%# Eval("Name")%></label>
									<asp:HiddenField ID="hdnClassId" Value='<%# Eval("ClassId") %>' runat="server" />
								</li>
							</ItemTemplate>
						</asp:Repeater>
					</ul>
					<asp:Label Visible="false" CssClass="feedback error" ID="lblClassFeedback" runat="server" />
				</fieldset>
				<fieldset class="checkbox">
					<legend>2) Vælg emne</legend>
					<ul>
						<asp:Repeater ID="rptTopics" runat="server">
							<ItemTemplate>
								<li>
									<coreM41:GroupRadioButton ID="rbTopic" GroupName="topics" runat="server" />
									<asp:HiddenField Value='<%# Eval("PollTopicId") %>' ID="hdnTopicId" runat="server" />
									<label for="<%# ((RepeaterItem)Container).FindControl("rbTopic").ClientID %>">
										<%# Eval("Title") %>
									</label>
								</li>
							</ItemTemplate>
						</asp:Repeater>
					</ul>
					<asp:Label Visible="false" CssClass="feedback error" ID="lblTopicFeedback" runat="server" />
				</fieldset>
				<fieldset>
					<legend>3) Skriv spørgsmål</legend>
					<asp:TextBox ID="txtQuestion" TextMode="MultiLine" placeholder="Skriv dit spørgsmål her..." runat="server" />
					<asp:RequiredFieldValidator CssClass="feedback error" ID="rfvTxtQuestion" ValidationGroup="poll" ControlToValidate="txtQuestion" runat="server" />
				</fieldset>
				<fieldset>
					<legend>4) Tilføj op til 4 svarmuligheder</legend>
					<ul class="answer-list">
						<asp:Repeater ID="rptAnswers" runat="server">

							<ItemTemplate>
								<li>
									<label>Svarmulighed <%# rptAnswers.Items.Count+1 %></label>
									<asp:TextBox placeholder="Skriv dit svar her..." ID="txtAnswer" runat="server" />
									<asp:HiddenField ID="hdnAnswerId" runat="server" />
                                    <label class="quizElement">Markér, hvis dette er det rigtige svar på quizzen</label>
                                    <coreM41:GroupRadioButton ID="rbCorrectAnswer" class="quizElement" GroupName="correct_answer" runat="server"  />
                                    <%--Checked="<%# rptAnswers.Items.Count == 0 %>"--%>
								</li>
							</ItemTemplate>
						</asp:Repeater>
					</ul>
					<asp:Label CssClass="feedback error" Visible="false" ID="lblAnswerFeedback" runat="server" />
                    <asp:Label CssClass="feedback error" Visible="false" ID="lblCorrectAnswerFeedback" runat="server" />
				</fieldset>
				<fieldset>
					<legend>5) Vælg status</legend>
					<asp:DropDownList ID="ddlStatus" DataTextField="" DataValueField="" runat="server">
						<asp:ListItem Text="Under udarbejdelse" Value="0" />
						<asp:ListItem Text="Publiceret" Value="1" />
					</asp:DropDownList>
				</fieldset>
				<asp:Label Visible="false" CssClass="feedback" ID="lblCreatePollFeedback" runat="server" />
				<asp:Button CssClass="button significant" ValidationGroup="poll" Text="Gem afstemning" OnClick="OnBtnSubmitClick" ID="btnSubmit" runat="server" />
			</div>
		</section>
            <script>
                $(document).ready(function () {
                    App.Util.ScrollTo(".scrollTo");
                });
            </script>
		</asp:Panel>
		<footer>
			<a href="/laerer/" class="button discreet">Tilbage</a>
		</footer>
	</section>
</div>
