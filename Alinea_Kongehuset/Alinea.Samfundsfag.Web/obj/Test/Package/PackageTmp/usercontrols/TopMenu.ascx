﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopMenu.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.TopMenu" %>


<nav>
	<%--TEACHER VIEWING AS STUDENT--%>
	<%--<asp:LinkButton ID="lnbTeacherSection" Visible="false" OnClick="OnLnbTeacherSectionClick" CssClass="button" runat="server" Text="Lærersektionen"/>--%>
	<asp:LinkButton Text="Gå til lærersite" ID="lnbTeacherSection" Visible="false" OnClick="OnLnbTeacherSectionClick" CssClass="button" runat="server" />

	<%--TEACHER--%>
    <asp:Hyperlink ID="lnkTeacherTopics" NavigateUrl="/laerer/" Visible="false" CssClass="button" runat="server">Gå til emnerne</asp:Hyperlink>
	
	<asp:Label ID="lblSelectTeacherClasses" CssClass="teacher-label" Visible="false" AssociatedControlID="ddlTeacherLoginAs" runat="server" />
	<asp:DropDownList DataTextField="Name" DataValueField="ClassId" Visible="false" ID="ddlSelectTeacherClasses" CssClass="multiselect teacher-classes" data-text="Vælg..." runat="server" />
	
	<asp:Label ID="lblTeacherLoginAs" CssClass="teacher-label open-class-list" Visible="false" AssociatedControlID="ddlTeacherLoginAs" runat="server" />
	<asp:DropDownList DataTextField="Name" DataValueField="ClassId" Visible="false" ID="ddlTeacherLoginAs" AutoPostBack="true" OnDataBound="OnDdlTeacherLoginAsDataBound" OnSelectedIndexChanged="OnDdlTeacherLoginAsSelectedIndexChanged" runat="server"/>
		
	<asp:PlaceHolder runat="server" ID="phAutoOpenSelectTeacherClases" Visible="false">
	<script type="text/javascript">
		$(window).load(function ()
		{
			$('.ui-multiselect').trigger('click');
		});
	</script>
	</asp:PlaceHolder>
	
	<%--STUDENT--%>
    <asp:Hyperlink ID="lnkStudentTopics" NavigateUrl="/elev/" Visible="false" CssClass="button" runat="server">Gå til emnerne</asp:Hyperlink>
	
	<asp:PlaceHolder runat="server" ID="phMyPage" Visible="false">
		<a href="<%=linkMyPage %>" style="<%= Request.Path == "/" ? "margin-left:15px;" : "" %>" class="button get-mypage partial<% if(!Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.IsCurrentUserTeacher) { %> partial-left<% } %>" id="lnkMyPage">
			<%	   
				// --START-- TO BE REPLACED WITH A AJAX CALL TO /services/UserArticleNotifications.ashx ??
				var newComments = 0;
				var newArticles = 0;
				using(var uacs = new Alinea.Samfundsfag.BusinessLogic.Services.UserArticleCommentService())
				{
					using (var uas = new Alinea.Samfundsfag.BusinessLogic.Services.UserArticleService())
					{
						if (Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.HasUserClass)
						{
							newComments = uacs.GetUnreadCommentsCount(Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.CurrentUserClass.ClassId, Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.CurrentUserId);

							if (Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.IsCurrentUserTeacher)
								newArticles = uas.GetUnseenArticlesCount(Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.CurrentUserClass.ClassId, Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.CurrentUserId);
						}
					}
				}
				// --END-- TO BE REPLACED WITH A AJAX CALL TO /services/UserArticleNotifications.ashx ??
			%>
			<span class="notification<%= (newComments + newArticles) > 0 ? " new" : "" %>"><%=newComments + newArticles %></span>
			<%=linkMyPageText %>
		</a>
		<% if(!Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.IsCurrentUserTeacher) { %>
		<a href="<%=linkArticleList %>" class="button get-classforum partial partial-right" id="lnkClassForum"><%=linkArticleListText %></a>
		<% } %>
	</asp:PlaceHolder>
	<a href="#messages" class="button get-messages" visible="false" id="lnkMessages" runat="server">
		<span class="notification"><%--<asp:Literal ID="litStudentMessages" runat="server" />--%></span>
		Beskeder
	</a>

	<%--BOTH--%>
	<profiler:Macro ID="mcrSearchBox" Alias="SearchBox" Visible="false" runat="server" />

	<%--LOGIN--%>
	<asp:HyperLink ID="lnkLogin" CssClass="button" runat="server">Login</asp:HyperLink>

	<%--VISIBLE ON ALL PAGES--%>
	<profiler:Macro Alias="TopRightMenu" runat="server" />
</nav>
