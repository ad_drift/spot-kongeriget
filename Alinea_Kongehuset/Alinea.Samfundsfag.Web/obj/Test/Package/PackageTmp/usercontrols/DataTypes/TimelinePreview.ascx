﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TimelinePreview.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.DataTypes.TimelinePreview" %>

<%@ Register TagPrefix="uc" TagName="TimelineDataTypeShared" Src="~/usercontrols/DataTypes/TimelineDataTypeShared.ascx" %>

<div class="alinea-custom">
	<div class="alinea-tl-canvas">
		<div class="alinea-tl-items">
			<ol>
				<asp:Repeater ID="rptTimelinePreview" runat="server">
					<ItemTemplate>
						<li class="alinea-tl-item" data-position="<%# Eval("Position") %>%" data-label="<%# Eval("Label") %>"></li>
					</ItemTemplate>
				</asp:Repeater>
			</ol>
		</div>
	</div>
</div>

<uc:TimelineDataTypeShared runat="server" />



