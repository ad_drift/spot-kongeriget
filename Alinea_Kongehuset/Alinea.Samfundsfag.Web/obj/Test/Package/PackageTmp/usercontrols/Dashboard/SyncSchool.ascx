﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SyncSchool.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.Dashboard.SyncSchool" %>
<%@ Register TagPrefix="umb" Namespace="ClientDependency.Core.Controls" Assembly="ClientDependency.Core" %>

<umb:CssInclude ID="CssInclude1" runat="server" FilePath="propertypane/style.css" PathNameAlias="UmbracoClient" />

<div class="dashboardWrapper">
    <h2>
		Synkroniser skole
    </h2>
    <img src="./dashboard/images/school.png" alt="Grundsystemer" class="dashboardIcon" />
	<p>
		Her kan du køre en manuel synkronisering af en instutition.
	</p>
	<asp:Label ID="lblSchool" runat="server" AssociatedControlID="txtSchool" Text="Instutitionsnavn" />
	<asp:TextBox ID="txtSchool" runat="server" />
	<asp:Button ID="btnSyncSchool" runat="server" Text="Synkroniser" OnClick="OnBtnSyncSchoolClick" />
	<br /><br />
	<p>
		<asp:Literal ID="litFeedback" runat="server" />
	</p>
</div>