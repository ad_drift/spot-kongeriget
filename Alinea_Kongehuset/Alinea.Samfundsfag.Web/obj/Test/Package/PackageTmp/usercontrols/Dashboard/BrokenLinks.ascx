﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrokenLinks.ascx.cs" Inherits="Alinea.Samfundsfag.Web.usercontrols.Dashboard.BrokenLinks" %>

<asp:Repeater ID="repBrokenLinks" runat="server">
	<HeaderTemplate>
		<table>
			<thead>
				<th>Dødt link</th>
				<th>Reference side</th>
			</thead>
			<tbody>
	</HeaderTemplate>
	<ItemTemplate>
			<tr>
				<td>
					<%# Eval("BrokenUrl") %>
				</td>
				<td>
					<a href='<%# Eval("ReferrerUrl")%>?brokenlink=<%# Eval("BrokenUrl") %>&<%= CrawlerToken %>=1' target="_blank">
						<%# Eval("ReferrerUrl")%>
					</a>
				</td>
			</tr>
	</ItemTemplate>
	<FooterTemplate>
			</tbody>
		</table>
	</FooterTemplate>
</asp:Repeater>

<br />
<asp:Label ID="lblLatestCrawl" runat="server" />
<br />
<asp:Button ID="btnForceUpdate" OnClick="OnBtnForceUpdateClick" Text="Opdater døde links" runat="server" />
<br />
<asp:Label Text="* dette kan tage et par timer" runat="server" />
<br />
<asp:Label ID="lblStatus" runat="server" />