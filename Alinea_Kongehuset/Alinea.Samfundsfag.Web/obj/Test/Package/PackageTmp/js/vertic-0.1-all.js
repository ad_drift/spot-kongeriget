﻿/* Kraftvaerk.min.Scaffold for js */

window.App = window.App || {};

(function( $ ){
	App = {
		
		init : function() {

			//App.Util.ScrollTo();
	
		}

	};

	App.Util = App.Util || {
		
		ScrollTo: function(selector) {
						
    		var scrollTo = $(selector).offset().top;

			$("html, body").animate({

				scrollTop: scrollTo

			}, 1000);
		}
	};

	$(document).ready(App.init);
	
})( jQuery );

/****************** Kraftvaerk End ****************/

/*
 * Vertic JS utility library - Core
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Date: Tue Jan 31 12:00:00 2012 +0200
 */
/*jslint browser: true, vars: true, nomen: true, white: true*/
/*globals */

// JSON Decode / Encode - https://github.com/douglascrockford/JSON-js
if (window.JSON === undefined) {
	var JSON;JSON||(JSON={});
	(function(){function k(a){return 10>a?"0"+a:a}function o(a){p.lastIndex=0;return p.test(a)?'"'+a.replace(p,function(a){var c=r[a];return"string"===typeof c?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function m(a,j){var c,d,h,n,g=e,f,b=j[a];b&&"object"===typeof b&&"function"===typeof b.toJSON&&(b=b.toJSON(a));"function"===typeof i&&(b=i.call(j,a,b));switch(typeof b){case "string":return o(b);case "number":return isFinite(b)?""+b:"null";case "boolean":case "null":return""+b;case "object":if(!b)return"null";e+=l;f=[];if("[object Array]"===Object.prototype.toString.apply(b)){n=b.length;for(c=0;c<n;c+=1)f[c]=m(c,b)||"null";h=0===f.length?"[]":e?"[\n"+e+f.join(",\n"+e)+"\n"+g+"]":"["+f.join(",")+"]";e=g;return h}if(i&&"object"===typeof i){n=i.length;for(c=0;c<n;c+=1)"string"===typeof i[c]&&(d=i[c],(h=m(d,b))&&f.push(o(d)+(e?": ":":")+h))}else for(d in b)Object.prototype.hasOwnProperty.call(b,d)&&(h=m(d,b))&&f.push(o(d)+(e?": ":":")+h);h=0===f.length?"{}":e?"{\n"+e+f.join(",\n"+e)+"\n"+g+"}":"{"+f.join(",")+"}";e=g;return h}}if("function"!==typeof Date.prototype.toJSON)Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+k(this.getUTCMonth()+1)+"-"+k(this.getUTCDate())+"T"+k(this.getUTCHours())+":"+k(this.getUTCMinutes())+":"+k(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()};var q=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,p=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,e,l,r={"\u0008":"\\b","\t":"\\t","\n":"\\n","\u000c":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},i;if("function"!==typeof JSON.stringify)JSON.stringify=function(a,j,c){var d;l=e="";if("number"===typeof c)for(d=0;d<c;d+=1)l+=" ";else"string"===typeof c&&(l=c);if((i=j)&&"function"!==typeof j&&("object"!==typeof j||"number"!==typeof j.length))throw Error("JSON.stringify");return m("",{"":a})};if("function"!==typeof JSON.parse)JSON.parse=function(a,e){function c(a,d){var g,f,b=a[d];if(b&&"object"===typeof b)for(g in b)Object.prototype.hasOwnProperty.call(b,g)&&(f=c(b,g),void 0!==f?b[g]=f:delete b[g]);return e.call(a,d,b)}var d,a=""+a;q.lastIndex=0;q.test(a)&&(a=a.replace(q,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return d=eval("("+a+")"),"function"===typeof e?c({"":d},""):d;throw new SyntaxError("JSON.parse");}})();
}
(function(jQuery){
	// Set up object
	var vertic = {};
	
	// Hook in to 3rd party libs
	jQuery = jQuery !== undefined ? jQuery : window.jQuery !== undefined ? window.jQuery : false;
	var $ = jQuery;
	
	// Utilities
	vertic.utils = {};
		// Logging
		var _Log = function(obj){
			// Set defaults
			this.history = [];
			this.debug = false;
			this.domConsole = false;
			this.meta = true;
			this.name = 'Vertic Log';
			
			// Update settings
			if (typeof obj === 'object') {
				if (typeof obj.debug === 'boolean') this.debug = obj.debug;
				if (typeof obj.name === 'string') this.name = obj.name;
				if (typeof obj.meta === 'boolean') this.meta = obj.meta;
				if (obj.domConsole) {		
					this.domConsole = true;
				}
			}
			// Check for jQuery to allow domConsole
			if ($) { // Require jQuery because I'm lazy
				this.canDoDomConsole = true;
			}
			return this;
		};
		_Log.prototype.write = function(arg,forceDebug){
			var orgArg = arg;
			
			// Add metadata
			arg = this.addMeta(arg);
			
			// Push object to internal log
			this.history.push(arg); 
			
			// Try native console, but fail silently
			try { console.log(arg); } catch(err) { }
			
			// Make loud debugging noises
			if (this.debug || forceDebug !== undefined) { alert('Log: ' + orgArg); }
			
			// Call to very basic DOM based console for browsers with no native console
			this.callDomConsole(arg);
			
			// Return base argument
			return orgArg;
		};
		_Log.prototype.setupDomConsole = function(){
			if (this.canDoDomConsole) {
				this.domConsoleElems = {
					container: $('<ol class="vertic-log" title="'+this.name+'" style="line-height:15px; max-height:100px; overflow:auto; background:#fffbd6; padding:10px; clear:both; margin:0; border-bottom:1px solid #ccc; position:relative; list-style:decimal; z-index:9999;"></ul>').prependTo('body'),
					item: $('<li style="line-height:15px; color:#000; font-size:12px; margin-left:2em; font-family:Monaco, \'Bitstream Vera Sans Mono\', \'Lucida Console\', Terminal, monospace"></li>')
				};
			}
		};
		_Log.prototype.callDomConsole = function(arg){
			if (this.domConsole === true && this.canDoDomConsole === true) { 
				if (typeof this.domConsoleElems !== 'object') this.setupDomConsole();
				this.domConsoleElems.container.append(this.domConsoleElems.item.clone().text(this.meta ? arg.datetime + ' - ' + arg.type + ' : ' + arg.string : arg));
			}
		};
		_Log.prototype.addMeta = function(arg){
			return this.meta ? {
				arg: arg,
				datetime: (function () {
					return new Date().getTime();
				})(),
				log: this.name,
				string: arg.toString(),
				type: typeof arg
			} : arg;
		};
		vertic.utils.log = _Log;
		
		// Error handling - inherits from log
		var _Error = function(){
		};
		_Error.prototype = new _Log();
		_Error.prototype.parent = _Log.prototype;
		_Error.prototype.write = function(arg,status,msg,die){
			var orgArg = arg;
			
			// Set optional parameters
			if (typeof status === 'undefined') status = 'unknown';
			if (typeof msg === 'undefined') msg = '';
			if (typeof die !== 'boolean') die = false;
			
			// Add metadata
			arg = this.addMeta(arg);
			if (this.meta) {
				arg.error = true;
				arg.errorType = status.toString();
				arg.errorMsg = msg.toString();
			}
			
			// Push object to internal log
			this.history.push(arg); 
			
			// Throw JS error if needed or simple warning if possible
			if (die) {
				throw new Error(status+': '+msg);
			} else {
				try { console.warn(status+': '+msg); console.warn(arg); } catch(err) {}
			}
			
			// Make loud debugging noises
			if (this.debug || forceDebug !== undefined) { alert('Error: ' + orgArg); }
			
			// Call to very basic DOM based console for browsers with no native console
			this.callDomConsole(arg);
			
			// TODO: Service call?
			
			// Return base argument
			return orgArg;
		};
		vertic.utils.error = _Error;
		
		// Remote script loading
		var loadRemoteScript = function(src,callback) {
			var head = document.getElementsByTagName('head')[0];
			if (head){		
				var script = document.createElement('script');
				script.setAttribute('src',src);
				script.setAttribute('type','text/javascript');
				var ieVersion = function(){
					var rv=-1;
					if (navigator.appName === 'Microsoft Internet Explorer') {
						var ua=navigator.userAgent;
						var re=new RegExp("MSIE ([0-9]{1,}[\g.0-9]{0,})");
						if(re.exec(ua)!==null)
							rv=parseFloat(RegExp.$1);
					}
					return rv;
				}();
				
				if ((ieVersion === -1) || (ieVersion > 8 && document.compatMode !== 'BackCompat')) {
					script.onload = callback;
				} else {
					var loadFunction = function(){
						if (this.readyState === 'complete' || this.readyState === 'loaded') {
							callback(); 
						}
					};
					script.onreadystatechange = loadFunction;
				}
				head.appendChild(script);
			}
		};
		vertic.utils.loadRemoteScript = loadRemoteScript;

	
	// Internal instances and shortcuts
	vertic.internal = {};
	vertic.internal.log = new _Log();
	vertic.internal.error = new _Error();
	vertic.log = function(){ return vertic.internal.log.write.apply(vertic.internal.log,arguments); };
	vertic.err = function(){ return vertic.internal.error.write.apply(vertic.internal.error,arguments); };

	vertic.utils.getURLParameter = function getURLParameter(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
	}
	
	// Expose libraryt
	window._v = vertic;
})();
/*
 * Vertic JS utility library - Services
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Date: Tue Jan 31 12:00:00 2012 +0200
 */
(function(jQuery){
	// Set up object
	var vertic = window._v;
	
	// Hook in to 3rd party libs
	jQuery = jQuery !== undefined ? jQuery : window.jQuery !== undefined ? window.jQuery : false;
	var $ = jQuery;
	
	// Set up service utilities
	vertic.services = {
		request:function(url,data,win,lose) {
			if (typeof data !== 'object') data = {};
			if (win === undefined) win = function(resp){ _v.log(resp); };
			if (lose === undefined) lose = function(resp,jqxhr){ _v.err(resp, 'Service error', jqxhr.responseText); };
			$.ajax({
				url:url,
				data:JSON.stringify(data),
				dataType:'json',
				type:'post',
				complete:function(jqxhr,ts){
					if (ts === "success") {
						var resp = jqxhr.responseText;
						if (resp) {
							resp = JSON.parse(resp);
							/* Where the hell did allowErr come from, or where did it go? */
							if (resp.Outcome.Success === true) { //|| allowErr) {
								win(resp,jqxhr);
							} else {
								lose(resp,jqxhr);
							}
						} else {
							_v.err(jqxhr, 'Response format error', resp);
						}
					} else {
						_v.err(jqxhr, 'Ajax request error', ts);
					}
				}
			});
		}
	};
	
	// Shortcuts
	vertic.request = function() { return vertic.services.request.apply(vertic.services.request,arguments); };
	
	// Expose library
	window._v = vertic;
})();
/*
* Vertic JS - Site functional wrapper
* http://labs.vertic.com
*
* Copyright 2012, Vertic A/S
*
* Date: Tue Jan 31 12:00:00 2012 +0200
*/
(function (_v, jQuery, Modernizr) {
	// Hook in to Vertic lib or create if unavailable
	_v = typeof _v !== 'undefined' ? _v : typeof window._v !== 'undefined' ? window._v : {};

	// Hook in to 3rd party libs and make "normal" variable names available
	jQuery = jQuery !== undefined ? jQuery : window.jQuery !== undefined ? window.jQuery : false;
	var $ = jQuery;
	Modernizr =  Modernizr !== undefined ? jQuery : window.Modernizr !== undefined ? window.Modernizr : false;

	// Site init function - added to Vertic lib to make available in global scope without cluttering the namespace
	_v.init = function () {
		// Plugins
		// jQuery SWFObject v1.1.1 MIT/GPL @jon_neal-  http://jquery.thewikies.com/swfobject
		(function (f, h, i) { function k(a, c) { var b = (a[0] || 0) - (c[0] || 0); return b > 0 || !b && a.length > 0 && k(a.slice(1), c.slice(1)) } function l(a) { if (typeof a != g) return a; var c = [], b = ""; for (var d in a) { b = typeof a[d] == g ? l(a[d]) : [d, m ? encodeURI(a[d]) : a[d]].join("="); c.push(b) } return c.join("&") } function n(a) { var c = []; for (var b in a) a[b] && c.push([b, '="', a[b], '"'].join("")); return c.join(" ") } function o(a) { var c = []; for (var b in a) c.push(['<param name="', b, '" value="', l(a[b]), '" />'].join("")); return c.join("") } var g = "object", m = true; try { var j = i.description || function () { return (new i("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version") }() } catch (p) { j = "Unavailable" } var e = j.match(/\d+/g) || [0]; f[h] = { available: e[0] > 0, activeX: i && !i.name, version: { original: j, array: e, string: e.join("."), major: parseInt(e[0], 10) || 0, minor: parseInt(e[1], 10) || 0, release: parseInt(e[2], 10) || 0 }, hasVersion: function (a) { a = /string|number/.test(typeof a) ? a.toString().split(".") : /object/.test(typeof a) ? [a.major, a.minor] : a || [0, 0]; return k(e, a) }, encodeParams: true, expressInstall: "expressInstall.swf", expressInstallIsActive: false, create: function (a) { if (!a.swf || this.expressInstallIsActive || !this.available && !a.hasVersionFail) return false; if (!this.hasVersion(a.hasVersion || 1)) { this.expressInstallIsActive = true; if (typeof a.hasVersionFail == "function") if (!a.hasVersionFail.apply(a)) return false; a = { swf: a.expressInstall || this.expressInstall, height: 137, width: 214, flashvars: { MMredirectURL: location.href, MMplayerType: this.activeX ? "ActiveX" : "PlugIn", MMdoctitle: document.title.slice(0, 47) + " - Flash Player Installation" } } } attrs = { data: a.swf, type: "application/x-shockwave-flash", id: a.id || "flash_" + Math.floor(Math.random() * 999999999), width: a.width || 320, height: a.height || 180, style: a.style || "" }; m = typeof a.useEncode !== "undefined" ? a.useEncode : this.encodeParams; a.movie = a.swf; a.wmode = a.wmode || "opaque"; delete a.fallback; delete a.hasVersion; delete a.hasVersionFail; delete a.height; delete a.id; delete a.swf; delete a.useEncode; delete a.width; var c = document.createElement("div"); c.innerHTML = ["<object ", n(attrs), ">", o(a), "</object>"].join(""); return c.firstChild } }; f.fn[h] = function (a) { var c = this.find(g).andSelf().filter(g); /string|object/.test(typeof a) && this.each(function () { var b = f(this), d; a = typeof a == g ? a : { swf: a }; a.fallback = this; if (d = f[h].create(a)) { b.children().remove(); b.html(d) } }); typeof a == "function" && c.each(function () { var b = this; b.jsInteractionTimeoutMs = b.jsInteractionTimeoutMs || 0; if (b.jsInteractionTimeoutMs < 660) b.clientWidth || b.clientHeight ? a.call(b) : setTimeout(function () { f(b)[h](a) }, b.jsInteractionTimeoutMs + 66) }); return c } })(jQuery, "flash", navigator.plugins["Shockwave Flash"] || window.ActiveXObject);
		// jQuery UI widgets
		(function (e, t) { function i(t, i) { var a, n, r, o = t.nodeName.toLowerCase(); return "area" === o ? (a = t.parentNode, n = a.name, t.href && n && "map" === a.nodeName.toLowerCase() ? (r = e("img[usemap=#" + n + "]")[0], !!r && s(r)) : !1) : (/input|select|textarea|button|object/.test(o) ? !t.disabled : "a" === o ? t.href || i : i) && s(t) } function s(t) { return e.expr.filters.visible(t) && !e(t).parents().addBack().filter(function () { return "hidden" === e.css(this, "visibility") }).length } var a = 0, n = /^ui-id-\d+$/; e.ui = e.ui || {}, e.extend(e.ui, { version: "1.10.3", keyCode: { BACKSPACE: 8, COMMA: 188, DELETE: 46, DOWN: 40, END: 35, ENTER: 13, ESCAPE: 27, HOME: 36, LEFT: 37, NUMPAD_ADD: 107, NUMPAD_DECIMAL: 110, NUMPAD_DIVIDE: 111, NUMPAD_ENTER: 108, NUMPAD_MULTIPLY: 106, NUMPAD_SUBTRACT: 109, PAGE_DOWN: 34, PAGE_UP: 33, PERIOD: 190, RIGHT: 39, SPACE: 32, TAB: 9, UP: 38 } }), e.fn.extend({ focus: function (t) { return function (i, s) { return "number" == typeof i ? this.each(function () { var t = this; setTimeout(function () { e(t).focus(), s && s.call(t) }, i) }) : t.apply(this, arguments) } }(e.fn.focus), scrollParent: function () { var t; return t = e.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () { return /(relative|absolute|fixed)/.test(e.css(this, "position")) && /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x")) }).eq(0) : this.parents().filter(function () { return /(auto|scroll)/.test(e.css(this, "overflow") + e.css(this, "overflow-y") + e.css(this, "overflow-x")) }).eq(0), /fixed/.test(this.css("position")) || !t.length ? e(document) : t }, zIndex: function (i) { if (i !== t) return this.css("zIndex", i); if (this.length) for (var s, a, n = e(this[0]) ; n.length && n[0] !== document;) { if (s = n.css("position"), ("absolute" === s || "relative" === s || "fixed" === s) && (a = parseInt(n.css("zIndex"), 10), !isNaN(a) && 0 !== a)) return a; n = n.parent() } return 0 }, uniqueId: function () { return this.each(function () { this.id || (this.id = "ui-id-" + ++a) }) }, removeUniqueId: function () { return this.each(function () { n.test(this.id) && e(this).removeAttr("id") }) } }), e.extend(e.expr[":"], { data: e.expr.createPseudo ? e.expr.createPseudo(function (t) { return function (i) { return !!e.data(i, t) } }) : function (t, i, s) { return !!e.data(t, s[3]) }, focusable: function (t) { return i(t, !isNaN(e.attr(t, "tabindex"))) }, tabbable: function (t) { var s = e.attr(t, "tabindex"), a = isNaN(s); return (a || s >= 0) && i(t, !a) } }), e("<a>").outerWidth(1).jquery || e.each(["Width", "Height"], function (i, s) { function a(t, i, s, a) { return e.each(n, function () { i -= parseFloat(e.css(t, "padding" + this)) || 0, s && (i -= parseFloat(e.css(t, "border" + this + "Width")) || 0), a && (i -= parseFloat(e.css(t, "margin" + this)) || 0) }), i } var n = "Width" === s ? ["Left", "Right"] : ["Top", "Bottom"], r = s.toLowerCase(), o = { innerWidth: e.fn.innerWidth, innerHeight: e.fn.innerHeight, outerWidth: e.fn.outerWidth, outerHeight: e.fn.outerHeight }; e.fn["inner" + s] = function (i) { return i === t ? o["inner" + s].call(this) : this.each(function () { e(this).css(r, a(this, i) + "px") }) }, e.fn["outer" + s] = function (t, i) { return "number" != typeof t ? o["outer" + s].call(this, t) : this.each(function () { e(this).css(r, a(this, t, !0, i) + "px") }) } }), e.fn.addBack || (e.fn.addBack = function (e) { return this.add(null == e ? this.prevObject : this.prevObject.filter(e)) }), e("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (e.fn.removeData = function (t) { return function (i) { return arguments.length ? t.call(this, e.camelCase(i)) : t.call(this) } }(e.fn.removeData)), e.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), e.support.selectstart = "onselectstart" in document.createElement("div"), e.fn.extend({ disableSelection: function () { return this.bind((e.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (e) { e.preventDefault() }) }, enableSelection: function () { return this.unbind(".ui-disableSelection") } }), e.extend(e.ui, { plugin: { add: function (t, i, s) { var a, n = e.ui[t].prototype; for (a in s) n.plugins[a] = n.plugins[a] || [], n.plugins[a].push([i, s[a]]) }, call: function (e, t, i) { var s, a = e.plugins[t]; if (a && e.element[0].parentNode && 11 !== e.element[0].parentNode.nodeType) for (s = 0; a.length > s; s++) e.options[a[s][0]] && a[s][1].apply(e.element, i) } }, hasScroll: function (t, i) { if ("hidden" === e(t).css("overflow")) return !1; var s = i && "left" === i ? "scrollLeft" : "scrollTop", a = !1; return t[s] > 0 ? !0 : (t[s] = 1, a = t[s] > 0, t[s] = 0, a) } }) })(jQuery); (function (e, t) { var i = 0, s = Array.prototype.slice, n = e.cleanData; e.cleanData = function (t) { for (var i, s = 0; null != (i = t[s]) ; s++) try { e(i).triggerHandler("remove") } catch (a) { } n(t) }, e.widget = function (i, s, n) { var a, r, o, h, l = {}, u = i.split(".")[0]; i = i.split(".")[1], a = u + "-" + i, n || (n = s, s = e.Widget), e.expr[":"][a.toLowerCase()] = function (t) { return !!e.data(t, a) }, e[u] = e[u] || {}, r = e[u][i], o = e[u][i] = function (e, i) { return this._createWidget ? (arguments.length && this._createWidget(e, i), t) : new o(e, i) }, e.extend(o, r, { version: n.version, _proto: e.extend({}, n), _childConstructors: [] }), h = new s, h.options = e.widget.extend({}, h.options), e.each(n, function (i, n) { return e.isFunction(n) ? (l[i] = function () { var e = function () { return s.prototype[i].apply(this, arguments) }, t = function (e) { return s.prototype[i].apply(this, e) }; return function () { var i, s = this._super, a = this._superApply; return this._super = e, this._superApply = t, i = n.apply(this, arguments), this._super = s, this._superApply = a, i } }(), t) : (l[i] = n, t) }), o.prototype = e.widget.extend(h, { widgetEventPrefix: r ? h.widgetEventPrefix : i }, l, { constructor: o, namespace: u, widgetName: i, widgetFullName: a }), r ? (e.each(r._childConstructors, function (t, i) { var s = i.prototype; e.widget(s.namespace + "." + s.widgetName, o, i._proto) }), delete r._childConstructors) : s._childConstructors.push(o), e.widget.bridge(i, o) }, e.widget.extend = function (i) { for (var n, a, r = s.call(arguments, 1), o = 0, h = r.length; h > o; o++) for (n in r[o]) a = r[o][n], r[o].hasOwnProperty(n) && a !== t && (i[n] = e.isPlainObject(a) ? e.isPlainObject(i[n]) ? e.widget.extend({}, i[n], a) : e.widget.extend({}, a) : a); return i }, e.widget.bridge = function (i, n) { var a = n.prototype.widgetFullName || i; e.fn[i] = function (r) { var o = "string" == typeof r, h = s.call(arguments, 1), l = this; return r = !o && h.length ? e.widget.extend.apply(null, [r].concat(h)) : r, o ? this.each(function () { var s, n = e.data(this, a); return n ? e.isFunction(n[r]) && "_" !== r.charAt(0) ? (s = n[r].apply(n, h), s !== n && s !== t ? (l = s && s.jquery ? l.pushStack(s.get()) : s, !1) : t) : e.error("no such method '" + r + "' for " + i + " widget instance") : e.error("cannot call methods on " + i + " prior to initialization; " + "attempted to call method '" + r + "'") }) : this.each(function () { var t = e.data(this, a); t ? t.option(r || {})._init() : e.data(this, a, new n(r, this)) }), l } }, e.Widget = function () { }, e.Widget._childConstructors = [], e.Widget.prototype = { widgetName: "widget", widgetEventPrefix: "", defaultElement: "<div>", options: { disabled: !1, create: null }, _createWidget: function (t, s) { s = e(s || this.defaultElement || this)[0], this.element = e(s), this.uuid = i++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = e.widget.extend({}, this.options, this._getCreateOptions(), t), this.bindings = e(), this.hoverable = e(), this.focusable = e(), s !== this && (e.data(s, this.widgetFullName, this), this._on(!0, this.element, { remove: function (e) { e.target === s && this.destroy() } }), this.document = e(s.style ? s.ownerDocument : s.document || s), this.window = e(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init() }, _getCreateOptions: e.noop, _getCreateEventData: e.noop, _create: e.noop, _init: e.noop, destroy: function () { this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus") }, _destroy: e.noop, widget: function () { return this.element }, option: function (i, s) { var n, a, r, o = i; if (0 === arguments.length) return e.widget.extend({}, this.options); if ("string" == typeof i) if (o = {}, n = i.split("."), i = n.shift(), n.length) { for (a = o[i] = e.widget.extend({}, this.options[i]), r = 0; n.length - 1 > r; r++) a[n[r]] = a[n[r]] || {}, a = a[n[r]]; if (i = n.pop(), s === t) return a[i] === t ? null : a[i]; a[i] = s } else { if (s === t) return this.options[i] === t ? null : this.options[i]; o[i] = s } return this._setOptions(o), this }, _setOptions: function (e) { var t; for (t in e) this._setOption(t, e[t]); return this }, _setOption: function (e, t) { return this.options[e] = t, "disabled" === e && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!t).attr("aria-disabled", t), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this }, enable: function () { return this._setOption("disabled", !1) }, disable: function () { return this._setOption("disabled", !0) }, _on: function (i, s, n) { var a, r = this; "boolean" != typeof i && (n = s, s = i, i = !1), n ? (s = a = e(s), this.bindings = this.bindings.add(s)) : (n = s, s = this.element, a = this.widget()), e.each(n, function (n, o) { function h() { return i || r.options.disabled !== !0 && !e(this).hasClass("ui-state-disabled") ? ("string" == typeof o ? r[o] : o).apply(r, arguments) : t } "string" != typeof o && (h.guid = o.guid = o.guid || h.guid || e.guid++); var l = n.match(/^(\w+)\s*(.*)$/), u = l[1] + r.eventNamespace, c = l[2]; c ? a.delegate(c, u, h) : s.bind(u, h) }) }, _off: function (e, t) { t = (t || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.unbind(t).undelegate(t) }, _delay: function (e, t) { function i() { return ("string" == typeof e ? s[e] : e).apply(s, arguments) } var s = this; return setTimeout(i, t || 0) }, _hoverable: function (t) { this.hoverable = this.hoverable.add(t), this._on(t, { mouseenter: function (t) { e(t.currentTarget).addClass("ui-state-hover") }, mouseleave: function (t) { e(t.currentTarget).removeClass("ui-state-hover") } }) }, _focusable: function (t) { this.focusable = this.focusable.add(t), this._on(t, { focusin: function (t) { e(t.currentTarget).addClass("ui-state-focus") }, focusout: function (t) { e(t.currentTarget).removeClass("ui-state-focus") } }) }, _trigger: function (t, i, s) { var n, a, r = this.options[t]; if (s = s || {}, i = e.Event(i), i.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(), i.target = this.element[0], a = i.originalEvent) for (n in a) n in i || (i[n] = a[n]); return this.element.trigger(i, s), !(e.isFunction(r) && r.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented()) } }, e.each({ show: "fadeIn", hide: "fadeOut" }, function (t, i) { e.Widget.prototype["_" + t] = function (s, n, a) { "string" == typeof n && (n = { effect: n }); var r, o = n ? n === !0 || "number" == typeof n ? i : n.effect || i : t; n = n || {}, "number" == typeof n && (n = { duration: n }), r = !e.isEmptyObject(n), n.complete = a, n.delay && s.delay(n.delay), r && e.effects && e.effects.effect[o] ? s[t](n) : o !== t && s[o] ? s[o](n.duration, n.easing, a) : s.queue(function (i) { e(this)[t](), a && a.call(s[0]), i() }) } }) })(jQuery); (function (e) { var t = !1; e(document).mouseup(function () { t = !1 }), e.widget("ui.mouse", { version: "1.10.3", options: { cancel: "input,textarea,button,select,option", distance: 1, delay: 0 }, _mouseInit: function () { var t = this; this.element.bind("mousedown." + this.widgetName, function (e) { return t._mouseDown(e) }).bind("click." + this.widgetName, function (i) { return !0 === e.data(i.target, t.widgetName + ".preventClickEvent") ? (e.removeData(i.target, t.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : undefined }), this.started = !1 }, _mouseDestroy: function () { this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && e(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate) }, _mouseDown: function (i) { if (!t) { this._mouseStarted && this._mouseUp(i), this._mouseDownEvent = i; var s = this, n = 1 === i.which, a = "string" == typeof this.options.cancel && i.target.nodeName ? e(i.target).closest(this.options.cancel).length : !1; return n && !a && this._mouseCapture(i) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () { s.mouseDelayMet = !0 }, this.options.delay)), this._mouseDistanceMet(i) && this._mouseDelayMet(i) && (this._mouseStarted = this._mouseStart(i) !== !1, !this._mouseStarted) ? (i.preventDefault(), !0) : (!0 === e.data(i.target, this.widgetName + ".preventClickEvent") && e.removeData(i.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (e) { return s._mouseMove(e) }, this._mouseUpDelegate = function (e) { return s._mouseUp(e) }, e(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), i.preventDefault(), t = !0, !0)) : !0 } }, _mouseMove: function (t) { return e.ui.ie && (!document.documentMode || 9 > document.documentMode) && !t.button ? this._mouseUp(t) : this._mouseStarted ? (this._mouseDrag(t), t.preventDefault()) : (this._mouseDistanceMet(t) && this._mouseDelayMet(t) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, t) !== !1, this._mouseStarted ? this._mouseDrag(t) : this._mouseUp(t)), !this._mouseStarted) }, _mouseUp: function (t) { return e(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, t.target === this._mouseDownEvent.target && e.data(t.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(t)), !1 }, _mouseDistanceMet: function (e) { return Math.max(Math.abs(this._mouseDownEvent.pageX - e.pageX), Math.abs(this._mouseDownEvent.pageY - e.pageY)) >= this.options.distance }, _mouseDelayMet: function () { return this.mouseDelayMet }, _mouseStart: function () { }, _mouseDrag: function () { }, _mouseStop: function () { }, _mouseCapture: function () { return !0 } }) })(jQuery); (function (t, e) { function i(t, e, i) { return [parseFloat(t[0]) * (p.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (p.test(t[1]) ? i / 100 : 1)] } function s(e, i) { return parseInt(t.css(e, i), 10) || 0 } function n(e) { var i = e[0]; return 9 === i.nodeType ? { width: e.width(), height: e.height(), offset: { top: 0, left: 0 } } : t.isWindow(i) ? { width: e.width(), height: e.height(), offset: { top: e.scrollTop(), left: e.scrollLeft() } } : i.preventDefault ? { width: 0, height: 0, offset: { top: i.pageY, left: i.pageX } } : { width: e.outerWidth(), height: e.outerHeight(), offset: e.offset() } } t.ui = t.ui || {}; var a, o = Math.max, r = Math.abs, h = Math.round, l = /left|center|right/, c = /top|center|bottom/, u = /[\+\-]\d+(\.[\d]+)?%?/, d = /^\w+/, p = /%$/, f = t.fn.position; t.position = { scrollbarWidth: function () { if (a !== e) return a; var i, s, n = t("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"), o = n.children()[0]; return t("body").append(n), i = o.offsetWidth, n.css("overflow", "scroll"), s = o.offsetWidth, i === s && (s = n[0].clientWidth), n.remove(), a = i - s }, getScrollInfo: function (e) { var i = e.isWindow ? "" : e.element.css("overflow-x"), s = e.isWindow ? "" : e.element.css("overflow-y"), n = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth, a = "scroll" === s || "auto" === s && e.height < e.element[0].scrollHeight; return { width: a ? t.position.scrollbarWidth() : 0, height: n ? t.position.scrollbarWidth() : 0 } }, getWithinInfo: function (e) { var i = t(e || window), s = t.isWindow(i[0]); return { element: i, isWindow: s, offset: i.offset() || { left: 0, top: 0 }, scrollLeft: i.scrollLeft(), scrollTop: i.scrollTop(), width: s ? i.width() : i.outerWidth(), height: s ? i.height() : i.outerHeight() } } }, t.fn.position = function (e) { if (!e || !e.of) return f.apply(this, arguments); e = t.extend({}, e); var a, p, m, g, v, b, _ = t(e.of), y = t.position.getWithinInfo(e.within), w = t.position.getScrollInfo(y), x = (e.collision || "flip").split(" "), k = {}; return b = n(_), _[0].preventDefault && (e.at = "left top"), p = b.width, m = b.height, g = b.offset, v = t.extend({}, g), t.each(["my", "at"], function () { var t, i, s = (e[this] || "").split(" "); 1 === s.length && (s = l.test(s[0]) ? s.concat(["center"]) : c.test(s[0]) ? ["center"].concat(s) : ["center", "center"]), s[0] = l.test(s[0]) ? s[0] : "center", s[1] = c.test(s[1]) ? s[1] : "center", t = u.exec(s[0]), i = u.exec(s[1]), k[this] = [t ? t[0] : 0, i ? i[0] : 0], e[this] = [d.exec(s[0])[0], d.exec(s[1])[0]] }), 1 === x.length && (x[1] = x[0]), "right" === e.at[0] ? v.left += p : "center" === e.at[0] && (v.left += p / 2), "bottom" === e.at[1] ? v.top += m : "center" === e.at[1] && (v.top += m / 2), a = i(k.at, p, m), v.left += a[0], v.top += a[1], this.each(function () { var n, l, c = t(this), u = c.outerWidth(), d = c.outerHeight(), f = s(this, "marginLeft"), b = s(this, "marginTop"), D = u + f + s(this, "marginRight") + w.width, T = d + b + s(this, "marginBottom") + w.height, C = t.extend({}, v), M = i(k.my, c.outerWidth(), c.outerHeight()); "right" === e.my[0] ? C.left -= u : "center" === e.my[0] && (C.left -= u / 2), "bottom" === e.my[1] ? C.top -= d : "center" === e.my[1] && (C.top -= d / 2), C.left += M[0], C.top += M[1], t.support.offsetFractions || (C.left = h(C.left), C.top = h(C.top)), n = { marginLeft: f, marginTop: b }, t.each(["left", "top"], function (i, s) { t.ui.position[x[i]] && t.ui.position[x[i]][s](C, { targetWidth: p, targetHeight: m, elemWidth: u, elemHeight: d, collisionPosition: n, collisionWidth: D, collisionHeight: T, offset: [a[0] + M[0], a[1] + M[1]], my: e.my, at: e.at, within: y, elem: c }) }), e.using && (l = function (t) { var i = g.left - C.left, s = i + p - u, n = g.top - C.top, a = n + m - d, h = { target: { element: _, left: g.left, top: g.top, width: p, height: m }, element: { element: c, left: C.left, top: C.top, width: u, height: d }, horizontal: 0 > s ? "left" : i > 0 ? "right" : "center", vertical: 0 > a ? "top" : n > 0 ? "bottom" : "middle" }; u > p && p > r(i + s) && (h.horizontal = "center"), d > m && m > r(n + a) && (h.vertical = "middle"), h.important = o(r(i), r(s)) > o(r(n), r(a)) ? "horizontal" : "vertical", e.using.call(this, t, h) }), c.offset(t.extend(C, { using: l })) }) }, t.ui.position = { fit: { left: function (t, e) { var i, s = e.within, n = s.isWindow ? s.scrollLeft : s.offset.left, a = s.width, r = t.left - e.collisionPosition.marginLeft, h = n - r, l = r + e.collisionWidth - a - n; e.collisionWidth > a ? h > 0 && 0 >= l ? (i = t.left + h + e.collisionWidth - a - n, t.left += h - i) : t.left = l > 0 && 0 >= h ? n : h > l ? n + a - e.collisionWidth : n : h > 0 ? t.left += h : l > 0 ? t.left -= l : t.left = o(t.left - r, t.left) }, top: function (t, e) { var i, s = e.within, n = s.isWindow ? s.scrollTop : s.offset.top, a = e.within.height, r = t.top - e.collisionPosition.marginTop, h = n - r, l = r + e.collisionHeight - a - n; e.collisionHeight > a ? h > 0 && 0 >= l ? (i = t.top + h + e.collisionHeight - a - n, t.top += h - i) : t.top = l > 0 && 0 >= h ? n : h > l ? n + a - e.collisionHeight : n : h > 0 ? t.top += h : l > 0 ? t.top -= l : t.top = o(t.top - r, t.top) } }, flip: { left: function (t, e) { var i, s, n = e.within, a = n.offset.left + n.scrollLeft, o = n.width, h = n.isWindow ? n.scrollLeft : n.offset.left, l = t.left - e.collisionPosition.marginLeft, c = l - h, u = l + e.collisionWidth - o - h, d = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0, p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0, f = -2 * e.offset[0]; 0 > c ? (i = t.left + d + p + f + e.collisionWidth - o - a, (0 > i || r(c) > i) && (t.left += d + p + f)) : u > 0 && (s = t.left - e.collisionPosition.marginLeft + d + p + f - h, (s > 0 || u > r(s)) && (t.left += d + p + f)) }, top: function (t, e) { var i, s, n = e.within, a = n.offset.top + n.scrollTop, o = n.height, h = n.isWindow ? n.scrollTop : n.offset.top, l = t.top - e.collisionPosition.marginTop, c = l - h, u = l + e.collisionHeight - o - h, d = "top" === e.my[1], p = d ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0, f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0, m = -2 * e.offset[1]; 0 > c ? (s = t.top + p + f + m + e.collisionHeight - o - a, t.top + p + f + m > c && (0 > s || r(c) > s) && (t.top += p + f + m)) : u > 0 && (i = t.top - e.collisionPosition.marginTop + p + f + m - h, t.top + p + f + m > u && (i > 0 || u > r(i)) && (t.top += p + f + m)) } }, flipfit: { left: function () { t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments) }, top: function () { t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments) } } }, function () { var e, i, s, n, a, o = document.getElementsByTagName("body")[0], r = document.createElement("div"); e = document.createElement(o ? "div" : "body"), s = { visibility: "hidden", width: 0, height: 0, border: 0, margin: 0, background: "none" }, o && t.extend(s, { position: "absolute", left: "-1000px", top: "-1000px" }); for (a in s) e.style[a] = s[a]; e.appendChild(r), i = o || document.documentElement, i.insertBefore(e, i.firstChild), r.style.cssText = "position: absolute; left: 10.7432222px;", n = t(r).offset().left, t.support.offsetFractions = n > 10 && 11 > n, e.innerHTML = "", i.removeChild(e) }() })(jQuery); (function (e) { e.widget("ui.draggable", e.ui.mouse, { version: "1.10.3", widgetEventPrefix: "drag", options: { addClasses: !0, appendTo: "parent", axis: !1, connectToSortable: !1, containment: !1, cursor: "auto", cursorAt: !1, grid: !1, handle: !1, helper: "original", iframeFix: !1, opacity: !1, refreshPositions: !1, revert: !1, revertDuration: 500, scope: "default", scroll: !0, scrollSensitivity: 20, scrollSpeed: 20, snap: !1, snapMode: "both", snapTolerance: 20, stack: !1, zIndex: !1, drag: null, start: null, stop: null }, _create: function () { "original" !== this.options.helper || /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative"), this.options.addClasses && this.element.addClass("ui-draggable"), this.options.disabled && this.element.addClass("ui-draggable-disabled"), this._mouseInit() }, _destroy: function () { this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"), this._mouseDestroy() }, _mouseCapture: function (t) { var i = this.options; return this.helper || i.disabled || e(t.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(t), this.handle ? (e(i.iframeFix === !0 ? "iframe" : i.iframeFix).each(function () { e("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({ width: this.offsetWidth + "px", height: this.offsetHeight + "px", position: "absolute", opacity: "0.001", zIndex: 1e3 }).css(e(this).offset()).appendTo("body") }), !0) : !1) }, _mouseStart: function (t) { var i = this.options; return this.helper = this._createHelper(t), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(), e.ui.ddmanager && (e.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = { top: this.offset.top - this.margins.top, left: this.offset.left - this.margins.left }, this.offset.scroll = !1, e.extend(this.offset, { click: { left: t.pageX - this.offset.left, top: t.pageY - this.offset.top }, parent: this._getParentOffset(), relative: this._getRelativeOffset() }), this.originalPosition = this.position = this._generatePosition(t), this.originalPageX = t.pageX, this.originalPageY = t.pageY, i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt), this._setContainment(), this._trigger("start", t) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), e.ui.ddmanager && !i.dropBehaviour && e.ui.ddmanager.prepareOffsets(this, t), this._mouseDrag(t, !0), e.ui.ddmanager && e.ui.ddmanager.dragStart(this, t), !0) }, _mouseDrag: function (t, i) { if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(t), this.positionAbs = this._convertPositionTo("absolute"), !i) { var s = this._uiHash(); if (this._trigger("drag", t, s) === !1) return this._mouseUp({}), !1; this.position = s.position } return this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), e.ui.ddmanager && e.ui.ddmanager.drag(this, t), !1 }, _mouseStop: function (t) { var i = this, s = !1; return e.ui.ddmanager && !this.options.dropBehaviour && (s = e.ui.ddmanager.drop(this, t)), this.dropped && (s = this.dropped, this.dropped = !1), "original" !== this.options.helper || e.contains(this.element[0].ownerDocument, this.element[0]) ? ("invalid" === this.options.revert && !s || "valid" === this.options.revert && s || this.options.revert === !0 || e.isFunction(this.options.revert) && this.options.revert.call(this.element, s) ? e(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () { i._trigger("stop", t) !== !1 && i._clear() }) : this._trigger("stop", t) !== !1 && this._clear(), !1) : !1 }, _mouseUp: function (t) { return e("div.ui-draggable-iframeFix").each(function () { this.parentNode.removeChild(this) }), e.ui.ddmanager && e.ui.ddmanager.dragStop(this, t), e.ui.mouse.prototype._mouseUp.call(this, t) }, cancel: function () { return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this }, _getHandle: function (t) { return this.options.handle ? !!e(t.target).closest(this.element.find(this.options.handle)).length : !0 }, _createHelper: function (t) { var i = this.options, s = e.isFunction(i.helper) ? e(i.helper.apply(this.element[0], [t])) : "clone" === i.helper ? this.element.clone().removeAttr("id") : this.element; return s.parents("body").length || s.appendTo("parent" === i.appendTo ? this.element[0].parentNode : i.appendTo), s[0] === this.element[0] || /(fixed|absolute)/.test(s.css("position")) || s.css("position", "absolute"), s }, _adjustOffsetFromHelper: function (t) { "string" == typeof t && (t = t.split(" ")), e.isArray(t) && (t = { left: +t[0], top: +t[1] || 0 }), "left" in t && (this.offset.click.left = t.left + this.margins.left), "right" in t && (this.offset.click.left = this.helperProportions.width - t.right + this.margins.left), "top" in t && (this.offset.click.top = t.top + this.margins.top), "bottom" in t && (this.offset.click.top = this.helperProportions.height - t.bottom + this.margins.top) }, _getParentOffset: function () { var t = this.offsetParent.offset(); return "absolute" === this.cssPosition && this.scrollParent[0] !== document && e.contains(this.scrollParent[0], this.offsetParent[0]) && (t.left += this.scrollParent.scrollLeft(), t.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && e.ui.ie) && (t = { top: 0, left: 0 }), { top: t.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: t.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) } }, _getRelativeOffset: function () { if ("relative" === this.cssPosition) { var e = this.element.position(); return { top: e.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(), left: e.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft() } } return { top: 0, left: 0 } }, _cacheMargins: function () { this.margins = { left: parseInt(this.element.css("marginLeft"), 10) || 0, top: parseInt(this.element.css("marginTop"), 10) || 0, right: parseInt(this.element.css("marginRight"), 10) || 0, bottom: parseInt(this.element.css("marginBottom"), 10) || 0 } }, _cacheHelperProportions: function () { this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() } }, _setContainment: function () { var t, i, s, n = this.options; return n.containment ? "window" === n.containment ? (this.containment = [e(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, e(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, e(window).scrollLeft() + e(window).width() - this.helperProportions.width - this.margins.left, e(window).scrollTop() + (e(window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top], undefined) : "document" === n.containment ? (this.containment = [0, 0, e(document).width() - this.helperProportions.width - this.margins.left, (e(document).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top], undefined) : n.containment.constructor === Array ? (this.containment = n.containment, undefined) : ("parent" === n.containment && (n.containment = this.helper[0].parentNode), i = e(n.containment), s = i[0], s && (t = "hidden" !== i.css("overflow"), this.containment = [(parseInt(i.css("borderLeftWidth"), 10) || 0) + (parseInt(i.css("paddingLeft"), 10) || 0), (parseInt(i.css("borderTopWidth"), 10) || 0) + (parseInt(i.css("paddingTop"), 10) || 0), (t ? Math.max(s.scrollWidth, s.offsetWidth) : s.offsetWidth) - (parseInt(i.css("borderRightWidth"), 10) || 0) - (parseInt(i.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (t ? Math.max(s.scrollHeight, s.offsetHeight) : s.offsetHeight) - (parseInt(i.css("borderBottomWidth"), 10) || 0) - (parseInt(i.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relative_container = i), undefined) : (this.containment = null, undefined) }, _convertPositionTo: function (t, i) { i || (i = this.position); var s = "absolute" === t ? 1 : -1, n = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && e.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent; return this.offset.scroll || (this.offset.scroll = { top: n.scrollTop(), left: n.scrollLeft() }), { top: i.top + this.offset.relative.top * s + this.offset.parent.top * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top) * s, left: i.left + this.offset.relative.left * s + this.offset.parent.left * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left) * s } }, _generatePosition: function (t) { var i, s, n, a, o = this.options, r = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && e.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, h = t.pageX, l = t.pageY; return this.offset.scroll || (this.offset.scroll = { top: r.scrollTop(), left: r.scrollLeft() }), this.originalPosition && (this.containment && (this.relative_container ? (s = this.relative_container.offset(), i = [this.containment[0] + s.left, this.containment[1] + s.top, this.containment[2] + s.left, this.containment[3] + s.top]) : i = this.containment, t.pageX - this.offset.click.left < i[0] && (h = i[0] + this.offset.click.left), t.pageY - this.offset.click.top < i[1] && (l = i[1] + this.offset.click.top), t.pageX - this.offset.click.left > i[2] && (h = i[2] + this.offset.click.left), t.pageY - this.offset.click.top > i[3] && (l = i[3] + this.offset.click.top)), o.grid && (n = o.grid[1] ? this.originalPageY + Math.round((l - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY, l = i ? n - this.offset.click.top >= i[1] || n - this.offset.click.top > i[3] ? n : n - this.offset.click.top >= i[1] ? n - o.grid[1] : n + o.grid[1] : n, a = o.grid[0] ? this.originalPageX + Math.round((h - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX, h = i ? a - this.offset.click.left >= i[0] || a - this.offset.click.left > i[2] ? a : a - this.offset.click.left >= i[0] ? a - o.grid[0] : a + o.grid[0] : a)), { top: l - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top), left: h - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left) } }, _clear: function () { this.helper.removeClass("ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1 }, _trigger: function (t, i, s) { return s = s || this._uiHash(), e.ui.plugin.call(this, t, [i, s]), "drag" === t && (this.positionAbs = this._convertPositionTo("absolute")), e.Widget.prototype._trigger.call(this, t, i, s) }, plugins: {}, _uiHash: function () { return { helper: this.helper, position: this.position, originalPosition: this.originalPosition, offset: this.positionAbs } } }), e.ui.plugin.add("draggable", "connectToSortable", { start: function (t, i) { var s = e(this).data("ui-draggable"), n = s.options, a = e.extend({}, i, { item: s.element }); s.sortables = [], e(n.connectToSortable).each(function () { var i = e.data(this, "ui-sortable"); i && !i.options.disabled && (s.sortables.push({ instance: i, shouldRevert: i.options.revert }), i.refreshPositions(), i._trigger("activate", t, a)) }) }, stop: function (t, i) { var s = e(this).data("ui-draggable"), n = e.extend({}, i, { item: s.element }); e.each(s.sortables, function () { this.instance.isOver ? (this.instance.isOver = 0, s.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert), this.instance._mouseStop(t), this.instance.options.helper = this.instance.options._helper, "original" === s.options.helper && this.instance.currentItem.css({ top: "auto", left: "auto" })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", t, n)) }) }, drag: function (t, i) { var s = e(this).data("ui-draggable"), n = this; e.each(s.sortables, function () { var a = !1, o = this; this.instance.positionAbs = s.positionAbs, this.instance.helperProportions = s.helperProportions, this.instance.offset.click = s.offset.click, this.instance._intersectsWith(this.instance.containerCache) && (a = !0, e.each(s.sortables, function () { return this.instance.positionAbs = s.positionAbs, this.instance.helperProportions = s.helperProportions, this.instance.offset.click = s.offset.click, this !== o && this.instance._intersectsWith(this.instance.containerCache) && e.contains(o.instance.element[0], this.instance.element[0]) && (a = !1), a })), a ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = e(n).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper = this.instance.options.helper, this.instance.options.helper = function () { return i.helper[0] }, t.target = this.instance.currentItem[0], this.instance._mouseCapture(t, !0), this.instance._mouseStart(t, !0, !0), this.instance.offset.click.top = s.offset.click.top, this.instance.offset.click.left = s.offset.click.left, this.instance.offset.parent.left -= s.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= s.offset.parent.top - this.instance.offset.parent.top, s._trigger("toSortable", t), s.dropped = this.instance.element, s.currentItem = s.element, this.instance.fromOutside = s), this.instance.currentItem && this.instance._mouseDrag(t)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", t, this.instance._uiHash(this.instance)), this.instance._mouseStop(t, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(), s._trigger("fromSortable", t), s.dropped = !1) }) } }), e.ui.plugin.add("draggable", "cursor", { start: function () { var t = e("body"), i = e(this).data("ui-draggable").options; t.css("cursor") && (i._cursor = t.css("cursor")), t.css("cursor", i.cursor) }, stop: function () { var t = e(this).data("ui-draggable").options; t._cursor && e("body").css("cursor", t._cursor) } }), e.ui.plugin.add("draggable", "opacity", { start: function (t, i) { var s = e(i.helper), n = e(this).data("ui-draggable").options; s.css("opacity") && (n._opacity = s.css("opacity")), s.css("opacity", n.opacity) }, stop: function (t, i) { var s = e(this).data("ui-draggable").options; s._opacity && e(i.helper).css("opacity", s._opacity) } }), e.ui.plugin.add("draggable", "scroll", { start: function () { var t = e(this).data("ui-draggable"); t.scrollParent[0] !== document && "HTML" !== t.scrollParent[0].tagName && (t.overflowOffset = t.scrollParent.offset()) }, drag: function (t) { var i = e(this).data("ui-draggable"), s = i.options, n = !1; i.scrollParent[0] !== document && "HTML" !== i.scrollParent[0].tagName ? (s.axis && "x" === s.axis || (i.overflowOffset.top + i.scrollParent[0].offsetHeight - t.pageY < s.scrollSensitivity ? i.scrollParent[0].scrollTop = n = i.scrollParent[0].scrollTop + s.scrollSpeed : t.pageY - i.overflowOffset.top < s.scrollSensitivity && (i.scrollParent[0].scrollTop = n = i.scrollParent[0].scrollTop - s.scrollSpeed)), s.axis && "y" === s.axis || (i.overflowOffset.left + i.scrollParent[0].offsetWidth - t.pageX < s.scrollSensitivity ? i.scrollParent[0].scrollLeft = n = i.scrollParent[0].scrollLeft + s.scrollSpeed : t.pageX - i.overflowOffset.left < s.scrollSensitivity && (i.scrollParent[0].scrollLeft = n = i.scrollParent[0].scrollLeft - s.scrollSpeed))) : (s.axis && "x" === s.axis || (t.pageY - e(document).scrollTop() < s.scrollSensitivity ? n = e(document).scrollTop(e(document).scrollTop() - s.scrollSpeed) : e(window).height() - (t.pageY - e(document).scrollTop()) < s.scrollSensitivity && (n = e(document).scrollTop(e(document).scrollTop() + s.scrollSpeed))), s.axis && "y" === s.axis || (t.pageX - e(document).scrollLeft() < s.scrollSensitivity ? n = e(document).scrollLeft(e(document).scrollLeft() - s.scrollSpeed) : e(window).width() - (t.pageX - e(document).scrollLeft()) < s.scrollSensitivity && (n = e(document).scrollLeft(e(document).scrollLeft() + s.scrollSpeed)))), n !== !1 && e.ui.ddmanager && !s.dropBehaviour && e.ui.ddmanager.prepareOffsets(i, t) } }), e.ui.plugin.add("draggable", "snap", { start: function () { var t = e(this).data("ui-draggable"), i = t.options; t.snapElements = [], e(i.snap.constructor !== String ? i.snap.items || ":data(ui-draggable)" : i.snap).each(function () { var i = e(this), s = i.offset(); this !== t.element[0] && t.snapElements.push({ item: this, width: i.outerWidth(), height: i.outerHeight(), top: s.top, left: s.left }) }) }, drag: function (t, i) { var s, n, a, o, r, h, l, u, c, d, p = e(this).data("ui-draggable"), f = p.options, m = f.snapTolerance, g = i.offset.left, v = g + p.helperProportions.width, b = i.offset.top, y = b + p.helperProportions.height; for (c = p.snapElements.length - 1; c >= 0; c--) r = p.snapElements[c].left, h = r + p.snapElements[c].width, l = p.snapElements[c].top, u = l + p.snapElements[c].height, r - m > v || g > h + m || l - m > y || b > u + m || !e.contains(p.snapElements[c].item.ownerDocument, p.snapElements[c].item) ? (p.snapElements[c].snapping && p.options.snap.release && p.options.snap.release.call(p.element, t, e.extend(p._uiHash(), { snapItem: p.snapElements[c].item })), p.snapElements[c].snapping = !1) : ("inner" !== f.snapMode && (s = m >= Math.abs(l - y), n = m >= Math.abs(u - b), a = m >= Math.abs(r - v), o = m >= Math.abs(h - g), s && (i.position.top = p._convertPositionTo("relative", { top: l - p.helperProportions.height, left: 0 }).top - p.margins.top), n && (i.position.top = p._convertPositionTo("relative", { top: u, left: 0 }).top - p.margins.top), a && (i.position.left = p._convertPositionTo("relative", { top: 0, left: r - p.helperProportions.width }).left - p.margins.left), o && (i.position.left = p._convertPositionTo("relative", { top: 0, left: h }).left - p.margins.left)), d = s || n || a || o, "outer" !== f.snapMode && (s = m >= Math.abs(l - b), n = m >= Math.abs(u - y), a = m >= Math.abs(r - g), o = m >= Math.abs(h - v), s && (i.position.top = p._convertPositionTo("relative", { top: l, left: 0 }).top - p.margins.top), n && (i.position.top = p._convertPositionTo("relative", { top: u - p.helperProportions.height, left: 0 }).top - p.margins.top), a && (i.position.left = p._convertPositionTo("relative", { top: 0, left: r }).left - p.margins.left), o && (i.position.left = p._convertPositionTo("relative", { top: 0, left: h - p.helperProportions.width }).left - p.margins.left)), !p.snapElements[c].snapping && (s || n || a || o || d) && p.options.snap.snap && p.options.snap.snap.call(p.element, t, e.extend(p._uiHash(), { snapItem: p.snapElements[c].item })), p.snapElements[c].snapping = s || n || a || o || d) } }), e.ui.plugin.add("draggable", "stack", { start: function () { var t, i = this.data("ui-draggable").options, s = e.makeArray(e(i.stack)).sort(function (t, i) { return (parseInt(e(t).css("zIndex"), 10) || 0) - (parseInt(e(i).css("zIndex"), 10) || 0) }); s.length && (t = parseInt(e(s[0]).css("zIndex"), 10) || 0, e(s).each(function (i) { e(this).css("zIndex", t + i) }), this.css("zIndex", t + s.length)) } }), e.ui.plugin.add("draggable", "zIndex", { start: function (t, i) { var s = e(i.helper), n = e(this).data("ui-draggable").options; s.css("zIndex") && (n._zIndex = s.css("zIndex")), s.css("zIndex", n.zIndex) }, stop: function (t, i) { var s = e(this).data("ui-draggable").options; s._zIndex && e(i.helper).css("zIndex", s._zIndex) } }) })(jQuery); (function (e) { function t(e) { return parseInt(e, 10) || 0 } function i(e) { return !isNaN(parseInt(e, 10)) } e.widget("ui.resizable", e.ui.mouse, { version: "1.10.3", widgetEventPrefix: "resize", options: { alsoResize: !1, animate: !1, animateDuration: "slow", animateEasing: "swing", aspectRatio: !1, autoHide: !1, containment: !1, ghost: !1, grid: !1, handles: "e,s,se", helper: !1, maxHeight: null, maxWidth: null, minHeight: 10, minWidth: 10, zIndex: 90, resize: null, start: null, stop: null }, _create: function () { var t, i, s, n, a, o = this, r = this.options; if (this.element.addClass("ui-resizable"), e.extend(this, { _aspectRatio: !!r.aspectRatio, aspectRatio: r.aspectRatio, originalElement: this.element, _proportionallyResizeElements: [], _helper: r.helper || r.ghost || r.animate ? r.helper || "ui-resizable-helper" : null }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(e("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({ position: this.element.css("position"), width: this.element.outerWidth(), height: this.element.outerHeight(), top: this.element.css("top"), left: this.element.css("left") })), this.element = this.element.parent().data("ui-resizable", this.element.data("ui-resizable")), this.elementIsWrapper = !0, this.element.css({ marginLeft: this.originalElement.css("marginLeft"), marginTop: this.originalElement.css("marginTop"), marginRight: this.originalElement.css("marginRight"), marginBottom: this.originalElement.css("marginBottom") }), this.originalElement.css({ marginLeft: 0, marginTop: 0, marginRight: 0, marginBottom: 0 }), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({ position: "static", zoom: 1, display: "block" })), this.originalElement.css({ margin: this.originalElement.css("margin") }), this._proportionallyResize()), this.handles = r.handles || (e(".ui-resizable-handle", this.element).length ? { n: ".ui-resizable-n", e: ".ui-resizable-e", s: ".ui-resizable-s", w: ".ui-resizable-w", se: ".ui-resizable-se", sw: ".ui-resizable-sw", ne: ".ui-resizable-ne", nw: ".ui-resizable-nw" } : "e,s,se"), this.handles.constructor === String) for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), t = this.handles.split(","), this.handles = {}, i = 0; t.length > i; i++) s = e.trim(t[i]), a = "ui-resizable-" + s, n = e("<div class='ui-resizable-handle " + a + "'></div>"), n.css({ zIndex: r.zIndex }), "se" === s && n.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[s] = ".ui-resizable-" + s, this.element.append(n); this._renderAxis = function (t) { var i, s, n, a; t = t || this.element; for (i in this.handles) this.handles[i].constructor === String && (this.handles[i] = e(this.handles[i], this.element).show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (s = e(this.handles[i], this.element), a = /sw|ne|nw|se|n|s/.test(i) ? s.outerHeight() : s.outerWidth(), n = ["padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left"].join(""), t.css(n, a), this._proportionallyResize()), e(this.handles[i]).length }, this._renderAxis(this.element), this._handles = e(".ui-resizable-handle", this.element).disableSelection(), this._handles.mouseover(function () { o.resizing || (this.className && (n = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), o.axis = n && n[1] ? n[1] : "se") }), r.autoHide && (this._handles.hide(), e(this.element).addClass("ui-resizable-autohide").mouseenter(function () { r.disabled || (e(this).removeClass("ui-resizable-autohide"), o._handles.show()) }).mouseleave(function () { r.disabled || o.resizing || (e(this).addClass("ui-resizable-autohide"), o._handles.hide()) })), this._mouseInit() }, _destroy: function () { this._mouseDestroy(); var t, i = function (t) { e(t).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove() }; return this.elementIsWrapper && (i(this.element), t = this.element, this.originalElement.css({ position: t.css("position"), width: t.outerWidth(), height: t.outerHeight(), top: t.css("top"), left: t.css("left") }).insertAfter(t), t.remove()), this.originalElement.css("resize", this.originalResizeStyle), i(this.originalElement), this }, _mouseCapture: function (t) { var i, s, n = !1; for (i in this.handles) s = e(this.handles[i])[0], (s === t.target || e.contains(s, t.target)) && (n = !0); return !this.options.disabled && n }, _mouseStart: function (i) { var s, n, a, o = this.options, r = this.element.position(), h = this.element; return this.resizing = !0, /absolute/.test(h.css("position")) ? h.css({ position: "absolute", top: h.css("top"), left: h.css("left") }) : h.is(".ui-draggable") && h.css({ position: "absolute", top: r.top, left: r.left }), this._renderProxy(), s = t(this.helper.css("left")), n = t(this.helper.css("top")), o.containment && (s += e(o.containment).scrollLeft() || 0, n += e(o.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = { left: s, top: n }, this.size = this._helper ? { width: h.outerWidth(), height: h.outerHeight() } : { width: h.width(), height: h.height() }, this.originalSize = this._helper ? { width: h.outerWidth(), height: h.outerHeight() } : { width: h.width(), height: h.height() }, this.originalPosition = { left: s, top: n }, this.sizeDiff = { width: h.outerWidth() - h.width(), height: h.outerHeight() - h.height() }, this.originalMousePosition = { left: i.pageX, top: i.pageY }, this.aspectRatio = "number" == typeof o.aspectRatio ? o.aspectRatio : this.originalSize.width / this.originalSize.height || 1, a = e(".ui-resizable-" + this.axis).css("cursor"), e("body").css("cursor", "auto" === a ? this.axis + "-resize" : a), h.addClass("ui-resizable-resizing"), this._propagate("start", i), !0 }, _mouseDrag: function (t) { var i, s = this.helper, n = {}, a = this.originalMousePosition, o = this.axis, r = this.position.top, h = this.position.left, l = this.size.width, u = this.size.height, c = t.pageX - a.left || 0, d = t.pageY - a.top || 0, p = this._change[o]; return p ? (i = p.apply(this, [t, c, d]), this._updateVirtualBoundaries(t.shiftKey), (this._aspectRatio || t.shiftKey) && (i = this._updateRatio(i, t)), i = this._respectSize(i, t), this._updateCache(i), this._propagate("resize", t), this.position.top !== r && (n.top = this.position.top + "px"), this.position.left !== h && (n.left = this.position.left + "px"), this.size.width !== l && (n.width = this.size.width + "px"), this.size.height !== u && (n.height = this.size.height + "px"), s.css(n), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), e.isEmptyObject(n) || this._trigger("resize", t, this.ui()), !1) : !1 }, _mouseStop: function (t) { this.resizing = !1; var i, s, n, a, o, r, h, l = this.options, u = this; return this._helper && (i = this._proportionallyResizeElements, s = i.length && /textarea/i.test(i[0].nodeName), n = s && e.ui.hasScroll(i[0], "left") ? 0 : u.sizeDiff.height, a = s ? 0 : u.sizeDiff.width, o = { width: u.helper.width() - a, height: u.helper.height() - n }, r = parseInt(u.element.css("left"), 10) + (u.position.left - u.originalPosition.left) || null, h = parseInt(u.element.css("top"), 10) + (u.position.top - u.originalPosition.top) || null, l.animate || this.element.css(e.extend(o, { top: h, left: r })), u.helper.height(u.size.height), u.helper.width(u.size.width), this._helper && !l.animate && this._proportionallyResize()), e("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", t), this._helper && this.helper.remove(), !1 }, _updateVirtualBoundaries: function (e) { var t, s, n, a, o, r = this.options; o = { minWidth: i(r.minWidth) ? r.minWidth : 0, maxWidth: i(r.maxWidth) ? r.maxWidth : 1 / 0, minHeight: i(r.minHeight) ? r.minHeight : 0, maxHeight: i(r.maxHeight) ? r.maxHeight : 1 / 0 }, (this._aspectRatio || e) && (t = o.minHeight * this.aspectRatio, n = o.minWidth / this.aspectRatio, s = o.maxHeight * this.aspectRatio, a = o.maxWidth / this.aspectRatio, t > o.minWidth && (o.minWidth = t), n > o.minHeight && (o.minHeight = n), o.maxWidth > s && (o.maxWidth = s), o.maxHeight > a && (o.maxHeight = a)), this._vBoundaries = o }, _updateCache: function (e) { this.offset = this.helper.offset(), i(e.left) && (this.position.left = e.left), i(e.top) && (this.position.top = e.top), i(e.height) && (this.size.height = e.height), i(e.width) && (this.size.width = e.width) }, _updateRatio: function (e) { var t = this.position, s = this.size, n = this.axis; return i(e.height) ? e.width = e.height * this.aspectRatio : i(e.width) && (e.height = e.width / this.aspectRatio), "sw" === n && (e.left = t.left + (s.width - e.width), e.top = null), "nw" === n && (e.top = t.top + (s.height - e.height), e.left = t.left + (s.width - e.width)), e }, _respectSize: function (e) { var t = this._vBoundaries, s = this.axis, n = i(e.width) && t.maxWidth && t.maxWidth < e.width, a = i(e.height) && t.maxHeight && t.maxHeight < e.height, o = i(e.width) && t.minWidth && t.minWidth > e.width, r = i(e.height) && t.minHeight && t.minHeight > e.height, h = this.originalPosition.left + this.originalSize.width, l = this.position.top + this.size.height, u = /sw|nw|w/.test(s), c = /nw|ne|n/.test(s); return o && (e.width = t.minWidth), r && (e.height = t.minHeight), n && (e.width = t.maxWidth), a && (e.height = t.maxHeight), o && u && (e.left = h - t.minWidth), n && u && (e.left = h - t.maxWidth), r && c && (e.top = l - t.minHeight), a && c && (e.top = l - t.maxHeight), e.width || e.height || e.left || !e.top ? e.width || e.height || e.top || !e.left || (e.left = null) : e.top = null, e }, _proportionallyResize: function () { if (this._proportionallyResizeElements.length) { var e, t, i, s, n, a = this.helper || this.element; for (e = 0; this._proportionallyResizeElements.length > e; e++) { if (n = this._proportionallyResizeElements[e], !this.borderDif) for (this.borderDif = [], i = [n.css("borderTopWidth"), n.css("borderRightWidth"), n.css("borderBottomWidth"), n.css("borderLeftWidth")], s = [n.css("paddingTop"), n.css("paddingRight"), n.css("paddingBottom"), n.css("paddingLeft")], t = 0; i.length > t; t++) this.borderDif[t] = (parseInt(i[t], 10) || 0) + (parseInt(s[t], 10) || 0); n.css({ height: a.height() - this.borderDif[0] - this.borderDif[2] || 0, width: a.width() - this.borderDif[1] - this.borderDif[3] || 0 }) } } }, _renderProxy: function () { var t = this.element, i = this.options; this.elementOffset = t.offset(), this._helper ? (this.helper = this.helper || e("<div style='overflow:hidden;'></div>"), this.helper.addClass(this._helper).css({ width: this.element.outerWidth() - 1, height: this.element.outerHeight() - 1, position: "absolute", left: this.elementOffset.left + "px", top: this.elementOffset.top + "px", zIndex: ++i.zIndex }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element }, _change: { e: function (e, t) { return { width: this.originalSize.width + t } }, w: function (e, t) { var i = this.originalSize, s = this.originalPosition; return { left: s.left + t, width: i.width - t } }, n: function (e, t, i) { var s = this.originalSize, n = this.originalPosition; return { top: n.top + i, height: s.height - i } }, s: function (e, t, i) { return { height: this.originalSize.height + i } }, se: function (t, i, s) { return e.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [t, i, s])) }, sw: function (t, i, s) { return e.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [t, i, s])) }, ne: function (t, i, s) { return e.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [t, i, s])) }, nw: function (t, i, s) { return e.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [t, i, s])) } }, _propagate: function (t, i) { e.ui.plugin.call(this, t, [i, this.ui()]), "resize" !== t && this._trigger(t, i, this.ui()) }, plugins: {}, ui: function () { return { originalElement: this.originalElement, element: this.element, helper: this.helper, position: this.position, size: this.size, originalSize: this.originalSize, originalPosition: this.originalPosition } } }), e.ui.plugin.add("resizable", "animate", { stop: function (t) { var i = e(this).data("ui-resizable"), s = i.options, n = i._proportionallyResizeElements, a = n.length && /textarea/i.test(n[0].nodeName), o = a && e.ui.hasScroll(n[0], "left") ? 0 : i.sizeDiff.height, r = a ? 0 : i.sizeDiff.width, h = { width: i.size.width - r, height: i.size.height - o }, l = parseInt(i.element.css("left"), 10) + (i.position.left - i.originalPosition.left) || null, u = parseInt(i.element.css("top"), 10) + (i.position.top - i.originalPosition.top) || null; i.element.animate(e.extend(h, u && l ? { top: u, left: l } : {}), { duration: s.animateDuration, easing: s.animateEasing, step: function () { var s = { width: parseInt(i.element.css("width"), 10), height: parseInt(i.element.css("height"), 10), top: parseInt(i.element.css("top"), 10), left: parseInt(i.element.css("left"), 10) }; n && n.length && e(n[0]).css({ width: s.width, height: s.height }), i._updateCache(s), i._propagate("resize", t) } }) } }), e.ui.plugin.add("resizable", "containment", { start: function () { var i, s, n, a, o, r, h, l = e(this).data("ui-resizable"), u = l.options, c = l.element, d = u.containment, p = d instanceof e ? d.get(0) : /parent/.test(d) ? c.parent().get(0) : d; p && (l.containerElement = e(p), /document/.test(d) || d === document ? (l.containerOffset = { left: 0, top: 0 }, l.containerPosition = { left: 0, top: 0 }, l.parentData = { element: e(document), left: 0, top: 0, width: e(document).width(), height: e(document).height() || document.body.parentNode.scrollHeight }) : (i = e(p), s = [], e(["Top", "Right", "Left", "Bottom"]).each(function (e, n) { s[e] = t(i.css("padding" + n)) }), l.containerOffset = i.offset(), l.containerPosition = i.position(), l.containerSize = { height: i.innerHeight() - s[3], width: i.innerWidth() - s[1] }, n = l.containerOffset, a = l.containerSize.height, o = l.containerSize.width, r = e.ui.hasScroll(p, "left") ? p.scrollWidth : o, h = e.ui.hasScroll(p) ? p.scrollHeight : a, l.parentData = { element: p, left: n.left, top: n.top, width: r, height: h })) }, resize: function (t) { var i, s, n, a, o = e(this).data("ui-resizable"), r = o.options, h = o.containerOffset, l = o.position, u = o._aspectRatio || t.shiftKey, c = { top: 0, left: 0 }, d = o.containerElement; d[0] !== document && /static/.test(d.css("position")) && (c = h), l.left < (o._helper ? h.left : 0) && (o.size.width = o.size.width + (o._helper ? o.position.left - h.left : o.position.left - c.left), u && (o.size.height = o.size.width / o.aspectRatio), o.position.left = r.helper ? h.left : 0), l.top < (o._helper ? h.top : 0) && (o.size.height = o.size.height + (o._helper ? o.position.top - h.top : o.position.top), u && (o.size.width = o.size.height * o.aspectRatio), o.position.top = o._helper ? h.top : 0), o.offset.left = o.parentData.left + o.position.left, o.offset.top = o.parentData.top + o.position.top, i = Math.abs((o._helper ? o.offset.left - c.left : o.offset.left - c.left) + o.sizeDiff.width), s = Math.abs((o._helper ? o.offset.top - c.top : o.offset.top - h.top) + o.sizeDiff.height), n = o.containerElement.get(0) === o.element.parent().get(0), a = /relative|absolute/.test(o.containerElement.css("position")), n && a && (i -= o.parentData.left), i + o.size.width >= o.parentData.width && (o.size.width = o.parentData.width - i, u && (o.size.height = o.size.width / o.aspectRatio)), s + o.size.height >= o.parentData.height && (o.size.height = o.parentData.height - s, u && (o.size.width = o.size.height * o.aspectRatio)) }, stop: function () { var t = e(this).data("ui-resizable"), i = t.options, s = t.containerOffset, n = t.containerPosition, a = t.containerElement, o = e(t.helper), r = o.offset(), h = o.outerWidth() - t.sizeDiff.width, l = o.outerHeight() - t.sizeDiff.height; t._helper && !i.animate && /relative/.test(a.css("position")) && e(this).css({ left: r.left - n.left - s.left, width: h, height: l }), t._helper && !i.animate && /static/.test(a.css("position")) && e(this).css({ left: r.left - n.left - s.left, width: h, height: l }) } }), e.ui.plugin.add("resizable", "alsoResize", { start: function () { var t = e(this).data("ui-resizable"), i = t.options, s = function (t) { e(t).each(function () { var t = e(this); t.data("ui-resizable-alsoresize", { width: parseInt(t.width(), 10), height: parseInt(t.height(), 10), left: parseInt(t.css("left"), 10), top: parseInt(t.css("top"), 10) }) }) }; "object" != typeof i.alsoResize || i.alsoResize.parentNode ? s(i.alsoResize) : i.alsoResize.length ? (i.alsoResize = i.alsoResize[0], s(i.alsoResize)) : e.each(i.alsoResize, function (e) { s(e) }) }, resize: function (t, i) { var s = e(this).data("ui-resizable"), n = s.options, a = s.originalSize, o = s.originalPosition, r = { height: s.size.height - a.height || 0, width: s.size.width - a.width || 0, top: s.position.top - o.top || 0, left: s.position.left - o.left || 0 }, h = function (t, s) { e(t).each(function () { var t = e(this), n = e(this).data("ui-resizable-alsoresize"), a = {}, o = s && s.length ? s : t.parents(i.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"]; e.each(o, function (e, t) { var i = (n[t] || 0) + (r[t] || 0); i && i >= 0 && (a[t] = i || null) }), t.css(a) }) }; "object" != typeof n.alsoResize || n.alsoResize.nodeType ? h(n.alsoResize) : e.each(n.alsoResize, function (e, t) { h(e, t) }) }, stop: function () { e(this).removeData("resizable-alsoresize") } }), e.ui.plugin.add("resizable", "ghost", { start: function () { var t = e(this).data("ui-resizable"), i = t.options, s = t.size; t.ghost = t.originalElement.clone(), t.ghost.css({ opacity: .25, display: "block", position: "relative", height: s.height, width: s.width, margin: 0, left: 0, top: 0 }).addClass("ui-resizable-ghost").addClass("string" == typeof i.ghost ? i.ghost : ""), t.ghost.appendTo(t.helper) }, resize: function () { var t = e(this).data("ui-resizable"); t.ghost && t.ghost.css({ position: "relative", height: t.size.height, width: t.size.width }) }, stop: function () { var t = e(this).data("ui-resizable"); t.ghost && t.helper && t.helper.get(0).removeChild(t.ghost.get(0)) } }), e.ui.plugin.add("resizable", "grid", { resize: function () { var t = e(this).data("ui-resizable"), i = t.options, s = t.size, n = t.originalSize, a = t.originalPosition, o = t.axis, r = "number" == typeof i.grid ? [i.grid, i.grid] : i.grid, h = r[0] || 1, l = r[1] || 1, u = Math.round((s.width - n.width) / h) * h, c = Math.round((s.height - n.height) / l) * l, d = n.width + u, p = n.height + c, f = i.maxWidth && d > i.maxWidth, m = i.maxHeight && p > i.maxHeight, g = i.minWidth && i.minWidth > d, v = i.minHeight && i.minHeight > p; i.grid = r, g && (d += h), v && (p += l), f && (d -= h), m && (p -= l), /^(se|s|e)$/.test(o) ? (t.size.width = d, t.size.height = p) : /^(ne)$/.test(o) ? (t.size.width = d, t.size.height = p, t.position.top = a.top - c) : /^(sw)$/.test(o) ? (t.size.width = d, t.size.height = p, t.position.left = a.left - u) : (t.size.width = d, t.size.height = p, t.position.top = a.top - c, t.position.left = a.left - u) } }) })(jQuery); (function (t) { var e = 0, i = {}, s = {}; i.height = i.paddingTop = i.paddingBottom = i.borderTopWidth = i.borderBottomWidth = "hide", s.height = s.paddingTop = s.paddingBottom = s.borderTopWidth = s.borderBottomWidth = "show", t.widget("ui.accordion", { version: "1.10.3", options: { active: 0, animate: {}, collapsible: !1, event: "click", header: "> li > :first-child,> :not(li):even", heightStyle: "auto", icons: { activeHeader: "ui-icon-triangle-1-s", header: "ui-icon-triangle-1-e" }, activate: null, beforeActivate: null }, _create: function () { var e = this.options; this.prevShow = this.prevHide = t(), this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist"), e.collapsible || e.active !== !1 && null != e.active || (e.active = 0), this._processPanels(), 0 > e.active && (e.active += this.headers.length), this._refresh() }, _getCreateEventData: function () { return { header: this.active, panel: this.active.length ? this.active.next() : t(), content: this.active.length ? this.active.next() : t() } }, _createIcons: function () { var e = this.options.icons; e && (t("<span>").addClass("ui-accordion-header-icon ui-icon " + e.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(e.header).addClass(e.activeHeader), this.headers.addClass("ui-accordion-icons")) }, _destroyIcons: function () { this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove() }, _destroy: function () { var t; this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"), this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function () { /^ui-accordion/.test(this.id) && this.removeAttribute("id") }), this._destroyIcons(), t = this.headers.next().css("display", "").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function () { /^ui-accordion/.test(this.id) && this.removeAttribute("id") }), "content" !== this.options.heightStyle && t.css("height", "") }, _setOption: function (t, e) { return "active" === t ? (this._activate(e), undefined) : ("event" === t && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || this.options.active !== !1 || this._activate(0), "icons" === t && (this._destroyIcons(), e && this._createIcons()), "disabled" === t && this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!e), undefined) }, _keydown: function (e) { if (!e.altKey && !e.ctrlKey) { var i = t.ui.keyCode, s = this.headers.length, n = this.headers.index(e.target), a = !1; switch (e.keyCode) { case i.RIGHT: case i.DOWN: a = this.headers[(n + 1) % s]; break; case i.LEFT: case i.UP: a = this.headers[(n - 1 + s) % s]; break; case i.SPACE: case i.ENTER: this._eventHandler(e); break; case i.HOME: a = this.headers[0]; break; case i.END: a = this.headers[s - 1] } a && (t(e.target).attr("tabIndex", -1), t(a).attr("tabIndex", 0), a.focus(), e.preventDefault()) } }, _panelKeyDown: function (e) { e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().focus() }, refresh: function () { var e = this.options; this._processPanels(), e.active === !1 && e.collapsible === !0 || !this.headers.length ? (e.active = !1, this.active = t()) : e.active === !1 ? this._activate(0) : this.active.length && !t.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (e.active = !1, this.active = t()) : this._activate(Math.max(0, e.active - 1)) : e.active = this.headers.index(this.active), this._destroyIcons(), this._refresh() }, _processPanels: function () { this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"), this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide() }, _refresh: function () { var i, s = this.options, n = s.heightStyle, a = this.element.parent(), o = this.accordionId = "ui-accordion-" + (this.element.attr("id") || ++e); this.active = this._findActive(s.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"), this.active.next().addClass("ui-accordion-content-active").show(), this.headers.attr("role", "tab").each(function (e) { var i = t(this), s = i.attr("id"), n = i.next(), a = n.attr("id"); s || (s = o + "-header-" + e, i.attr("id", s)), a || (a = o + "-panel-" + e, n.attr("id", a)), i.attr("aria-controls", a), n.attr("aria-labelledby", s) }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({ "aria-selected": "false", tabIndex: -1 }).next().attr({ "aria-expanded": "false", "aria-hidden": "true" }).hide(), this.active.length ? this.active.attr({ "aria-selected": "true", tabIndex: 0 }).next().attr({ "aria-expanded": "true", "aria-hidden": "false" }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(s.event), "fill" === n ? (i = a.height(), this.element.siblings(":visible").each(function () { var e = t(this), s = e.css("position"); "absolute" !== s && "fixed" !== s && (i -= e.outerHeight(!0)) }), this.headers.each(function () { i -= t(this).outerHeight(!0) }), this.headers.next().each(function () { t(this).height(Math.max(0, i - t(this).innerHeight() + t(this).height())) }).css("overflow", "auto")) : "auto" === n && (i = 0, this.headers.next().each(function () { i = Math.max(i, t(this).css("height", "").height()) }).height(i)) }, _activate: function (e) { var i = this._findActive(e)[0]; i !== this.active[0] && (i = i || this.active[0], this._eventHandler({ target: i, currentTarget: i, preventDefault: t.noop })) }, _findActive: function (e) { return "number" == typeof e ? this.headers.eq(e) : t() }, _setupEvents: function (e) { var i = { keydown: "_keydown" }; e && t.each(e.split(" "), function (t, e) { i[e] = "_eventHandler" }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, i), this._on(this.headers.next(), { keydown: "_panelKeyDown" }), this._hoverable(this.headers), this._focusable(this.headers) }, _eventHandler: function (e) { var i = this.options, s = this.active, n = t(e.currentTarget), a = n[0] === s[0], o = a && i.collapsible, r = o ? t() : n.next(), h = s.next(), l = { oldHeader: s, oldPanel: h, newHeader: o ? t() : n, newPanel: r }; e.preventDefault(), a && !i.collapsible || this._trigger("beforeActivate", e, l) === !1 || (i.active = o ? !1 : this.headers.index(n), this.active = a ? t() : n, this._toggle(l), s.removeClass("ui-accordion-header-active ui-state-active"), i.icons && s.children(".ui-accordion-header-icon").removeClass(i.icons.activeHeader).addClass(i.icons.header), a || (n.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"), i.icons && n.children(".ui-accordion-header-icon").removeClass(i.icons.header).addClass(i.icons.activeHeader), n.next().addClass("ui-accordion-content-active"))) }, _toggle: function (e) { var i = e.newPanel, s = this.prevShow.length ? this.prevShow : e.oldPanel; this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = i, this.prevHide = s, this.options.animate ? this._animate(i, s, e) : (s.hide(), i.show(), this._toggleComplete(e)), s.attr({ "aria-expanded": "false", "aria-hidden": "true" }), s.prev().attr("aria-selected", "false"), i.length && s.length ? s.prev().attr("tabIndex", -1) : i.length && this.headers.filter(function () { return 0 === t(this).attr("tabIndex") }).attr("tabIndex", -1), i.attr({ "aria-expanded": "true", "aria-hidden": "false" }).prev().attr({ "aria-selected": "true", tabIndex: 0 }) }, _animate: function (t, e, n) { var a, o, r, h = this, l = 0, c = t.length && (!e.length || t.index() < e.index()), u = this.options.animate || {}, d = c && u.down || u, p = function () { h._toggleComplete(n) }; return "number" == typeof d && (r = d), "string" == typeof d && (o = d), o = o || d.easing || u.easing, r = r || d.duration || u.duration, e.length ? t.length ? (a = t.show().outerHeight(), e.animate(i, { duration: r, easing: o, step: function (t, e) { e.now = Math.round(t) } }), t.hide().animate(s, { duration: r, easing: o, complete: p, step: function (t, i) { i.now = Math.round(t), "height" !== i.prop ? l += i.now : "content" !== h.options.heightStyle && (i.now = Math.round(a - e.outerHeight() - l), l = 0) } }), undefined) : e.animate(i, r, o, p) : t.animate(s, r, o, p) }, _toggleComplete: function (t) { var e = t.oldPanel; e.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"), e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t) } }) })(jQuery); (function (t) { var e = 0; t.widget("ui.autocomplete", { version: "1.10.3", defaultElement: "<input>", options: { appendTo: null, autoFocus: !1, delay: 300, minLength: 1, position: { my: "left top", at: "left bottom", collision: "none" }, source: null, change: null, close: null, focus: null, open: null, response: null, search: null, select: null }, pending: 0, _create: function () { var e, i, s, n = this.element[0].nodeName.toLowerCase(), a = "textarea" === n, o = "input" === n; this.isMultiLine = a ? !0 : o ? !1 : this.element.prop("isContentEditable"), this.valueMethod = this.element[a || o ? "val" : "text"], this.isNewMenu = !0, this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off"), this._on(this.element, { keydown: function (n) { if (this.element.prop("readOnly")) return e = !0, s = !0, i = !0, undefined; e = !1, s = !1, i = !1; var a = t.ui.keyCode; switch (n.keyCode) { case a.PAGE_UP: e = !0, this._move("previousPage", n); break; case a.PAGE_DOWN: e = !0, this._move("nextPage", n); break; case a.UP: e = !0, this._keyEvent("previous", n); break; case a.DOWN: e = !0, this._keyEvent("next", n); break; case a.ENTER: case a.NUMPAD_ENTER: this.menu.active && (e = !0, n.preventDefault(), this.menu.select(n)); break; case a.TAB: this.menu.active && this.menu.select(n); break; case a.ESCAPE: this.menu.element.is(":visible") && (this._value(this.term), this.close(n), n.preventDefault()); break; default: i = !0, this._searchTimeout(n) } }, keypress: function (s) { if (e) return e = !1, (!this.isMultiLine || this.menu.element.is(":visible")) && s.preventDefault(), undefined; if (!i) { var n = t.ui.keyCode; switch (s.keyCode) { case n.PAGE_UP: this._move("previousPage", s); break; case n.PAGE_DOWN: this._move("nextPage", s); break; case n.UP: this._keyEvent("previous", s); break; case n.DOWN: this._keyEvent("next", s) } } }, input: function (t) { return s ? (s = !1, t.preventDefault(), undefined) : (this._searchTimeout(t), undefined) }, focus: function () { this.selectedItem = null, this.previous = this._value() }, blur: function (t) { return this.cancelBlur ? (delete this.cancelBlur, undefined) : (clearTimeout(this.searching), this.close(t), this._change(t), undefined) } }), this._initSource(), this.menu = t("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({ role: null }).hide().data("ui-menu"), this._on(this.menu.element, { mousedown: function (e) { e.preventDefault(), this.cancelBlur = !0, this._delay(function () { delete this.cancelBlur }); var i = this.menu.element[0]; t(e.target).closest(".ui-menu-item").length || this._delay(function () { var e = this; this.document.one("mousedown", function (s) { s.target === e.element[0] || s.target === i || t.contains(i, s.target) || e.close() }) }) }, menufocus: function (e, i) { if (this.isNewMenu && (this.isNewMenu = !1, e.originalEvent && /^mouse/.test(e.originalEvent.type))) return this.menu.blur(), this.document.one("mousemove", function () { t(e.target).trigger(e.originalEvent) }), undefined; var s = i.item.data("ui-autocomplete-item"); !1 !== this._trigger("focus", e, { item: s }) ? e.originalEvent && /^key/.test(e.originalEvent.type) && this._value(s.value) : this.liveRegion.text(s.value) }, menuselect: function (t, e) { var i = e.item.data("ui-autocomplete-item"), s = this.previous; this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = s, this._delay(function () { this.previous = s, this.selectedItem = i })), !1 !== this._trigger("select", t, { item: i }) && this._value(i.value), this.term = this._value(), this.close(t), this.selectedItem = i } }), this.liveRegion = t("<span>", { role: "status", "aria-live": "polite" }).addClass("ui-helper-hidden-accessible").insertBefore(this.element), this._on(this.window, { beforeunload: function () { this.element.removeAttr("autocomplete") } }) }, _destroy: function () { clearTimeout(this.searching), this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove() }, _setOption: function (t, e) { this._super(t, e), "source" === t && this._initSource(), "appendTo" === t && this.menu.element.appendTo(this._appendTo()), "disabled" === t && e && this.xhr && this.xhr.abort() }, _appendTo: function () { var e = this.options.appendTo; return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e || (e = this.element.closest(".ui-front")), e.length || (e = this.document[0].body), e }, _initSource: function () { var e, i, s = this; t.isArray(this.options.source) ? (e = this.options.source, this.source = function (i, s) { s(t.ui.autocomplete.filter(e, i.term)) }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function (e, n) { s.xhr && s.xhr.abort(), s.xhr = t.ajax({ url: i, data: e, dataType: "json", success: function (t) { n(t) }, error: function () { n([]) } }) }) : this.source = this.options.source }, _searchTimeout: function (t) { clearTimeout(this.searching), this.searching = this._delay(function () { this.term !== this._value() && (this.selectedItem = null, this.search(null, t)) }, this.options.delay) }, search: function (t, e) { return t = null != t ? t : this._value(), this.term = this._value(), t.length < this.options.minLength ? this.close(e) : this._trigger("search", e) !== !1 ? this._search(t) : undefined }, _search: function (t) { this.pending++, this.element.addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({ term: t }, this._response()) }, _response: function () { var t = this, i = ++e; return function (s) { i === e && t.__response(s), t.pending--, t.pending || t.element.removeClass("ui-autocomplete-loading") } }, __response: function (t) { t && (t = this._normalize(t)), this._trigger("response", null, { content: t }), !this.options.disabled && t && t.length && !this.cancelSearch ? (this._suggest(t), this._trigger("open")) : this._close() }, close: function (t) { this.cancelSearch = !0, this._close(t) }, _close: function (t) { this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", t)) }, _change: function (t) { this.previous !== this._value() && this._trigger("change", t, { item: this.selectedItem }) }, _normalize: function (e) { return e.length && e[0].label && e[0].value ? e : t.map(e, function (e) { return "string" == typeof e ? { label: e, value: e } : t.extend({ label: e.label || e.value, value: e.value || e.label }, e) }) }, _suggest: function (e) { var i = this.menu.element.empty(); this._renderMenu(i, e), this.isNewMenu = !0, this.menu.refresh(), i.show(), this._resizeMenu(), i.position(t.extend({ of: this.element }, this.options.position)), this.options.autoFocus && this.menu.next() }, _resizeMenu: function () { var t = this.menu.element; t.outerWidth(Math.max(t.width("").outerWidth() + 1, this.element.outerWidth())) }, _renderMenu: function (e, i) { var s = this; t.each(i, function (t, i) { s._renderItemData(e, i) }) }, _renderItemData: function (t, e) { return this._renderItem(t, e).data("ui-autocomplete-item", e) }, _renderItem: function (e, i) { return t("<li>").append(t("<a>").text(i.label)).appendTo(e) }, _move: function (t, e) { return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(t) || this.menu.isLastItem() && /^next/.test(t) ? (this._value(this.term), this.menu.blur(), undefined) : (this.menu[t](e), undefined) : (this.search(null, e), undefined) }, widget: function () { return this.menu.element }, _value: function () { return this.valueMethod.apply(this.element, arguments) }, _keyEvent: function (t, e) { (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(t, e), e.preventDefault()) } }), t.extend(t.ui.autocomplete, { escapeRegex: function (t) { return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") }, filter: function (e, i) { var s = RegExp(t.ui.autocomplete.escapeRegex(i), "i"); return t.grep(e, function (t) { return s.test(t.label || t.value || t) }) } }), t.widget("ui.autocomplete", t.ui.autocomplete, { options: { messages: { noResults: "No search results.", results: function (t) { return t + (t > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate." } } }, __response: function (t) { var e; this._superApply(arguments), this.options.disabled || this.cancelSearch || (e = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults, this.liveRegion.text(e)) } }) })(jQuery); (function (t) { var e, i, s, n, a = "ui-button ui-widget ui-state-default ui-corner-all", o = "ui-state-hover ui-state-active ", r = "ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only", h = function () { var e = t(this); setTimeout(function () { e.find(":ui-button").button("refresh") }, 1) }, l = function (e) { var i = e.name, s = e.form, n = t([]); return i && (i = i.replace(/'/g, "\\'"), n = s ? t(s).find("[name='" + i + "']") : t("[name='" + i + "']", e.ownerDocument).filter(function () { return !this.form })), n }; t.widget("ui.button", { version: "1.10.3", defaultElement: "<button>", options: { disabled: null, text: !0, label: null, icons: { primary: null, secondary: null } }, _create: function () { this.element.closest("form").unbind("reset" + this.eventNamespace).bind("reset" + this.eventNamespace, h), "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled), this._determineButtonType(), this.hasTitle = !!this.buttonElement.attr("title"); var o = this, r = this.options, c = "checkbox" === this.type || "radio" === this.type, u = c ? "" : "ui-state-active", d = "ui-state-focus"; null === r.label && (r.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html()), this._hoverable(this.buttonElement), this.buttonElement.addClass(a).attr("role", "button").bind("mouseenter" + this.eventNamespace, function () { r.disabled || this === e && t(this).addClass("ui-state-active") }).bind("mouseleave" + this.eventNamespace, function () { r.disabled || t(this).removeClass(u) }).bind("click" + this.eventNamespace, function (t) { r.disabled && (t.preventDefault(), t.stopImmediatePropagation()) }), this.element.bind("focus" + this.eventNamespace, function () { o.buttonElement.addClass(d) }).bind("blur" + this.eventNamespace, function () { o.buttonElement.removeClass(d) }), c && (this.element.bind("change" + this.eventNamespace, function () { n || o.refresh() }), this.buttonElement.bind("mousedown" + this.eventNamespace, function (t) { r.disabled || (n = !1, i = t.pageX, s = t.pageY) }).bind("mouseup" + this.eventNamespace, function (t) { r.disabled || (i !== t.pageX || s !== t.pageY) && (n = !0) })), "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function () { return r.disabled || n ? !1 : undefined }) : "radio" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function () { if (r.disabled || n) return !1; t(this).addClass("ui-state-active"), o.buttonElement.attr("aria-pressed", "true"); var e = o.element[0]; l(e).not(e).map(function () { return t(this).button("widget")[0] }).removeClass("ui-state-active").attr("aria-pressed", "false") }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function () { return r.disabled ? !1 : (t(this).addClass("ui-state-active"), e = this, o.document.one("mouseup", function () { e = null }), undefined) }).bind("mouseup" + this.eventNamespace, function () { return r.disabled ? !1 : (t(this).removeClass("ui-state-active"), undefined) }).bind("keydown" + this.eventNamespace, function (e) { return r.disabled ? !1 : ((e.keyCode === t.ui.keyCode.SPACE || e.keyCode === t.ui.keyCode.ENTER) && t(this).addClass("ui-state-active"), undefined) }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function () { t(this).removeClass("ui-state-active") }), this.buttonElement.is("a") && this.buttonElement.keyup(function (e) { e.keyCode === t.ui.keyCode.SPACE && t(this).click() })), this._setOption("disabled", r.disabled), this._resetButton() }, _determineButtonType: function () { var t, e, i; this.type = this.element.is("[type=checkbox]") ? "checkbox" : this.element.is("[type=radio]") ? "radio" : this.element.is("input") ? "input" : "button", "checkbox" === this.type || "radio" === this.type ? (t = this.element.parents().last(), e = "label[for='" + this.element.attr("id") + "']", this.buttonElement = t.find(e), this.buttonElement.length || (t = t.length ? t.siblings() : this.element.siblings(), this.buttonElement = t.filter(e), this.buttonElement.length || (this.buttonElement = t.find(e))), this.element.addClass("ui-helper-hidden-accessible"), i = this.element.is(":checked"), i && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", i)) : this.buttonElement = this.element }, widget: function () { return this.buttonElement }, _destroy: function () { this.element.removeClass("ui-helper-hidden-accessible"), this.buttonElement.removeClass(a + " " + o + " " + r).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()), this.hasTitle || this.buttonElement.removeAttr("title") }, _setOption: function (t, e) { return this._super(t, e), "disabled" === t ? (e ? this.element.prop("disabled", !0) : this.element.prop("disabled", !1), undefined) : (this._resetButton(), undefined) }, refresh: function () { var e = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled"); e !== this.options.disabled && this._setOption("disabled", e), "radio" === this.type ? l(this.element[0]).each(function () { t(this).is(":checked") ? t(this).button("widget").addClass("ui-state-active").attr("aria-pressed", "true") : t(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false") }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false")) }, _resetButton: function () { if ("input" === this.type) return this.options.label && this.element.val(this.options.label), undefined; var e = this.buttonElement.removeClass(r), i = t("<span></span>", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(e.empty()).text(), s = this.options.icons, n = s.primary && s.secondary, a = []; s.primary || s.secondary ? (this.options.text && a.push("ui-button-text-icon" + (n ? "s" : s.primary ? "-primary" : "-secondary")), s.primary && e.prepend("<span class='ui-button-icon-primary ui-icon " + s.primary + "'></span>"), s.secondary && e.append("<span class='ui-button-icon-secondary ui-icon " + s.secondary + "'></span>"), this.options.text || (a.push(n ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || e.attr("title", t.trim(i)))) : a.push("ui-button-text-only"), e.addClass(a.join(" ")) } }), t.widget("ui.buttonset", { version: "1.10.3", options: { items: "button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)" }, _create: function () { this.element.addClass("ui-buttonset") }, _init: function () { this.refresh() }, _setOption: function (t, e) { "disabled" === t && this.buttons.button("option", t, e), this._super(t, e) }, refresh: function () { var e = "rtl" === this.element.css("direction"); this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function () { return t(this).button("widget")[0] }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(e ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(e ? "ui-corner-left" : "ui-corner-right").end().end() }, _destroy: function () { this.element.removeClass("ui-buttonset"), this.buttons.map(function () { return t(this).button("widget")[0] }).removeClass("ui-corner-left ui-corner-right").end().button("destroy") } }) })(jQuery); (function (t, e) {
			function i() { this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = { closeText: "Done", prevText: "Prev", nextText: "Next", currentText: "Today", monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"], weekHeader: "Wk", dateFormat: "mm/dd/yy", firstDay: 0, isRTL: !1, showMonthAfterYear: !1, yearSuffix: "" }, this._defaults = { showOn: "focus", showAnim: "fadeIn", showOptions: {}, defaultDate: null, appendText: "", buttonText: "...", buttonImage: "", buttonImageOnly: !1, hideIfNoPrevNext: !1, navigationAsDateFormat: !1, gotoCurrent: !1, changeMonth: !1, changeYear: !1, yearRange: "c-10:c+10", showOtherMonths: !1, selectOtherMonths: !1, showWeek: !1, calculateWeek: this.iso8601Week, shortYearCutoff: "+10", minDate: null, maxDate: null, duration: "fast", beforeShowDay: null, beforeShow: null, onSelect: null, onChangeMonthYear: null, onClose: null, numberOfMonths: 1, showCurrentAtPos: 0, stepMonths: 1, stepBigMonths: 12, altField: "", altFormat: "", constrainInput: !0, showButtonPanel: !1, autoSize: !1, disabled: !1 }, t.extend(this._defaults, this.regional[""]), this.dpDiv = s(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) } function s(e) { var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a"; return e.delegate(i, "mouseout", function () { t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover") }).delegate(i, "mouseover", function () { t.datepicker._isDisabledDatepicker(a.inline ? e.parent()[0] : a.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover")) }) } function n(e, i) { t.extend(e, i); for (var s in i) null == i[s] && (e[s] = i[s]); return e } t.extend(t.ui, { datepicker: { version: "1.10.3" } }); var a, r = "datepicker"; t.extend(i.prototype, {
				markerClassName: "hasDatepicker", maxRows: 4, _widgetDatepicker: function () { return this.dpDiv }, setDefaults: function (t) { return n(this._defaults, t || {}), this }, _attachDatepicker: function (e, i) { var s, n, a; s = e.nodeName.toLowerCase(), n = "div" === s || "span" === s, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), a = this._newInst(t(e), n), a.settings = t.extend({}, i || {}), "input" === s ? this._connectDatepicker(e, a) : n && this._inlineDatepicker(e, a) }, _newInst: function (e, i) { var n = e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"); return { id: n, input: e, selectedDay: 0, selectedMonth: 0, selectedYear: 0, drawMonth: 0, drawYear: 0, inline: i, dpDiv: i ? s(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv } }, _connectDatepicker: function (e, i) { var s = t(e); i.append = t([]), i.trigger = t([]), s.hasClass(this.markerClassName) || (this._attachments(s, i), s.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(i), t.data(e, r, i), i.settings.disabled && this._disableDatepicker(e)) }, _attachments: function (e, i) { var s, n, a, r = this._get(i, "appendText"), o = this._get(i, "isRTL"); i.append && i.append.remove(), r && (i.append = t("<span class='" + this._appendClass + "'>" + r + "</span>"), e[o ? "before" : "after"](i.append)), e.unbind("focus", this._showDatepicker), i.trigger && i.trigger.remove(), s = this._get(i, "showOn"), ("focus" === s || "both" === s) && e.focus(this._showDatepicker), ("button" === s || "both" === s) && (n = this._get(i, "buttonText"), a = this._get(i, "buttonImage"), i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({ src: a, alt: n, title: n }) : t("<button type='button'></button>").addClass(this._triggerClass).html(a ? t("<img/>").attr({ src: a, alt: n, title: n }) : n)), e[o ? "before" : "after"](i.trigger), i.trigger.click(function () { return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1 })) }, _autoSize: function (t) { if (this._get(t, "autoSize") && !t.inline) { var e, i, s, n, a = new Date(2009, 11, 20), r = this._get(t, "dateFormat"); r.match(/[DM]/) && (e = function (t) { for (i = 0, s = 0, n = 0; t.length > n; n++) t[n].length > i && (i = t[n].length, s = n); return s }, a.setMonth(e(this._get(t, r.match(/MM/) ? "monthNames" : "monthNamesShort"))), a.setDate(e(this._get(t, r.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - a.getDay())), t.input.attr("size", this._formatDate(t, a).length) } }, _inlineDatepicker: function (e, i) { var s = t(e); s.hasClass(this.markerClassName) || (s.addClass(this.markerClassName).append(i.dpDiv), t.data(e, r, i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block")) }, _dialogDatepicker: function (e, i, s, a, o) { var h, l, c, u, d, p = this._dialogInst; return p || (this.uuid += 1, h = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + h + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.keydown(this._doKeyDown), t("body").append(this._dialogInput), p = this._dialogInst = this._newInst(this._dialogInput, !1), p.settings = {}, t.data(this._dialogInput[0], r, p)), n(p.settings, a || {}), i = i && i.constructor === Date ? this._formatDate(p, i) : i, this._dialogInput.val(i), this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null, this._pos || (l = document.documentElement.clientWidth, c = document.documentElement.clientHeight, u = document.documentElement.scrollLeft || document.body.scrollLeft, d = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [l / 2 - 100 + u, c / 2 - 150 + d]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), p.settings.onSelect = s, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], r, p), this }, _destroyDatepicker: function (e) { var i, s = t(e), n = t.data(e, r); s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, r), "input" === i ? (n.append.remove(), n.trigger.remove(), s.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown", this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && s.removeClass(this.markerClassName).empty()) }, _enableDatepicker: function (e) { var i, s, n = t(e), a = t.data(e, r); n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !1, a.trigger.filter("button").each(function () { this.disabled = !1 }).end().filter("img").css({ opacity: "1.0", cursor: "" })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().removeClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function (t) { return t === e ? null : t })) }, _disableDatepicker: function (e) { var i, s, n = t(e), a = t.data(e, r); n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !0, a.trigger.filter("button").each(function () { this.disabled = !0 }).end().filter("img").css({ opacity: "0.5", cursor: "default" })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().addClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function (t) { return t === e ? null : t }), this._disabledInputs[this._disabledInputs.length] = e) }, _isDisabledDatepicker: function (t) { if (!t) return !1; for (var e = 0; this._disabledInputs.length > e; e++) if (this._disabledInputs[e] === t) return !0; return !1 }, _getInst: function (e) { try { return t.data(e, r) } catch (i) { throw "Missing instance data for this datepicker" } }, _optionDatepicker: function (i, s, a) { var r, o, h, l, c = this._getInst(i); return 2 === arguments.length && "string" == typeof s ? "defaults" === s ? t.extend({}, t.datepicker._defaults) : c ? "all" === s ? t.extend({}, c.settings) : this._get(c, s) : null : (r = s || {}, "string" == typeof s && (r = {}, r[s] = a), c && (this._curInst === c && this._hideDatepicker(), o = this._getDateDatepicker(i, !0), h = this._getMinMaxDate(c, "min"), l = this._getMinMaxDate(c, "max"), n(c.settings, r), null !== h && r.dateFormat !== e && r.minDate === e && (c.settings.minDate = this._formatDate(c, h)), null !== l && r.dateFormat !== e && r.maxDate === e && (c.settings.maxDate = this._formatDate(c, l)), "disabled" in r && (r.disabled ? this._disableDatepicker(i) : this._enableDatepicker(i)), this._attachments(t(i), c), this._autoSize(c), this._setDate(c, o), this._updateAlternate(c), this._updateDatepicker(c)), e) }, _changeDatepicker: function (t, e, i) { this._optionDatepicker(t, e, i) }, _refreshDatepicker: function (t) { var e = this._getInst(t); e && this._updateDatepicker(e) }, _setDateDatepicker: function (t, e) { var i = this._getInst(t); i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i)) }, _getDateDatepicker: function (t, e) { var i = this._getInst(t); return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null }, _doKeyDown: function (e) { var i, s, n, a = t.datepicker._getInst(e.target), r = !0, o = a.dpDiv.is(".ui-datepicker-rtl"); if (a._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) { case 9: t.datepicker._hideDatepicker(), r = !1; break; case 13: return n = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", a.dpDiv), n[0] && t.datepicker._selectDay(e.target, a.selectedMonth, a.selectedYear, n[0]), i = t.datepicker._get(a, "onSelect"), i ? (s = t.datepicker._formatDate(a), i.apply(a.input ? a.input[0] : null, [s, a])) : t.datepicker._hideDatepicker(), !1; case 27: t.datepicker._hideDatepicker(); break; case 33: t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M"); break; case 34: t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M"); break; case 35: (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), r = e.ctrlKey || e.metaKey; break; case 36: (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), r = e.ctrlKey || e.metaKey; break; case 37: (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, o ? 1 : -1, "D"), r = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(a, "stepBigMonths") : -t.datepicker._get(a, "stepMonths"), "M"); break; case 38: (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), r = e.ctrlKey || e.metaKey; break; case 39: (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, o ? -1 : 1, "D"), r = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(a, "stepBigMonths") : +t.datepicker._get(a, "stepMonths"), "M"); break; case 40: (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), r = e.ctrlKey || e.metaKey; break; default: r = !1 } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : r = !1; r && (e.preventDefault(), e.stopPropagation()) }, _doKeyPress: function (i) { var s, n, a = t.datepicker._getInst(i.target); return t.datepicker._get(a, "constrainInput") ? (s = t.datepicker._possibleChars(t.datepicker._get(a, "dateFormat")), n = String.fromCharCode(null == i.charCode ? i.keyCode : i.charCode), i.ctrlKey || i.metaKey || " " > n || !s || s.indexOf(n) > -1) : e }, _doKeyUp: function (e) { var i, s = t.datepicker._getInst(e.target); if (s.input.val() !== s.lastVal) try { i = t.datepicker.parseDate(t.datepicker._get(s, "dateFormat"), s.input ? s.input.val() : null, t.datepicker._getFormatConfig(s)), i && (t.datepicker._setDateFromField(s), t.datepicker._updateAlternate(s), t.datepicker._updateDatepicker(s)) } catch (n) { } return !0 }, _showDatepicker: function (e) { if (e = e.target || e, "input" !== e.nodeName.toLowerCase() && (e = t("input", e.parentNode)[0]), !t.datepicker._isDisabledDatepicker(e) && t.datepicker._lastInput !== e) { var i, s, a, r, o, h, l; i = t.datepicker._getInst(e), t.datepicker._curInst && t.datepicker._curInst !== i && (t.datepicker._curInst.dpDiv.stop(!0, !0), i && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), s = t.datepicker._get(i, "beforeShow"), a = s ? s.apply(e, [e, i]) : {}, a !== !1 && (n(i.settings, a), i.lastVal = null, t.datepicker._lastInput = e, t.datepicker._setDateFromField(i), t.datepicker._inDialog && (e.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(e), t.datepicker._pos[1] += e.offsetHeight), r = !1, t(e).parents().each(function () { return r |= "fixed" === t(this).css("position"), !r }), o = { left: t.datepicker._pos[0], top: t.datepicker._pos[1] }, t.datepicker._pos = null, i.dpDiv.empty(), i.dpDiv.css({ position: "absolute", display: "block", top: "-1000px" }), t.datepicker._updateDatepicker(i), o = t.datepicker._checkOffset(i, o, r), i.dpDiv.css({ position: t.datepicker._inDialog && t.blockUI ? "static" : r ? "fixed" : "absolute", display: "none", left: o.left + "px", top: o.top + "px" }), i.inline || (h = t.datepicker._get(i, "showAnim"), l = t.datepicker._get(i, "duration"), i.dpDiv.zIndex(t(e).zIndex() + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[h] ? i.dpDiv.show(h, t.datepicker._get(i, "showOptions"), l) : i.dpDiv[h || "show"](h ? l : null), t.datepicker._shouldFocusInput(i) && i.input.focus(), t.datepicker._curInst = i)) } }, _updateDatepicker: function (e) { this.maxRows = 4, a = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e), e.dpDiv.find("." + this._dayOverClass + " a").mouseover(); var i, s = this._getNumberOfMonths(e), n = s[1], r = 17; e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), n > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + n).css("width", r * n + "em"), e.dpDiv[(1 !== s[0] || 1 !== s[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.focus(), e.yearshtml && (i = e.yearshtml, setTimeout(function () { i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), i = e.yearshtml = null }, 0)) }, _shouldFocusInput: function (t) { return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus") }, _checkOffset: function (e, i, s) { var n = e.dpDiv.outerWidth(), a = e.dpDiv.outerHeight(), r = e.input ? e.input.outerWidth() : 0, o = e.input ? e.input.outerHeight() : 0, h = document.documentElement.clientWidth + (s ? 0 : t(document).scrollLeft()), l = document.documentElement.clientHeight + (s ? 0 : t(document).scrollTop()); return i.left -= this._get(e, "isRTL") ? n - r : 0, i.left -= s && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, i.top -= s && i.top === e.input.offset().top + o ? t(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + n > h && h > n ? Math.abs(i.left + n - h) : 0), i.top -= Math.min(i.top, i.top + a > l && l > a ? Math.abs(a + o) : 0), i }, _findPos: function (e) { for (var i, s = this._getInst(e), n = this._get(s, "isRTL") ; e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e)) ;) e = e[n ? "previousSibling" : "nextSibling"]; return i = t(e).offset(), [i.left, i.top] }, _hideDatepicker: function (e) { var i, s, n, a, o = this._curInst; !o || e && o !== t.data(e, r) || this._datepickerShowing && (i = this._get(o, "showAnim"), s = this._get(o, "duration"), n = function () { t.datepicker._tidyDialog(o) }, t.effects && (t.effects.effect[i] || t.effects[i]) ? o.dpDiv.hide(i, t.datepicker._get(o, "showOptions"), s, n) : o.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? s : null, n), i || n(), this._datepickerShowing = !1, a = this._get(o, "onClose"), a && a.apply(o.input ? o.input[0] : null, [o.input ? o.input.val() : "", o]), this._lastInput = null, this._inDialog && (this._dialogInput.css({ position: "absolute", left: "0", top: "-100px" }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1) }, _tidyDialog: function (t) { t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar") }, _checkExternalClick: function (e) { if (t.datepicker._curInst) { var i = t(e.target), s = t.datepicker._getInst(i[0]); (i[0].id !== t.datepicker._mainDivId && 0 === i.parents("#" + t.datepicker._mainDivId).length && !i.hasClass(t.datepicker.markerClassName) && !i.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || i.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== s) && t.datepicker._hideDatepicker() } }, _adjustDate: function (e, i, s) { var n = t(e), a = this._getInst(n[0]); this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(a, i + ("M" === s ? this._get(a, "showCurrentAtPos") : 0), s), this._updateDatepicker(a)) }, _gotoToday: function (e) { var i, s = t(e), n = this._getInst(s[0]); this._get(n, "gotoCurrent") && n.currentDay ? (n.selectedDay = n.currentDay, n.drawMonth = n.selectedMonth = n.currentMonth, n.drawYear = n.selectedYear = n.currentYear) : (i = new Date, n.selectedDay = i.getDate(), n.drawMonth = n.selectedMonth = i.getMonth(), n.drawYear = n.selectedYear = i.getFullYear()), this._notifyChange(n), this._adjustDate(s) }, _selectMonthYear: function (e, i, s) { var n = t(e), a = this._getInst(n[0]); a["selected" + ("M" === s ? "Month" : "Year")] = a["draw" + ("M" === s ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(a), this._adjustDate(n) }, _selectDay: function (e, i, s, n) { var a, r = t(e); t(n).hasClass(this._unselectableClass) || this._isDisabledDatepicker(r[0]) || (a = this._getInst(r[0]), a.selectedDay = a.currentDay = t("a", n).html(), a.selectedMonth = a.currentMonth = i, a.selectedYear = a.currentYear = s, this._selectDate(e, this._formatDate(a, a.currentDay, a.currentMonth, a.currentYear))) }, _clearDate: function (e) { var i = t(e); this._selectDate(i, "") }, _selectDate: function (e, i) { var s, n = t(e), a = this._getInst(n[0]); i = null != i ? i : this._formatDate(a), a.input && a.input.val(i), this._updateAlternate(a), s = this._get(a, "onSelect"), s ? s.apply(a.input ? a.input[0] : null, [i, a]) : a.input && a.input.trigger("change"), a.inline ? this._updateDatepicker(a) : (this._hideDatepicker(), this._lastInput = a.input[0], "object" != typeof a.input[0] && a.input.focus(), this._lastInput = null) }, _updateAlternate: function (e) { var i, s, n, a = this._get(e, "altField"); a && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), s = this._getDate(e), n = this.formatDate(i, s, this._getFormatConfig(e)), t(a).each(function () { t(this).val(n) })) }, noWeekends: function (t) { var e = t.getDay(); return [e > 0 && 6 > e, ""] }, iso8601Week: function (t) { var e, i = new Date(t.getTime()); return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1 }, parseDate: function (i, s, n) { if (null == i || null == s) throw "Invalid arguments"; if (s = "object" == typeof s ? "" + s : s + "", "" === s) return null; var a, r, o, h, l = 0, c = (n ? n.shortYearCutoff : null) || this._defaults.shortYearCutoff, u = "string" != typeof c ? c : (new Date).getFullYear() % 100 + parseInt(c, 10), d = (n ? n.dayNamesShort : null) || this._defaults.dayNamesShort, p = (n ? n.dayNames : null) || this._defaults.dayNames, f = (n ? n.monthNamesShort : null) || this._defaults.monthNamesShort, m = (n ? n.monthNames : null) || this._defaults.monthNames, g = -1, v = -1, _ = -1, b = -1, y = !1, x = function (t) { var e = i.length > a + 1 && i.charAt(a + 1) === t; return e && a++, e }, k = function (t) { var e = x(t), i = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2, n = RegExp("^\\d{1," + i + "}"), a = s.substring(l).match(n); if (!a) throw "Missing number at position " + l; return l += a[0].length, parseInt(a[0], 10) }, w = function (i, n, a) { var r = -1, o = t.map(x(i) ? a : n, function (t, e) { return [[e, t]] }).sort(function (t, e) { return -(t[1].length - e[1].length) }); if (t.each(o, function (t, i) { var n = i[1]; return s.substr(l, n.length).toLowerCase() === n.toLowerCase() ? (r = i[0], l += n.length, !1) : e }), -1 !== r) return r + 1; throw "Unknown name at position " + l }, D = function () { if (s.charAt(l) !== i.charAt(a)) throw "Unexpected literal at position " + l; l++ }; for (a = 0; i.length > a; a++) if (y) "'" !== i.charAt(a) || x("'") ? D() : y = !1; else switch (i.charAt(a)) { case "d": _ = k("d"); break; case "D": w("D", d, p); break; case "o": b = k("o"); break; case "m": v = k("m"); break; case "M": v = w("M", f, m); break; case "y": g = k("y"); break; case "@": h = new Date(k("@")), g = h.getFullYear(), v = h.getMonth() + 1, _ = h.getDate(); break; case "!": h = new Date((k("!") - this._ticksTo1970) / 1e4), g = h.getFullYear(), v = h.getMonth() + 1, _ = h.getDate(); break; case "'": x("'") ? D() : y = !0; break; default: D() } if (s.length > l && (o = s.substr(l), !/^\s+/.test(o))) throw "Extra/unparsed characters found in date: " + o; if (-1 === g ? g = (new Date).getFullYear() : 100 > g && (g += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (u >= g ? 0 : -100)), b > -1) for (v = 1, _ = b; ;) { if (r = this._getDaysInMonth(g, v - 1), r >= _) break; v++, _ -= r } if (h = this._daylightSavingAdjust(new Date(g, v - 1, _)), h.getFullYear() !== g || h.getMonth() + 1 !== v || h.getDate() !== _) throw "Invalid date"; return h }, ATOM: "yy-mm-dd", COOKIE: "D, dd M yy", ISO_8601: "yy-mm-dd", RFC_822: "D, d M y", RFC_850: "DD, dd-M-y", RFC_1036: "D, d M y", RFC_1123: "D, d M yy", RFC_2822: "D, d M yy", RSS: "D, d M y", TICKS: "!", TIMESTAMP: "@", W3C: "yy-mm-dd", _ticksTo1970: 1e7 * 60 * 60 * 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)), formatDate: function (t, e, i) { if (!e) return ""; var s, n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort, a = (i ? i.dayNames : null) || this._defaults.dayNames, r = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort, o = (i ? i.monthNames : null) || this._defaults.monthNames, h = function (e) { var i = t.length > s + 1 && t.charAt(s + 1) === e; return i && s++, i }, l = function (t, e, i) { var s = "" + e; if (h(t)) for (; i > s.length;) s = "0" + s; return s }, c = function (t, e, i, s) { return h(t) ? s[e] : i[e] }, u = "", d = !1; if (e) for (s = 0; t.length > s; s++) if (d) "'" !== t.charAt(s) || h("'") ? u += t.charAt(s) : d = !1; else switch (t.charAt(s)) { case "d": u += l("d", e.getDate(), 2); break; case "D": u += c("D", e.getDay(), n, a); break; case "o": u += l("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3); break; case "m": u += l("m", e.getMonth() + 1, 2); break; case "M": u += c("M", e.getMonth(), r, o); break; case "y": u += h("y") ? e.getFullYear() : (10 > e.getYear() % 100 ? "0" : "") + e.getYear() % 100; break; case "@": u += e.getTime(); break; case "!": u += 1e4 * e.getTime() + this._ticksTo1970; break; case "'": h("'") ? u += "'" : d = !0; break; default: u += t.charAt(s) } return u }, _possibleChars: function (t) { var e, i = "", s = !1, n = function (i) { var s = t.length > e + 1 && t.charAt(e + 1) === i; return s && e++, s }; for (e = 0; t.length > e; e++) if (s) "'" !== t.charAt(e) || n("'") ? i += t.charAt(e) : s = !1; else switch (t.charAt(e)) { case "d": case "m": case "y": case "@": i += "0123456789"; break; case "D": case "M": return null; case "'": n("'") ? i += "'" : s = !0; break; default: i += t.charAt(e) } return i }, _get: function (t, i) { return t.settings[i] !== e ? t.settings[i] : this._defaults[i] }, _setDateFromField: function (t, e) { if (t.input.val() !== t.lastVal) { var i = this._get(t, "dateFormat"), s = t.lastVal = t.input ? t.input.val() : null, n = this._getDefaultDate(t), a = n, r = this._getFormatConfig(t); try { a = this.parseDate(i, s, r) || n } catch (o) { s = e ? "" : s } t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), t.currentDay = s ? a.getDate() : 0, t.currentMonth = s ? a.getMonth() : 0, t.currentYear = s ? a.getFullYear() : 0, this._adjustInstDate(t) } }, _getDefaultDate: function (t) { return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date)) }, _determineDate: function (e, i, s) { var n = function (t) { var e = new Date; return e.setDate(e.getDate() + t), e }, a = function (i) { try { return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e)) } catch (s) { } for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date, a = n.getFullYear(), r = n.getMonth(), o = n.getDate(), h = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, l = h.exec(i) ; l;) { switch (l[2] || "d") { case "d": case "D": o += parseInt(l[1], 10); break; case "w": case "W": o += 7 * parseInt(l[1], 10); break; case "m": case "M": r += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(a, r)); break; case "y": case "Y": a += parseInt(l[1], 10), o = Math.min(o, t.datepicker._getDaysInMonth(a, r)) } l = h.exec(i) } return new Date(a, r, o) }, r = null == i || "" === i ? s : "string" == typeof i ? a(i) : "number" == typeof i ? isNaN(i) ? s : n(i) : new Date(i.getTime()); return r = r && "Invalid Date" == "" + r ? s : r, r && (r.setHours(0), r.setMinutes(0), r.setSeconds(0), r.setMilliseconds(0)), this._daylightSavingAdjust(r) }, _daylightSavingAdjust: function (t) { return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null }, _setDate: function (t, e, i) { var s = !e, n = t.selectedMonth, a = t.selectedYear, r = this._restrictMinMax(t, this._determineDate(t, e, new Date)); t.selectedDay = t.currentDay = r.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = r.getMonth(), t.drawYear = t.selectedYear = t.currentYear = r.getFullYear(), n === t.selectedMonth && a === t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(s ? "" : this._formatDate(t)) }, _getDate: function (t) { var e = !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay)); return e }, _attachHandlers: function (e) { var i = this._get(e, "stepMonths"), s = "#" + e.id.replace(/\\\\/g, "\\"); e.dpDiv.find("[data-handler]").map(function () { var e = { prev: function () { t.datepicker._adjustDate(s, -i, "M") }, next: function () { t.datepicker._adjustDate(s, +i, "M") }, hide: function () { t.datepicker._hideDatepicker() }, today: function () { t.datepicker._gotoToday(s) }, selectDay: function () { return t.datepicker._selectDay(s, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1 }, selectMonth: function () { return t.datepicker._selectMonthYear(s, this, "M"), !1 }, selectYear: function () { return t.datepicker._selectMonthYear(s, this, "Y"), !1 } }; t(this).bind(this.getAttribute("data-event"), e[this.getAttribute("data-handler")]) }) }, _generateHTML: function (t) { var e, i, s, n, a, r, o, h, l, c, u, d, p, f, m, g, v, _, b, y, x, k, w, D, T, C, M, S, N, I, P, A, z, H, E, F, O, W, j, R = new Date, L = this._daylightSavingAdjust(new Date(R.getFullYear(), R.getMonth(), R.getDate())), Y = this._get(t, "isRTL"), B = this._get(t, "showButtonPanel"), J = this._get(t, "hideIfNoPrevNext"), K = this._get(t, "navigationAsDateFormat"), Q = this._getNumberOfMonths(t), V = this._get(t, "showCurrentAtPos"), U = this._get(t, "stepMonths"), q = 1 !== Q[0] || 1 !== Q[1], X = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)), G = this._getMinMaxDate(t, "min"), $ = this._getMinMaxDate(t, "max"), Z = t.drawMonth - V, te = t.drawYear; if (0 > Z && (Z += 12, te--), $) for (e = this._daylightSavingAdjust(new Date($.getFullYear(), $.getMonth() - Q[0] * Q[1] + 1, $.getDate())), e = G && G > e ? G : e; this._daylightSavingAdjust(new Date(te, Z, 1)) > e;) Z--, 0 > Z && (Z = 11, te--); for (t.drawMonth = Z, t.drawYear = te, i = this._get(t, "prevText"), i = K ? this.formatDate(i, this._daylightSavingAdjust(new Date(te, Z - U, 1)), this._getFormatConfig(t)) : i, s = this._canAdjustMonth(t, -1, te, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>" : J ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>", n = this._get(t, "nextText"), n = K ? this.formatDate(n, this._daylightSavingAdjust(new Date(te, Z + U, 1)), this._getFormatConfig(t)) : n, a = this._canAdjustMonth(t, 1, te, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>" : J ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>", r = this._get(t, "currentText"), o = this._get(t, "gotoCurrent") && t.currentDay ? X : L, r = K ? this.formatDate(r, o, this._getFormatConfig(t)) : r, h = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", l = B ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (Y ? h : "") + (this._isInRange(t, o) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + r + "</button>" : "") + (Y ? "" : h) + "</div>" : "", c = parseInt(this._get(t, "firstDay"), 10), c = isNaN(c) ? 0 : c, u = this._get(t, "showWeek"), d = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), m = this._get(t, "monthNamesShort"), g = this._get(t, "beforeShowDay"), v = this._get(t, "showOtherMonths"), _ = this._get(t, "selectOtherMonths"), b = this._getDefaultDate(t), y = "", k = 0; Q[0] > k; k++) { for (w = "", this.maxRows = 4, D = 0; Q[1] > D; D++) { if (T = this._daylightSavingAdjust(new Date(te, Z, t.selectedDay)), C = " ui-corner-all", M = "", q) { if (M += "<div class='ui-datepicker-group", Q[1] > 1) switch (D) { case 0: M += " ui-datepicker-group-first", C = " ui-corner-" + (Y ? "right" : "left"); break; case Q[1] - 1: M += " ui-datepicker-group-last", C = " ui-corner-" + (Y ? "left" : "right"); break; default: M += " ui-datepicker-group-middle", C = "" } M += "'>" } for (M += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + C + "'>" + (/all|left/.test(C) && 0 === k ? Y ? a : s : "") + (/all|right/.test(C) && 0 === k ? Y ? s : a : "") + this._generateMonthYearHeader(t, Z, te, G, $, k > 0 || D > 0, f, m) + "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>", S = u ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", x = 0; 7 > x; x++) N = (x + c) % 7, S += "<th" + ((x + c + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + d[N] + "'>" + p[N] + "</span></th>"; for (M += S + "</tr></thead><tbody>", I = this._getDaysInMonth(te, Z), te === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, I)), P = (this._getFirstDayOfMonth(te, Z) - c + 7) % 7, A = Math.ceil((P + I) / 7), z = q ? this.maxRows > A ? this.maxRows : A : A, this.maxRows = z, H = this._daylightSavingAdjust(new Date(te, Z, 1 - P)), E = 0; z > E; E++) { for (M += "<tr>", F = u ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(H) + "</td>" : "", x = 0; 7 > x; x++) O = g ? g.apply(t.input ? t.input[0] : null, [H]) : [!0, ""], W = H.getMonth() !== Z, j = W && !_ || !O[0] || G && G > H || $ && H > $, F += "<td class='" + ((x + c + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (W ? " ui-datepicker-other-month" : "") + (H.getTime() === T.getTime() && Z === t.selectedMonth && t._keyEvent || b.getTime() === H.getTime() && b.getTime() === T.getTime() ? " " + this._dayOverClass : "") + (j ? " " + this._unselectableClass + " ui-state-disabled" : "") + (W && !v ? "" : " " + O[1] + (H.getTime() === X.getTime() ? " " + this._currentClass : "") + (H.getTime() === L.getTime() ? " ui-datepicker-today" : "")) + "'" + (W && !v || !O[2] ? "" : " title='" + O[2].replace(/'/g, "&#39;") + "'") + (j ? "" : " data-handler='selectDay' data-event='click' data-month='" + H.getMonth() + "' data-year='" + H.getFullYear() + "'") + ">" + (W && !v ? "&#xa0;" : j ? "<span class='ui-state-default'>" + H.getDate() + "</span>" : "<a class='ui-state-default" + (H.getTime() === L.getTime() ? " ui-state-highlight" : "") + (H.getTime() === X.getTime() ? " ui-state-active" : "") + (W ? " ui-priority-secondary" : "") + "' href='#'>" + H.getDate() + "</a>") + "</td>", H.setDate(H.getDate() + 1), H = this._daylightSavingAdjust(H); M += F + "</tr>" } Z++, Z > 11 && (Z = 0, te++), M += "</tbody></table>" + (q ? "</div>" + (Q[0] > 0 && D === Q[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), w += M } y += w } return y += l, t._keyEvent = !1, y }, _generateMonthYearHeader: function (t, e, i, s, n, a, r, o) {
					var h, l, c, u, d, p, f, m, g = this._get(t, "changeMonth"), v = this._get(t, "changeYear"), _ = this._get(t, "showMonthAfterYear"), b = "<div class='ui-datepicker-title'>", y = ""; if (a || !g) y += "<span class='ui-datepicker-month'>" + r[e] + "</span>"; else { for (h = s && s.getFullYear() === i, l = n && n.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", c = 0; 12 > c; c++) (!h || c >= s.getMonth()) && (!l || n.getMonth() >= c) && (y += "<option value='" + c + "'" + (c === e ? " selected='selected'" : "") + ">" + o[c] + "</option>"); y += "</select>" } if (_ || (b += y + (!a && g && v ? "" : "&#xa0;")), !t.yearshtml) if (t.yearshtml = "", a || !v) b += "<span class='ui-datepicker-year'>" + i + "</span>"; else {
						for (u = this._get(t, "yearRange").split(":"), d = (new Date).getFullYear(), p = function (t) {
						var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? d + parseInt(t, 10) : parseInt(t, 10);
				return isNaN(e) ? d : e
						}, f = p(u[0]), m = Math.max(f, p(u[1] || "")), f = s ? Math.max(f, s.getFullYear()) : f, m = n ? Math.min(m, n.getFullYear()) : m, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; m >= f; f++) t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>"; t.yearshtml += "</select>", b += t.yearshtml, t.yearshtml = null
					} return b += this._get(t, "yearSuffix"), _ && (b += (!a && g && v ? "" : "&#xa0;") + y), b += "</div>"
				}, _adjustInstDate: function (t, e, i) { var s = t.drawYear + ("Y" === i ? e : 0), n = t.drawMonth + ("M" === i ? e : 0), a = Math.min(t.selectedDay, this._getDaysInMonth(s, n)) + ("D" === i ? e : 0), r = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(s, n, a))); t.selectedDay = r.getDate(), t.drawMonth = t.selectedMonth = r.getMonth(), t.drawYear = t.selectedYear = r.getFullYear(), ("M" === i || "Y" === i) && this._notifyChange(t) }, _restrictMinMax: function (t, e) { var i = this._getMinMaxDate(t, "min"), s = this._getMinMaxDate(t, "max"), n = i && i > e ? i : e; return s && n > s ? s : n }, _notifyChange: function (t) { var e = this._get(t, "onChangeMonthYear"); e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t]) }, _getNumberOfMonths: function (t) { var e = this._get(t, "numberOfMonths"); return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e }, _getMinMaxDate: function (t, e) { return this._determineDate(t, this._get(t, e + "Date"), null) }, _getDaysInMonth: function (t, e) { return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate() }, _getFirstDayOfMonth: function (t, e) { return new Date(t, e, 1).getDay() }, _canAdjustMonth: function (t, e, i, s) { var n = this._getNumberOfMonths(t), a = this._daylightSavingAdjust(new Date(i, s + (0 > e ? e : n[0] * n[1]), 1)); return 0 > e && a.setDate(this._getDaysInMonth(a.getFullYear(), a.getMonth())), this._isInRange(t, a) }, _isInRange: function (t, e) { var i, s, n = this._getMinMaxDate(t, "min"), a = this._getMinMaxDate(t, "max"), r = null, o = null, h = this._get(t, "yearRange"); return h && (i = h.split(":"), s = (new Date).getFullYear(), r = parseInt(i[0], 10), o = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (r += s), i[1].match(/[+\-].*/) && (o += s)), (!n || e.getTime() >= n.getTime()) && (!a || e.getTime() <= a.getTime()) && (!r || e.getFullYear() >= r) && (!o || o >= e.getFullYear()) }, _getFormatConfig: function (t) { var e = this._get(t, "shortYearCutoff"); return e = "string" != typeof e ? e : (new Date).getFullYear() % 100 + parseInt(e, 10), { shortYearCutoff: e, dayNamesShort: this._get(t, "dayNamesShort"), dayNames: this._get(t, "dayNames"), monthNamesShort: this._get(t, "monthNamesShort"), monthNames: this._get(t, "monthNames") } }, _formatDate: function (t, e, i, s) { e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear); var n = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(s, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay)); return this.formatDate(this._get(t, "dateFormat"), n, this._getFormatConfig(t)) }
			}), t.fn.datepicker = function (e) { if (!this.length) return this; t.datepicker.initialized || (t(document).mousedown(t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv); var i = Array.prototype.slice.call(arguments, 1); return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) : this.each(function () { "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(i)) : t.datepicker._attachDatepicker(this, e) }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) }, t.datepicker = new i, t.datepicker.initialized = !1, t.datepicker.uuid = (new Date).getTime(), t.datepicker.version = "1.10.3"
		})(jQuery); (function (t) { var e = { buttons: !0, height: !0, maxHeight: !0, maxWidth: !0, minHeight: !0, minWidth: !0, width: !0 }, i = { maxHeight: !0, maxWidth: !0, minHeight: !0, minWidth: !0 }; t.widget("ui.dialog", { version: "1.10.3", options: { appendTo: "body", autoOpen: !0, buttons: [], closeOnEscape: !0, closeText: "close", dialogClass: "", draggable: !0, hide: null, height: "auto", maxHeight: null, maxWidth: null, minHeight: 150, minWidth: 150, modal: !1, position: { my: "center", at: "center", of: window, collision: "fit", using: function (e) { var i = t(this).css(e).offset().top; 0 > i && t(this).css("top", e.top - i) } }, resizable: !0, show: null, title: null, width: 300, beforeClose: null, close: null, drag: null, dragStart: null, dragStop: null, focus: null, open: null, resize: null, resizeStart: null, resizeStop: null }, _create: function () { this.originalCss = { display: this.element[0].style.display, width: this.element[0].style.width, minHeight: this.element[0].style.minHeight, maxHeight: this.element[0].style.maxHeight, height: this.element[0].style.height }, this.originalPosition = { parent: this.element.parent(), index: this.element.parent().children().index(this.element) }, this.originalTitle = this.element.attr("title"), this.options.title = this.options.title || this.originalTitle, this._createWrapper(), this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog), this._createTitlebar(), this._createButtonPane(), this.options.draggable && t.fn.draggable && this._makeDraggable(), this.options.resizable && t.fn.resizable && this._makeResizable(), this._isOpen = !1 }, _init: function () { this.options.autoOpen && this.open() }, _appendTo: function () { var e = this.options.appendTo; return e && (e.jquery || e.nodeType) ? t(e) : this.document.find(e || "body").eq(0) }, _destroy: function () { var t, e = this.originalPosition; this._destroyOverlay(), this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(), this.uiDialog.stop(!0, !0).remove(), this.originalTitle && this.element.attr("title", this.originalTitle), t = e.parent.children().eq(e.index), t.length && t[0] !== this.element[0] ? t.before(this.element) : e.parent.append(this.element) }, widget: function () { return this.uiDialog }, disable: t.noop, enable: t.noop, close: function (e) { var i = this; this._isOpen && this._trigger("beforeClose", e) !== !1 && (this._isOpen = !1, this._destroyOverlay(), this.opener.filter(":focusable").focus().length || t(this.document[0].activeElement).blur(), this._hide(this.uiDialog, this.options.hide, function () { i._trigger("close", e) })) }, isOpen: function () { return this._isOpen }, moveToTop: function () { this._moveToTop() }, _moveToTop: function (t, e) { var i = !!this.uiDialog.nextAll(":visible").insertBefore(this.uiDialog).length; return i && !e && this._trigger("focus", t), i }, open: function () { var e = this; return this._isOpen ? (this._moveToTop() && this._focusTabbable(), undefined) : (this._isOpen = !0, this.opener = t(this.document[0].activeElement), this._size(), this._position(), this._createOverlay(), this._moveToTop(null, !0), this._show(this.uiDialog, this.options.show, function () { e._focusTabbable(), e._trigger("focus") }), this._trigger("open"), undefined) }, _focusTabbable: function () { var t = this.element.find("[autofocus]"); t.length || (t = this.element.find(":tabbable")), t.length || (t = this.uiDialogButtonPane.find(":tabbable")), t.length || (t = this.uiDialogTitlebarClose.filter(":tabbable")), t.length || (t = this.uiDialog), t.eq(0).focus() }, _keepFocus: function (e) { function i() { var e = this.document[0].activeElement, i = this.uiDialog[0] === e || t.contains(this.uiDialog[0], e); i || this._focusTabbable() } e.preventDefault(), i.call(this), this._delay(i) }, _createWrapper: function () { this.uiDialog = t("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({ tabIndex: -1, role: "dialog" }).appendTo(this._appendTo()), this._on(this.uiDialog, { keydown: function (e) { if (this.options.closeOnEscape && !e.isDefaultPrevented() && e.keyCode && e.keyCode === t.ui.keyCode.ESCAPE) return e.preventDefault(), this.close(e), undefined; if (e.keyCode === t.ui.keyCode.TAB) { var i = this.uiDialog.find(":tabbable"), s = i.filter(":first"), n = i.filter(":last"); e.target !== n[0] && e.target !== this.uiDialog[0] || e.shiftKey ? e.target !== s[0] && e.target !== this.uiDialog[0] || !e.shiftKey || (n.focus(1), e.preventDefault()) : (s.focus(1), e.preventDefault()) } }, mousedown: function (t) { this._moveToTop(t) && this._focusTabbable() } }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({ "aria-describedby": this.element.uniqueId().attr("id") }) }, _createTitlebar: function () { var e; this.uiDialogTitlebar = t("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog), this._on(this.uiDialogTitlebar, { mousedown: function (e) { t(e.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.focus() } }), this.uiDialogTitlebarClose = t("<button></button>").button({ label: this.options.closeText, icons: { primary: "ui-icon-closethick" }, text: !1 }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar), this._on(this.uiDialogTitlebarClose, { click: function (t) { t.preventDefault(), this.close(t) } }), e = t("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar), this._title(e), this.uiDialog.attr({ "aria-labelledby": e.attr("id") }) }, _title: function (t) { this.options.title || t.html("&#160;"), t.text(this.options.title) }, _createButtonPane: function () { this.uiDialogButtonPane = t("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"), this.uiButtonSet = t("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane), this._createButtons() }, _createButtons: function () { var e = this, i = this.options.buttons; return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), t.isEmptyObject(i) || t.isArray(i) && !i.length ? (this.uiDialog.removeClass("ui-dialog-buttons"), undefined) : (t.each(i, function (i, s) { var n, a; s = t.isFunction(s) ? { click: s, text: i } : s, s = t.extend({ type: "button" }, s), n = s.click, s.click = function () { n.apply(e.element[0], arguments) }, a = { icons: s.icons, text: s.showText }, delete s.icons, delete s.showText, t("<button></button>", s).button(a).appendTo(e.uiButtonSet) }), this.uiDialog.addClass("ui-dialog-buttons"), this.uiDialogButtonPane.appendTo(this.uiDialog), undefined) }, _makeDraggable: function () { function e(t) { return { position: t.position, offset: t.offset } } var i = this, s = this.options; this.uiDialog.draggable({ cancel: ".ui-dialog-content, .ui-dialog-titlebar-close", handle: ".ui-dialog-titlebar", containment: "document", start: function (s, n) { t(this).addClass("ui-dialog-dragging"), i._blockFrames(), i._trigger("dragStart", s, e(n)) }, drag: function (t, s) { i._trigger("drag", t, e(s)) }, stop: function (n, a) { s.position = [a.position.left - i.document.scrollLeft(), a.position.top - i.document.scrollTop()], t(this).removeClass("ui-dialog-dragging"), i._unblockFrames(), i._trigger("dragStop", n, e(a)) } }) }, _makeResizable: function () { function e(t) { return { originalPosition: t.originalPosition, originalSize: t.originalSize, position: t.position, size: t.size } } var i = this, s = this.options, n = s.resizable, a = this.uiDialog.css("position"), o = "string" == typeof n ? n : "n,e,s,w,se,sw,ne,nw"; this.uiDialog.resizable({ cancel: ".ui-dialog-content", containment: "document", alsoResize: this.element, maxWidth: s.maxWidth, maxHeight: s.maxHeight, minWidth: s.minWidth, minHeight: this._minHeight(), handles: o, start: function (s, n) { t(this).addClass("ui-dialog-resizing"), i._blockFrames(), i._trigger("resizeStart", s, e(n)) }, resize: function (t, s) { i._trigger("resize", t, e(s)) }, stop: function (n, a) { s.height = t(this).height(), s.width = t(this).width(), t(this).removeClass("ui-dialog-resizing"), i._unblockFrames(), i._trigger("resizeStop", n, e(a)) } }).css("position", a) }, _minHeight: function () { var t = this.options; return "auto" === t.height ? t.minHeight : Math.min(t.minHeight, t.height) }, _position: function () { var t = this.uiDialog.is(":visible"); t || this.uiDialog.show(), this.uiDialog.position(this.options.position), t || this.uiDialog.hide() }, _setOptions: function (s) { var n = this, a = !1, o = {}; t.each(s, function (t, s) { n._setOption(t, s), t in e && (a = !0), t in i && (o[t] = s) }), a && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", o) }, _setOption: function (t, e) { var i, s, n = this.uiDialog; "dialogClass" === t && n.removeClass(this.options.dialogClass).addClass(e), "disabled" !== t && (this._super(t, e), "appendTo" === t && this.uiDialog.appendTo(this._appendTo()), "buttons" === t && this._createButtons(), "closeText" === t && this.uiDialogTitlebarClose.button({ label: "" + e }), "draggable" === t && (i = n.is(":data(ui-draggable)"), i && !e && n.draggable("destroy"), !i && e && this._makeDraggable()), "position" === t && this._position(), "resizable" === t && (s = n.is(":data(ui-resizable)"), s && !e && n.resizable("destroy"), s && "string" == typeof e && n.resizable("option", "handles", e), s || e === !1 || this._makeResizable()), "title" === t && this._title(this.uiDialogTitlebar.find(".ui-dialog-title"))) }, _size: function () { var t, e, i, s = this.options; this.element.show().css({ width: "auto", minHeight: 0, maxHeight: "none", height: 0 }), s.minWidth > s.width && (s.width = s.minWidth), t = this.uiDialog.css({ height: "auto", width: s.width }).outerHeight(), e = Math.max(0, s.minHeight - t), i = "number" == typeof s.maxHeight ? Math.max(0, s.maxHeight - t) : "none", "auto" === s.height ? this.element.css({ minHeight: e, maxHeight: i, height: "auto" }) : this.element.height(Math.max(0, s.height - t)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight()) }, _blockFrames: function () { this.iframeBlocks = this.document.find("iframe").map(function () { var e = t(this); return t("<div>").css({ position: "absolute", width: e.outerWidth(), height: e.outerHeight() }).appendTo(e.parent()).offset(e.offset())[0] }) }, _unblockFrames: function () { this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks) }, _allowInteraction: function (e) { return t(e.target).closest(".ui-dialog").length ? !0 : !!t(e.target).closest(".ui-datepicker").length }, _createOverlay: function () { if (this.options.modal) { var e = this, i = this.widgetFullName; t.ui.dialog.overlayInstances || this._delay(function () { t.ui.dialog.overlayInstances && this.document.bind("focusin.dialog", function (s) { e._allowInteraction(s) || (s.preventDefault(), t(".ui-dialog:visible:last .ui-dialog-content").data(i)._focusTabbable()) }) }), this.overlay = t("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()), this._on(this.overlay, { mousedown: "_keepFocus" }), t.ui.dialog.overlayInstances++ } }, _destroyOverlay: function () { this.options.modal && this.overlay && (t.ui.dialog.overlayInstances--, t.ui.dialog.overlayInstances || this.document.unbind("focusin.dialog"), this.overlay.remove(), this.overlay = null) } }), t.ui.dialog.overlayInstances = 0, t.uiBackCompat !== !1 && t.widget("ui.dialog", t.ui.dialog, { _position: function () { var e, i = this.options.position, s = [], n = [0, 0]; i ? (("string" == typeof i || "object" == typeof i && "0" in i) && (s = i.split ? i.split(" ") : [i[0], i[1]], 1 === s.length && (s[1] = s[0]), t.each(["left", "top"], function (t, e) { +s[t] === s[t] && (n[t] = s[t], s[t] = e) }), i = { my: s[0] + (0 > n[0] ? n[0] : "+" + n[0]) + " " + s[1] + (0 > n[1] ? n[1] : "+" + n[1]), at: s.join(" ") }), i = t.extend({}, t.ui.dialog.prototype.options.position, i)) : i = t.ui.dialog.prototype.options.position, e = this.uiDialog.is(":visible"), e || this.uiDialog.show(), this.uiDialog.position(i), e || this.uiDialog.hide() } }) })(jQuery); (function (t) { t.widget("ui.menu", { version: "1.10.3", defaultElement: "<ul>", delay: 300, options: { icons: { submenu: "ui-icon-carat-1-e" }, menus: "ul", position: { my: "left top", at: "right top" }, role: "menu", blur: null, focus: null, select: null }, _create: function () { this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({ role: this.options.role, tabIndex: 0 }).bind("click" + this.eventNamespace, t.proxy(function (t) { this.options.disabled && t.preventDefault() }, this)), this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true"), this._on({ "mousedown .ui-menu-item > a": function (t) { t.preventDefault() }, "click .ui-state-disabled > a": function (t) { t.preventDefault() }, "click .ui-menu-item:has(a)": function (e) { var i = t(e.target).closest(".ui-menu-item"); !this.mouseHandled && i.not(".ui-state-disabled").length && (this.mouseHandled = !0, this.select(e), i.has(".ui-menu").length ? this.expand(e) : this.element.is(":focus") || (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer))) }, "mouseenter .ui-menu-item": function (e) { var i = t(e.currentTarget); i.siblings().children(".ui-state-active").removeClass("ui-state-active"), this.focus(e, i) }, mouseleave: "collapseAll", "mouseleave .ui-menu": "collapseAll", focus: function (t, e) { var i = this.active || this.element.children(".ui-menu-item").eq(0); e || this.focus(t, i) }, blur: function (e) { this._delay(function () { t.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(e) }) }, keydown: "_keydown" }), this.refresh(), this._on(this.document, { click: function (e) { t(e.target).closest(".ui-menu").length || this.collapseAll(e), this.mouseHandled = !1 } }) }, _destroy: function () { this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(), this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function () { var e = t(this); e.data("ui-menu-submenu-carat") && e.remove() }), this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content") }, _keydown: function (e) { function i(t) { return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&") } var s, n, a, o, r, h = !0; switch (e.keyCode) { case t.ui.keyCode.PAGE_UP: this.previousPage(e); break; case t.ui.keyCode.PAGE_DOWN: this.nextPage(e); break; case t.ui.keyCode.HOME: this._move("first", "first", e); break; case t.ui.keyCode.END: this._move("last", "last", e); break; case t.ui.keyCode.UP: this.previous(e); break; case t.ui.keyCode.DOWN: this.next(e); break; case t.ui.keyCode.LEFT: this.collapse(e); break; case t.ui.keyCode.RIGHT: this.active && !this.active.is(".ui-state-disabled") && this.expand(e); break; case t.ui.keyCode.ENTER: case t.ui.keyCode.SPACE: this._activate(e); break; case t.ui.keyCode.ESCAPE: this.collapse(e); break; default: h = !1, n = this.previousFilter || "", a = String.fromCharCode(e.keyCode), o = !1, clearTimeout(this.filterTimer), a === n ? o = !0 : a = n + a, r = RegExp("^" + i(a), "i"), s = this.activeMenu.children(".ui-menu-item").filter(function () { return r.test(t(this).children("a").text()) }), s = o && -1 !== s.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : s, s.length || (a = String.fromCharCode(e.keyCode), r = RegExp("^" + i(a), "i"), s = this.activeMenu.children(".ui-menu-item").filter(function () { return r.test(t(this).children("a").text()) })), s.length ? (this.focus(e, s), s.length > 1 ? (this.previousFilter = a, this.filterTimer = this._delay(function () { delete this.previousFilter }, 1e3)) : delete this.previousFilter) : delete this.previousFilter } h && e.preventDefault() }, _activate: function (t) { this.active.is(".ui-state-disabled") || (this.active.children("a[aria-haspopup='true']").length ? this.expand(t) : this.select(t)) }, refresh: function () { var e, i = this.options.icons.submenu, s = this.element.find(this.options.menus); s.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({ role: this.options.role, "aria-hidden": "true", "aria-expanded": "false" }).each(function () { var e = t(this), s = e.prev("a"), n = t("<span>").addClass("ui-menu-icon ui-icon " + i).data("ui-menu-submenu-carat", !0); s.attr("aria-haspopup", "true").prepend(n), e.attr("aria-labelledby", s.attr("id")) }), e = s.add(this.element), e.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role", "presentation").children("a").uniqueId().addClass("ui-corner-all").attr({ tabIndex: -1, role: this._itemRole() }), e.children(":not(.ui-menu-item)").each(function () { var e = t(this); /[^\-\u2014\u2013\s]/.test(e.text()) || e.addClass("ui-widget-content ui-menu-divider") }), e.children(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !t.contains(this.element[0], this.active[0]) && this.blur() }, _itemRole: function () { return { menu: "menuitem", listbox: "option" }[this.options.role] }, _setOption: function (t, e) { "icons" === t && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(e.submenu), this._super(t, e) }, focus: function (t, e) { var i, s; this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), s = this.active.children("a").addClass("ui-state-focus"), this.options.role && this.element.attr("aria-activedescendant", s.attr("id")), this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function () { this._close() }, this.delay), i = e.children(".ui-menu"), i.length && /^mouse/.test(t.type) && this._startOpening(i), this.activeMenu = e.parent(), this._trigger("focus", t, { item: e }) }, _scrollIntoView: function (e) { var i, s, n, a, o, r; this._hasScroll() && (i = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, s = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, n = e.offset().top - this.activeMenu.offset().top - i - s, a = this.activeMenu.scrollTop(), o = this.activeMenu.height(), r = e.height(), 0 > n ? this.activeMenu.scrollTop(a + n) : n + r > o && this.activeMenu.scrollTop(a + n - o + r)) }, blur: function (t, e) { e || clearTimeout(this.timer), this.active && (this.active.children("a").removeClass("ui-state-focus"), this.active = null, this._trigger("blur", t, { item: this.active })) }, _startOpening: function (t) { clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function () { this._close(), this._open(t) }, this.delay)) }, _open: function (e) { var i = t.extend({ of: this.active }, this.options.position); clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i) }, collapseAll: function (e, i) { clearTimeout(this.timer), this.timer = this._delay(function () { var s = i ? this.element : t(e && e.target).closest(this.element.find(".ui-menu")); s.length || (s = this.element), this._close(s), this.blur(e), this.activeMenu = s }, this.delay) }, _close: function (t) { t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find("a.ui-state-active").removeClass("ui-state-active") }, collapse: function (t) { var e = this.active && this.active.parent().closest(".ui-menu-item", this.element); e && e.length && (this._close(), this.focus(t, e)) }, expand: function (t) { var e = this.active && this.active.children(".ui-menu ").children(".ui-menu-item").first(); e && e.length && (this._open(e.parent()), this._delay(function () { this.focus(t, e) })) }, next: function (t) { this._move("next", "first", t) }, previous: function (t) { this._move("prev", "last", t) }, isFirstItem: function () { return this.active && !this.active.prevAll(".ui-menu-item").length }, isLastItem: function () { return this.active && !this.active.nextAll(".ui-menu-item").length }, _move: function (t, e, i) { var s; this.active && (s = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), s && s.length && this.active || (s = this.activeMenu.children(".ui-menu-item")[e]()), this.focus(i, s) }, nextPage: function (e) { var i, s, n; return this.active ? (this.isLastItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.nextAll(".ui-menu-item").each(function () { return i = t(this), 0 > i.offset().top - s - n }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item")[this.active ? "last" : "first"]())), undefined) : (this.next(e), undefined) }, previousPage: function (e) { var i, s, n; return this.active ? (this.isFirstItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.prevAll(".ui-menu-item").each(function () { return i = t(this), i.offset().top - s + n > 0 }), this.focus(e, i)) : this.focus(e, this.activeMenu.children(".ui-menu-item").first())), undefined) : (this.next(e), undefined) }, _hasScroll: function () { return this.element.outerHeight() < this.element.prop("scrollHeight") }, select: function (e) { this.active = this.active || t(e.target).closest(".ui-menu-item"); var i = { item: this.active }; this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, i) } }) })(jQuery); (function (t, e) { t.widget("ui.progressbar", { version: "1.10.3", options: { max: 100, value: 0, change: null, complete: null }, min: 0, _create: function () { this.oldValue = this.options.value = this._constrainedValue(), this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({ role: "progressbar", "aria-valuemin": this.min }), this.valueDiv = t("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element), this._refreshValue() }, _destroy: function () { this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.valueDiv.remove() }, value: function (t) { return t === e ? this.options.value : (this.options.value = this._constrainedValue(t), this._refreshValue(), e) }, _constrainedValue: function (t) { return t === e && (t = this.options.value), this.indeterminate = t === !1, "number" != typeof t && (t = 0), this.indeterminate ? !1 : Math.min(this.options.max, Math.max(this.min, t)) }, _setOptions: function (t) { var e = t.value; delete t.value, this._super(t), this.options.value = this._constrainedValue(e), this._refreshValue() }, _setOption: function (t, e) { "max" === t && (e = Math.max(this.min, e)), this._super(t, e) }, _percentage: function () { return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min) }, _refreshValue: function () { var e = this.options.value, i = this._percentage(); this.valueDiv.toggle(this.indeterminate || e > this.min).toggleClass("ui-corner-right", e === this.options.max).width(i.toFixed(0) + "%"), this.element.toggleClass("ui-progressbar-indeterminate", this.indeterminate), this.indeterminate ? (this.element.removeAttr("aria-valuenow"), this.overlayDiv || (this.overlayDiv = t("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))) : (this.element.attr({ "aria-valuemax": this.options.max, "aria-valuenow": e }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== e && (this.oldValue = e, this._trigger("change")), e === this.options.max && this._trigger("complete") } }) })(jQuery); (function (t) { var e = 5; t.widget("ui.slider", t.ui.mouse, { version: "1.10.3", widgetEventPrefix: "slide", options: { animate: !1, distance: 0, max: 100, min: 0, orientation: "horizontal", range: !1, step: 1, value: 0, values: null, change: null, slide: null, start: null, stop: null }, _create: function () { this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this.element.addClass("ui-slider ui-slider-" + this.orientation + " ui-widget" + " ui-widget-content" + " ui-corner-all"), this._refresh(), this._setOption("disabled", this.options.disabled), this._animateOff = !1 }, _refresh: function () { this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue() }, _createHandles: function () { var e, i, s = this.options, n = this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"), a = "<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>", o = []; for (i = s.values && s.values.length || 1, n.length > i && (n.slice(i).remove(), n = n.slice(0, i)), e = n.length; i > e; e++) o.push(a); this.handles = n.add(t(o.join("")).appendTo(this.element)), this.handle = this.handles.eq(0), this.handles.each(function (e) { t(this).data("ui-slider-handle-index", e) }) }, _createRange: function () { var e = this.options, i = ""; e.range ? (e.range === !0 && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [e.values[0], e.values[0]] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({ left: "", bottom: "" }) : (this.range = t("<div></div>").appendTo(this.element), i = "ui-slider-range ui-widget-header ui-corner-all"), this.range.addClass(i + ("min" === e.range || "max" === e.range ? " ui-slider-range-" + e.range : ""))) : this.range = t([]) }, _setupEvents: function () { var t = this.handles.add(this.range).filter("a"); this._off(t), this._on(t, this._handleEvents), this._hoverable(t), this._focusable(t) }, _destroy: function () { this.handles.remove(), this.range.remove(), this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"), this._mouseDestroy() }, _mouseCapture: function (e) { var i, s, n, a, o, r, h, l, u = this, c = this.options; return c.disabled ? !1 : (this.elementSize = { width: this.element.outerWidth(), height: this.element.outerHeight() }, this.elementOffset = this.element.offset(), i = { x: e.pageX, y: e.pageY }, s = this._normValueFromMouse(i), n = this._valueMax() - this._valueMin() + 1, this.handles.each(function (e) { var i = Math.abs(s - u.values(e)); (n > i || n === i && (e === u._lastChangedValue || u.values(e) === c.min)) && (n = i, a = t(this), o = e) }), r = this._start(e, o), r === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = o, a.addClass("ui-state-active").focus(), h = a.offset(), l = !t(e.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = l ? { left: 0, top: 0 } : { left: e.pageX - h.left - a.width() / 2, top: e.pageY - h.top - a.height() / 2 - (parseInt(a.css("borderTopWidth"), 10) || 0) - (parseInt(a.css("borderBottomWidth"), 10) || 0) + (parseInt(a.css("marginTop"), 10) || 0) }, this.handles.hasClass("ui-state-hover") || this._slide(e, o, s), this._animateOff = !0, !0)) }, _mouseStart: function () { return !0 }, _mouseDrag: function (t) { var e = { x: t.pageX, y: t.pageY }, i = this._normValueFromMouse(e); return this._slide(t, this._handleIndex, i), !1 }, _mouseStop: function (t) { return this.handles.removeClass("ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1 }, _detectOrientation: function () { this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal" }, _normValueFromMouse: function (t) { var e, i, s, n, a; return "horizontal" === this.orientation ? (e = this.elementSize.width, i = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, i = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), s = i / e, s > 1 && (s = 1), 0 > s && (s = 0), "vertical" === this.orientation && (s = 1 - s), n = this._valueMax() - this._valueMin(), a = this._valueMin() + s * n, this._trimAlignValue(a) }, _start: function (t, e) { var i = { handle: this.handles[e], value: this.value() }; return this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("start", t, i) }, _slide: function (t, e, i) { var s, n, a; this.options.values && this.options.values.length ? (s = this.values(e ? 0 : 1), 2 === this.options.values.length && this.options.range === !0 && (0 === e && i > s || 1 === e && s > i) && (i = s), i !== this.values(e) && (n = this.values(), n[e] = i, a = this._trigger("slide", t, { handle: this.handles[e], value: i, values: n }), s = this.values(e ? 0 : 1), a !== !1 && this.values(e, i, !0))) : i !== this.value() && (a = this._trigger("slide", t, { handle: this.handles[e], value: i }), a !== !1 && this.value(i)) }, _stop: function (t, e) { var i = { handle: this.handles[e], value: this.value() }; this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._trigger("stop", t, i) }, _change: function (t, e) { if (!this._keySliding && !this._mouseSliding) { var i = { handle: this.handles[e], value: this.value() }; this.options.values && this.options.values.length && (i.value = this.values(e), i.values = this.values()), this._lastChangedValue = e, this._trigger("change", t, i) } }, value: function (t) { return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), this._change(null, 0), undefined) : this._value() }, values: function (e, i) { var s, n, a; if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(i), this._refreshValue(), this._change(null, e), undefined; if (!arguments.length) return this._values(); if (!t.isArray(arguments[0])) return this.options.values && this.options.values.length ? this._values(e) : this.value(); for (s = this.options.values, n = arguments[0], a = 0; s.length > a; a += 1) s[a] = this._trimAlignValue(n[a]), this._change(null, a); this._refreshValue() }, _setOption: function (e, i) { var s, n = 0; switch ("range" === e && this.options.range === !0 && ("min" === i ? (this.options.value = this._values(0), this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), t.isArray(this.options.values) && (n = this.options.values.length), t.Widget.prototype._setOption.apply(this, arguments), e) { case "orientation": this._detectOrientation(), this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-" + this.orientation), this._refreshValue(); break; case "value": this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1; break; case "values": for (this._animateOff = !0, this._refreshValue(), s = 0; n > s; s += 1) this._change(null, s); this._animateOff = !1; break; case "min": case "max": this._animateOff = !0, this._refreshValue(), this._animateOff = !1; break; case "range": this._animateOff = !0, this._refresh(), this._animateOff = !1 } }, _value: function () { var t = this.options.value; return t = this._trimAlignValue(t) }, _values: function (t) { var e, i, s; if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e); if (this.options.values && this.options.values.length) { for (i = this.options.values.slice(), s = 0; i.length > s; s += 1) i[s] = this._trimAlignValue(i[s]); return i } return [] }, _trimAlignValue: function (t) { if (this._valueMin() >= t) return this._valueMin(); if (t >= this._valueMax()) return this._valueMax(); var e = this.options.step > 0 ? this.options.step : 1, i = (t - this._valueMin()) % e, s = t - i; return 2 * Math.abs(i) >= e && (s += i > 0 ? e : -e), parseFloat(s.toFixed(5)) }, _valueMin: function () { return this.options.min }, _valueMax: function () { return this.options.max }, _refreshValue: function () { var e, i, s, n, a, o = this.options.range, r = this.options, h = this, l = this._animateOff ? !1 : r.animate, u = {}; this.options.values && this.options.values.length ? this.handles.each(function (s) { i = 100 * ((h.values(s) - h._valueMin()) / (h._valueMax() - h._valueMin())), u["horizontal" === h.orientation ? "left" : "bottom"] = i + "%", t(this).stop(1, 1)[l ? "animate" : "css"](u, r.animate), h.options.range === !0 && ("horizontal" === h.orientation ? (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({ left: i + "%" }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({ width: i - e + "%" }, { queue: !1, duration: r.animate })) : (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({ bottom: i + "%" }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({ height: i - e + "%" }, { queue: !1, duration: r.animate }))), e = i }) : (s = this.value(), n = this._valueMin(), a = this._valueMax(), i = a !== n ? 100 * ((s - n) / (a - n)) : 0, u["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[l ? "animate" : "css"](u, r.animate), "min" === o && "horizontal" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({ width: i + "%" }, r.animate), "max" === o && "horizontal" === this.orientation && this.range[l ? "animate" : "css"]({ width: 100 - i + "%" }, { queue: !1, duration: r.animate }), "min" === o && "vertical" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({ height: i + "%" }, r.animate), "max" === o && "vertical" === this.orientation && this.range[l ? "animate" : "css"]({ height: 100 - i + "%" }, { queue: !1, duration: r.animate })) }, _handleEvents: { keydown: function (i) { var s, n, a, o, r = t(i.target).data("ui-slider-handle-index"); switch (i.keyCode) { case t.ui.keyCode.HOME: case t.ui.keyCode.END: case t.ui.keyCode.PAGE_UP: case t.ui.keyCode.PAGE_DOWN: case t.ui.keyCode.UP: case t.ui.keyCode.RIGHT: case t.ui.keyCode.DOWN: case t.ui.keyCode.LEFT: if (i.preventDefault(), !this._keySliding && (this._keySliding = !0, t(i.target).addClass("ui-state-active"), s = this._start(i, r), s === !1)) return } switch (o = this.options.step, n = a = this.options.values && this.options.values.length ? this.values(r) : this.value(), i.keyCode) { case t.ui.keyCode.HOME: a = this._valueMin(); break; case t.ui.keyCode.END: a = this._valueMax(); break; case t.ui.keyCode.PAGE_UP: a = this._trimAlignValue(n + (this._valueMax() - this._valueMin()) / e); break; case t.ui.keyCode.PAGE_DOWN: a = this._trimAlignValue(n - (this._valueMax() - this._valueMin()) / e); break; case t.ui.keyCode.UP: case t.ui.keyCode.RIGHT: if (n === this._valueMax()) return; a = this._trimAlignValue(n + o); break; case t.ui.keyCode.DOWN: case t.ui.keyCode.LEFT: if (n === this._valueMin()) return; a = this._trimAlignValue(n - o) } this._slide(i, r, a) }, click: function (t) { t.preventDefault() }, keyup: function (e) { var i = t(e.target).data("ui-slider-handle-index"); this._keySliding && (this._keySliding = !1, this._stop(e, i), this._change(e, i), t(e.target).removeClass("ui-state-active")) } } }) })(jQuery); (function (t) { function e(t) { return function () { var e = this.element.val(); t.apply(this, arguments), this._refresh(), e !== this.element.val() && this._trigger("change") } } t.widget("ui.spinner", { version: "1.10.3", defaultElement: "<input>", widgetEventPrefix: "spin", options: { culture: null, icons: { down: "ui-icon-triangle-1-s", up: "ui-icon-triangle-1-n" }, incremental: !0, max: null, min: null, numberFormat: null, page: 10, step: 1, change: null, spin: null, start: null, stop: null }, _create: function () { this._setOption("max", this.options.max), this._setOption("min", this.options.min), this._setOption("step", this.options.step), this._value(this.element.val(), !0), this._draw(), this._on(this._events), this._refresh(), this._on(this.window, { beforeunload: function () { this.element.removeAttr("autocomplete") } }) }, _getCreateOptions: function () { var e = {}, i = this.element; return t.each(["min", "max", "step"], function (t, s) { var n = i.attr(s); void 0 !== n && n.length && (e[s] = n) }), e }, _events: { keydown: function (t) { this._start(t) && this._keydown(t) && t.preventDefault() }, keyup: "_stop", focus: function () { this.previous = this.element.val() }, blur: function (t) { return this.cancelBlur ? (delete this.cancelBlur, void 0) : (this._stop(), this._refresh(), this.previous !== this.element.val() && this._trigger("change", t), void 0) }, mousewheel: function (t, e) { if (e) { if (!this.spinning && !this._start(t)) return !1; this._spin((e > 0 ? 1 : -1) * this.options.step, t), clearTimeout(this.mousewheelTimer), this.mousewheelTimer = this._delay(function () { this.spinning && this._stop(t) }, 100), t.preventDefault() } }, "mousedown .ui-spinner-button": function (e) { function i() { var t = this.element[0] === this.document[0].activeElement; t || (this.element.focus(), this.previous = s, this._delay(function () { this.previous = s })) } var s; s = this.element[0] === this.document[0].activeElement ? this.previous : this.element.val(), e.preventDefault(), i.call(this), this.cancelBlur = !0, this._delay(function () { delete this.cancelBlur, i.call(this) }), this._start(e) !== !1 && this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e) }, "mouseup .ui-spinner-button": "_stop", "mouseenter .ui-spinner-button": function (e) { return t(e.currentTarget).hasClass("ui-state-active") ? this._start(e) === !1 ? !1 : (this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e), void 0) : void 0 }, "mouseleave .ui-spinner-button": "_stop" }, _draw: function () { var t = this.uiSpinner = this.element.addClass("ui-spinner-input").attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml()); this.element.attr("role", "spinbutton"), this.buttons = t.find(".ui-spinner-button").attr("tabIndex", -1).button().removeClass("ui-corner-all"), this.buttons.height() > Math.ceil(.5 * t.height()) && t.height() > 0 && t.height(t.height()), this.options.disabled && this.disable() }, _keydown: function (e) { var i = this.options, s = t.ui.keyCode; switch (e.keyCode) { case s.UP: return this._repeat(null, 1, e), !0; case s.DOWN: return this._repeat(null, -1, e), !0; case s.PAGE_UP: return this._repeat(null, i.page, e), !0; case s.PAGE_DOWN: return this._repeat(null, -i.page, e), !0 } return !1 }, _uiSpinnerHtml: function () { return "<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>" }, _buttonHtml: function () { return "<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon " + this.options.icons.up + "'>&#9650;</span>" + "</a>" + "<a class='ui-spinner-button ui-spinner-down ui-corner-br'>" + "<span class='ui-icon " + this.options.icons.down + "'>&#9660;</span>" + "</a>" }, _start: function (t) { return this.spinning || this._trigger("start", t) !== !1 ? (this.counter || (this.counter = 1), this.spinning = !0, !0) : !1 }, _repeat: function (t, e, i) { t = t || 500, clearTimeout(this.timer), this.timer = this._delay(function () { this._repeat(40, e, i) }, t), this._spin(e * this.options.step, i) }, _spin: function (t, e) { var i = this.value() || 0; this.counter || (this.counter = 1), i = this._adjustValue(i + t * this._increment(this.counter)), this.spinning && this._trigger("spin", e, { value: i }) === !1 || (this._value(i), this.counter++) }, _increment: function (e) { var i = this.options.incremental; return i ? t.isFunction(i) ? i(e) : Math.floor(e * e * e / 5e4 - e * e / 500 + 17 * e / 200 + 1) : 1 }, _precision: function () { var t = this._precisionOf(this.options.step); return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), t }, _precisionOf: function (t) { var e = "" + t, i = e.indexOf("."); return -1 === i ? 0 : e.length - i - 1 }, _adjustValue: function (t) { var e, i, s = this.options; return e = null !== s.min ? s.min : 0, i = t - e, i = Math.round(i / s.step) * s.step, t = e + i, t = parseFloat(t.toFixed(this._precision())), null !== s.max && t > s.max ? s.max : null !== s.min && s.min > t ? s.min : t }, _stop: function (t) { this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), this.counter = 0, this.spinning = !1, this._trigger("stop", t)) }, _setOption: function (t, e) { if ("culture" === t || "numberFormat" === t) { var i = this._parse(this.element.val()); return this.options[t] = e, this.element.val(this._format(i)), void 0 } ("max" === t || "min" === t || "step" === t) && "string" == typeof e && (e = this._parse(e)), "icons" === t && (this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(e.up), this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(e.down)), this._super(t, e), "disabled" === t && (e ? (this.element.prop("disabled", !0), this.buttons.button("disable")) : (this.element.prop("disabled", !1), this.buttons.button("enable"))) }, _setOptions: e(function (t) { this._super(t), this._value(this.element.val()) }), _parse: function (t) { return "string" == typeof t && "" !== t && (t = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(t, 10, this.options.culture) : +t), "" === t || isNaN(t) ? null : t }, _format: function (t) { return "" === t ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(t, this.options.numberFormat, this.options.culture) : t }, _refresh: function () { this.element.attr({ "aria-valuemin": this.options.min, "aria-valuemax": this.options.max, "aria-valuenow": this._parse(this.element.val()) }) }, _value: function (t, e) { var i; "" !== t && (i = this._parse(t), null !== i && (e || (i = this._adjustValue(i)), t = this._format(i))), this.element.val(t), this._refresh() }, _destroy: function () { this.element.removeClass("ui-spinner-input").prop("disabled", !1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"), this.uiSpinner.replaceWith(this.element) }, stepUp: e(function (t) { this._stepUp(t) }), _stepUp: function (t) { this._start() && (this._spin((t || 1) * this.options.step), this._stop()) }, stepDown: e(function (t) { this._stepDown(t) }), _stepDown: function (t) { this._start() && (this._spin((t || 1) * -this.options.step), this._stop()) }, pageUp: e(function (t) { this._stepUp((t || 1) * this.options.page) }), pageDown: e(function (t) { this._stepDown((t || 1) * this.options.page) }), value: function (t) { return arguments.length ? (e(this._value).call(this, t), void 0) : this._parse(this.element.val()) }, widget: function () { return this.uiSpinner } }) })(jQuery); (function (t, e) { function i() { return ++n } function s(t) { return t.hash.length > 1 && decodeURIComponent(t.href.replace(a, "")) === decodeURIComponent(location.href.replace(a, "")) } var n = 0, a = /#.*$/; t.widget("ui.tabs", { version: "1.10.3", delay: 300, options: { active: null, collapsible: !1, event: "click", heightStyle: "content", hide: null, show: null, activate: null, beforeActivate: null, beforeLoad: null, load: null }, _create: function () { var e = this, i = this.options; this.running = !1, this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible", i.collapsible).delegate(".ui-tabs-nav > li", "mousedown" + this.eventNamespace, function (e) { t(this).is(".ui-state-disabled") && e.preventDefault() }).delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function () { t(this).closest("li").is(".ui-state-disabled") && this.blur() }), this._processTabs(), i.active = this._initialActive(), t.isArray(i.disabled) && (i.disabled = t.unique(i.disabled.concat(t.map(this.tabs.filter(".ui-state-disabled"), function (t) { return e.tabs.index(t) }))).sort()), this.active = this.options.active !== !1 && this.anchors.length ? this._findActive(i.active) : t(), this._refresh(), this.active.length && this.load(i.active) }, _initialActive: function () { var i = this.options.active, s = this.options.collapsible, n = location.hash.substring(1); return null === i && (n && this.tabs.each(function (s, a) { return t(a).attr("aria-controls") === n ? (i = s, !1) : e }), null === i && (i = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), (null === i || -1 === i) && (i = this.tabs.length ? 0 : !1)), i !== !1 && (i = this.tabs.index(this.tabs.eq(i)), -1 === i && (i = s ? !1 : 0)), !s && i === !1 && this.anchors.length && (i = 0), i }, _getCreateEventData: function () { return { tab: this.active, panel: this.active.length ? this._getPanelForTab(this.active) : t() } }, _tabKeydown: function (i) { var s = t(this.document[0].activeElement).closest("li"), n = this.tabs.index(s), a = !0; if (!this._handlePageNav(i)) { switch (i.keyCode) { case t.ui.keyCode.RIGHT: case t.ui.keyCode.DOWN: n++; break; case t.ui.keyCode.UP: case t.ui.keyCode.LEFT: a = !1, n--; break; case t.ui.keyCode.END: n = this.anchors.length - 1; break; case t.ui.keyCode.HOME: n = 0; break; case t.ui.keyCode.SPACE: return i.preventDefault(), clearTimeout(this.activating), this._activate(n), e; case t.ui.keyCode.ENTER: return i.preventDefault(), clearTimeout(this.activating), this._activate(n === this.options.active ? !1 : n), e; default: return } i.preventDefault(), clearTimeout(this.activating), n = this._focusNextTab(n, a), i.ctrlKey || (s.attr("aria-selected", "false"), this.tabs.eq(n).attr("aria-selected", "true"), this.activating = this._delay(function () { this.option("active", n) }, this.delay)) } }, _panelKeydown: function (e) { this._handlePageNav(e) || e.ctrlKey && e.keyCode === t.ui.keyCode.UP && (e.preventDefault(), this.active.focus()) }, _handlePageNav: function (i) { return i.altKey && i.keyCode === t.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : i.altKey && i.keyCode === t.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : e }, _findNextTab: function (e, i) { function s() { return e > n && (e = 0), 0 > e && (e = n), e } for (var n = this.tabs.length - 1; -1 !== t.inArray(s(), this.options.disabled) ;) e = i ? e + 1 : e - 1; return e }, _focusNextTab: function (t, e) { return t = this._findNextTab(t, e), this.tabs.eq(t).focus(), t }, _setOption: function (t, i) { return "active" === t ? (this._activate(i), e) : "disabled" === t ? (this._setupDisabled(i), e) : (this._super(t, i), "collapsible" === t && (this.element.toggleClass("ui-tabs-collapsible", i), i || this.options.active !== !1 || this._activate(0)), "event" === t && this._setupEvents(i), "heightStyle" === t && this._setupHeightStyle(i), e) }, _tabId: function (t) { return t.attr("aria-controls") || "ui-tabs-" + i() }, _sanitizeSelector: function (t) { return t ? t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : "" }, refresh: function () { var e = this.options, i = this.tablist.children(":has(a[href])"); e.disabled = t.map(i.filter(".ui-state-disabled"), function (t) { return i.index(t) }), this._processTabs(), e.active !== !1 && this.anchors.length ? this.active.length && !t.contains(this.tablist[0], this.active[0]) ? this.tabs.length === e.disabled.length ? (e.active = !1, this.active = t()) : this._activate(this._findNextTab(Math.max(0, e.active - 1), !1)) : e.active = this.tabs.index(this.active) : (e.active = !1, this.active = t()), this._refresh() }, _refresh: function () { this._setupDisabled(this.options.disabled), this._setupEvents(this.options.event), this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({ "aria-selected": "false", tabIndex: -1 }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({ "aria-expanded": "false", "aria-hidden": "true" }), this.active.length ? (this.active.addClass("ui-tabs-active ui-state-active").attr({ "aria-selected": "true", tabIndex: 0 }), this._getPanelForTab(this.active).show().attr({ "aria-expanded": "true", "aria-hidden": "false" })) : this.tabs.eq(0).attr("tabIndex", 0) }, _processTabs: function () { var e = this; this.tablist = this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role", "tablist"), this.tabs = this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({ role: "tab", tabIndex: -1 }), this.anchors = this.tabs.map(function () { return t("a", this)[0] }).addClass("ui-tabs-anchor").attr({ role: "presentation", tabIndex: -1 }), this.panels = t(), this.anchors.each(function (i, n) { var a, o, r, h = t(n).uniqueId().attr("id"), l = t(n).closest("li"), u = l.attr("aria-controls"); s(n) ? (a = n.hash, o = e.element.find(e._sanitizeSelector(a))) : (r = e._tabId(l), a = "#" + r, o = e.element.find(a), o.length || (o = e._createPanel(r), o.insertAfter(e.panels[i - 1] || e.tablist)), o.attr("aria-live", "polite")), o.length && (e.panels = e.panels.add(o)), u && l.data("ui-tabs-aria-controls", u), l.attr({ "aria-controls": a.substring(1), "aria-labelledby": h }), o.attr("aria-labelledby", h) }), this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role", "tabpanel") }, _getList: function () { return this.element.find("ol,ul").eq(0) }, _createPanel: function (e) { return t("<div>").attr("id", e).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy", !0) }, _setupDisabled: function (e) { t.isArray(e) && (e.length ? e.length === this.anchors.length && (e = !0) : e = !1); for (var i, s = 0; i = this.tabs[s]; s++) e === !0 || -1 !== t.inArray(s, e) ? t(i).addClass("ui-state-disabled").attr("aria-disabled", "true") : t(i).removeClass("ui-state-disabled").removeAttr("aria-disabled"); this.options.disabled = e }, _setupEvents: function (e) { var i = { click: function (t) { t.preventDefault() } }; e && t.each(e.split(" "), function (t, e) { i[e] = "_eventHandler" }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(this.anchors, i), this._on(this.tabs, { keydown: "_tabKeydown" }), this._on(this.panels, { keydown: "_panelKeydown" }), this._focusable(this.tabs), this._hoverable(this.tabs) }, _setupHeightStyle: function (e) { var i, s = this.element.parent(); "fill" === e ? (i = s.height(), i -= this.element.outerHeight() - this.element.height(), this.element.siblings(":visible").each(function () { var e = t(this), s = e.css("position"); "absolute" !== s && "fixed" !== s && (i -= e.outerHeight(!0)) }), this.element.children().not(this.panels).each(function () { i -= t(this).outerHeight(!0) }), this.panels.each(function () { t(this).height(Math.max(0, i - t(this).innerHeight() + t(this).height())) }).css("overflow", "auto")) : "auto" === e && (i = 0, this.panels.each(function () { i = Math.max(i, t(this).height("").height()) }).height(i)) }, _eventHandler: function (e) { var i = this.options, s = this.active, n = t(e.currentTarget), a = n.closest("li"), o = a[0] === s[0], r = o && i.collapsible, h = r ? t() : this._getPanelForTab(a), l = s.length ? this._getPanelForTab(s) : t(), u = { oldTab: s, oldPanel: l, newTab: r ? t() : a, newPanel: h }; e.preventDefault(), a.hasClass("ui-state-disabled") || a.hasClass("ui-tabs-loading") || this.running || o && !i.collapsible || this._trigger("beforeActivate", e, u) === !1 || (i.active = r ? !1 : this.tabs.index(a), this.active = o ? t() : a, this.xhr && this.xhr.abort(), l.length || h.length || t.error("jQuery UI Tabs: Mismatching fragment identifier."), h.length && this.load(this.tabs.index(a), e), this._toggle(e, u)) }, _toggle: function (e, i) { function s() { a.running = !1, a._trigger("activate", e, i) } function n() { i.newTab.closest("li").addClass("ui-tabs-active ui-state-active"), o.length && a.options.show ? a._show(o, a.options.show, s) : (o.show(), s()) } var a = this, o = i.newPanel, r = i.oldPanel; this.running = !0, r.length && this.options.hide ? this._hide(r, this.options.hide, function () { i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), n() }) : (i.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"), r.hide(), n()), r.attr({ "aria-expanded": "false", "aria-hidden": "true" }), i.oldTab.attr("aria-selected", "false"), o.length && r.length ? i.oldTab.attr("tabIndex", -1) : o.length && this.tabs.filter(function () { return 0 === t(this).attr("tabIndex") }).attr("tabIndex", -1), o.attr({ "aria-expanded": "true", "aria-hidden": "false" }), i.newTab.attr({ "aria-selected": "true", tabIndex: 0 }) }, _activate: function (e) { var i, s = this._findActive(e); s[0] !== this.active[0] && (s.length || (s = this.active), i = s.find(".ui-tabs-anchor")[0], this._eventHandler({ target: i, currentTarget: i, preventDefault: t.noop })) }, _findActive: function (e) { return e === !1 ? t() : this.tabs.eq(e) }, _getIndex: function (t) { return "string" == typeof t && (t = this.anchors.index(this.anchors.filter("[href$='" + t + "']"))), t }, _destroy: function () { this.xhr && this.xhr.abort(), this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"), this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"), this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(), this.tabs.add(this.panels).each(function () { t.data(this, "ui-tabs-destroy") ? t(this).remove() : t(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role") }), this.tabs.each(function () { var e = t(this), i = e.data("ui-tabs-aria-controls"); i ? e.attr("aria-controls", i).removeData("ui-tabs-aria-controls") : e.removeAttr("aria-controls") }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "") }, enable: function (i) { var s = this.options.disabled; s !== !1 && (i === e ? s = !1 : (i = this._getIndex(i), s = t.isArray(s) ? t.map(s, function (t) { return t !== i ? t : null }) : t.map(this.tabs, function (t, e) { return e !== i ? e : null })), this._setupDisabled(s)) }, disable: function (i) { var s = this.options.disabled; if (s !== !0) { if (i === e) s = !0; else { if (i = this._getIndex(i), -1 !== t.inArray(i, s)) return; s = t.isArray(s) ? t.merge([i], s).sort() : [i] } this._setupDisabled(s) } }, load: function (e, i) { e = this._getIndex(e); var n = this, a = this.tabs.eq(e), o = a.find(".ui-tabs-anchor"), r = this._getPanelForTab(a), h = { tab: a, panel: r }; s(o[0]) || (this.xhr = t.ajax(this._ajaxSettings(o, i, h)), this.xhr && "canceled" !== this.xhr.statusText && (a.addClass("ui-tabs-loading"), r.attr("aria-busy", "true"), this.xhr.success(function (t) { setTimeout(function () { r.html(t), n._trigger("load", i, h) }, 1) }).complete(function (t, e) { setTimeout(function () { "abort" === e && n.panels.stop(!1, !0), a.removeClass("ui-tabs-loading"), r.removeAttr("aria-busy"), t === n.xhr && delete n.xhr }, 1) }))) }, _ajaxSettings: function (e, i, s) { var n = this; return { url: e.attr("href"), beforeSend: function (e, a) { return n._trigger("beforeLoad", i, t.extend({ jqXHR: e, ajaxSettings: a }, s)) } } }, _getPanelForTab: function (e) { var i = t(e).attr("aria-controls"); return this.element.find(this._sanitizeSelector("#" + i)) } }) })(jQuery); (function (t) { function e(e, i) { var s = (e.attr("aria-describedby") || "").split(/\s+/); s.push(i), e.data("ui-tooltip-id", i).attr("aria-describedby", t.trim(s.join(" "))) } function i(e) { var i = e.data("ui-tooltip-id"), s = (e.attr("aria-describedby") || "").split(/\s+/), n = t.inArray(i, s); -1 !== n && s.splice(n, 1), e.removeData("ui-tooltip-id"), s = t.trim(s.join(" ")), s ? e.attr("aria-describedby", s) : e.removeAttr("aria-describedby") } var s = 0; t.widget("ui.tooltip", { version: "1.10.3", options: { content: function () { var e = t(this).attr("title") || ""; return t("<a>").text(e).html() }, hide: !0, items: "[title]:not([disabled])", position: { my: "left top+15", at: "left bottom", collision: "flipfit flip" }, show: !0, tooltipClass: null, track: !1, close: null, open: null }, _create: function () { this._on({ mouseover: "open", focusin: "open" }), this.tooltips = {}, this.parents = {}, this.options.disabled && this._disable() }, _setOption: function (e, i) { var s = this; return "disabled" === e ? (this[i ? "_disable" : "_enable"](), this.options[e] = i, void 0) : (this._super(e, i), "content" === e && t.each(this.tooltips, function (t, e) { s._updateContent(e) }), void 0) }, _disable: function () { var e = this; t.each(this.tooltips, function (i, s) { var n = t.Event("blur"); n.target = n.currentTarget = s[0], e.close(n, !0) }), this.element.find(this.options.items).addBack().each(function () { var e = t(this); e.is("[title]") && e.data("ui-tooltip-title", e.attr("title")).attr("title", "") }) }, _enable: function () { this.element.find(this.options.items).addBack().each(function () { var e = t(this); e.data("ui-tooltip-title") && e.attr("title", e.data("ui-tooltip-title")) }) }, open: function (e) { var i = this, s = t(e ? e.target : this.element).closest(this.options.items); s.length && !s.data("ui-tooltip-id") && (s.attr("title") && s.data("ui-tooltip-title", s.attr("title")), s.data("ui-tooltip-open", !0), e && "mouseover" === e.type && s.parents().each(function () { var e, s = t(this); s.data("ui-tooltip-open") && (e = t.Event("blur"), e.target = e.currentTarget = this, i.close(e, !0)), s.attr("title") && (s.uniqueId(), i.parents[this.id] = { element: this, title: s.attr("title") }, s.attr("title", "")) }), this._updateContent(s, e)) }, _updateContent: function (t, e) { var i, s = this.options.content, n = this, a = e ? e.type : null; return "string" == typeof s ? this._open(e, t, s) : (i = s.call(t[0], function (i) { t.data("ui-tooltip-open") && n._delay(function () { e && (e.type = a), this._open(e, t, i) }) }), i && this._open(e, t, i), void 0) }, _open: function (i, s, n) { function a(t) { l.of = t, o.is(":hidden") || o.position(l) } var o, r, h, l = t.extend({}, this.options.position); if (n) { if (o = this._find(s), o.length) return o.find(".ui-tooltip-content").html(n), void 0; s.is("[title]") && (i && "mouseover" === i.type ? s.attr("title", "") : s.removeAttr("title")), o = this._tooltip(s), e(s, o.attr("id")), o.find(".ui-tooltip-content").html(n), this.options.track && i && /^mouse/.test(i.type) ? (this._on(this.document, { mousemove: a }), a(i)) : o.position(t.extend({ of: s }, this.options.position)), o.hide(), this._show(o, this.options.show), this.options.show && this.options.show.delay && (h = this.delayedShow = setInterval(function () { o.is(":visible") && (a(l.of), clearInterval(h)) }, t.fx.interval)), this._trigger("open", i, { tooltip: o }), r = { keyup: function (e) { if (e.keyCode === t.ui.keyCode.ESCAPE) { var i = t.Event(e); i.currentTarget = s[0], this.close(i, !0) } }, remove: function () { this._removeTooltip(o) } }, i && "mouseover" !== i.type || (r.mouseleave = "close"), i && "focusin" !== i.type || (r.focusout = "close"), this._on(!0, s, r) } }, close: function (e) { var s = this, n = t(e ? e.currentTarget : this.element), a = this._find(n); this.closing || (clearInterval(this.delayedShow), n.data("ui-tooltip-title") && n.attr("title", n.data("ui-tooltip-title")), i(n), a.stop(!0), this._hide(a, this.options.hide, function () { s._removeTooltip(t(this)) }), n.removeData("ui-tooltip-open"), this._off(n, "mouseleave focusout keyup"), n[0] !== this.element[0] && this._off(n, "remove"), this._off(this.document, "mousemove"), e && "mouseleave" === e.type && t.each(this.parents, function (e, i) { t(i.element).attr("title", i.title), delete s.parents[e] }), this.closing = !0, this._trigger("close", e, { tooltip: a }), this.closing = !1) }, _tooltip: function (e) { var i = "ui-tooltip-" + s++, n = t("<div>").attr({ id: i, role: "tooltip" }).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content " + (this.options.tooltipClass || "")); return t("<div>").addClass("ui-tooltip-content").appendTo(n), n.appendTo(this.document[0].body), this.tooltips[i] = e, n }, _find: function (e) { var i = e.data("ui-tooltip-id"); return i ? t("#" + i) : t() }, _removeTooltip: function (t) { t.remove(), delete this.tooltips[t.attr("id")] }, _destroy: function () { var e = this; t.each(this.tooltips, function (i, s) { var n = t.Event("blur"); n.target = n.currentTarget = s[0], e.close(n, !0), t("#" + i).remove(), s.data("ui-tooltip-title") && (s.attr("title", s.data("ui-tooltip-title")), s.removeData("ui-tooltip-title")) }) } }) })(jQuery);
		// Multiselect plugin - https://github.com/ehynds/jquery-ui-multiselect-widget
		//(function (d) { var k = 0; d.widget("ech.multiselect", { options: { header: !0, height: 'auto', minWidth: 80, classes: "", checkAllText: "Check all", uncheckAllText: "Uncheck all", noneSelectedText: "Select options", selectedText: "# selected", selectedList: 0, show: null, hide: null, autoOpen: !1, multiple: !0, position: {} }, _create: function () { var a = this.element.hide(), b = this.options; this.speed = d.fx.speeds._default; this._isOpen = !1; a = (this.button = d('<button type="button"><span class="ui-icon ui-icon-triangle-2-n-s"></span></button>')).addClass("ui-multiselect ui-widget ui-state-default ui-corner-all").addClass(b.classes).attr({ title: a.attr("title"), "aria-haspopup": !0, tabIndex: a.attr("tabIndex") }).insertAfter(a); (this.buttonlabel = d("<span />")).html(b.noneSelectedText).appendTo(a); var a = (this.menu = d("<div />")).addClass("ui-multiselect-menu ui-widget ui-widget-content ui-corner-all").addClass(b.classes).appendTo(document.body), c = (this.header = d("<div />")).addClass("ui-widget-header ui-corner-all ui-multiselect-header ui-helper-clearfix").appendTo(a); (this.headerLinkContainer = d("<ul />")).addClass("ui-helper-reset").html(function () { return !0 === b.header ? '<li><a class="ui-multiselect-all" href="#"><span class="ui-icon ui-icon-check"></span><span>' + b.checkAllText + '</span></a></li><li><a class="ui-multiselect-none" href="#"><span class="ui-icon ui-icon-closethick"></span><span>' + b.uncheckAllText + "</span></a></li>" : "string" === typeof b.header ? "<li>" + b.header + "</li>" : "" }).append('<li class="ui-multiselect-close"><a href="#" class="ui-multiselect-close"><span class="ui-icon ui-icon-circle-close"></span></a></li>').appendTo(c); (this.checkboxContainer = d("<ul />")).addClass("ui-multiselect-checkboxes ui-helper-reset").appendTo(a); this._bindEvents(); this.refresh(!0); b.multiple || a.addClass("ui-multiselect-single") }, _init: function () { !1 === this.options.header && this.header.hide(); this.options.multiple || this.headerLinkContainer.find(".ui-multiselect-all, .ui-multiselect-none").hide(); this.options.autoOpen && this.open(); this.element.is(":disabled") && this.disable() }, refresh: function (a) { var b = this.element, c = this.options, f = this.menu, h = this.checkboxContainer, g = [], e = "", i = b.attr("id") || k++; b.find("option").each(function (b) { d(this); var a = this.parentNode, f = this.innerHTML, h = this.title, k = this.value, b = "ui-multiselect-" + (this.id || i + "-option-" + b), l = this.disabled, n = this.selected, m = ["ui-corner-all"], o = (l ? "ui-multiselect-disabled " : " ") + this.className, j; "OPTGROUP" === a.tagName && (j = a.getAttribute("label"), -1 === d.inArray(j, g) && (e += '<li class="ui-multiselect-optgroup-label ' + a.className + '"><a href="#">' + j + "</a></li>", g.push(j))); l && m.push("ui-state-disabled"); n && !c.multiple && m.push("ui-state-active"); e += '<li class="' + o + '">'; e += '<label for="' + b + '" title="' + h + '" class="' + m.join(" ") + '">'; e += '<input id="' + b + '" name="multiselect_' + i + '" type="' + (c.multiple ? "checkbox" : "radio") + '" value="' + k + '" title="' + f + '"'; n && (e += ' checked="checked"', e += ' aria-selected="true"'); l && (e += ' disabled="disabled"', e += ' aria-disabled="true"'); e += " /><span>" + f + "</span></label></li>" }); h.html(e); this.labels = f.find("label"); this.inputs = this.labels.children("input"); this._setButtonWidth(); this._setMenuWidth(); this.button[0].defaultValue = this.update(); a || this._trigger("refresh") }, update: function () { var a = this.options, b = this.inputs, c = b.filter(":checked"), f = c.length, a = 0 === f ? a.noneSelectedText : d.isFunction(a.selectedText) ? a.selectedText.call(this, f, b.length, c.get()) : /\d/.test(a.selectedList) && 0 < a.selectedList && f <= a.selectedList ? c.map(function () { return d(this).next().html() }).get().join(", ") : a.selectedText.replace("#", f).replace("#", b.length); this.buttonlabel.html(a); return a }, _bindEvents: function () { function a() { b[b._isOpen ? "close" : "open"](); return !1 } var b = this, c = this.button; c.find("span").bind("click.multiselect", a); c.bind({ click: a, keypress: function (a) { switch (a.which) { case 27: case 38: case 37: b.close(); break; case 39: case 40: b.open() } }, mouseenter: function () { c.hasClass("ui-state-disabled") || d(this).addClass("ui-state-hover") }, mouseleave: function () { d(this).removeClass("ui-state-hover") }, focus: function () { c.hasClass("ui-state-disabled") || d(this).addClass("ui-state-focus") }, blur: function () { d(this).removeClass("ui-state-focus") } }); this.header.delegate("a", "click.multiselect", function (a) { if (d(this).hasClass("ui-multiselect-close")) b.close(); else b[d(this).hasClass("ui-multiselect-all") ? "checkAll" : "uncheckAll"](); a.preventDefault() }); this.menu.delegate("li.ui-multiselect-optgroup-label a", "click.multiselect", function (a) { a.preventDefault(); var c = d(this), g = c.parent().nextUntil("li.ui-multiselect-optgroup-label").find("input:visible:not(:disabled)"), e = g.get(), c = c.parent().text(); !1 !== b._trigger("beforeoptgrouptoggle", a, { inputs: e, label: c }) && (b._toggleChecked(g.filter(":checked").length !== g.length, g), b._trigger("optgrouptoggle", a, { inputs: e, label: c, checked: e[0].checked })) }).delegate("label", "mouseenter.multiselect", function () { d(this).hasClass("ui-state-disabled") || (b.labels.removeClass("ui-state-hover"), d(this).addClass("ui-state-hover").find("input").focus()) }).delegate("label", "keydown.multiselect", function (a) { a.preventDefault(); switch (a.which) { case 9: case 27: b.close(); break; case 38: case 40: case 37: case 39: b._traverse(a.which, this); break; case 13: d(this).find("input")[0].click() } }).delegate('input[type="checkbox"], input[type="radio"]', "click.multiselect", function (a) { var c = d(this), g = this.value, e = this.checked, i = b.element.find("option"); this.disabled || !1 === b._trigger("click", a, { value: g, text: this.title, checked: e }) ? a.preventDefault() : (c.focus(), c.attr("aria-selected", e), i.each(function () { this.value === g ? this.selected = e : b.options.multiple || (this.selected = !1) }), b.options.multiple || (b.labels.removeClass("ui-state-active"), c.closest("label").toggleClass("ui-state-active", e), b.close()), b.element.trigger("change"), setTimeout(d.proxy(b.update, b), 10)) }); d(document).bind("mousedown.multiselect", function (a) { b._isOpen && (!d.contains(b.menu[0], a.target) && !d.contains(b.button[0], a.target) && a.target !== b.button[0]) && b.close() }); d(this.element[0].form).bind("reset.multiselect", function () { setTimeout(d.proxy(b.refresh, b), 10) }) }, _setButtonWidth: function () { var a = this.element.outerWidth(), b = this.options; /\d/.test(b.minWidth) && a < b.minWidth && (a = b.minWidth); this.button.width(a) }, _setMenuWidth: function () { var a = this.menu, b = this.button.outerWidth() - parseInt(a.css("padding-left"), 10) - parseInt(a.css("padding-right"), 10) - parseInt(a.css("border-right-width"), 10) - parseInt(a.css("border-left-width"), 10); a.width(b || this.button.outerWidth()) }, _traverse: function (a, b) { var c = d(b), f = 38 === a || 37 === a, c = c.parent()[f ? "prevAll" : "nextAll"]("li:not(.ui-multiselect-disabled, .ui-multiselect-optgroup-label)")[f ? "last" : "first"](); c.length ? c.find("label").trigger("mouseover") : (c = this.menu.find("ul").last(), this.menu.find("label")[f ? "last" : "first"]().trigger("mouseover"), c.scrollTop(f ? c.height() : 0)) }, _toggleState: function (a, b) { return function () { this.disabled || (this[a] = b); b ? this.setAttribute("aria-selected", !0) : this.removeAttribute("aria-selected") } }, _toggleChecked: function (a, b) { var c = b && b.length ? b : this.inputs, f = this; c.each(this._toggleState("checked", a)); c.eq(0).focus(); this.update(); var h = c.map(function () { return this.value }).get(); this.element.find("option").each(function () { !this.disabled && -1 < d.inArray(this.value, h) && f._toggleState("selected", a).call(this) }); c.length && this.element.trigger("change") }, _toggleDisabled: function (a) { this.button.attr({ disabled: a, "aria-disabled": a })[a ? "addClass" : "removeClass"]("ui-state-disabled"); var b = this.menu.find("input"), b = a ? b.filter(":enabled").data("ech-multiselect-disabled", !0) : b.filter(function () { return !0 === d.data(this, "ech-multiselect-disabled") }).removeData("ech-multiselect-disabled"); b.attr({ disabled: a, "arial-disabled": a }).parent()[a ? "addClass" : "removeClass"]("ui-state-disabled"); this.element.attr({ disabled: a, "aria-disabled": a }) }, open: function () { var a = this.button, b = this.menu, c = this.speed, f = this.options, h = []; if (!(!1 === this._trigger("beforeopen") || a.hasClass("ui-state-disabled") || this._isOpen)) { var g = b.find("ul").last(), e = f.show, i = a.offset(); d.isArray(f.show) && (e = f.show[0], c = f.show[1] || this.speed); e && (h = [e, c]); g.scrollTop(0).height(f.height); d.ui.position && !d.isEmptyObject(f.position) ? (f.position.of = f.position.of || a, b.show().position(f.position).hide()) : b.css({ top: i.top + a.outerHeight(), left: i.left }); d.fn.show.apply(b, h); this.labels.eq(0).trigger("mouseover").trigger("mouseenter").find("input").trigger("focus"); a.addClass("ui-state-active"); this._isOpen = !0; this._trigger("open") } }, close: function () { if (!1 !== this._trigger("beforeclose")) { var a = this.options, b = a.hide, c = this.speed, f = []; d.isArray(a.hide) && (b = a.hide[0], c = a.hide[1] || this.speed); b && (f = [b, c]); d.fn.hide.apply(this.menu, f); this.button.removeClass("ui-state-active").trigger("blur").trigger("mouseleave"); this._isOpen = !1; this._trigger("close") } }, enable: function () { this._toggleDisabled(!1) }, disable: function () { this._toggleDisabled(!0) }, checkAll: function () { this._toggleChecked(!0); this._trigger("checkAll") }, uncheckAll: function () { this._toggleChecked(!1); this._trigger("uncheckAll") }, getChecked: function () { return this.menu.find("input").filter(":checked") }, destroy: function () { d.Widget.prototype.destroy.call(this); this.button.remove(); this.menu.remove(); this.element.show(); return this }, isOpen: function () { return this._isOpen }, widget: function () { return this.menu }, getButton: function () { return this.button }, _setOption: function (a, b) { var c = this.menu; switch (a) { case "header": c.find("div.ui-multiselect-header")[b ? "show" : "hide"](); break; case "checkAllText": c.find("a.ui-multiselect-all span").eq(-1).text(b); break; case "uncheckAllText": c.find("a.ui-multiselect-none span").eq(-1).text(b); break; case "height": c.find("ul").last().height(parseInt(b, 10)); break; case "minWidth": this.options[a] = parseInt(b, 10); this._setButtonWidth(); this._setMenuWidth(); break; case "selectedText": case "selectedList": case "noneSelectedText": this.options[a] = b; this.update(); break; case "classes": c.add(this.button).removeClass(this.options.classes).addClass(b); break; case "multiple": c.toggleClass("ui-multiselect-single", !b), this.options.multiple = b, this.element[0].multiple = b, this.refresh() } d.Widget.prototype._setOption.apply(this, arguments) } }) })(jQuery);
		(function (d) {
			var k = 0;
			d.widget("ech.multiselect", {
				options: {
					header: !0,
					height: 'auto',
					minWidth: 80,
					classes: "",
					checkAllText: "Check all",
					uncheckAllText: "Uncheck all",
					noneSelectedText: "Select options",
					selectedText: "# selected",
					selectedList: 0,
					show: null,
					hide: null,
					autoOpen: !1,
					multiple: !0,
					position: {}
				},
				_create: function () {
					var a = this.element.hide(),
		                b = this.options;
					this.speed = d.fx.speeds._default;
					this._isOpen = !1;
					a = (this.button = d('<button type="button"><span class="ui-icon ui-icon-triangle-2-n-s"></span></button>')).addClass("ui-multiselect ui-widget ui-state-default ui-corner-all").addClass(b.classes).attr({
						title: a.attr("title"),
						"aria-haspopup": !0,
						tabIndex: a.attr("tabIndex")
					}).insertAfter(a);
					(this.buttonlabel = d("<span />")).html(b.noneSelectedText).appendTo(a);
					var a = (this.menu = d("<div />")).addClass("ui-multiselect-menu ui-widget ui-widget-content ui-corner-all").addClass(b.classes).appendTo(document.body),
		                c = (this.header = d("<div />")).addClass("ui-widget-header ui-corner-all ui-multiselect-header ui-helper-clearfix").appendTo(a);
					(this.headerLinkContainer = d("<ul />")).addClass("ui-helper-reset").html(function () {
						return !0 === b.header ? '<li><a class="ui-multiselect-all" href="#"><span class="ui-icon ui-icon-check"></span><span>' + b.checkAllText + '</span></a></li><li><a class="ui-multiselect-none" href="#"><span class="ui-icon ui-icon-closethick"></span><span>' + b.uncheckAllText + "</span></a></li>" : "string" === typeof b.header ? "<li>" + b.header + "</li>" : ""
					}).append('<li class="ui-multiselect-close"><a href="#" class="ui-multiselect-close"><span class="ui-icon ui-icon-circle-close"></span></a></li>').appendTo(c);
					(this.checkboxContainer = d("<ul />")).addClass("ui-multiselect-checkboxes ui-helper-reset").appendTo(a);
					this._bindEvents();
					this.refresh(!0);
					b.multiple || a.addClass("ui-multiselect-single")
				},
				_init: function () {
					!1 === this.options.header && this.header.hide();
					this.options.multiple || this.headerLinkContainer.find(".ui-multiselect-all, .ui-multiselect-none").hide();
					this.options.autoOpen && this.open();
					this.element.is(":disabled") && this.disable()
				},
				refresh: function (a) {
					var b = this.element,
		                c = this.options,
		                f = this.menu,
		                h = this.checkboxContainer,
		                g = [],
		                e = "",
		                i = b.attr("id") || k++;
					b.find("option").each(function (b) {
						d(this);
						var a = this.parentNode,
		                    f = this.innerHTML,
		                    h = this.title,
		                    k = this.value,
		                    b = "ui-multiselect-" + (this.id || i + "-option-" + b),
		                    l = this.disabled,
		                    n = false, //this.selected,
		                    m = ["ui-corner-all"],
		                    o = (l ? "ui-multiselect-disabled " : " ") + this.className,
		                    j;
						"OPTGROUP" === a.tagName && (j = a.getAttribute("label"), -1 === d.inArray(j, g) && (e += '<li class="ui-multiselect-optgroup-label ' + a.className + '"><a href="#">' + j + "</a></li>", g.push(j)));
						l && m.push("ui-state-disabled");
						n && !c.multiple && m.push("ui-state-active");
						e += '<li class="' + o + '">';
						e += '<label for="' + b + '" title="' + h + '" class="' + m.join(" ") + '">';
						e += '<input id="' + b + '" name="multiselect_' + i + '" type="' + (c.multiple ? "checkbox" : "radio") + '" value="' + k + '" title="' + f + '"';
						n && (e += ' checked="checked"', e += ' aria-selected="true"');
						l && (e += ' disabled="disabled"', e += ' aria-disabled="true"');
						e += " /><span>" + f + "</span></label></li>"
					});
					h.html(e);
					this.labels = f.find("label");
					this.inputs = this.labels.children("input");
					this._setButtonWidth();
					this._setMenuWidth();
					this.button[0].defaultValue = this.update();
					a || this._trigger("refresh")
				},
				update: function () {
					var a = this.options,
		                b = this.inputs,
		                c = b.filter(":checked"),
		                f = c.length,
		                a = 0 === f ? a.noneSelectedText : d.isFunction(a.selectedText) ? a.selectedText.call(this, f, b.length, c.get()) : /\d/.test(a.selectedList) && 0 < a.selectedList && f <= a.selectedList ? c.map(function () {
		                	return d(this).next().html()
		                }).get().join(", ") : a.selectedText.replace("#", f).replace("#", b.length);
					this.buttonlabel.html(a);
					return a
				},
				_bindEvents: function () {
					function a() {
						b[b._isOpen ? "close" : "open"]();
						return !1
					}
					var b = this,
		                c = this.button;
					c.find("span").bind("click.multiselect", a);
					c.bind({
						click: a,
						keypress: function (a) {
							switch (a.which) {
								case 27:
								case 38:
								case 37:
									b.close();
									break;
								case 39:
								case 40:
									b.open()
							}
						},
						mouseenter: function () {
							c.hasClass("ui-state-disabled") || d(this).addClass("ui-state-hover")
						},
						mouseleave: function () {
							d(this).removeClass("ui-state-hover")
						},
						focus: function () {
							c.hasClass("ui-state-disabled") || d(this).addClass("ui-state-focus")
						},
						blur: function () {
							d(this).removeClass("ui-state-focus")
						}
					});
					this.header.delegate("a", "click.multiselect", function (a) {
						if (d(this).hasClass("ui-multiselect-close")) b.close();
						else b[d(this).hasClass("ui-multiselect-all") ? "checkAll" : "uncheckAll"]();
						a.preventDefault()
					});
					this.menu.delegate("li.ui-multiselect-optgroup-label a", "click.multiselect", function (a) {
						a.preventDefault();
						var c = d(this),
		                    g = c.parent().nextUntil("li.ui-multiselect-optgroup-label").find("input:visible:not(:disabled)"),
		                    e = g.get(),
		                    c = c.parent().text();
						!1 !== b._trigger("beforeoptgrouptoggle", a, {
							inputs: e,
							label: c
						}) && (b._toggleChecked(g.filter(":checked").length !== g.length, g), b._trigger("optgrouptoggle", a, {
							inputs: e,
							label: c,
							checked: e[0].checked
						}))
					}).delegate("label", "mouseenter.multiselect", function () {
						d(this).hasClass("ui-state-disabled") || (b.labels.removeClass("ui-state-hover"), d(this).addClass("ui-state-hover").find("input").focus())
					}).delegate("label", "keydown.multiselect", function (a) {
						a.preventDefault();
						switch (a.which) {
							case 9:
							case 27:
								b.close();
								break;
							case 38:
							case 40:
							case 37:
							case 39:
								b._traverse(a.which, this);
								break;
							case 13:
								d(this).find("input")[0].click()
						}
					}).delegate('input[type="checkbox"], input[type="radio"]', "click.multiselect", function (a) {
						var c = d(this),
		                    g = this.value,
		                    e = this.checked,
		                    i = b.element.find("option");
						this.disabled || !1 === b._trigger("click", a, {
							value: g,
							text: this.title,
							checked: e
						}) ? a.preventDefault() : (c.focus(), c.attr("aria-selected", e), i.each(function () {
							this.value === g ? this.selected = e : b.options.multiple || (this.selected = !1)
						}), b.options.multiple || (b.labels.removeClass("ui-state-active"), c.closest("label").toggleClass("ui-state-active", e), b.close()), b.element.trigger("change"), setTimeout(d.proxy(b.update, b), 10))
					});
					d(document).bind("mousedown.multiselect", function (a) {
						b._isOpen && (!d.contains(b.menu[0], a.target) && !d.contains(b.button[0], a.target) && a.target !== b.button[0]) /*JPL: Removed by request 14/8-2013*/ /*&& b.close()*/
					});
					d(this.element[0].form).bind("reset.multiselect", function () {
						setTimeout(d.proxy(b.refresh, b), 10)
					})
				},
				_setButtonWidth: function () {
					var a = this.element.outerWidth(),
		                b = this.options;
					/\d/.test(b.minWidth) && a < b.minWidth && (a = b.minWidth);
					this.button.width(a)
				},
				_setMenuWidth: function () {
					var a = this.menu,
		                b = this.button.outerWidth() - parseInt(a.css("padding-left"), 10) - parseInt(a.css("padding-right"), 10) - parseInt(a.css("border-right-width"), 10) - parseInt(a.css("border-left-width"), 10);
					a.width(b + 5 || this.button.outerWidth() + 5)
				},
				_traverse: function (a, b) {
					var c = d(b),
		                f = 38 === a || 37 === a,
		                c = c.parent()[f ? "prevAll" : "nextAll"]("li:not(.ui-multiselect-disabled, .ui-multiselect-optgroup-label)")[f ? "last" : "first"]();
					c.length ? c.find("label").trigger("mouseover") : (c = this.menu.find("ul").last(), this.menu.find("label")[f ? "last" : "first"]().trigger("mouseover"), c.scrollTop(f ? c.height() : 0))
				},
				_toggleState: function (a, b) {
					return function () {
						this.disabled || (this[a] = b);
						b ? this.setAttribute("aria-selected", !0) : this.removeAttribute("aria-selected")
					}
				},
				_toggleChecked: function (a, b) {
					var c = b && b.length ? b : this.inputs,
		                f = this;
					c.each(this._toggleState("checked", a));
					c.eq(0).focus();
					this.update();
					var h = c.map(function () {
						return this.value
					}).get();
					this.element.find("option").each(function () {
						!this.disabled && -1 < d.inArray(this.value, h) && f._toggleState("selected", a).call(this)
					});
					c.length && this.element.trigger("change")
				},
				_toggleDisabled: function (a) {
					this.button.attr({
						disabled: a,
						"aria-disabled": a
					})[a ? "addClass" : "removeClass"]("ui-state-disabled");
					var b = this.menu.find("input"),
		                b = a ? b.filter(":enabled").data("ech-multiselect-disabled", !0) : b.filter(function () {
		                	return !0 === d.data(this, "ech-multiselect-disabled")
		                }).removeData("ech-multiselect-disabled");
					b.attr({
						disabled: a,
						"arial-disabled": a
					}).parent()[a ? "addClass" : "removeClass"]("ui-state-disabled");
					this.element.attr({
						disabled: a,
						"aria-disabled": a
					})
				},
				open: function () {
					var a = this.button,
		                b = this.menu,
		                c = this.speed,
		                f = this.options,
		                h = [];
					if (!(!1 === this._trigger("beforeopen") || a.hasClass("ui-state-disabled") || this._isOpen)) {
						var g = b.find("ul").last(),
		                    e = f.show,
		                    i = a.offset();
						d.isArray(f.show) && (e = f.show[0], c = f.show[1] || this.speed);
						e && (h = [e, c]);
						g.scrollTop(0).height(f.height);
						d.ui.position && !d.isEmptyObject(f.position) ? (f.position.of = f.position.of || a, b.show().position(f.position).hide()) : b.css({
							top: i.top + a.outerHeight(),
							left: i.left
						});
						d.fn.show.apply(b, h);
						this.labels.eq(0).trigger("mouseover").trigger("mouseenter").find("input").trigger("focus");
						a.addClass("ui-state-active");
						this._isOpen = !0;
						this._trigger("open")
					}
				},
				close: function () {
					if (!1 !== this._trigger("beforeclose")) {
						var a = this.options, b = a.hide, c = this.speed, f = [];
						d.isArray(a.hide) && (b = a.hide[0], c = a.hide[1] || this.speed);
						b && (f = [b, c]);
						d.fn.hide.apply(this.menu, f);
						this.button.removeClass("ui-state-active").trigger("blur").trigger("mouseleave");
						this._isOpen = !1;
						this._trigger("close")
					}
				},
				enable: function () {
					this._toggleDisabled(!1)
				},
				disable: function () {
					this._toggleDisabled(!0)
				},
				checkAll: function () {
					this._toggleChecked(!0);
					this._trigger("checkAll")
				},
				uncheckAll: function () {
					this._toggleChecked(!1);
					this._trigger("uncheckAll")
				},
				getChecked: function () {
					return this.menu.find("input").filter(":checked")
				},
				destroy: function () {
					d.Widget.prototype.destroy.call(this);
					this.button.remove();
					this.menu.remove();
					this.element.show();
					return this
				},
				isOpen: function () {
					returnthis._isOpen
				},
				widget: function () {
					return this.menu
				},
				getButton: function () {
					return this.button
				},
				_setOption: function (a, b) {
					var c = this.menu;
					switch (a) {
						case "header":
							c.find("div.ui-multiselect-header")[b ? "show" : "hide"]();
							break;
						case "checkAllText":
							c.find("a.ui-multiselect-all span").eq(-1).text(b);
							break;
						case "uncheckAllText":
							c.find("a.ui-multiselect-none span").eq(-1).text(b);
							break;
						case "height":
							c.find("ul").last().height(parseInt(b, 10));
							break;
						case "minWidth":
							this.options[a] = parseInt(b, 10);
							this._setButtonWidth();
							this._setMenuWidth();
							break;
						case "selectedText":
						case "selectedList":
						case "noneSelectedText":
							this.options[a] = b;
							this.update();
							break;
						case "classes":
							c.add(this.button).removeClass(this.options.classes).addClass(b);
							break;
						case "multiple":
							c.toggleClass("ui-multiselect-single", !b), this.options.multiple = b, this.element[0].multiple = b, this.refresh()
					}
					d.Widget.prototype._setOption.apply(this, arguments)
				}
			})
		})(jQuery);
		// Lettering.js - letteringjs.com
		(function (b) { function c(a, e, c, d) { var e = a.text().split(e), f = ""; e.length && (b(e).each(function (a, b) { f += '<span class="' + c + (a + 1) + '">' + b + "</span>" + d }), a.empty().append(f)) } var d = { init: function () { return this.each(function () { c(b(this), "", "char", "") }) }, words: function () { return this.each(function () { c(b(this), " ", "word", " ") }) }, lines: function () { return this.each(function () { c(b(this).children("br").replaceWith("eefec303079ad17405c889e092e105b0").end(), "eefec303079ad17405c889e092e105b0", "line", "") }) } }; b.fn.lettering = function (a) { if (a && d[a]) return d[a].apply(this, [].slice.call(arguments, 1)); if ("letters" === a || !a) return d.init.apply(this, [].slice.call(arguments, 0)); b.error("Method " + a + " does not exist on jQuery.lettering"); return this } })(jQuery);
		// FancyBox - jQuery Plugin - Examples and documentation at: http://fancybox.net
		(function (b) { var m, t, u, f, D, j, E, n, z, A, q = 0, e = {}, o = [], p = 0, d = {}, l = [], G = null, v = new Image, J = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, W = /[^\.]\.(swf)\s*$/i, K, L = 1, y = 0, s = "", r, i, h = false, B = b.extend(b("<div/>")[0], { prop: 0 }), M = b.browser.msie && b.browser.version < 7 && !window.XMLHttpRequest, N = function () { t.hide(); v.onerror = v.onload = null; G && G.abort(); m.empty() }, O = function () { if (false === e.onError(o, q, e)) { t.hide(); h = false } else { e.titleShow = false; e.width = "auto"; e.height = "auto"; m.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>'); F() } }, I = function () { var a = o[q], c, g, k, C, P, w; N(); e = b.extend({}, b.fn.fancybox.defaults, typeof b(a).data("fancybox") == "undefined" ? e : b(a).data("fancybox")); w = e.onStart(o, q, e); if (w === false) h = false; else { if (typeof w == "object") e = b.extend(e, w); k = e.title || (a.nodeName ? b(a).attr("title") : a.title) || ""; if (a.nodeName && !e.orig) e.orig = b(a).children("img:first").length ? b(a).children("img:first") : b(a); if (k === "" && e.orig && e.titleFromAlt) k = e.orig.attr("alt"); c = e.href || (a.nodeName ? b(a).attr("href") : a.href) || null; if (/^(?:javascript)/i.test(c) || c == "#") c = null; if (e.type) { g = e.type; if (!c) c = e.content } else if (e.content) g = "html"; else if (c) g = c.match(J) ? "image" : c.match(W) ? "swf" : b(a).hasClass("iframe") ? "iframe" : c.indexOf("#") === 0 ? "inline" : "ajax"; if (g) { if (g == "inline") { a = c.substr(c.indexOf("#")); g = b(a).length > 0 ? "inline" : "ajax" } e.type = g; e.href = c; e.title = k; if (e.autoDimensions) if (e.type == "html" || e.type == "inline" || e.type == "ajax") { e.width = "auto"; e.height = "auto" } else e.autoDimensions = false; if (e.modal) { e.overlayShow = true; e.hideOnOverlayClick = false; e.hideOnContentClick = false; e.enableEscapeButton = false; e.showCloseButton = false } e.padding = parseInt(e.padding, 10); e.margin = parseInt(e.margin, 10); m.css("padding", e.padding + e.margin); b(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () { b(this).replaceWith(j.children()) }); switch (g) { case "html": m.html(e.content); F(); break; case "inline": if (b(a).parent().is("#fancybox-content") === true) { h = false; break } b('<div class="fancybox-inline-tmp" />').hide().insertBefore(b(a)).bind("fancybox-cleanup", function () { b(this).replaceWith(j.children()) }).bind("fancybox-cancel", function () { b(this).replaceWith(m.children()) }); b(a).appendTo(m); F(); break; case "image": h = false; b.fancybox.showActivity(); v = new Image; v.onerror = function () { O() }; v.onload = function () { h = true; v.onerror = v.onload = null; e.width = v.width; e.height = v.height; b("<img />").attr({ id: "fancybox-img", src: v.src, alt: e.title }).appendTo(m); Q() }; v.src = c; break; case "swf": e.scrolling = "no"; C = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + e.width + '" height="' + e.height + '"><param name="movie" value="' + c + '"></param>'; P = ""; b.each(e.swf, function (x, H) { C += '<param name="' + x + '" value="' + H + '"></param>'; P += " " + x + '="' + H + '"' }); C += '<embed src="' + c + '" type="application/x-shockwave-flash" width="' + e.width + '" height="' + e.height + '"' + P + "></embed></object>"; m.html(C); F(); break; case "ajax": h = false; b.fancybox.showActivity(); e.ajax.win = e.ajax.success; G = b.ajax(b.extend({}, e.ajax, { url: c, data: e.ajax.data || {}, error: function (x) { x.status > 0 && O() }, success: function (x, H, R) { if ((typeof R == "object" ? R : G).status == 200) { if (typeof e.ajax.win == "function") { w = e.ajax.win(c, x, H, R); if (w === false) { t.hide(); return } else if (typeof w == "string" || typeof w == "object") x = w } m.html(x); F() } } })); break; case "iframe": Q() } } else O() } }, F = function () { var a = e.width, c = e.height; a = a.toString().indexOf("%") > -1 ? parseInt((b(window).width() - e.margin * 2) * parseFloat(a) / 100, 10) + "px" : a == "auto" ? "auto" : a + "px"; c = c.toString().indexOf("%") > -1 ? parseInt((b(window).height() - e.margin * 2) * parseFloat(c) / 100, 10) + "px" : c == "auto" ? "auto" : c + "px"; m.wrapInner('<div style="width:' + a + ";height:" + c + ";overflow: " + (e.scrolling == "auto" ? "auto" : e.scrolling == "yes" ? "scroll" : "hidden") + ';position:relative;"></div>'); e.width = m.width(); e.height = m.height(); Q() }, Q = function () { var a, c; t.hide(); if (f.is(":visible") && false === d.onCleanup(l, p, d)) { b.event.trigger("fancybox-cancel"); h = false } else { h = true; b(j.add(u)).unbind(); b(window).unbind("resize.fb scroll.fb"); b(document).unbind("keydown.fb"); f.is(":visible") && d.titlePosition !== "outside" && f.css("height", f.height()); l = o; p = q; d = e; if (d.overlayShow) { u.css({ "background-color": d.overlayColor, opacity: d.overlayOpacity, cursor: d.hideOnOverlayClick ? "pointer" : "auto", height: b(document).height() }); if (!u.is(":visible")) { M && b("select:not(#fancybox-tmp select)").filter(function () { return this.style.visibility !== "hidden" }).css({ visibility: "hidden" }).one("fancybox-cleanup", function () { this.style.visibility = "inherit" }); u.show() } } else u.hide(); i = X(); s = d.title || ""; y = 0; n.empty().removeAttr("style").removeClass(); if (d.titleShow !== false) { if (b.isFunction(d.titleFormat)) a = d.titleFormat(s, l, p, d); else a = s && s.length ? d.titlePosition == "float" ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + s + '</td><td id="fancybox-title-float-right"></td></tr></table>' : '<div id="fancybox-title-' + d.titlePosition + '">' + s + "</div>" : false; s = a; if (!(!s || s === "")) { n.addClass("fancybox-title-" + d.titlePosition).html(s).appendTo("body").show(); switch (d.titlePosition) { case "inside": n.css({ width: i.width - d.padding * 2, marginLeft: d.padding, marginRight: d.padding }); y = n.outerHeight(true); n.appendTo(D); i.height += y; break; case "over": n.css({ marginLeft: d.padding, width: i.width - d.padding * 2, bottom: d.padding }).appendTo(D); break; case "float": n.css("left", parseInt((n.width() - i.width - 40) / 2, 10) * -1).appendTo(f); break; default: n.css({ width: i.width - d.padding * 2, paddingLeft: d.padding, paddingRight: d.padding }).appendTo(f) } } } n.hide(); if (f.is(":visible")) { b(E.add(z).add(A)).hide(); a = f.position(); r = { top: a.top, left: a.left, width: f.width(), height: f.height() }; c = r.width == i.width && r.height == i.height; j.fadeTo(d.changeFade, 0.3, function () { var g = function () { j.html(m.contents()).fadeTo(d.changeFade, 1, S) }; b.event.trigger("fancybox-change"); j.empty().removeAttr("filter").css({ "border-width": d.padding, width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }); if (c) g(); else { B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.changeSpeed, easing: d.easingChange, step: T, complete: g }) } }) } else { f.removeAttr("style"); j.css("border-width", d.padding); if (d.transitionIn == "elastic") { r = V(); j.html(m.contents()); f.show(); if (d.opacity) i.opacity = 0; B.prop = 0; b(B).animate({ prop: 1 }, { duration: d.speedIn, easing: d.easingIn, step: T, complete: S }) } else { d.titlePosition == "inside" && y > 0 && n.show(); j.css({ width: i.width - d.padding * 2, height: e.autoDimensions ? "auto" : i.height - y - d.padding * 2 }).html(m.contents()); f.css(i).fadeIn(d.transitionIn == "none" ? 0 : d.speedIn, S) } } } }, Y = function () { if (d.enableEscapeButton || d.enableKeyboardNav) b(document).bind("keydown.fb", function (a) { if (a.keyCode == 27 && d.enableEscapeButton) { a.preventDefault(); b.fancybox.close() } else if ((a.keyCode == 37 || a.keyCode == 39) && d.enableKeyboardNav && a.target.tagName !== "INPUT" && a.target.tagName !== "TEXTAREA" && a.target.tagName !== "SELECT") { a.preventDefault(); b.fancybox[a.keyCode == 37 ? "prev" : "next"]() } }); if (d.showNavArrows) { if (d.cyclic && l.length > 1 || p !== 0) z.show(); if (d.cyclic && l.length > 1 || p != l.length - 1) A.show() } else { z.hide(); A.hide() } }, S = function () { if (!b.support.opacity) { j.get(0).style.removeAttribute("filter"); f.get(0).style.removeAttribute("filter") } e.autoDimensions && j.css("height", "auto"); f.css("height", "auto"); s && s.length && n.show(); d.showCloseButton && E.show(); Y(); d.hideOnContentClick && j.bind("click", b.fancybox.close); d.hideOnOverlayClick && u.bind("click", b.fancybox.close); b(window).bind("resize.fb", b.fancybox.resize); d.centerOnScroll && b(window).bind("scroll.fb", b.fancybox.center); if (d.type == "iframe") b('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (b.browser.msie ? 'allowtransparency="true""' : "") + ' scrolling="' + e.scrolling + '" src="' + d.href + '"></iframe>').appendTo(j); f.show(); h = false; b.fancybox.center(); d.onComplete(l, p, d); var a, c; if (l.length - 1 > p) { a = l[p + 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } } if (p > 0) { a = l[p - 1].href; if (typeof a !== "undefined" && a.match(J)) { c = new Image; c.src = a } } }, T = function (a) { var c = { width: parseInt(r.width + (i.width - r.width) * a, 10), height: parseInt(r.height + (i.height - r.height) * a, 10), top: parseInt(r.top + (i.top - r.top) * a, 10), left: parseInt(r.left + (i.left - r.left) * a, 10) }; if (typeof i.opacity !== "undefined") c.opacity = a < 0.5 ? 0.5 : a; f.css(c); j.css({ width: c.width - d.padding * 2, height: c.height - y * a - d.padding * 2 }) }, U = function () { return [b(window).width() - d.margin * 2, b(window).height() - d.margin * 2, b(document).scrollLeft() + d.margin, b(document).scrollTop() + d.margin] }, X = function () { var a = U(), c = {}, g = d.autoScale, k = d.padding * 2; c.width = d.width.toString().indexOf("%") > -1 ? parseInt(a[0] * parseFloat(d.width) / 100, 10) : d.width + k; c.height = d.height.toString().indexOf("%") > -1 ? parseInt(a[1] * parseFloat(d.height) / 100, 10) : d.height + k; if (g && (c.width > a[0] || c.height > a[1])) if (e.type == "image" || e.type == "swf") { g = d.width / d.height; if (c.width > a[0]) { c.width = a[0]; c.height = parseInt((c.width - k) / g + k, 10) } if (c.height > a[1]) { c.height = a[1]; c.width = parseInt((c.height - k) * g + k, 10) } } else { c.width = Math.min(c.width, a[0]); c.height = Math.min(c.height, a[1]) } c.top = parseInt(Math.max(a[3] - 20, a[3] + (a[1] - c.height - 40) * 0.5), 10); c.left = parseInt(Math.max(a[2] - 20, a[2] + (a[0] - c.width - 40) * 0.5), 10); return c }, V = function () { var a = e.orig ? b(e.orig) : false, c = {}; if (a && a.length) { c = a.offset(); c.top += parseInt(a.css("paddingTop"), 10) || 0; c.left += parseInt(a.css("paddingLeft"), 10) || 0; c.top += parseInt(a.css("border-top-width"), 10) || 0; c.left += parseInt(a.css("border-left-width"), 10) || 0; c.width = a.width(); c.height = a.height(); c = { width: c.width + d.padding * 2, height: c.height + d.padding * 2, top: c.top - d.padding - 20, left: c.left - d.padding - 20} } else { a = U(); c = { width: d.padding * 2, height: d.padding * 2, top: parseInt(a[3] + a[1] * 0.5, 10), left: parseInt(a[2] + a[0] * 0.5, 10)} } return c }, Z = function () { if (t.is(":visible")) { b("div", t).css("top", L * -40 + "px"); L = (L + 1) % 12 } else clearInterval(K) }; b.fn.fancybox = function (a) { if (!b(this).length) return this; b(this).data("fancybox", b.extend({}, a, b.metadata ? b(this).metadata() : {})).unbind("click.fb").bind("click.fb", function (c) { c.preventDefault(); if (!h) { h = true; b(this).blur(); o = []; q = 0; c = b(this).attr("rel") || ""; if (!c || c == "" || c === "nofollow") o.push(this); else { o = b("a[rel=" + c + "], area[rel=" + c + "]"); q = o.index(this) } I() } }); return this }; b.fancybox = function (a, c) { var g; if (!h) { h = true; g = typeof c !== "undefined" ? c : {}; o = []; q = parseInt(g.index, 10) || 0; if (b.isArray(a)) { for (var k = 0, C = a.length; k < C; k++) if (typeof a[k] == "object") b(a[k]).data("fancybox", b.extend({}, g, a[k])); else a[k] = b({}).data("fancybox", b.extend({ content: a[k] }, g)); o = jQuery.merge(o, a) } else { if (typeof a == "object") b(a).data("fancybox", b.extend({}, g, a)); else a = b({}).data("fancybox", b.extend({ content: a }, g)); o.push(a) } if (q > o.length || q < 0) q = 0; I() } }; b.fancybox.showActivity = function () { clearInterval(K); t.show(); K = setInterval(Z, 66) }; b.fancybox.hideActivity = function () { t.hide() }; b.fancybox.next = function () { return b.fancybox.pos(p + 1) }; b.fancybox.prev = function () { return b.fancybox.pos(p - 1) }; b.fancybox.pos = function (a) { if (!h) { a = parseInt(a); o = l; if (a > -1 && a < l.length) { q = a; I() } else if (d.cyclic && l.length > 1) { q = a >= l.length ? 0 : l.length - 1; I() } } }; b.fancybox.cancel = function () { if (!h) { h = true; b.event.trigger("fancybox-cancel"); N(); e.onCancel(o, q, e); h = false } }; b.fancybox.close = function () { function a() { u.fadeOut("fast"); n.empty().hide(); f.hide(); b.event.trigger("fancybox-cleanup"); j.empty(); d.onClosed(l, p, d); l = e = []; p = q = 0; d = e = {}; h = false } if (!(h || f.is(":hidden"))) { h = true; if (d && false === d.onCleanup(l, p, d)) h = false; else { N(); b(E.add(z).add(A)).hide(); b(j.add(u)).unbind(); b(window).unbind("resize.fb scroll.fb"); b(document).unbind("keydown.fb"); j.find("iframe").attr("src", M && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank"); d.titlePosition !== "inside" && n.empty(); f.stop(); if (d.transitionOut == "elastic") { r = V(); var c = f.position(); i = { top: c.top, left: c.left, width: f.width(), height: f.height() }; if (d.opacity) i.opacity = 1; n.empty().hide(); B.prop = 1; b(B).animate({ prop: 0 }, { duration: d.speedOut, easing: d.easingOut, step: T, complete: a }) } else f.fadeOut(d.transitionOut == "none" ? 0 : d.speedOut, a) } } }; b.fancybox.resize = function () { u.is(":visible") && u.css("height", b(document).height()); b.fancybox.center(true) }; b.fancybox.center = function (a) { var c, g; if (!h) { g = a === true ? 1 : 0; c = U(); !g && (f.width() > c[0] || f.height() > c[1]) || f.stop().animate({ top: parseInt(Math.max(c[3] - 20, c[3] + (c[1] - j.height() - 40) * 0.5 - d.padding)), left: parseInt(Math.max(c[2] - 20, c[2] + (c[0] - j.width() - 40) * 0.5 - d.padding)) }, typeof a == "number" ? a : 200) } }; b.fancybox.init = function () { if (!b("#fancybox-wrap").length) { b("body").append(m = b('<div id="fancybox-tmp"></div>'), t = b('<div id="fancybox-loading"><div></div></div>'), u = b('<div id="fancybox-overlay"></div>'), f = b('<div id="fancybox-wrap"></div>')); D = b('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(f); D.append(j = b('<div id="fancybox-content"></div>'), E = b('<a id="fancybox-close"></a>'), n = b('<div id="fancybox-title"></div>'), z = b('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), A = b('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')); E.click(b.fancybox.close); t.click(b.fancybox.cancel); z.click(function (a) { a.preventDefault(); b.fancybox.prev() }); A.click(function (a) { a.preventDefault(); b.fancybox.next() }); b.fn.mousewheel && f.bind("mousewheel.fb", function (a, c) { if (h) a.preventDefault(); else if (b(a.target).get(0).clientHeight == 0 || b(a.target).get(0).scrollHeight === b(a.target).get(0).clientHeight) { a.preventDefault(); b.fancybox[c > 0 ? "prev" : "next"]() } }); b.support.opacity || f.addClass("fancybox-ie"); if (M) { t.addClass("fancybox-ie6"); f.addClass("fancybox-ie6"); b('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(D) } } }; b.fn.fancybox.defaults = { padding: 30, margin: 40, opacityenable: false, modal: false, cyclic: false, scrolling: "auto", width: 560, height: 340, autoScale: true, autoDimensions: true, centerOnScroll: false, ajax: {}, swf: { wmode: "transparent" }, hideOnOverlayClick: true, hideOnContentClick: false, overlayShow: true, overlayOpacity: 0.7, overlayColor: "#000", titleShow: true, titlePosition: "float", titleFormat: null, titleFromAlt: false, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing", easingOut: "swing", showCloseButton: true, showNavArrows: true, enableEscapeButton: true, enableKeyboardNav: true, onStart: function () { }, onCancel: function () { }, onComplete: function () { }, onCleanup: function () { }, onClosed: function () { }, onError: function () { } }; b(document).ready(function () { b.fancybox.init() }) })(jQuery);
		// Debounced resize listener - http://paulirish.com/2009/throttled-smartresize-jquery-event-handler/
		(function (h, b) { var g = function (a, b, d) { var c; return function () { var e = this, f = arguments; c ? clearTimeout(c) : d && a.apply(e, f); c = setTimeout(function () { d || a.apply(e, f); c = null }, b || 100) } }; jQuery.fn[b] = function (a) { return a ? this.bind("resize", g(a)) : this.trigger(b) } })(jQuery, "smartresize");
		// Custom dropdowns - ported from simpleFormUI - https://bitbucket.org/laustdeleuran/simpleformui/
		(function (c) { c.fn.simpleFormUI = function (k) { var d = { cssClass: "simpleFormUI", inheritClasses: !0, inlineStyles: !0, keyboardSupport: !0, hideStyles: { visibility: "hidden", position: "absolute", left: "-9999em", outline: "none none" } }; k && c.extend(d, k); var i = d.cssClass ? "." + d.cssClass : "", e = { selectOption: function (a, b, e, d) { if (c(a).attr("id")) { var a = c(a), b = a.closest(b + ".select"), f = a.attr("id").split("select-")[1], f = b.find('option[value="' + f + '"]'), d = "undefined" === typeof d ? !0 : d; b.find("li").removeClass("selected"); a.addClass("selected"); b.find("option").removeAttr("selected"); f.attr("selected", "selected"); d && f.parent().trigger("change"); e.text(f.text()).removeAttr("class").attr("class", a.attr("data-cssclass")) } }, showList: function (a, b) { a.offset().top + b.outerHeight() + a.outerHeight() > c(window).height() + c(window).scrollTop() ? b.css({ top: "auto", bottom: "110%" }).slideDown(175) : b.css({ top: "110%", bottom: "auto" }).slideDown(175); a.css("z-index", 1E4).addClass('open'); /*JPL:Removed by request 14/8-2013*/ /*c("html").bind("click", { container: a, list: b }, e.htmlHideList)*/ }, htmlHideList: function (a) { e.hideList(a.data.container, a.data.list); c("html").unbind("click", e.htmlHideList) }, hideList: function (a, b) { b.css("display", "none"); a.css("z-index", 1).removeClass('open'); c("html").unbind("click", e.htmlHideList) }, toggleList: function (a, b) { "block" == b.css("display") ? e.hideList(a, b) : e.showList(a, b) }, hideOtherLists: function (a) { c("." + d.cssClass + ".select").find("ul").each(function () { this !== a && c(this).hide() }) } }; return this.each(function () { if (c(this).is("select")) { var a = c(this), b = a.clone(), g = c('<div class="' + d.cssClass + ' select"' + (d.inlineStyles ? ' style="position:relative;"' : "") + "></div>").append(b), h = c("<ul" + (d.inlineStyles ? ' style="display:none;position:absolute;top:110%;left:0;z-index:9000;margin:0;"' : "") + "></ul>").appendTo(g), f = c("<div></div>").appendTo(g), j = c('<a href="javascript:void(0);"' + (d.inlineStyles ? ' style="display:block;overflow:hidden;"' : "") + ">Default</a>").appendTo(f); "" != b.attr("class") && d.inheritClasses && (g.addClass(b.attr("class")), g.attr("data-cssclass", b.attr("class"))); d.inlineStyles && b.css(d.hideStyles); b.find("option").each(function () { var a = d.inlineStyles ? ' style="list-style:none; margin:0;"' : "", b = c(this), a = c("<li" + a + '><a href="#">' + b.text() + "</a></li>"); "" != b.attr("class") && (a.attr("class", b.attr("class")), a.attr("data-cssclass", b.attr("class"))); a.attr("id", "select-" + c(this).attr("value")); a.click(function (a) { a.preventDefault(); e.selectOption(this, i, j); f.click() }); h.append(a); this.selected && e.selectOption(a, i, j, !1) }); a.replaceWith(g); f.click(function () { e.hideOtherLists(h[0]); e.toggleList(g, h) }); f.keydown(function (a) { var b = a.keyCode; if (38 == b || 40 == b || 27 == b) a.preventDefault(), a = h.find("li.selected"), a[0] ? 38 == b ? e.selectOption(a.prev("li"), i, j) : 40 == b ? e.selectOption(a.next("li"), i, j) : 27 == b && e.hideList(h) : h.children().first().click() }); g.click(function (a) { a.stopPropagation() }) } }) } })(jQuery);
		// Image Preload - v1.2 - https://github.com/farinspace/jquery.imgpreload 
		if ("undefined" != typeof jQuery) { (function (a) { a.imgpreload = function (b, c) { c = a.extend({}, a.fn.imgpreload.defaults, c instanceof Function ? { all: c} : c); if ("string" == typeof b) { b = [b] } var d = []; var e = b.length; for (var f = 0; f < e; f++) { var g = new Image; a(g).bind("load error", function (b) { d.push(this); a.data(this, "loaded", "error" == b.type ? false : true); if (c.each instanceof Function) { c.each.call(this) } if (d.length >= e && c.all instanceof Function) { c.all.call(d) } }); g.src = b[f] } }; a.fn.imgpreload = function (b) { var c = []; this.each(function () { c.push(a(this).attr("src")) }); a.imgpreload(c, b); return this }; a.fn.imgpreload.defaults = { each: null, all: null} })(jQuery) };
		/* jQuery.ThreeDots.min Author Jeremy Horn Version 1.0.10 Date: 1/25/2009 More: http://tpgblog.com/ThreeDots/ compiled by http://yui.2clics.net/ */
		(function (e) { e.fn.ThreeDots = function (h) { var g = this; if ((typeof h == "object") || (h == undefined)) { e.fn.ThreeDots.the_selected = this; var g = e.fn.ThreeDots.update(h) } return g }; e.fn.ThreeDots.update = function (u) { var k, t = null; var m, j, s, q, o; var l, i; var r, h, n; if ((typeof u == "object") || (u == undefined)) { e.fn.ThreeDots.c_settings = e.extend({}, e.fn.ThreeDots.settings, u); var p = e.fn.ThreeDots.c_settings.max_rows; if (p < 1) { return e.fn.ThreeDots.the_selected } var g = false; jQuery.each(e.fn.ThreeDots.c_settings.valid_delimiters, function (v, w) { if (((new String(w)).length == 1)) { g = true } }); if (g == false) { return e.fn.ThreeDots.the_selected } e.fn.ThreeDots.the_selected.each(function () { k = e(this); if (e(k).children("." + e.fn.ThreeDots.c_settings.text_span_class).length == 0) { return true } l = e(k).children("." + e.fn.ThreeDots.c_settings.text_span_class).get(0); var y = a(k, true); var x = e(l).text(); d(k, l, y); var v = e(l).text(); if ((h = e(k).attr("threedots")) != undefined) { e(l).text(h); e(k).children("." + e.fn.ThreeDots.c_settings.e_span_class).remove() } r = e(l).text(); if (r.length <= 0) { r = "" } e(k).attr("threedots", x); if (a(k, y) > p) { curr_ellipsis = e(k).append('<span style="white-space:nowrap" class="' + e.fn.ThreeDots.c_settings.e_span_class + '">' + e.fn.ThreeDots.c_settings.ellipsis_string + "</span>"); while (a(k, y) > p) { i = b(e(l).text()); e(l).text(i.updated_string); t = i.word; n = i.del; if (n == null) { break } } if (t != null) { var w = c(k, y); if ((a(k, y) <= p - 1) || (w) || (!e.fn.ThreeDots.c_settings.whole_word)) { r = e(l).text(); if (i.del != null) { e(l).text(r + n) } if (a(k, y) > p) { e(l).text(r) } else { e(l).text(e(l).text() + t); if ((a(k, y) > p + 1) || (!e.fn.ThreeDots.c_settings.whole_word) || (v == t) || w) { while ((a(k, y) > p)) { if (e(l).text().length > 0) { e(l).text(e(l).text().substr(0, e(l).text().length - 1)) } else { break } } } } } } } if (x == e(e(k).children("." + e.fn.ThreeDots.c_settings.text_span_class).get(0)).text()) { e(k).children("." + e.fn.ThreeDots.c_settings.e_span_class).remove() } else { if ((e(k).children("." + e.fn.ThreeDots.c_settings.e_span_class)).length > 0) { if (e.fn.ThreeDots.c_settings.alt_text_t) { e(k).children("." + e.fn.ThreeDots.c_settings.text_span_class).attr("title", x) } if (e.fn.ThreeDots.c_settings.alt_text_e) { e(k).children("." + e.fn.ThreeDots.c_settings.e_span_class).attr("title", x) } } } }) } return e.fn.ThreeDots.the_selected }; e.fn.ThreeDots.settings = { valid_delimiters: [" ", ",", "."], ellipsis_string: "...", max_rows: 2, text_span_class: "ellipsis_text", e_span_class: "threedots_ellipsis", whole_word: true, allow_dangle: false, alt_text_e: false, alt_text_t: false }; function c(k, h) { if (e.fn.ThreeDots.c_settings.allow_dangle == true) { return false } var l = e(k).children("." + e.fn.ThreeDots.c_settings.e_span_class).get(0); var g = e(l).css("display"); var i = a(k, h); e(l).css("display", "none"); var j = a(k, h); e(l).css("display", g); if (i > j) { return true } else { return false } } function a(i, j) { var g = typeof j; if ((g == "object") || (g == undefined)) { return e(i).height() / j.lh } else { if (g == "boolean") { var h = f(e(i)); return { lh: h } } } } function b(k) { var j; var i = e.fn.ThreeDots.c_settings.valid_delimiters; k = jQuery.trim(k); var g = -1; var h = null; var l = null; jQuery.each(i, function (m, o) { if (((new String(o)).length != 1) || (o == null)) { return false } var n = k.lastIndexOf(o); if (n != -1) { if (n > g) { g = n; h = k.substring(g + 1); l = o } } }); if (g > 0) { return { updated_string: jQuery.trim(k.substring(0, g)), word: h, del: l } } else { return { updated_string: "", word: jQuery.trim(k), del: null } } } function f(h) { e(h).append("<div id='temp_ellipsis_div' style='position:absolute; visibility:hidden'>H</div>"); var g = e("#temp_ellipsis_div").height(); e("#temp_ellipsis_div").remove(); return g } function d(k, l, m) { var q = e(l).text(); var i = q; var o = e.fn.ThreeDots.c_settings.max_rows; var h, g, n, r, j; var p; if (a(k, m) <= o) { return } else { p = 0; curr_length = i.length; curr_middle = Math.floor((curr_length - p) / 2); h = q.substring(p, p + curr_middle); g = q.substring(p + curr_middle); while (curr_middle != 0) { e(l).text(h); if (a(k, m) <= (o)) { j = Math.floor(g.length / 2); n = g.substring(0, j); p = h.length; i = h + n; curr_length = i.length; e(l).text(i) } else { i = h; curr_length = i.length } curr_middle = Math.floor((curr_length - p) / 2); h = q.substring(0, p + curr_middle); g = q.substring(p + curr_middle) } } } })(jQuery);
		// hitTestObject - http://upshots.org/javascript/jquery-hittestobject
		$.fn.hitTestObject = function (obj) {
			var bounds = this.offset();
			bounds.right = bounds.left + this.outerWidth();
			bounds.bottom = bounds.top + this.outerHeight();
			var compare = obj.offset();
			compare.right = compare.left + obj.outerWidth();
			compare.bottom = compare.top + obj.outerHeight();
			return (!(compare.right < bounds.left ||
					compare.left > bounds.right ||
					compare.bottom < bounds.top ||
					compare.top > bounds.bottom));
		};
		// Reverse // http://forum.jquery.com/topic/jquery-reverse-a-collection-of-jquery-elements
		$.fn.reverse = [].reverse;
		// :before pseudo selector test.
		// By @laustdeleuran
		// Please see http://reference.sitepoint.com/css/pseudoelement-before for inconsistencies
		Modernizr.addTest('before', function () {
			var hasBefore,
						rules = ['#modernizr-before{visibility:hidden;}', '#modernizr-before:before{content:"modernizr-before"}'],
						head = document.getElementsByTagName('head')[0] || (function () {
							return document.documentElement.appendChild(document.createElement('head'));
						} ()),
						root = document.body || (function () {
							return document.documentElement.appendChild(document.createElement('body'));
						} ()),
						before = document.createElement('div'),
						style = document.createElement('style');

			style.type = "text/css";
			if (style.styleSheet) { style.styleSheet.cssText = rules.join(''); }
			else { style.appendChild(document.createTextNode(rules.join(''))); }
			head.appendChild(style);

			before.id = "modernizr-before";
			root.appendChild(before);
			hasBefore = before.offsetHeight > 0,

				head.removeChild(style);
			root.removeChild(before);

			return hasBefore;
		});


		// Behaviour
		// Set up project handles
		_v.alinea = new Object();
		var $doc = $(document),
			$win = $(window);

		// Set up local log
		_v.alinea.log = new _v.utils.log({ meta: false });
		var log = function (arg) { _v.alinea.log.write(arg); };

		// Set up touch event handling
		var touchClick = Modernizr.touch ? 'click' : 'click',
			touchUp = Modernizr.touch ? 'touchend' : 'mouseup',
			touchDown = Modernizr.touch ? 'touchstart' : 'mousedown',
			touchMove = Modernizr.touch ? 'touchmove' : 'mousemove';

		// Bridge mobile safari label click
		$('label').on('click', function () { });

		// Convert time function
		var convertTime = function (ms) {
			var two = function (x) { return ((x > 9) ? "" : "0") + x },
				three = function (x) { return ((x > 99) ? "" : "0") + ((x > 9) ? "" : "0") + x };
			var sec = Math.floor(ms / 1000);
			ms = ms % 1000;
			var min = Math.floor(sec / 60);
			sec = sec % 60;
			var hr = Math.floor(min / 60);
			min = min % 60;
			var day = Math.floor(hr / 60);
			hr = hr % 60;

			return { ms: three(ms), s: two(sec), m: two(min), h: two(hr), d: day };
		}

		// Array filtering object method
		if (!Array.prototype.filter) {
			Array.prototype.filter = function (fun /*, thisp*/) {
				var len = this.length;
				if (typeof fun != "function") throw new TypeError();

				var res = new Array();
				var thisp = arguments[1];
				for (var i = 0; i < len; i++) {
					if (i in this) {
						var val = this[i]; // in case fun mutates this
						if (fun.call(thisp, val, i, this)) res.push(val);
					}
				}

				return res;
			};
		}

		/**
		* Rich Media embedding
		* Utilizes Video-JS for video and SoundManager 2 for audio.
		* Both uses the HTML5 API with flash fallbacks with a similar JS API.
		* For ultimate fallback, markup is build around a link and a placeholder image.
		**/
		var initMedia = function ($mediaItems)
		{
			if ($mediaItems.length > 0)
			{
				if (typeof _v.alinea.players !== 'object') _v.alinea.players = new Object();

				soundManager.url = '/js/soundmanager-2/';
				soundManager.waitForWindowLoad = true;

				soundManager.reboot();
			}

			$mediaItems.each(function (i) {
				// Determine type
				var $container = $(this), type = false;

				
				if ($container.hasClass('vid')) type = 'video';
				if ($container.hasClass('aud')) type = 'audio';
				if ($container.hasClass('vid') && !$container.data("src").length) type = 'novideo';
				if ($container.hasClass('iframe')) return false;
				if (!type) {
					_v.err('Unrecognized media type on ".media" element.')
					return false;
				}

				// Get data
				var id = type + '-' + i + '-' + Math.round(Math.random() * 10000),
					$fallback = $container.children('.fallback'),
					src = $container.data('src').split(',');
				$container.addClass('enhanced');

				// Build new markup
				switch (type) {
					/*rene*/
					case 'novideo': // Video

						// Get extra data
						var width = $container.data('width'),
							height = $container.data('height'),
							poster = $fallback.children('img').attr('src'),
							source = '',
							$chapters = $container.find('.chapters'),
							$info = $container.find('.info'),
							player = false;

						// Set up info
						if ($info[0]) {
							var $infoItems = $info.find('.item'), infoIsShown = false;
							$info.addClass('enhanced').css({
								width: width,
								height: height,
								'margin-left': -Math.round(width / 2)
							});

							// Build navigation
							var $infoNav = $('<ul class="info-nav" />').css({
								width: width,
								'margin-left': -Math.round(width / 2)
							});
							$infoItems.each(function () {
								var $infoItem = $(this),
									$infoNavItem = $('<li>' + $infoItem.find('h1,h2,h3,h4').first().text() + '</li>');

								$infoNavItem.bind(touchClick, function () {
									var $t = $(this);

									if ($t.hasClass('active')) {
										$info.hide();
										$infoItems.hide();
										$(this).removeClass('active');
										infoIsShown = false;
									} else {
										$info.show();
										$infoItem.toggle().siblings('.item').hide();
										$(this).addClass('active').siblings().removeClass('active');
										if (player) player.pause();
										infoIsShown = true;
									}
								});
								$infoNavItem.appendTo($infoNav);
							});
							$infoNav.insertBefore($info);

							// Show/hide navigation
							if (!Modernizr.touch) {
								$infoNav.hide();
								var infoNavTimer = 0,
									showInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 1 }, 250);
									},
									hideInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										if (!infoIsShown) {
											infoNavTimer = setTimeout(function () {
												$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 0 }, 750, function () { $infoNav.css({ 'display': 'none' }); });
											}, 500);
										}
									};
								$container.delegate('.video-js', 'mouseenter', showInfoNav).delegate('.video-js', 'mouseleave', hideInfoNav);
								//$infoNav.hover(showInfoNav, hideInfoNav);
							}

							// Add close button
							$infoItems.each(function () {
								var $infoItem = $(this),
								$close = $('<a href="#close" class="close">Close</a>').prependTo($infoItem);
								$close.bind('click', function (e) {
									$(".die").click();
								});
							});


						}

						break;

					case 'video': // Video

						if (typeof _v.alinea.players.video !== 'object') _v.alinea.players.video = new Array();
						
	
						// Get extra data
						var width = $container.data('width'),
							height = $container.data('height'),
							poster = $fallback.children('img').attr('src'),
							source = '',
							$chapters = $container.find('.chapters'),
							$info = $container.find('.info'),
							player = false;

						// Set up info
						if ($info[0]) {
							var $infoItems = $info.find('.item'), infoIsShown = false;
							$info.addClass('enhanced').css({
								width: width,
								height: height,
								'margin-left': -Math.round(width / 2)
							});

							// Build navigation
							var $infoNav = $('<ul class="info-nav" />').css({
								width: width,
								'margin-left': -Math.round(width / 2)
							});
							$infoItems.each(function () {
								var $infoItem = $(this),
									$infoNavItem = $('<li>' + $infoItem.find('h1,h2,h3,h4').first().text() + '</li>');

								$infoNavItem.bind(touchClick, function () {
									var $t = $(this);

									if ($t.hasClass('active')) {
										$info.hide();
										$infoItems.hide();
										$(this).removeClass('active');
										infoIsShown = false;
									} else {
										$info.show();
										$infoItem.toggle().siblings('.item').hide();
										$(this).addClass('active').siblings().removeClass('active');
										if (player) player.pause();
										infoIsShown = true;
									}
								});
								$infoNavItem.appendTo($infoNav);
							});
							$infoNav.insertBefore($info);

							// Show/hide navigation
							if (!Modernizr.touch) {
								$infoNav.hide();
								var infoNavTimer = 0,
									showInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 1 }, 250);
									},
									hideInfoNav = function (e) {
										clearTimeout(infoNavTimer);
										if (!infoIsShown) {
											infoNavTimer = setTimeout(function () {
												$infoNav.stop().css({ 'display': 'block' }).animate({ 'opacity': 0 }, 750, function () { $infoNav.css({ 'display': 'none' }); });
											}, 500);
										}
									};
								$container.delegate('.video-js', 'mouseenter', showInfoNav).delegate('.video-js', 'mouseleave', hideInfoNav);
								$infoNav.hover(showInfoNav, hideInfoNav);
							}

							// Add close button
							$infoItems.each(function () {
								var $infoItem = $(this),
								$close = $('<a href="#close" class="close">Close</a>').prependTo($infoItem);
								$close.bind('click', function (e) {
									e.preventDefault();
									$info.hide();
									$infoItems.hide();
									$infoNav.children().removeClass('active');
									infoIsShown = false;
								});
							});
						}

						// Set up chapters
						if ($chapters[0]) {
							$chapters.width(width);
							$container.addClass('has-chapters');

							// Set handles
							var $cNext = $chapters.find('.next'),
								$cPrev = $chapters.find('.prev'),
								$cScroll = $chapters.find('.scroll'),
								$cList = $chapters.find('ul'),
								$cItems = $cList.find('li'),
								$cLinks = $cList.find('a'),
								cItemWidth = $cItems.outerWidth(true),
								step = 0,
								shown = Math.floor($cScroll.width() / cItemWidth);

							// Set list width
							$cList.width(cItemWidth * $cItems.length);

							// Set prev/next buttons
							var cShowHidePrevNext = function () {
								var $btns = $cPrev.add($cNext);
								$btns.removeClass('hidden');
								$btns.stop().animate({ 'opacity': 1 }, 250);
								if (step == $cItems.length - shown) {
									$cNext.stop().animate({ 'opacity': 0 }, 250).addClass('hidden');
								} else if (step == 0) {
									$cPrev.stop().animate({ 'opacity': 0 }, 250).addClass('hidden');
								}
							};
							$cNext.bind('click', function (e) {
								e.preventDefault();
								if (step < $cItems.length - shown) {
									step++;
									$cList.animate({ 'margin-left': -step * cItemWidth }, 500);
								}
								cShowHidePrevNext();
							});
							$cPrev.bind('click', function (e) {
								e.preventDefault();
								if (step > 0) {
									step--;
									$cList.animate({ 'margin-left': -step * cItemWidth }, 500);
								}
								cShowHidePrevNext();
							});
							cShowHidePrevNext();

							// Set link clicks
							$cLinks.bind(touchClick, function (e) {
								e.preventDefault();
								var ms = $(this).attr('href').replace('#', '');
								if (player) {
									player.pause().currentTime(ms / 1000).pause().play();
								} else {
									log('Player not yet ready or not found (' + player + ' ' + typeof player + ').');
								}
							});
						}

						// Set dimensions
						
						if (!($container.parents(".entry").length > 0 || $container.parents(".media-content").length > 0)) {
							$container.css({
								//'min-width': width,
								height: $chapters[0] ? height + 100 : height
							});
						}

						// Build source links
						$.each(src, function (i) {
							var type = src[i].indexOf('.mp4') > -1 ? 'video/mp4' : src[i].indexOf('.webm') > -1 ? 'video/webm' : src[i].indexOf('.ogv') > -1 || src[i].indexOf('.oga') > -1 || src[i].indexOf('.ogg') > -1 ? 'video/ogv' : 'video';
							source = source + '<source src="' + src[i] + '" type="' + type + '" />';
						});

						// Load video
						var loadVideo = function (id, $container) {
							// Insert new markup
							$fallback.remove();
							if ($container.parents(".entry").length > 0) {
								height = "100%";
								width = "100%";
							}
							var $video;

							if ($.browser.msie && $.browser.version <= 9 && ($container.parents(".entry").length > 0)) {
								$video = $('<video id="' + id + '" class="video-js vjs-default-skin" controls preload="auto" poster="' + poster + '">' + source + '</video>').prependTo($container);
							} else {
								$video = $('<video id="' + id + '" class="video-js vjs-default-skin" height="' + height + '" width="' + width + '" controls preload="auto" poster="' + poster + '">' + source + '</video>').prependTo($container);
							}
							var $caption = $container.find('.caption');
							$chapters.insertAfter($video);	
							
							if ($caption.length && $container.next().is('footer')) {
								$container.css('margin-bottom', parseInt($caption.height()) + 50);
							}

							// Init video-js
							var path = typeof basePath === 'string' ? basePath + 'js/video-js/video-js.swf' : '../js/video-js/video-js.swf';
							
							player = _V_(id, { flash: { swf: path, iFrameMode: true }, techOrder: ["html5", "flash"] });

							// Fix fluid width in IE8
							player.ready(function () {
								if ($.browser.msie && $.browser.version == 8 && ($container.parents(".entry").length > 0)) {
									$container.find('.video-js').css({ width: '100%', height: '0' });
								}
							});

							player.addEvent('ended', function () {
								setTimeout(function () {
									$container.find('.vjs-loading-spinner').hide();
									$container.find('.vjs-poster').show();
								}, 100);
							});

							player.addEvent("fullscreenchange", function () {
								if (!$('html').hasClass('touch')) {
									if (!this.isFullScreen) {
										$('#fancybox-close').show();
									} else {
										$('#fancybox-close').hide();
									}
								}
							});

							function resizeIE(myPlayer) {
								var width = document.getElementById(myPlayer.id).parentElement.offsetWidth;
								myPlayer.width(width).height(0);
							}

							if ($.browser.msie && $.browser.version == 9 && ($container.parents(".entry").length > 0)) {
								$(window).resize(function() {
									resizeIE(player);
								});
								resizeIE(player);
							}


							if ($.browser.msie && parseInt($.browser.version) > 9) {
								var $banner = $container.closest('.banner');
								$banner.children('img').height($banner.height());
							}

							// Make player publicly available
							$container.data('player', player);
							_v.alinea.players.video.push(player);

							// Support banner box
							var $bannerBox = $container.find('.box');
							if ($bannerBox[0]) {
								setTimeout(function () {
									try { player.controlBar.fadeIn(); } catch (err) { _v.err(err.toString()); };
								}, 500);
								$bannerBox.bind(touchClick, function () {
									player.play();
								});
								player.addEvent('play', function () {
									$bannerBox.fadeOut();
								});
								player.addEvent('pause', function () {
									$bannerBox.fadeIn();
								});
							}
						};

						if (typeof _V_ === "function") {
							loadVideo(id, $container);
						} else {
							_v.utils.loadRemoteScript('/js/video-js/video.js', function () {
								loadVideo(id, $container);
							});
						}
						break;
					case 'audio': // Audio
						if (typeof _v.alinea.players.audio !== 'object') _v.alinea.players.audio = new Array();

						
						// Get source
						var src = $fallback.attr('href');
						var $printImage = $fallback.find('img');

						// Build UI
						var $ui = $('<div class="sm-ui" id="' + id + '" />'),
							$play = $('<a href="' + src + '" class="sm-play" target="_blank">Play</a>').appendTo($ui),
							$timeLapsed = $('<div class="sm-time-lapsed">00:00</div>').appendTo($ui),
							$progress = $('<div class="sm-progress" />').appendTo($ui),
							$progressBar = $('<div class="sm-bar" />').appendTo($progress),
							$progressLoaded = $('<div class="sm-loaded" />').appendTo($progress),
							$progressPlayed = $('<div class="sm-lapsed" />').appendTo($progress),
							$progressHandle = $('<div class="sm-handle" />').appendTo($progress),
							$timeLeft = $('<div class="sm-time-left">-00:00</div>').appendTo($ui),
							$muteControl = $('<div class="sm-mute-control" />').appendTo($ui),
							$volume = $('<div class="sm-volume" />').appendTo($ui),
							$volumeBar = $('<div class="sm-bar" />').appendTo($volume),
							$volumeLevel = $('<div class="sm-level" />').appendTo($volume),
							$volumeHandle = $('<div class="sm-handle" />').appendTo($volume);



						// Load sound
						var loadSound = function () {
							// Insert new markup
							$fallback.remove();
							$ui.appendTo($container);

							$printImage.addClass('print-image').prependTo($container);
							// Init SM2
							
							soundManager.onready(function () {
								
								// Load sound
								var sound = soundManager.createSound({
									id: id,
									url: src,
									whileloading: updateUi
								});

								// Make player publicly available
								$container.data('player', sound);
								_v.alinea.players.audio.push(player);

								// Set up functions
								var loop = 0,
										updateUi = function () {
											// Stop stacking of loop
											clearTimeout(loop);

											// Update UI
											updateVolume();
											updateTimers();
											updateProgress();
											if (sound.playState > 0 && !sound.paused) {
												if (!$ui.hasClass('playing')) $ui.addClass('playing');
											} else {
												if ($ui.hasClass('playing')) $ui.removeClass('playing');
											}

											// Loop
											if (!sound.paused && sound.playState > 0) loop = setTimeout(updateUi, 100);
										},
										togglePlayback = function () {
											if (sound.playState > 0 && !sound.paused) {
												sound.pause();
											} else {
												sound.play();
											}
											updateUi();
										},
										updateVolume = function () {
											$volumeLevel.height(sound.volume + '%');
											$volumeHandle.css('top', (100 - sound.volume) + '%');
											$ui.removeClass('vol-0 vol-1 vol-2 vol-3').addClass('vol-' + Math.round(sound.volume / 30));
										},
										updateTimers = function () {
											if (sound.duration) {
												var lapsed = convertTime(sound.position),
												left = convertTime(sound.duration - sound.position),
												lapsedText = '',
												leftText = '';

												if (parseInt(lapsed.h) > 0) lapsedText += lapsed.h + ':';
												lapsedText += lapsed.m + ':' + lapsed.s;
												if (parseInt(left.h) > 0) leftText += left.h + ':';
												leftText += left.m + ':' + left.s;

												$timeLapsed.text(lapsedText);
												$timeLeft.text('-' + leftText);
											}
										},
										updateProgress = function () {
											if (sound.duration) {
												var progress = Math.round(sound.position / sound.duration * 100),
												loaded = Math.round(sound.bytesLoaded / sound.bytesTotal * 100);
												$progressPlayed.width(progress + '%');
												$progressHandle.css('left', progress + '%');
												$progressLoaded.width(loaded + '%');
											}
										};

								// Bind events
								// Play click
								$play.bind(touchClick, function (e) {
									e.preventDefault();
									togglePlayback();
								});
								// Progress click
								$progressPlayed.add($progressLoaded).bind(touchClick, function (e) {
									if (sound.duration) {
										var position = e.pageX,
												offset = $(this).offset().left,
												width = $progress.width(),
												pct = Math.round((position - offset) / width * 100);
										sound.setPosition(sound.duration * pct / 100);
										updateUi();
										sound.resume();
									}
								});
								// Progress handle drag
								var listenProgressDrag = function (e) {
									if (sound.duration) {
										sound.pause();
										var position = e.pageX,
												offset = $progress.offset().left,
												width = $progress.width(),
												pct = Math.round((position - offset) / width * 100),
												loadedPct = Math.round($progressLoaded.width() / $progress.width() * 100);
										pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
										if (pct > loadedPct) pct = loadedPct;
										sound.setPosition(sound.duration * pct / 100);
										updateUi();
									}
								},
									isProgressDragging = false;
								$progressHandle.bind(touchDown, function (e) {
									isProgressDragging = true;
									e.stopPropagation();
									sound.pause();
									updateUi();
									$ui.bind(touchMove, listenProgressDrag);
								})
								$ui.bind(touchUp, function (e) {
									if (isProgressDragging) {
										$ui.unbind(touchMove, listenProgressDrag);
										sound.resume();
										updateUi();
										isProgressDragging = false;
									}
								});
								// Volume click
								$volumeBar.add($volumeLevel).bind(touchClick, function (e) {
									var position = e.pageY,
											offset = $volume.offset().top,
											width = $volume.height(),
											pct = 100 - Math.round((position - offset) / width * 100);
									pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
									soundManager.setVolume(id, pct);
									updateUi();
								});
								// Volume handle drag
								var listenVolumeDrag = function (e) {
									var position = e.pageY,
											offset = $volume.offset().top,
											width = $volume.height(),
											pct = 100 - Math.round((position - offset) / width * 100);
									pct = pct > 100 ? 100 : pct < 0 ? 0 : pct;
									soundManager.setVolume(id, pct);
									updateUi();
								},
								isVolumeDragging = false;
								$volumeHandle.bind(touchDown, function (e) {
									isVolumeDragging = true;
									e.stopPropagation();
									updateUi();
									$ui.bind(touchMove, listenVolumeDrag);
								})
								$ui.bind(touchUp, function (e) {
									if (isVolumeDragging) {
										$ui.unbind(touchMove, listenVolumeDrag);
										updateUi();
										isDragging = false;
									}
								});
								// Mute control click
								var showVolume = function () {
									$volume.show();
								},
										hideVolume = function () {
											$volume.hide();
										},
										toggleVolume = function () {
											$volume.toggle();
										};
								hideVolume();
								if (Modernizr.touch) {
									$muteControl.bind(touchClick, function () {
										toggleVolume();
									});
								} else {
									var lastVolume = 100,
											lazyHover = 0;
									$muteControl.bind(touchClick, function () {
										var newVolume = sound.volume > 0 ? 0 : lastVolume;
										lastVolume = sound.volume;
										soundManager.setVolume(id, newVolume);
										updateUi();
									});
									$muteControl.add($volume).hover(function () {
										showVolume();
										clearTimeout(lazyHover);
									}, function () {
										clearTimeout(lazyHover);
										lazyHover = setTimeout(function () {
											hideVolume();
										}, 1000);
									});
								}
							});
						};
						if (typeof soundManager === "object") {
							loadSound(id, $container);
						} else {
							_v.utils.loadRemoteScript('/js/soundmanager-2/soundmanager2-nodebug-jsmin.js', function () {
								loadSound(id, $container);
							});
						}
						break;
				}
			});
		};

		// DOM ready
		$(function () {
			/**
			* Header functionality
			*
			* Header
			**/


			$('.news-listing h2').each(function () {
				$(this).wrapInner('<span class="ellipsis_text" />').ThreeDots({ max_rows: 4 });
			});

			$('.tinycontent p a').each(function ()
			{
				$(this).attr('target', '_blank');
			});

			// Site nav dropdowns
			$('.site-header nav>select:not(.multiselect)').simpleFormUI({ cssClass: 'custom-dropdown' });

			// Enable click event on text prev to custom dropdown
			$('.site-header nav .open-class-list').on('click', function () {
				$(this).parent().find('.custom-dropdown > div > a').trigger('click');
			});

			$('.teacher-label').on('click', function () {
				$(this).parent().find('.ui-multiselect').trigger('click');
			});

			$('.teacher-label').hover(function () {
				$(this).css({
					'text-decoration': 'underline',
					'cursor': 'pointer'
				});
			}, function () {
				$(this).css('text-decoration', 'none');
			});

			$('.site-header nav .custom-dropdown select:not(.multiselect)').parent().prev().hover(function () {
				$(this).css({
					'text-decoration': 'underline',
					'cursor': 'pointer'
				});
			}, function () {
				$(this).css('text-decoration', 'none');
			});

			if ($('.logo.teacher').length > 0)
			{
				$('.site-header nav .custom-dropdown ul li:last').after('<li class="button-holder"><button class="button significant" onclick="location.href = \'/laerer/?changeClasses=true\'; return false;">Rediger</button></li>');

				if ($('.multiselect.teacher-classes option').length >= 10) {
					$('.site-header nav .custom-dropdown ul li:first').before('<li class="button-holder"><button class="button significant" onclick="location.href = \'/laerer/?changeClasses=true\'; return false;">Rediger</button></li>');
				}
			}

			$('.site-header nav .custom-dropdown ul li:first').hide();

			// Multiselect
			$('.site-header nav>select.multiselect option').removeAttr('selected');
			$('.site-header nav>select.multiselect').multiselect({
				noneSelectedText: $('.site-header nav>select.multiselect').data('text'),
				selectedText: function (numChecked, numTotal, checkedItems)
				{
					return $('.site-header nav>select.multiselect').data('text');
				},
				beforeopen: function ()
				{
					$('.multiselect.teacher-classes').find('option').each(function ()
					{
						if ($(this).data('selected') != undefined) {
							$('.ui-multiselect-menu').find('input:checkbox[value=' + $(this).attr('value') + ']').attr('checked', 'checked').trigger('change');
						} else {
							$('.ui-multiselect-menu').find('input:checkbox[value=' + $(this).attr('value') + ']').attr('checked', false);//.trigger('change');
						}
					});
				}
			});

			// Apply form classes to checkboxes
			var multiSelectActions = '<li class="submit"><button class="button significant" onclick="location.href = \'/services/SelectTeacherClasses.ashx?returnUrl=/laerer/&classIds=\' + $(\'select.multiselect.teacher-classes\').multiselect(\'getChecked\').map(function(){ return this.value.toString(); }).toArray().join(\',\');">Gem</button></li>';
			$('.ui-multiselect-menu').addClass('form').find('ul').addClass('checkbox').find('li:last').after(multiSelectActions);

			if ($('.ui-multiselect-menu ul li').length > 10) {
				$('.ui-multiselect-menu').addClass('form').find('ul').addClass('checkbox').find('li:first-child').before(multiSelectActions);
			}

			// Message box functionality
			$('.messages').each(function () {
				// Disable functionality on preview
				if ($(this).hasClass('form-example')) return false;

				// Find common elements and make open close functionality
				var $msgBox = $('.messages').addClass('closed'),
					$msgBtn = $('.button.get-messages'),
					classId = $msgBox.find(".class-id input").val(),
					userId = $msgBox.find(".user-id input").val(),
					openMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if ($msgBox.hasClass('closed')) $msgBox.removeClass('closed');
					},
					closeMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if (!$msgBox.hasClass('closed')) $msgBox.addClass('closed');
					},
					toggleMsgBox = function (e) {
						try { e.preventDefault(); } catch (err) { };
						if ($msgBox.hasClass('closed')) {
							$msgBox.removeClass('closed');
							updateMessageStatus($msgBox.find('article.active'));
							updateTitle($msgBox.find('article.active'));
						} else {
							$msgBox.addClass('closed');
						}
					};

				// Break on missing elements
				if (!$msgBox[0]) {
					log('Unexpected error trying to create message box - required elements not present.');
					return false;
				}
				// Align box offset
				var alignOffset = function () {
					if ($msgBtn[0]) {
						var btnOffset = $msgBtn.offset(),
							btnDim = { width: $msgBtn.outerWidth(), height: $msgBtn.outerHeight() };
						$msgBox.css({ top: btnOffset.top + btnDim.height + 2, left: btnOffset.left + btnDim.width / 2 });
					}
				}
				$win.smartresize(alignOffset).load(alignOffset);
				alignOffset();

				// Show articles and switch between them
				var $articles = $msgBox.find('article'),
					$paging = $msgBox.find('.paging'),
					$next = $paging.find('.next'),
					$prev = $paging.find('.prev'),
					$title = $paging.find('p'),
					updateTitle = function ($el) {
						var s = 0;
						var newCount = 0;
						$articles.each(function (i, e) {
							s += 1;
							if (e == $el[0]) return false;
						});
						newCount = $articles.filter('.new').length;

						$title.text(s + ' af ' + $articles.length);
						$('.button.get-messages .notification').text(newCount);
						
						if (newCount < 1) $('.button.get-messages .notification').removeClass("new");
						else $('.button.get-messages .notification').addClass("new");
					},
					updateMessageStatus = function ($el) {
						var messageId = $el.data("messageid"),
							service = $msgBox.data('serviceurl') + "?MessageId=" + messageId + "&ClassId=" + classId + "&UserId=" + userId,
							data = $msgBox.data('servicedata');
						if ($el.hasClass("new")) {
							_v.services.request(service, data, function(resp) {
								if (resp.Outcome.Validated) {
									var newMessageCount = resp.Response.UnreadMessages;
									$msgBtn.find(".notification").text(newMessageCount);
									$el.removeClass("new");

									if (newMessageCount < 1) {
										$msgBtn.find(".notification").removeClass("new");
									} else {
										$msgBtn.find(".notification").addClass("new");
									}

								} else if (resp.Outcome.Error) {
									feedback(resp.Outcome.Error);
								} else {
									_v.err(this, 'Unexpected error', 'Unexpected format returned.');
								}
							});
						}
					};

				// Update title and set first one active
				updateTitle($articles.first().addClass('active'));

				// Create and bind functionality for prev/next buttons
				$next.bind(touchClick, function (e) {
					e.preventDefault();
					var $current = $articles.filter('.active'),
						$next = $current.next();
					$articles.removeClass('active');
					if ($next.is('article')) {
						$next.addClass('active');
					} else {
						$next = $articles.first().addClass('active');
					}
					updateTitle($next);
					updateMessageStatus($next);
				});
				$prev.bind(touchClick, function (e) {
					e.preventDefault();
					var $current = $articles.filter('.active'),
						$prev = $current.prev();
						$articles.removeClass('active');

					if ($prev.is('article')) {
						$prev.addClass('active');
					} else {
						$prev = $articles.last().addClass('active');
					}
					updateTitle($prev);
					updateMessageStatus($prev);
				});

				// Bind close/open events
				$msgBtn.bind(touchClick, toggleMsgBox);
				$('<a href="#close" class="close">Luk</a>').bind(touchClick, closeMsgBox).appendTo($msgBox);
			});

			// Search form
			$('.search-form').each(function () {
				var $container = $(this),
					$input = $container.find('input[type="text"],input[type="search"]').attr('autocomplete', 'off'),
					$btn = $container.find('input[type="submit"],input[type="button"]'),
					$form = $input.closest('form'),
					$kh = $('<ul class="keywords" />').appendTo($container).hide(),
					timeout = 0;

				// Get keywords
				var keywords = $container.data('keywords'),
					service = $container.data('serviceurl'),
					data = $container.data('servicedata'),
					feedback = function (arg) { return log(arg); };

				// Format prefilled keywords
				if (keywords) {
					keywords = keywords.replace(/'/g, '"');
					try { keywords = eval(keywords); } catch (err) { }
					keywords = typeof keywords === 'object' ? keywords : new Array;
				} else {
					keywords = new Array();
				}

				// Get service keywords
				if (service) {
					$container.addClass('loading');

					// Format prefilled data
					data = data.replace(/'/g, '"');
					try { data = eval('(' + data + ')'); } catch (err) { }
					data = typeof data === 'object' ? data : {};

					// Perform service call
					_v.services.request(service, data, function (resp) {
						$container.removeClass('loading');
						if (resp.Outcome.Validated) {
							keywords = resp.Response.Keywords;
						} else if (resp.Outcome.Error) {
							feedback(resp.Outcome.Error);
						} else {
							_v.err(this, 'Unexpected error', 'Unexpected format returned.');
						}
					});
				}

				// Lookup keywords
				var curVal = false, hideKh = function () { $kh.empty().hide(); };
				$container.hover(function () {
					$container.addClass('hover');
				}, function () {
					$container.removeClass('hover');
				});
				var once = false
				$input.bind('keyup', function (e) {
					if (e.keyCode == 13 || e.keyCode == 32 || e.keyCode == 38 || e.keyCode == 40) return;
					clearTimeout(timeout);
					var val = $input.val().replace(/^\s+/g, "");
					if (val.length > 0) {
						curVal = val;
						setTimeout(function () {
							var filterCriteria = function (e, i, a) {
								return (e.toLowerCase().indexOf(val.toLowerCase()) == 0) && e.length > val.length;
							},
							filteredKeywords = keywords.filter(filterCriteria);

							// Show keywords
							if (filteredKeywords.length > 0) {
								$kh.empty().show();
								$.each(filteredKeywords, function (i, e) {
									var $item = $('<li>' + e + '</li>');
									$item.appendTo($kh).hover(function () {
										$kh.children().removeClass('active');
										$item.addClass('active');
									}, function () {
										$item.removeClass('active');
									});
									$item.bind(touchClick, function () {
										$input.val($item.text());
										hideKh();
										$btn.click();
									});
								});
							} else {
								hideKh();
							}
						}, 50);
					} else if (val.length == 0) {
						hideKh();
					}
				}).bind('keydown', function (e) {
					var $active = $kh.find('.active');
					if ((e.keyCode == 13 || e.keyCode == 32) && $active.length) { // 13:enter, 32:space
						$input.val($active.text());
					} else if ((e.keyCode == 38 || e.keyCode == 40) && $kh.children().length) { // 38:up arrow, 40:down arrow
						var $next = e.keyCode == 38 ? $active.prev() : $active.next();
						if (!$next.length) {
							var $next = e.keyCode == 38 ? $kh.children().last() : $kh.children().first();
						}
						$next.addClass('active').siblings().removeClass('active');
					} else if (e.keyCode == 27) {
						hideKh();
					}
					if (e.keyCode == 13 && $.browser.msie && $.browser.version <= 8) {
						$btn.click();
					}
				}).bind('blur', function () {
					clearTimeout(timeout);
					if (!$container.hasClass('hover')) {
						hideKh();
					}
				});
			});

			/**
			* Comment list toggling
			**/
			$('.teacherlist-toggle').on('click', function (event) {
				event.preventDefault();

				var $toggle = $(this),
					$container = $toggle.closest('.comment-list.is-teacherlist'),
					$elements = $container.find('.comment-item, .comment-field');

				
				if ($container.hasClass('is-closed')) {
					$elements.slideDown();
					$toggle.text('Skjul kommentar');
					$container.removeClass('is-closed');
				} else {
					$elements.slideUp();
					$toggle.text('Vis kommentar');
					$container.addClass('is-closed');
				}
			});

			/**
			* Quiz toggling
			**/
			
			$('.is-quiz li #quiz-btn').on('click', function () {
				if (!$('.is-quiz li #quiz-btn').parent().hasClass('checked')) {
					
					$('.quizElement').slideDown();
					
				}
			});
			$('.is-quiz li #poll-btn').on('click', function () {
				if (!$('.is-quiz li #poll-btn').parent().hasClass('checked')) {
					
					$('.quizElement').slideUp();
					
				}
			});

			if ($('.is-quiz li #quiz-btn').prev().is(':checked')){
				$('.is-quiz li #quiz-btn').trigger('click');
			}

			/**
			* Example cookie (show/hide)
			**/

			$('.example-button').on('click', function (event) {
				checkCookie();

			});

			function setCookie(cname, cvalue, exdays) {
			    var d = new Date();
			    d.setTime(d.getTime() + (exdays*24*60*60*1000));
			    var expires = "expires="+d.toGMTString();
			    document.cookie = cname + "=" + cvalue + "; " + expires+ ";path=/";
			}

			function getCookie(cname) {
			    var name = cname + "=";
			    var ca = document.cookie.split(';');
			    for(var i=0; i<ca.length; i++) {
			        var c = ca[i].trim();
			        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
			    }
			    return "";
			}

			function checkCookie() {
			    var showexamplearticle=getCookie("showexamplearticle");
			    if (showexamplearticle!="") {
			        document.cookie = "showexamplearticle=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
			        window.location.reload()
			    }else{
			        showexamplearticle = "false"
			        if (showexamplearticle != "" && showexamplearticle != null) {
			            setCookie("showexamplearticle", showexamplearticle, 100);
			            window.location.reload()
			        }
			    }
			}

			/**
			* Methode Toggling (easy/hard)
			**/

			$('.easy-button').on('click', function (event) {
				event.preventDefault();
				if($('.easy-method').hasClass('current')){

				}
				else{
					$('.easy-method').addClass('current')
					$('.easy-button').addClass('current')
					$('.hard-method').removeClass('current')
					$('.hard-button').removeClass('current')

				}
			});
			$('.hard-button').on('click', function (event) {
				event.preventDefault();
				if($('.hard-method').hasClass('current')){

				}
				else{
					$('.hard-method').addClass('current')
					$('.hard-button').addClass('current')
					$('.easy-method').removeClass('current')
					$('.easy-button').removeClass('current')

				}
			});

			/**
			* Topic Toggling (all/mellemtrin/udskoling)
			**/

			$('.topicfilter-button').on('click', function (event) {
				event.preventDefault();
				if(!$(this).hasClass('current')){
					$('.topicfilter-button').removeClass('current')
					$(this).addClass('current')
					if($(this).attr("data-id") == "mellemtrin"){
						$('.Udskoling').hide();
						$('.Mellemtrin').show();
					}
					if($(this).attr("data-id") == "udskoling"){
						$('.Mellemtrin').hide();
						$('.Udskoling').show();
					}
					if($(this).attr("data-id") == "all"){
						$('.Mellemtrin, .Udskoling').show();
					}
				}
			});


			/**
			* Helptext Toggling
			**/
			function toggleHelp(trigger, textName) {
				var $trigger = $(trigger),
					$textName = $(textName);
				
				$trigger.on('click', function (event) {
					event.preventDefault();
					
					if (!$trigger.hasClass('is-open')) {
						$textName.stop(false, true).slideDown(200);
						$trigger.addClass('is-open');
					} else {
						$textName.stop(false, true).slideUp(200);
						$trigger.removeClass('is-open');
					}
				});
			}
			toggleHelp('.title-help-toggle', '.title-help');
			toggleHelp('.list-image-help-toggle', '.list-image-help');
			toggleHelp('.authorlist-help-toggle', '.authorlist-help');
			toggleHelp('.authorlist-toggle', '.authorlist');
			toggleHelp('.subjectlist-help-toggle', '.subjectlist-help');
			toggleHelp('.subjectlist-toggle', '.subjectlist');
			toggleHelp('.introtext-help-toggle', '.introtext-help');
			toggleHelp('.text1-help-toggle', '.text1-help');
			toggleHelp('.text2-help-toggle', '.text2-help');
			toggleHelp('.image1-help-toggle', '.image1-help');
			toggleHelp('.image2-help-toggle', '.image2-help');
			toggleHelp('.image3-help-toggle', '.image3-help');
			toggleHelp('.youtube-help-toggle', '.youtube-help');
			toggleHelp('.audio-help-toggle', '.audio-help');
			toggleHelp('.timeline-help-toggle', '.timeline-help');

			$('.submit').on('click', function (event) {
				event.preventDefault();
				$(this).closest('form').submit();
			});

			/**
			* Banner formatting and functionality
			*
			* Banner
			**/
			$('.banner').each(function () {
				var $banner = $(this);
				
				// Banner box in video banner
				if ($banner.hasClass('video')) {
					$banner.find('.media .box').each(function () {
						var $t = $(this);
						//$t.css('margin-left', -Math.round($t.outerWidth() / 2));
					});
				}

				// Video in cut banner

				if ($banner.hasClass('cut')) {
					(function () {
						var $wrapper = $banner.find('.wrapper'),
						$bannerBox = $banner.add($wrapper),
						$btn = $banner.find('.die'),
						$vid = $banner.find('.media.vid').addClass('hidden'),
						$box = $banner.find('.box');
						$img = $btn.parent().next("img");

						// Set up button
						var btnText = $btn.text(), btnAltText = $btn.data('alttext');
						$win.load(function () {

							if($banner.find(".media").data("src").length > 0)	{
								$btn.bind(touchClick, function (e) {
									
									e.preventDefault();
									if ($btn.hasClass('active')) {
										$btn.removeClass('active').text(btnText);
										$img.removeClass("ipad");
										hideVideo();
										try { $vid.data('player').pause(); } catch (err) { log('Unable to catch media player (' + err.toString() + ')'); }
									} else {
										$btn.addClass('active').text(btnAltText);
										$img.addClass("ipad");
										showVideo();
										try { $vid.data('player').pause(); } catch (err) { log('Unable to catch media player (' + err.toString() + ')'); }
									}
								});
							} else {
								
								$btn.bind(touchClick, function (e) {
									e.preventDefault();
									if ($btn.hasClass('active')) {
										$btn.removeClass('active').text(btnText);
										$img.removeClass("ipad");
										hideinfo();
									} else {
										$btn.addClass('active').text(btnAltText);
										$img.addClass("ipad");
										showinfo();
									}
								});

							}
						});

						// Set up hide/show functions
						var vidHeight = $vid.outerHeight(),
 						orgHeight = $bannerBox.height(),
						isSmaller = vidHeight > orgHeight;

						getDimensions = function () {
							vidHeight = $vid.outerHeight();
							isSmaller = vidHeight > orgHeight;
						};
						getNovideoDimensions = function () {
							vidHeight = 490;
							isSmaller = vidHeight > orgHeight;
							i = $(".subject.item.entry").index();
						};
						showinfo = function() {
							getNovideoDimensions();
							$bannerBox.animate(
								{ 'height': isSmaller ? vidHeight : orgHeight },
								isSmaller ? 250 : 0,
								function () {
									$box.stop().fadeOut(250);
									$vid.removeClass('hidden').fadeIn(250);
									$banner.find(".fallback").removeClass("fallback").removeAttr("href");
									$banner.find(".info.enhanced div:eq("+i+")").fadeIn();
									$banner.find(".info-nav li:eq("+i+")").addClass("active");
									$banner.find(".info-nav").fadeIn();
									$banner.find(".info").fadeIn();

								}
							);

						};
						hideinfo = function() {
							getNovideoDimensions();
							$vid.stop().fadeOut(250, function () {
								$vid.addClass('hidden');
								$banner.find(".info-nav li").removeClass("active");
								$banner.find(".info .item").hide();
								$bannerBox.stop().animate({ 'height': orgHeight }, isSmaller ? 250 : 0);
								$box.stop().fadeIn(250);
							});

						};
						showVideo = function () {
							getDimensions();
							$bannerBox.stop().animate(
								{ 'height': isSmaller ? vidHeight : orgHeight },
								isSmaller ? 250 : 0,
								function () {
									$box.stop().fadeOut(250);
									$vid.removeClass('hidden').fadeIn(250);
								}
							);
						};
						hideVideo = function () {
							getDimensions();
							$vid.stop().fadeOut(250, function () {
								$vid.addClass('hidden');
								$bannerBox.stop().animate({ 'height': orgHeight }, isSmaller ? 250 : 0);
								$box.stop().fadeIn(250);
							});
						};
					})();
				}
			});


			// Media gallery
			$('.media-gallery').each(function () {
				var $container = $(this),
					$mediaAdditional = $container.find('.additional-subjects'),
					$showAll = $container.find('.show-all');
				$mediaAdditional.toggle(0);

				$showAll.bind(touchClick, function (e) {
					e.preventDefault();
					var alttext = $showAll.data('alttext');
					$showAll.data('alttext', $showAll.text());
					$showAll.text(alttext);
					$mediaAdditional.slideToggle(300);
				});

				var $mediaLists = $container.find('.media-list');
				$mediaLists.each(function () {
					var $mediaItems = $(this).find('li');
					$mediaItems.each(function (i, e) {
						var $li = $(this),
							$item = $li.find('.item'),
							type = $item.hasClass('media-video') ? 'video' : 'image',
							$link = $item.find('figure a, .figure a'),
							$download = $item.find('.download'),
							title = $item.find('h3').text(),
							href = $link.attr('href'),
							openLb;

						// Attach link event
						$link.on(touchClick, function (e) {
							e.preventDefault();

							// Build content
							var $content = $('<div class="media-content" />'),
								$footer = $('<div class="footer" />').appendTo($content),
								$dl = $download.clone().appendTo($footer),
								$close = $('<a href="#close" class="close">Close</a>').appendTo($footer).hide(),
								$paging = $('<div class="paging"><p>' + (i + 1) + ' af ' + $mediaItems.length + '</p></div>').appendTo($footer),
								$prev = $('<a href="#prev" class="button prev">&larr;</a>').appendTo($paging),
								$next = $('<a href="#next" class="button next">&rarr;</a>').appendTo($paging),
								$media = false;

							// Build media
							switch (type) {
								case 'video':
									var vidSrc = href,
										vidWidth = $item.data('videowidth'),
										vidHeight = $item.data('videoheight'),
										vidImg = $link.find('img').attr('src');
									vidWidth = vidWidth ? vidWidth : 800;
									vidHeight = vidHeight ? vidHeight : 450;
									$media = $('<div class="media vid" data-src="' + vidSrc + '" data-width="' + vidWidth + '" data-height="' + vidHeight + '" style="width:' + vidWidth + 'px;height:' + vidHeight + 'px;"><a href="/alinea/samfundsfag/html/media/video.mp4" class="fallback" target="_blank"><img src="' + vidImg + '" alt="Video" style="width:' + vidWidth + 'px;height:' + vidHeight + 'px;" /></a></div>').prependTo($content);
									break;
								case 'image':
									$media = $('<img class="image" src="' + href + '" alt="' + title + '" />').prependTo($content);
									$media.load(function () { $media.data('load', true); });
									break;
								default:
									$media = $();
									break;
							}

							// Set up open function
							openLb = function () {
								$.fancybox({
									content: $content,
									padding: 0,
									enableEscapeButton: false,
									ajax: {
										cache: false
									},
									onStart: function () {
										//$('#fancybox-close').hide();
									},
									onComplete: function () {
										// Reinit standard content
										var $fbc = formatContent('#fancybox-content');

										// Reinit rich media
										initMedia($fbc.find('.media').not('.loaded'));

										// Reinit back button
										$fbc.find('.button.back').bind(touchClick, function (e) {
											e.preventDefault();
											$.fancybox.close();
										});

										// Reinit print button
										$fbc.find('.print').bind(touchClick, function (e) {
											e.preventDefault();
											var pw = window.open(href + '#print', "printWindow");
											$(pw).load(function () {
											    pw.print();
												pw.close();
											});
										});

										// Set up interactions
										$close.bind(touchClick, function (e) {
											e.preventDefault();
											$.fancybox.close();
										});
										$prev.bind(touchClick, function (e) {
											e.preventDefault();
											var $next = $li.prev();
											$next = $next[0] ? $next : $mediaItems.last();
											$next.find('figure a').trigger(touchClick);
										});
										$next.bind(touchClick, function (e) {
											e.preventDefault();
											var $next = $li.next();
											$next = $next[0] ? $next : $mediaItems.first();
											$next.find('figure a').trigger(touchClick);
										});
									}
								});
							};


							// Show fancybox
							if (type == "image" && !$media.data('load')) {
								$.imgpreload([href], {
									all: function () {
										openLb();
									}
								});
							} else {
								openLb();
							}
						});
					});
				});
			});

			var tinyMCEPlugins = "link autolink";
			if (tinymce.Env.iOS === false) {
				tinyMCEPlugins += " paste";
			}

			// TinyMCE 4
			tinymce.init({
				selector: ".rte-box",
				plugins: tinyMCEPlugins,
				toolbar: "styleselect | undo redo | bold italic underline | bullist | link unlink",
				statusbar:false,
				menubar: false,
				visual: false,
				language: 'da',
				style_formats: [
					{ title: 'Overskrift 1', block: 'h2' },
					{ title: 'Overskrift 2', block: 'h3' },
					{ title: 'Brødtekst', block: 'p' }
				],
				target_list: {}
			});

			function submitCreateForm(form) {
				tinymce.triggerSave();
				$('#formaction').val('save user article');
				form.submit();
			}
			function swapValues(a, b) {
				var x = a.val();

				a.val(b.val());
				b.val(x);
			}

			/**
			* Content formatting and functionality - Most of which is wrapped in a function to allow reinit with lightbox
			*
			* Content
			**/
			// Reinit available function
			var formatContent = function (fc) {
				var $fc = $(fc);

				// Bridge nth-child selector
				$('.method-list>li:nth-child(2n+1)').addClass('nth-child-2n-plus-1');
				$('.subject > ul > li:nth-child(odd)').addClass('nth-child-odd');
				$('.subject-list li:nth-child(2n+1)').addClass('nth-child-2n-plus-1');
				$('nav.dropdown li:nth-child(3n+1)').addClass('nth-child-3n-plus-1');
				$('.media-gallery ul li:nth-child(4n+1)').addClass('nth-child-4n-plus-1');

				function getYoutubeId(youTubeUrl) {
					var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
					var match = youTubeUrl.match(regExp);
					if (match && match[2].length === 11) {
						return match[2];
					}
				}

				// Alerts
				$fc.find('a.alert').click(function (e) {
					var text = $(this).data('alert');
					text = text.length > 0 ? text : 'Er du sikker?';
					if (!confirm(text)) e.preventDefault();
				});

				// Iframes in articles
				$fc.find('.entry iframe').each(function () {
					var $iframe, $wrap, youtubeId, $youtubeFallback;
					$iframe = $(this);
					$wrap = $('<div class="iframe"></div>');

					$iframe.show();

					if ($iframe.data('src').indexOf('youtube') > -1) {
						$iframe.show().attr('src', $iframe.data('src'));

						$wrap.addClass('youtube');
						youtubeId = getYoutubeId($iframe.data('src'));

						$youtubeFallback = $('<div class="print-fallback"><img src="http://img.youtube.com/vi/' + youtubeId + '/0.jpg" class="vjs-poster" alt="" /></div>');
					}
					$iframe.wrap($wrap).after($youtubeFallback);

				});

				// Slideshow
				$fc.find('.slideshow').each(function () {
					var $container = $(this).addClass('enhanced'),
						$list = $container.find('ul'),
						$items = $list.children(),
						curSlide = false,
						getSlide = function (i) {
							if (i < 0 || i >= $items.length) return false;
							$items.filter('.active').removeClass('active');
							curSlide = i;
							$pageText.text((i + 1) + ' af ' + $items.length);
							return $($items[i]).addClass('active');
						},
						getNextSlide = function () {
							var nextSlide = curSlide + 1 >= $items.length ? 0 : curSlide + 1;
							getSlide(nextSlide);
						},
						getPrevSlide = function () {
							var nextSlide = curSlide - 1 < 0 ? $items.length - 1 : curSlide - 1;
							getSlide(nextSlide);
						},
						$paging = $('<div class="paging" />'),
						$pageText = $('<p />').appendTo($paging);

					// Add next/prev
					$('<a href="#prev" class="button prev">&larr;</a>').bind(touchClick, function (e) {
						e.preventDefault();
						getPrevSlide();
					}).appendTo($paging);
					$('<a href="#next" class="button next">&rarr;</a>').bind(touchClick, function (e) {
						e.preventDefault();
						getNextSlide();
					}).appendTo($paging);

					// Insert paging
					$paging.appendTo($container);

					// Hide / show entries
					$items.each(function () {
						var $item = $(this),
							$entry = $item.find('.entry');

						if ($entry[0]) {
							$entry.append('<span class="arrow" />');
							$entry.find('a').bind(touchClick, function (e) {
								e.stopPropagation();
							});
							$entry.bind(touchClick, function (e) {
								e.stopPropagation();
								$entry.toggleClass('closed');
							});
						}
					});

					// Set first active
					getSlide(0);
				});

				// Timeline canvas
				$fc.find('.tl-canvas').each(function () {
					var $canvas = $(this).addClass('enhanced'),
						$list = $canvas.find('.tl-items>ol'),
						$items = $list.children(),
						$heading = $canvas.children('.tl-heading,.tl-desc'),
						toggleHeading = function () {
							if (!$items.filter('.active')[0]) {
								$heading.stop().fadeIn(250);
							} else {
								$heading.stop().fadeOut(250);
							}
						};

					// Insert labels, close and listen for click event
					$items.each(function (i, el) {
						var $item = $(this),
							$article = $item.find('.tl-article');



						//S Set up position
						$item.css({ 'left': $item.data('position') });

						// Insert labels
						var labelText = $item.data('label');
						if (labelText) $('<span class="tl-jsline"></span><span class="tl-jslabel">' + labelText + '</span>').appendTo($item);

						// Insert close
						$('<a href="#close" class="tl-jsclose">Luk</a>').prependTo($article.find('.entry')).bind(touchClick, function (e) {
							e.preventDefault();
							$items.removeClass('active')
							toggleHeading();
						});

						// Insert arrow
						$('<span class="tl-arrow"></span>').appendTo($item);

						// Stop propagation
						$item.find('.tl-article').bind(touchClick, function (e) {
							e.stopPropagation();
						});

						// Set alignment before first show
						var cl = $canvas.offset().left,
							il = $article.offset().left;
							if (il - cl > $article.width() * 1.2) {
								$item.addClass('right');
							}



						// Bind event
						$item.bind(touchClick, function (e) {
							e.preventDefault();
							$item.toggleClass('active').siblings().removeClass('active');
							toggleHeading();

							// Set alignment
							$item.removeClass('right');
							var cl = $canvas.offset().left,
								il = $article.offset().left;
							if (il - cl > $article.width() * 1.2) {
								$item.addClass('right');
							}
						});
					});

					// Set offset on labels
					var $labels = $items.find('.tl-jslabel');
					$items.reverse().each(function () {
						var $item = $(this),
							$label = $item.find('.tl-jslabel'),
							hit = false;
						if ($label[0]) {
							$labels.each(function (i, e) {
								if ($label[0] == $labels[i]) return;
								if ($label.hitTestObject($(this))) hit = true;
							});
							if (hit) $item.addClass('offset');
						} else {
							return false;
						}
					}).reverse();
				});

				// Form 
				$fc.find('.form').each(function () {
					var $form = $(this);

					// Dropdowns
					$form.find('select').simpleFormUI({ cssClass: 'custom-dropdown' });

					// Preview
					$form.find('a.preview').each(function () {
						var $link = $(this),
							$target = $($link.attr('href')),
							$container = $target.parent('.form-preview');

						if ($target[0] && $container[0]) {
							var $html = $('html'),
								pos = $link.position(),
								height = $link.outerHeight();

							$link.click(function (e) {
								e.preventDefault();
								e.stopPropagation();

								$container.css({ 'top': pos.top + height, 'left': pos.left });

								$target.css({ 'display': 'block' }).siblings().css({ 'display': 'none' });

								$html.bind('click', function () {
									$target.css({ 'display': 'none' });
								});
							});
						}
					});
				});

				// Goal table
				$fc.find('.entry table.goals').each(function () {
					var $table = $(this),
					$inputs = $table.find('input[type="checkbox"], input[type="radio"]');

					$inputs.each(function () {
						var $input = $(this);
						if (this.checked) {
							$input.parent().addClass('checked');
						} else {
							$input.parent().addClass('unchecked');
						}
					});
				});

				// Form checkboxes
				$fc.find('.form .checkbox, .form .radio').each(function () {
					var $container = $(this),
						$inputs = $container.find('input'),
						$iContainers = $inputs.parent();

					$inputs.change(function () {
						$iContainers.removeClass('checked');
						$inputs.each(function () {
							if (this.checked) {
								$(this).parent().addClass('checked');
							}
						});
					}).trigger('change');
				});

				// Form checkboxes
				$fc.find('.authorlist .checkbox').each(function () {
					var $container = $(this),
						$input = $container.find('input'),
						$label = $container.find('label');

					$input.on('change', function () {
						$label.removeClass('checked');
						if ($input.get(0).checked) {
							$label.addClass('checked');
						}
					}).trigger('change');;
				});

				$fc.find('.subjectlist .checkbox').each(function () {
					var $container = $(this),
						$input = $container.find('input'),
						$label = $container.find('label');

					$input.on('change', function () {
						$label.removeClass('checked');
						if ($input.get(0).checked) {
							$label.addClass('checked');
						}
					}).trigger('change');;
				});

				// Header typographic alignment
				var headerAlignment = function () {
					$fc.find('.entry.article').find('h2').each(function () {
						var $header = $(this);
						$header.css('margin-bottom', -$header.height());
					});
				};
				$win.smartresize(headerAlignment).load(headerAlignment);
				headerAlignment();

				// Abbreviation speech bubble
				$fc.find('.entry abbr').each(function () {
					var $abbr = $(this).addClass('abbr hidden'),
							$bubble = $('<span class="abbr-bubble"><span class="top"></span><span class="inner">' + $abbr.attr('title') + '</span><span class="bottom"></span></span>').appendTo($abbr),
							hideBubble = function () { return !$abbr.hasClass('hidden') ? $abbr.addClass('hidden') : false; },
							showBubble = function () { return $abbr.hasClass('hidden') ? $abbr.removeClass('hidden') : false; },
							toggleBubble = function () { return $abbr.toggleClass('hidden'); };

					$abbr.removeAttr('title');
					if (Modernizr.touch) {
						$abbr.bind(touchClick, function (e) {
							e.stopPropagation();
							$fc.find('.entry abbr').addClass('hidden');
							toggleBubble();
						}).closest('.entry').bind(touchClick, function () {
							hideBubble();
						});
					} else {
						$abbr.hover(function () {
							showBubble();
						}, function () {
							hideBubble();
						});
					}
				});

				// Assignments auto-fold
				$fc.find('.content-box.assignment').each(function () {
					var $ass = $(this).removeClass('hidden').height('auto'),
						init = function () {

							if ($ass.outerHeight() - 200 > 50) {
								$ass.addClass('hidden');
								var $shade = $ass.find('.shade')[0] ? $ass.find('.shade') : $('<span class="shade"></span>').appendTo($ass),
									$link = $ass.find('.show')[0] ? $ass.find('.show') : $('<a href="#show" class="show">Vis hele opgaven &darr;</a>').appendTo($ass),
									origHeight = $ass.height(),
									closedHeight = $ass.find('h4').outerHeight() + 110,
									boxOpen = false;

								$ass.height(closedHeight);

								$link.bind(touchClick, function (e) {
									e.preventDefault();
									boxOpen = !boxOpen;
									if (boxOpen) {
										$ass.animate({ height: origHeight }, 500, function () {
											$ass.height('auto');
										});
										$link.text("Skjul opgave \u2193") /* ↑ */
										if ($.browser.msie && parseInt($.browser.version) < 9) {
											$shade.css("display", "none");
											//JPL: Changed by request 13/2-2013
											//$link.add($shade).remove();
										} else {
											$shade.fadeOut(200);
											//JPL: Changed by request 13/2-2013
											//$link.add($shade).fadeOut(500, function () {
											//	$(this).remove();
											//});
										}
									}
									else {
										$ass.animate({ height: closedHeight }, 500);
										$link.text("Vis hele opgaven ↓");
										if ($.browser.msie && parseInt($.browser.version) < 9) {
											$shade.css("display", "block");
										}
										else {
											$shade.fadeIn(200);
										}
									}
								});
							}
						};
					init();
				});

				// Poll - radio buttons
				$fc.find('.content-box.opinion .options').each(function ()
				{
					if ($(this).closest('.content-box.opinion').hasClass('form-example')) return false;

					var $container = $(this),
						$inputs = $container.find('input'),
						$labels = $container.find('label'),
						$btn = $container.siblings('.button[data-service]');

					if ($inputs.length > 0) {
						$inputs.on('change', function () {
							var $input = $(this),
								$label = $input.next('label'); // Requires label be first element after the input field in the DOM

							if ($input.is(':checked')) {
								$labels.removeClass('checked');
								$label.addClass('checked');
							}
						});

						// Bind submit
						$btn.bind(touchClick, function (e)
						{
							e.preventDefault();
							var $t = $(this),
								service = $t.data('service'),
								hasAnswer = false,
								$feedback = $container.parent().find('.feedback')[0] ? $container.parent().find('.feedback') : $('<p class="feedback"></p>').insertAfter($container),
								feedback = function (str, cls) {
									$feedback.hide().text(str).removeClass('success error').addClass(cls).fadeIn(250);
								};
							try {
								var data = eval('(' + $t.data('parameters') + ')');
							} catch (err) {
								var data = new Object();
							}
							data = typeof data !== 'object' ? new Object() : data;
							$feedback.hide();
							if ($t.hasClass('disabled')) {
								feedback('Du kan kun svare én gang på hver afstemning', 'error');
								return false;
							}
							$container.addClass('loading');
							if (service) {
								$inputs.each(function () {
									if (this.checked) {
										hasAnswer = true;
										data[$(this).attr('name')] = $(this).val();
									}
								});
								if (hasAnswer) {
									_v.services.request(service, data, function (resp) {
										$container.removeClass('loading');
										if (resp.Outcome.Validated) {
											var items = resp.Response.Responses;
											$.each(items, function (i, v) {
												var $item = $inputs.attr('disabled', 'disabled').filter('#' + v.InputId.toString()).parent(),
														$bar = $('<span class="bar" style="width:0%"></span>').appendTo($item).attr('title', v.Percent + '%').animate({ width: '50%' }, 500),
														$val = $('<span class="val" style="width:0%"></span>').appendTo($bar).animate({ width: v.Percent + '%' }, 500);
												$btn.remove();
											});
										} else if (resp.Outcome.Error) {
											feedback(resp.Outcome.Error);
										} else {
											_v.err(this, 'Unexpected error', 'Unexpected format returned.');
										}
									});
								} else {
									$container.removeClass('loading');
									feedback('Du skal først vælge dit svar', 'error');
								}
							} else {
								$container.removeClass('loading');
								_v.err(this, 'Unexpected error', 'No service string found, even though attribute is present');
							}
						});
					}
				});

				// Navigational dropdown
				$fc.find('nav.dropdown').each(function () {
					var $container = $(this),
						$link = $container.children('a'),
						$list = $container.children('ul'),
						toggleList = function () {
							if ($list.hasClass('active')) {
								hideList();
							} else {
								showList();
							}
						},
						showList = function () {
							$list.hide().addClass('active').slideDown(250);
						},
						hideList = function () {
							$list.slideUp(250, function () {
								$list.removeClass('active').show()
							});
						},
						lazyTimer = 0;

					// Bind events depending on touch device
					if (Modernizr.touch) {
						$link.bind('click', function (e) {
							e.preventDefault();
							toggleList();
						});
					} else {
						$container.hover(function () {
							clearTimeout(lazyTimer);
							if (!$list.hasClass('active')) showList();
						}, function () {
							clearTimeout(lazyTimer);
							lazyTimer = setTimeout(function () {
								hideList();
							}, 750);
						});
					}

					// Align items 
					var $collection = $(),
							maxHeight = 0,
							s = 0, t = 2;
					$list.addClass('active').find('a').each(function (i, e) {
						if (s > t) {
							s = 0;
							$collection.height(maxHeight);
							maxHeight = 0;
							$collection = $();
						}
						s++;
						var $t = $(this),
								th = $t.height();
						$collection = $collection.add($t);
						maxHeight = maxHeight < th ? th : maxHeight;
					});
					$list.removeClass('active');
				});

				// Subject list 
				$fc.find('.subject-list').each(function () {
					var $container = $(this),
						$areaLinks = $container.find('li nav a'),
						$shortTexts = $container.find('.short-texts').children();

					// Bind buttons
					$areaLinks.bind(touchClick, function (e) {
						var $link = $(this),
							$item = $link.closest('li'),
							$itemLink = $item.children('a'),
							itemPos = $item.position(),
							itemDim = { w: $itemLink.width(), h: $item.height(), ml: parseInt($item.css('margin-left')) + parseInt($item.css('padding-left')) },
							$shortText = $shortTexts.filter('#' + $link.data('shorttext'));

						$areaLinks.removeClass("active");
						$link.addClass("active");

						if ($shortText[0]) {
							e.preventDefault();
							$shortTexts.removeClass('active');
							$shortText.addClass('active').css({ 'top': itemPos.top, 'left': Math.round(itemPos.left + itemDim.ml), 'width': itemDim.w, 'height': itemDim.h - $link.height() - 1 });
						}
					});

					// Add close link
					$('<a href="#close" class="close">Luk</a>').appendTo($shortTexts).bind(touchClick, function (e) {
						e.preventDefault();
						$areaLinks.removeClass("active");
						$shortTexts.removeClass('active');
					});
				});

				// Content box news
				$fc.find('.content-box.news, .content-box.classforum').each(function () {
					var $container = $(this),
							$items = $container.find('article'),
							itemHeight;

					// Show first
					$items.first().addClass('show');

					// Paging
					$items.each(function (i, e) {
						var $item = $(this),
								$paging = $item.find('.paging'),
								$pagingTitle = $paging.find('p'),
								$nextBtn = $paging.find('.next'),
								$prevBtn = $paging.find(' .prev');

						// Correct text
						$pagingTitle.text((i + 1) + ' af ' + $items.length);

						// Bind click events
						$nextBtn.bind(touchClick, function (e) {
							e.preventDefault();
							var $get = $item.next();
							$get = $get[0] ? $get : $items.first();
							$get.addClass('show');

							//JPL: Changed by bug request 28/2-2013
							$get.siblings('.show').removeClass('show').addClass('hide').hide();
							$get.siblings('.hide').removeClass('hide').show();

						});
						$prevBtn.bind(touchClick, function (e) {
							e.preventDefault();
							var $get = $item.prev();
							$get = $get[0] ? $get : $items.last();
							$get.addClass('show');

							//JPL: Changed by bug request 28/2-2013
							$get.siblings('.show').removeClass('show').addClass('hide').hide();
							$get.siblings('.hide').removeClass('hide').show();
						});
						var resizeItems = function () {
							itemHeight = 0;
							$items.each(function () {
								var $item = $(this),
								height = parseInt($item.outerHeight());

								$item.removeAttr('style');

								itemHeight = itemHeight < height ? height : itemHeight;
							}).each(function () {
								var $item = $(this),
								$entry = $item.find('.entry').first(),
								paddingBottom,
								margin = itemHeight - parseInt($item.outerHeight());

								if (margin > 0) {
									//JPL: Removed by bug request 26/2-2013
									//paddingBottom = parseInt($entry.css('padding-bottom'));
									//$entry.css('padding-bottom', paddingBottom + margin);
								}
							});
						};
						if ($items.length === 1) {
							$items.find('.paging').hide();
						} else {
							resizeItems();
							$(window).smartresize(resizeItems).load(resizeItems);

						}
					});
				});

				// Lettering
				//$('.entry.article h2').lettering();

				// Polyfills
				if (!Modernizr.before) {
					$('.entry.article h2').prepend('<span class="before"></span>');
				}

				return $fc;
			};
			formatContent(document);

			// Init media and audio
			initMedia($('.media').addClass('loaded'));

			/**
			 * Sort order module
			 *
			 * @author echristensen
			 */
			$('.article-create').on('click', '.button.up', function (event) {
				event.preventDefault();

				var $trigger = $(this),
					$thisItem = $trigger.closest('.sort-container'),
					$thisItemSortValue = $thisItem.find('.sort-value'),
					$previousItemSortValue =  $thisItem.prev().find('.sort-value'),
					form = $trigger.closest('form');

				// Make sure this is not the top item
				if ($thisItem.prev().hasClass('sort-container')) {
					// Switch sort values
					swapValues($thisItemSortValue, $previousItemSortValue);
					
					submitCreateForm(form);
				}
			});

			/**
			 * Sort order module down
			 *
			 * @author echristensen
			 */
			$('.article-create').on('click', '.button.down', function (event) {
				event.preventDefault();

				var $trigger = $(this),
					$thisItem = $trigger.closest('.sort-container'),
					$thisItemSortValue = $thisItem.find('.sort-value'),
					$nextItemSortValue = $thisItem.next().find('.sort-value'),
					form = $trigger.closest('form');

				// Make sure this is not the top item
				if ($thisItem.next().hasClass('sort-container')) {
					// Switch sort values
					swapValues($thisItemSortValue, $nextItemSortValue);

					submitCreateForm(form);
				}
			});

			// Print
			if (window.location.hash === '#print') {
				window.print();
			}

			$('.print').bind(touchClick, function (e) {
				var height, width, maxWidth, resized;

				e.preventDefault();

				maxWidth = 960;
				resized = false;
				height = $win.height();
				width = $win.width();

				if ($.browser.msie && width > maxWidth) {
					resized = true;
					window.resizeTo(maxWidth, height);
					setTimeout(function () {
						window.print();
					}, 250);
				} else {
					window.print();

					// Chrome hack - chrome won't print if there's videos on the page ? Niiiice..
					if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
						location.reload(true);
					}
				}
			});

			// Back buttons
			$('.button.back').bind(touchClick, function (e) {
				e.preventDefault();
				history.back();
			});

			// Content iframes
			$('.lb-content').each(function () {
				var $link = $(this),
				href = $link.attr('href');

				$link.bind(touchClick, function (e) {
					e.preventDefault();

					var $content = $('<div>');
					window.c = $content;
					$link.addClass('loading');

					var $youtubes = $('.youtube');
					$youtubes.addClass('is-hidden');

					$content.load(href + ' .content', function (rt, ts, xhr) {
						$link.removeClass('loading');
						if (ts == 'success' || ts == 'notmodified') {
							// Open fancybox
							$.fancybox({
								content: $content,
								enableEscapeButton: false,
								onComplete: function () {
									// Reinit standard content
									var $fbc = formatContent('#fancybox-content');

									// Reinit rich media
									initMedia($fbc.find('.media').not('.loaded'));

									// Reinit back button
									$fbc.find('.button.back').bind(touchClick, function (e) {
										e.preventDefault();
										$.fancybox.close();
									});

									// Reinit print button
									$fbc.find('.print').bind(touchClick, function (e) {
										e.preventDefault();
										var pw = window.open(href + '#print', "printWindow");
										$(pw).load(function () {
											// Url #print is trigged before load.
											pw.close();
										});
									});
								},
								onClosed: function () {
									$youtubes.removeClass('is-hidden');
								}
							});

						} else {
							log('Error getting href in attempt to open content lightbox (' + ts + ').');
						}
					});
				});
			});

			// Images in iframes
			$('.lb-image').fancybox({
				'padding': 0,
				enableEscapeButton: false
			});

			$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: true,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false
	});

			// Broken links highlighting
			var getQuerystring = function getQuerystring(name) {
				name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				var regexS = "[\\?&]" + name + "=([^&#]*)";
				var regex = new RegExp(regexS);
				var results = regex.exec(window.location.search);
				if (results == null)
					return "";
				else
					return decodeURIComponent(results[1].replace(/\+/g, " "));
			}

			if (getQuerystring('brokenlink').length > 0) {
				var brokenLink = getQuerystring('brokenlink');
				$('a[href="' + brokenLink + '"]').css('border', '5px solid #f00');
				$('img[src="' + brokenLink + '"]').css('border', '5px solid #f00');
			}


			// IE7 hacks
			if ($.browser.msie && parseInt($.browser.version) < 8) {
				// Danger: Very, very ugly hack ahead
				$win.load(function () {
					var $flickr = $('.content-box, .paging .button, .options');
					setTimeout(function () {
						$flickr.css({ 'display': 'none' });
						setTimeout(function () {
							$flickr.css({ 'display': 'block' });
						}, 20);
					}, 20);
				});
			}


			// Set min/max width to disable textarea resizing
			$('textarea').each(function () {
				var $this = $(this),
					width = $this.width() + parseInt($this.css('padding-left').replace('px', '')) + parseInt($this.css('padding-right').replace('px', ''));
				
				$this.css({
					'max-width': width,
					'min-width': width
				});
			});


			// Check if browser support HTML5 placeholder attr.
			var supportsPlaceholder = (function () {
				var i = document.createElement('input');

				return 'placeholder' in i;
			}());
			
			
			if (!supportsPlaceholder) {

				var $inputs = $("input[type='text'], textarea");

				$inputs.each(function () {


					var $input = $(this),
						placeholderText = $input.attr('placeholder'),
						$label = $('<label></label>'),
						$wrapper = $('<div class="placeholder-wrapper"></div>');

					if ($input.filter('textarea').length > 0) {
						$wrapper.addClass('indent');
					}

					if (placeholderText !== "") {
						// We're building this in chrome, so let's temp clear the placeholder value.
						$input.removeAttr('placeholder');

						$input.wrap($wrapper);

						// Prepend label to wrapper.
						$label.prependTo($input.parent());

						// Set placeholder initial state
						!($input.val() || document.activeElement == this) ? $label.show() : $label.hide();

						// Add event handlers
						$input.on('keyup', function () {
							// Show placeholder
							!$input.val() ? $label.show() : $label.hide();
						});

						$label.on('click', function () {
							// Give focus to input
							$input.focus();
						});

						$label.css("width", $input.width());

						// Insert placeholder.
						$label.addClass('placeholder');
						$label.text(placeholderText);
						$label.insertBefore($input);
					}
				});
			}
		});
	};
	_v.init();
})();



//SLIDER

Slider = {
		init : function(){
			var $scrollable = $(".scrollable");
				if ($scrollable.length>0) {
					$scrollable.each(function(){
						var $el = $(this);
			
						var $panels = $el.find(".panels");
						$panels.find(".panel:first").addClass("firstPanel");
						var hasPaging = $el.hasClass("has-paging");
						$panel = $el.find(".panel");
						
						if($panel.length == 1){
							$el.removeClass("has-prevnext");
							return false;
						}
			
							var conf = {
								speed:		500,
								autoplay:	false,
								paging:		hasPaging,
								interval:	5000,
								loop:		true,
								width:		null,
								height:		null,
								keyboard:	true
							};
			
							var meta = $el.metadata();
							conf = $.extend(conf, meta);
						
						if($el.hasClass("scrollable-vertical")){
							$el = $el.find(".panels-container");
							
							var api = $el.scrollable({items:'.panels', vertical: true, speed: conf.speed });
							
							if (conf.paging){
								api.navigator({ navi:".scrollable-vertical .nav", activeClass:'current', indexed:false});
							}
						}else {
							if (!conf.width){
								conf.width = $el.width();
							}
							if (!conf.height){
								conf.height = $panels.height();
							}
							
							$el.height(conf.height).width(conf.width);
							
							$panelPadding = $panel.width() - $panel.innerWidth();
							if($.browser.msie){
								$panelPadding = $panelPadding + 1; // IE8 Fix: 1px float panel roundup bug
							}
							$panel.width(conf.width + $panelPadding + 1);
							
							var api = $el.scrollable({items:'.panels', circular: conf.loop, speed: conf.speed, easing: conf.easing, keyboard:conf.keyboard });
							
							if (conf.paging){
								// page counter fix for ie7
								if($.browser.msie && parseInt($.browser.version, 10) == 7) {
									$el.find(".panel ").not(".cloned").each(function() {
										$el.find(".panel-count").append("<a href='#'>&nbsp;</a>");
									});
									$el.find(".panel-count a:first").addClass("current");
								}
								api.navigator({ navi:".panel-count", naviItem:'a', activeClass:'current', indexed:false});
							}
							
						}
						
						if(conf.autoplay){
							api.autoscroll({ autoplay:true, interval: conf.interval, api:true });
							
							var apiData = api.data("scrollable");
							apiData.getNaviButtons().click(function(){
									apiData.stop();
							});
						}
					});
				}
		}
	};

	$(window).load(Slider.init);

	/**
		* Light boxes - made with Fancybox and hits all links with class .lightbox. Can be modified with .lightbox-iframe, .lightbox-ajax or .lightbox-inline.
		* Width and/or height can be forced by using data-width or data-height on the link.
		* All Fancybox events can be triggered by attaching the relevant function to the link's jQuery .data().
		*
		* Light boxes
		*
		* @author ldeleuran
		**/
		$( document ).ready(function() {
			var init = function () {
				// Using delegation on body to get all lb-links, even those inserted after DOM ready (http://api.jquery.com/on/)
				$('body').on('click', 'a.lightbox', function (e) {
					e.preventDefault();

					// Set up vars
					var $link = $(this),
						href = $link.attr('href'),
						height = $link.data('height'),
						width = $link.data('width'),
						type = $link.hasClass('lightbox-iframe') ? 'iframe' : $link.hasClass('lightbox-ajax') ? 'ajax' : $link.hasClass('lightbox-inline') ? 'inline' : undefined,
						settings;

					// Append href
					href = (href.indexOf('?') > -1 ? href + '&' : href + '?') + 'lightbox=true';

					// Set up settings for Fancybox - for API info see http://fancybox.net/api
					settings = {
						href: href,
						type: type,
						// Overlay color settings
						overlayOpacity: 0.3,
						overlayColor: '#000',
						// Border (padding) and spacing settings
						padding: 4,
						margin: 20,
						scrolling: 'auto',
						// Dimensions - depend on defined width/height if present
						width: width || 610,
						height: height || 488,
						autoScale: width || height ? false : true,
						autoDimensions: width || height ? false : true,
						// Allow hooks to Fancybox events
						onStart: function () {
							var func = $link.data('onstart');
							if (typeof func === 'function') {
								func.apply(this, arguments);
							}
						},
						onCancel: function () {
							var func = $link.data('oncancel');
							if (typeof func === 'function') {
								func.apply(this, arguments);
							}
						},
						onComplete: function () {
							var func = $link.data('oncomplete');
							if (typeof func === 'function') {
								func.apply(this, arguments);
							}
						},
						onCleanup: function () {
							var func = $link.data('oncleanup');
							if (typeof func === 'function') {
								func.apply(this, arguments);
							}
						},
						onClosed: function () {
							var func = $link.data('onclosed');
							if (typeof func === 'function') {
								func.apply(this, arguments);
							}
							$('body').trigger('fancyboxOnClosed');
						}
					};

					// Load Fancybox
					$.fancybox(settings);
				});
			};

			// Load lib and init (prevent lightboxes from openening inside lightboxes)
			if ($.fancybox === undefined && !vertic.utils.getURLParameter('lightbox')) { // Check if fancybox is available - otherwise remote load it.
				vertic.utils.loadRemoteScript(basePath + 'js/jquery.fancybox-1.3.4.pack.js', function () {
					init();
				});
			} else {
				init();
			}
		});