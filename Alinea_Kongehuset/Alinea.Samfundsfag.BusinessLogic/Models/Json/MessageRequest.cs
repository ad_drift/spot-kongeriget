﻿using CoreM41.Web.Services.Generic.Serialization;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
    public class MessageRequest
    {
        public string Id
        {
            get;
            set;
        }

		public int MessageId
        {
            get;
            set;
        }

		public int UserId
		{
			get;
			set;
		}

		public int ClassId
		{
			get;
			set;
		}
    }
}
