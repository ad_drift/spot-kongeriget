﻿using CoreM41.Web.Services.Generic.Serialization;
using System.Collections.Generic;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
    public class PollsResponse
    {
        [Data(OmitListElement = true)]
        public List<Response> Responses
        {
            get;
            set;
        }

        public class Response
        {
            public string InputId
            {
                get;
                set;
            }
            public string Percent
            {
                get;
                set;
            }
        }
    }
}
