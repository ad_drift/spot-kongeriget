﻿using CoreM41.Web.Services.Generic.Serialization;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
    public class PollsRequest
    {
        public string Id
        {
            get;
            set;
        }
        public int AnswerId
        {
            get;
            set;
        }
    }
}
