﻿using CoreM41.Web.Services.Generic.Serialization;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
	/// <summary>
	/// Tags request class.
	/// </summary>
	public class TagsRequest
	{
		/// <summary>
		/// Optional search string to be used when returning the tags.
		/// </summary>
		[Data(Required = false)]
		public string SearchString
		{
			get;
			set;
		}

		/// <summary>
		/// Defines if a non-cached result should be returned.
		/// </summary>
		[Data(Required = false)]
		public bool SkipCache
		{
			get;
			set;
		}
	}
}
