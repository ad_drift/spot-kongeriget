﻿namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
	/// <summary>
	/// Tags response object. Contains the matching tags.
	/// </summary>
	public class TagsResponse
	{
		/// <summary>
		/// Gets or sets the tag keywords.
		/// </summary>
		public string[] Keywords
		{
			get;
			set;
		}
	}
}
