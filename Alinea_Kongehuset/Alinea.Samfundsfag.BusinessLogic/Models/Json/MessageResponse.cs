﻿using CoreM41.Web.Services.Generic.Serialization;
using System.Collections.Generic;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
	public class MessageResponse
    {
		public int UnreadMessages
        {
            get;
            set;
        }
    }
}
