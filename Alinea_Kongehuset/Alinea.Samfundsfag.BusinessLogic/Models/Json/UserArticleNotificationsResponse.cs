﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.Models.Json
{
	public class UserArticleNotificationsResponse
	{
		public int NewComments
		{
			get;
			set;
		}

		public int NewArticles
		{
			get;
			set;
		}
	}
}
