﻿using System;
using umbraco.dialogs;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	public class UserArticleTimelineItem
	{
		public int Id { get; set; }
		public int UserArticleId { get; set; }
		public string Label { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public int Position { get; set; }

		public bool HasImage { get; set; }
		public string ImageSrc { get; set; }

		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public int? ModifiedBy { get; set; }
		public DateTime? ModifiedDate { get; set; }
		public bool IsDeleted { get; set; }
	}
}