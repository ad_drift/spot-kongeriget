﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	public class Subject
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
