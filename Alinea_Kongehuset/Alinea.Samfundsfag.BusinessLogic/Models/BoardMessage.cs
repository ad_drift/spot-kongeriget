﻿using System;
using System.Collections.Generic;
using Alinea.Samfundsfag.BusinessLogic.Common;
using umbraco.cms.businesslogic.media;
using CoreM41.Diagnostics.Logging;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	public class BoardMessage
	{
		public BoardMessage(DataAccess.BoardMessage boardMessage, List<DataAccess.Class> classes, bool textAsHtml, bool isRead)
		{
            BoardMessageId = boardMessage.BoardMessageId;
			Status = boardMessage.IsPublished ? Constants.States.Published : Constants.States.WorkInProgress;
			TeacherName = boardMessage.User.Name;
			Preview = boardMessage.Text.Max(Constants.PreviewLength);
			DownloadAvailable = false;
			IsRead = isRead;

			if (!IsRead)
			{
				CssClass = "new";
			}

			try
			{
				Date = boardMessage.LastModified.ToString("yyyy-MM-dd HH:mm");
				DateMicroFormat = boardMessage.CreationDate.ToString("dd-MM-yyyy HH:mm");

                string classNames = string.Empty;
                classes.ForEach(_class => classNames += _class.Name + ", ");
                classNames = classNames.TrimEnd(' ', ',');
                ClassNames = classNames;
			}
			catch (Exception exception)
			{
                Logger.LogError(exception);

                Date = string.Empty;
                DateMicroFormat = string.Empty;
                ClassNames = string.Empty;
			}

			if (textAsHtml)
			{
                Text = string.Format("<p>{0}</p>", boardMessage.Text.ConvertNewLinesToHtmlBreakTags());
			}
			else
			{
				Text = boardMessage.Text;
			}

			if (boardMessage.MediaFileId > 0 && boardMessage.MediaFileId.HasValue)
			{
				Media media = null;
				try
				{
					media = new Media(boardMessage.MediaFileId.Value, true);
				}
                catch (Exception exception)
                {
                    Logger.LogError(exception);
                }

				if (media != null)
				{
					if (media.getProperty("umbracoFile") != null)
					{
						DownloadLink = "/download.ashx?file=" + media.getProperty("umbracoFile").Value.ToString();
						//Minor hack - TODO FIX - Media.Text not working(?)
                        DownloadName = DownloadLink.Split('/')[4];
						DownloadAvailable = true;
					}
				}
			}
		}

		public int BoardMessageId
		{
			get;
			set;
		}
		public string Preview
		{
			get;
			set;
		}
		public string Text
		{
			get;
			set;
		}
		public string Date
		{
			get;
			set;
		}
		public string DateMicroFormat
		{
			get;
			set;
		}
		public string TeacherName
		{
			get;
			set;
		}
		public string ClassNames
		{
			get;
			set;
		}
		public string Status
		{
			get;
			set;
		}
		public string DownloadName
		{
			get;
			set;
		}
		public string DownloadLink
		{
			get;
			set;
		}
		public bool DownloadAvailable
		{
			get;
			set;
		}
		public bool IsRead
		{
			get;
			set;
		}
		public string CssClass
		{
			get;
			set;
		}
	}
}
