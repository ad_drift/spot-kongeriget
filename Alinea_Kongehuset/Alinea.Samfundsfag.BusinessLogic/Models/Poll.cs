﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.NodeFactory;
using Alinea.Samfundsfag.BusinessLogic.Common;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
    public class Poll
    {
        public Poll(DataAccess.Poll poll)
        {
            string classNames = string.Empty;
            poll.PollClasses.ForEach(pollClass => classNames += pollClass.Class.Name + ", ");
            ClassNames = classNames.TrimEnd(' ', ',');

            var category = new Node(poll.CategoryId);
            if (category.GetProperty(Constants.NodePropertyNames.Title) != null)
            {
                Topic = category.GetProperty(Constants.NodePropertyNames.Title).Value;
            }
            else
            {
                Topic = "Findes ikke længere";
            }
            

            Answers = poll.PollAnswers.Select(pollAnswer => new Models.Answer(pollAnswer)).ToList();

            PollId = poll.PollId;
            Question = poll.Question;
            Preview = poll.Question.Max(Constants.PreviewLength);
			Date = poll.CreationDate.ToString("dd-MM-yyyy HH:mm");
			DateMicroFormat = poll.CreationDate.ToString("dd-MM-yyyy HH:mm");
            Status = poll.IsPublished ? Constants.States.Published : Constants.States.WorkInProgress;
        }

        public int PollId { get; set; }
        public string Question { get; set; }
        public string Preview { get; set; }
        public string Topic { get; set; }
        public string Date { get; set; }
        public string DateMicroFormat { get; set; }
        public string ClassNames { get; set; }
        public string Status { get; set; }
        public List<Answer> Answers { get; set; }
    }
}
