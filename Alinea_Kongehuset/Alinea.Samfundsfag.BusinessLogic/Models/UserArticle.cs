﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	public class UserArticle
	{
		private const int _image_max_height = 600;

		private string _author = null;
		private List<string> _coAuthors = null;
		private List<int> _coAuthorIds = null;
		private List<string> _subjects = null;
		private List<int> _subjectIds = null;
		private List<UserArticleComment> _comments = null;
		private List<UserArticleComment> _teacherFeedback = null;
		private DateTime? _lastSeen = null;
		private List<UserArticleTimelineItem> _timelineItems = null; 

		public int Id { get; set; }

		public string Title { get; set; }
		public string Introduction { get; set; }
		public string Text1 { get; set; }
		public int Text1SortOrder { get; set; }
		public string Text2 { get; set; }
		public int Text2SortOrder { get; set; }
		public string Image1Text { get; set; }
		public int Image1SortOrder { get; set; }
		public string Image2Text { get; set; }
		public int Image2SortOrder { get; set; }
		public string Image3Text { get; set; }
		public int Image3SortOrder { get; set; }
		public string Video1YouTubeUrl { get; set; }
		public int Video1SortOrder { get; set; }
		public string Audio1Text { get; set; }
		public int Audio1SortOrder { get; set; }
		public string TimelineHeadline { get; set; }
		public int TimelineActiveItem { get; set; }
		public int? AssignmentId { get; set; }

		public int ClassId { get; set; }
		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public int? ModifiedBy { get; set; }
		public DateTime? ModifiedDate { get; set; }
		public bool IsPublished { get; set; }
		public DateTime? PublishedDate { get; set; }
		public bool IsDeleted { get; set; }

		private string CoverImageSrcRaw { get { return "/media/UserArticles/" + this.Id + "/cover.jpg"; } }
		private string Image1SrcRaw { get { return "/media/UserArticles/" + this.Id + "/image1.jpg"; } }
		private string Image2SrcRaw { get { return "/media/UserArticles/" + this.Id + "/image2.jpg"; } }
		private string Image3SrcRaw { get { return "/media/UserArticles/" + this.Id + "/image3.jpg"; } }
		private string Audio1SrcRaw { get { return "/media/UserArticles/" + this.Id + "/audio1.mp3"; } }

		public bool HasCoverImageNoFallback { get { return FileExist(this.CoverImageSrcRaw); } }
		public bool HasCoverImage { get { return FileExist(this.CoverImageSrcRaw) || FileExist(this.Image1SrcRaw); } }
		public bool HasImage1 { get { return FileExist(this.Image1SrcRaw); } }
		public bool HasImage2 { get { return FileExist(this.Image2SrcRaw); } }
		public bool HasImage3 { get { return FileExist(this.Image3SrcRaw); } }
		public bool HasAudio1 { get { return FileExist(this.Audio1SrcRaw); } }

		public string CoverImageSrcNoFallback { get { return "/img.ashx?img=" + this.CoverImageSrcRaw + "&width=380&height=280&crop=true&background=333333" + "&rnd=" + (new Random().Next(100000)); } }
		public string CoverImageSrc { get { return "/img.ashx?img=" + (FileExist(this.CoverImageSrcRaw) ? this.CoverImageSrcRaw : this.Image1SrcRaw) + "&width=380&height=280&crop=true&background=333333" + "&rnd=" + (new Random().Next(100000)); } }
		public string CoverImageForSliderSrc { get { return "/img.ashx?img=" + (FileExist(this.CoverImageSrcRaw) ? this.CoverImageSrcRaw : this.Image1SrcRaw) + "&width=400&height=300&crop=true&background=333333" + "&rnd=" + (new Random().Next(100000)); } }
		public string Image1Src { get { return GetImageSrc(this.Image1SrcRaw); } }
		public string Image2Src { get { return GetImageSrc(this.Image2SrcRaw); } }
		public string Image3Src { get { return GetImageSrc(this.Image3SrcRaw); } }
		public string AudioSrc { get { return this.Audio1SrcRaw; } }

		private bool FileExist(string srcRaw)
		{
			return new FileInfo(HttpContext.Current.Server.MapPath(srcRaw)).Exists;
		}

		private string GetImageSrc(string srcRaw)
		{
			var height = 0;
			if (this.FileExist(srcRaw))
			{
				try
				{
					using (var img = Image.FromFile(HttpContext.Current.Server.MapPath(srcRaw)))
					{
						height = img.Height;
					}
				}
				catch (OutOfMemoryException)
				{
					// Buhuu. File corrupt? 
				}
			}

			return "/img.ashx?img=" + srcRaw + "&width=590" + (height > _image_max_height ? "&height=" + _image_max_height + "&crop=true&background=333333" : "") + "&rnd=" + (new Random().Next(100000));
		}

		public string Author
		{
			get
			{
				if (_author == null)
				{
					using (var us = new UserService())
					{
						var author = us.GetUser(this.CreatedBy);
						_author = author.Name;
					}
				}
				return _author;
			}
		}

		public List<string> CoAuthors
		{
			get
			{
				if (_coAuthors == null)
				{
					using (var uas = new UserArticleService())
					{
						_coAuthors = uas.GetCoAuthors(this.Id);
					}
				}
				return _coAuthors;
			}
		}

		public List<int> CoAuthorIds
		{
			get
			{
				if (_coAuthorIds == null)
				{
					using (var uas = new UserArticleService())
					{
						_coAuthorIds = uas.GetCoAuthorIds(this.Id);
					}
				}
				return _coAuthorIds;
			}
		}

		public List<string> Subjects
		{
			get
			{
				if (_subjects == null)
				{
					using (var ss = new SubjectService())
					{
						_subjects = ss.GetSubjects(this.Id);
					}
				}
				return _subjects;
			}
		}

		public List<int> SubjectIds
		{
			get
			{
				if (_subjectIds == null)
				{
					using (var ss = new SubjectService())
					{
						_subjectIds = ss.GetSubjectIds(this.Id);
					}
				}
				return _subjectIds;
			}
		}

		public List<UserArticleComment> Comments
		{
			get
			{
				if (_comments == null)
				{
					using (var uacs = new UserArticleCommentService())
					{
						_comments = uacs.GetComments(this.Id);
					}
				}
				return _comments;
			}
		}

		public List<UserArticleComment> TeacherFeedback
		{
			get
			{
				if (_teacherFeedback == null)
				{
					using (var uacs = new UserArticleCommentService())
					{
						_teacherFeedback = uacs.GetComments(this.Id, teacherFeedback: true);
					}
				}
				return _teacherFeedback;
			}
		}

		public bool Unread
		{
			get
			{
				return this.LastSeen == DateTime.MinValue;
			}
		}

		public DateTime LastSeen
		{
			get
			{
				if (_lastSeen == null)
				{
					using (var uas = new UserArticleService())
					{
						var lastSeen = uas.LastSeen(this.Id, SessionController.CurrentUserId);
						if (lastSeen.HasValue)
							_lastSeen = lastSeen.Value;
						else
							_lastSeen = DateTime.MinValue;
					}
				}

				return _lastSeen.Value;
			}
		}

		public string Video1YouTubeVideoId 
		{
			get
			{
				var videoId = "";
				if (!string.IsNullOrEmpty(this.Video1YouTubeUrl))
				{
					try
					{
						var uri = new Uri(this.Video1YouTubeUrl);
						if (string.IsNullOrEmpty(videoId) && uri.Host.ToLower() == "youtu.be")
						{
							videoId = uri.AbsolutePath.TrimStart('/');
						}
						else
						{
							videoId = HttpUtility.ParseQueryString(uri.Query).Get("v");
						}
					}
					catch (Exception)
					{ }
				}
				return videoId;
			}
		}
		public List<UserArticleTimelineItem> TimelineItems
		{
			get
			{
				return _timelineItems;
			}
			set
			{
				foreach (var item in value)
				{
					var srcRaw = "/media/UserArticles/" + this.Id + "/timelineimage" + item.Id + ".jpg";
					item.HasImage = FileExist(srcRaw);
					item.ImageSrc = "/img.ashx?img=" + srcRaw + "&width=180&height=135&rnd=" + (new Random().Next(100000));
				}
				_timelineItems = value;
			}
		}
	}
}
