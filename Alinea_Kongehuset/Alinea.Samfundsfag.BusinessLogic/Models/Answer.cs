﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
    public class Answer
    {
        public Answer(DataAccess.PollAnswer pollAnswer)
        {
            AnswerId = pollAnswer.PollAnswerId;
            Title = pollAnswer.Answer;
        }

        public int AnswerId { get; set; }
        public string Title { get; set; }
    }
}
