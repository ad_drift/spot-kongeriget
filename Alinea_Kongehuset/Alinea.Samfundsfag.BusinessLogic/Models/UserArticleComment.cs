﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	public class UserArticleComment
	{
		private string _author = null;

		public int Id { get; set; }
		public int ArticleId { get; set; }

		public string Comment { get; set; }

		public List<int> ReadBy { get; set; }

		public int CreatedBy { get; set; }
		public DateTime CreatedDate { get; set; }
		public bool IsTeacherFeedback { get; set; }
		public bool IsDeleted { get; set; }

		public string Author 
		{
			get
			{
				if (_author == null)
				{
					var us = new UserService();
					var author = us.GetUser(this.CreatedBy);
					_author = author.Name;
				}
				return _author;
			}
		}
	}
}
