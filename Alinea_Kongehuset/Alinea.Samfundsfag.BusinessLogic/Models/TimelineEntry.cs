﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
    public class TimelineEntry
    {
        public string Title { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Position { get; set; }
    }
}
