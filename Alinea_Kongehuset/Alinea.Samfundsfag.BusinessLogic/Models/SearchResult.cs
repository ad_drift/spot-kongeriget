﻿using System;
using Alinea.Samfundsfag.BusinessLogic.Common;
using CoreM41.Diagnostics.Logging;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
	/// <summary>
	/// Search result class used when searching the site.
	/// </summary>
	public class SearchResult
	{
		private const int MaxContentAbstractLength = 300;

		/// <summary>
		/// Gets the node ID of the search result.
		/// </summary>
		public int NodeId
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the search result title.
		/// </summary>
		public string Title
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the search result title.
		/// </summary>
		public string NodeType
		{
			get
			{
				switch (this.NodeTypeAlias)
				{
					case Constants.NodeDocumentTypes.NewsArticle:
						return "Spot";
					case Constants.NodeDocumentTypes.Angle:
						return "Artikel";
					case Constants.NodeDocumentTypes.Profile:
						return "Person";
					case Constants.NodeDocumentTypes.Method:
						return "Metode";
					default:
						return "";
				}
			}
		}

		/// <summary>
		/// Gets the search result content (all the content).
		/// </summary>
		public string Content
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the search result page date (if the page contains it).
		/// </summary>
		public DateTime? Date
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the URL to the page.
		/// </summary>
		public string Url
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the node type alias of the site.
		/// </summary>
		public string NodeTypeAlias
		{
			get;
			internal set;
		}

		/// <summary>
		/// Gets the date formatted to a pretty string or an empty string if no date is set.
		/// Dates are only displayed for news articles.
		/// </summary>
		public string FormattedDate
		{
			get
			{
				// Don't display dates for non-news articles or no date is set
				if (!Date.HasValue || NodeTypeAlias != Constants.NodeTypes.NewsArticle)
				{
					return string.Empty;
				}

				return Date.Value.ToString(Constants.DateFormat);
			}
		}

		/// <summary>
		/// Returns the content abstract.
		/// </summary>
		public string ContentAbstract
		{
			get
			{
				return Content;
			}
		}

		/// <summary>
		/// Gets the category name based on the NodeTypeAlias of the search result.
		/// </summary>
		public string Category
		{
			get
			{
				switch (NodeTypeAlias)
				{
					case Constants.NodeTypes.NewsArticle:
						return "Nyhedsartikel";

					case Constants.NodeTypes.Angle:
						return "Artikel";

					default:
						var exception = new Exception("NodeType is not supported.");
						Logger.LogError(exception);
						throw exception;
				}
			}
		}
	}
}
