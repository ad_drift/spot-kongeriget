﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.interfaces;
using Alinea.Samfundsfag.BusinessLogic.Common;

namespace Alinea.Samfundsfag.BusinessLogic.Models
{
    public class PollTopic
    {
        public PollTopic(INode node)
        {
            PollTopicId = node.Id;
            Title = node.GetProperty(Constants.NodePropertyNames.Title).Value;
        }

        public int PollTopicId { get; set; }
        public string Title { get; set; }
    }
}
