﻿--
-- Installs all the tables for the Samfundsfag project
--

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


/******************/
/** SCHOOL YEARS **/
/******************/
CREATE TABLE [dbo].[SchoolYears] (
	[SchoolYearId]		[int] IDENTITY(1,1) NOT NULL,
	[Name]				[nvarchar](50) NOT NULL,
	[StartDate]			[datetime] NOT NULL,
	CONSTRAINT [PK_SchoolYears] PRIMARY KEY CLUSTERED 
	(
		[SchoolYearId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_SchoolYears_StartDate ON [dbo].[SchoolYears] ([StartDate])
GO

SET DATEFORMAT dmy
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2011/2012', '01-08-2011')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2012/2013', '01-08-2012')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2013/2014', '01-08-2013')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2014/2015', '01-08-2014')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2015/2016', '01-08-2015')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2016/2017', '01-08-2016')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2017/2018', '01-08-2017')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2018/2019', '01-08-2018')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2019/2020', '01-08-2019')
INSERT INTO [dbo].[SchoolYears] ([Name], [StartDate]) VALUES ('2020/2021', '01-08-2020')
GO


/******************/
/** CLASS LEVELS **/
/******************/
CREATE TABLE [dbo].[ClassLevels] (
	[ClassLevelId]		[tinyint] NOT NULL,
	[Name]				[nvarchar](50) NOT NULL,
	[IsEnabled]			[bit] NOT NULL,
	CONSTRAINT [PK_ClassLevels] PRIMARY KEY CLUSTERED 
	(
		[ClassLevelId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ClassLevels] ADD CONSTRAINT [DF_ClassLevels_IsEnabled] DEFAULT ((1)) FOR [IsEnabled]
GO

INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (0, '0.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (1, '1.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (2, '2.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (3, '3.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (4, '4.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (5, '5.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name], [IsEnabled]) VALUES (6, '6.', 0)
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name]) VALUES (7, '7.')
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name]) VALUES (8, '8.')
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name]) VALUES (9, '9.')
INSERT INTO [dbo].[ClassLevels] ([ClassLevelId], [Name]) VALUES (10, '10.')
GO


/*************/
/** SCHOOLS **/
/*************/
CREATE TABLE [dbo].[Schools] (
	[SchoolId]			[int] IDENTITY(1,1) NOT NULL,
	[UniInstituionId]	[varchar](50) NOT NULL,
	[Name]				[varchar](250) NOT NULL,
	[CreationDate]		[datetime] NOT NULL,
	CONSTRAINT [PK_Schools] PRIMARY KEY CLUSTERED 
	(
		[SchoolId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX UX_Schools_UniInstituionId ON [dbo].[Schools] ([UniInstituionId])
GO

ALTER TABLE [dbo].[Schools] ADD CONSTRAINT [DF_Schools_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO


/*************/
/** CLASSES **/
/*************/
CREATE TABLE [dbo].[Classes] (
	[ClassId]		[int] IDENTITY(1,1) NOT NULL,
	[SchoolYearId]	[int] NOT NULL,
	[SchoolId]		[int] NOT NULL,
	[ClassLevelId]	[tinyint] NULL,
	[Name]			[nvarchar](100) NOT NULL,
	[CreationDate]	[datetime] NOT NULL,
	CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
	(
		[ClassId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX UX_Classes_SchoolYearId_SchoolId_ClassLevelId_Name ON [dbo].[Classes] ([SchoolYearId], [SchoolId], [ClassLevelId], [Name])
GO
CREATE NONCLUSTERED INDEX IX_Classes_SchoolId ON [dbo].[Classes] ([SchoolId])
GO
-- These indexes can be created if needed - not sure yet if it will be relevant in the queries
-- CREATE NONCLUSTERED INDEX IX_Classes_ClassLevelId ON [dbo].[Classes] ([ClassLevelId])
-- GO
-- CREATE NONCLUSTERED INDEX IX_Classes_Name ON [dbo].[Classes] ([Name])
-- GO

ALTER TABLE [dbo].[Classes] ADD CONSTRAINT [DF_Classes_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO

ALTER TABLE [dbo].[Classes] WITH CHECK ADD CONSTRAINT [FK_Classes_SchoolYears] FOREIGN KEY([SchoolYearId])
REFERENCES [dbo].[SchoolYears] ([SchoolYearId])
GO
ALTER TABLE [dbo].[Classes] WITH CHECK ADD CONSTRAINT [FK_Classes_Schools] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[Schools] ([SchoolId])
GO
ALTER TABLE [dbo].[Classes] WITH CHECK ADD CONSTRAINT [FK_Classes_ClassLevels] FOREIGN KEY([ClassLevelId])
REFERENCES [dbo].[ClassLevels] ([ClassLevelId])
GO


/***********/
/** USERS **/
/***********/
CREATE TABLE [dbo].[Users] (
	[UserId]					[int] IDENTITY(1,1) NOT NULL,
	[UniLoginId]				[nvarchar](20) NOT NULL,
	[Name]						[nvarchar](100) NOT NULL,
	[IsTeacher]					[bit] NOT NULL,
	[PrimaryInstitutionId]		[nvarchar](50) NULL,
	[PrimaryInstitutionName]	[nvarchar](150) NULL,
	[CreationDate]				[datetime] NOT NULL,
	[LastModified]				[datetime] NULL,
	[LastLoginDate]				[datetime] NULL,
	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
	(
		[UserId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_Users_IsTeacher ON [dbo].[Users] ([IsTeacher])
GO
CREATE UNIQUE INDEX UX_Users_UniLogin ON [dbo].[Users] ([UniLoginId])
GO

ALTER TABLE [dbo].[Users] ADD CONSTRAINT [DF_Users_IsTeacher] DEFAULT ((1)) FOR [IsTeacher]
GO
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [DF_Users_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO


/******************/
/** USER CLASSES **/
/******************/
CREATE TABLE [dbo].[UserClasses] (
	[UserId]		[int] NOT NULL,
	[ClassId]		[int] NOT NULL,
	CONSTRAINT [PK_UserClasses] PRIMARY KEY CLUSTERED 
	(
		[UserId] ASC,
		[ClassId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_UserClassess_ClassId ON [dbo].[UserClasses] ([ClassId])
GO

ALTER TABLE [dbo].[UserClasses] WITH CHECK ADD CONSTRAINT [FK_UserClasses_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserClasses] WITH CHECK ADD CONSTRAINT [FK_UserClasses_Classes] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Classes] ([ClassId])
GO


/***********/
/** POLLS **/
/***********/
-- Stores the user defined polls (usually created by teachers)
CREATE TABLE [dbo].[Polls] (
	[PollId]				[int] IDENTITY(1,1) NOT NULL,
	[CategoryId]			[int] NOT NULL, -- References the Umbraco category node ID
	[Question]				[nvarchar](4000) NOT NULL,
	[IsPublished]			[bit] NOT NULL,
	[IsDeleted]				[bit] NOT NULL,
	[CreatedBy]				[int] NOT NULL, -- References the teacher user
	[CreationDate]			[datetime] NOT NULL,
	[ModifiedBy]			[int] NOT NULL, -- References the teacher user
	[LastModified]			[datetime] NOT NULL,
	CONSTRAINT [PK_Polls] PRIMARY KEY CLUSTERED 
	(
 		[PollId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

ALTER TABLE [dbo].[Polls] ADD CONSTRAINT [DF_Polls_IsDeleted] DEFAULT (0) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Polls] ADD CONSTRAINT [DF_Polls_IsPublished] DEFAULT (0) FOR [IsPublished]
GO
ALTER TABLE [dbo].[Polls] ADD CONSTRAINT [DF_Polls_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO
ALTER TABLE [dbo].[Polls] ADD CONSTRAINT [DF_Polls_LastModified] DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [dbo].[Polls] WITH CHECK ADD CONSTRAINT [FK_Polls_Users_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Polls] WITH CHECK ADD CONSTRAINT [FK_Polls_Users_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO


/********************/
/** POLL ANSWERS **/
/********************/
-- Stores the poll answers for Polls
CREATE TABLE [dbo].[PollAnswers] (
	[PollAnswerId]			[int] IDENTITY(1,1) NOT NULL,
	[PollId]				[int] NOT NULL, 
	[SortOrder]				[tinyint] NOT NULL, 
	[Answer]				[nvarchar](1000) NOT NULL,
	[IsDeleted]				[bit] NOT NULL,
	[CreatedBy]				[int] NOT NULL, -- References the teacher user
	[CreationDate]			[datetime] NOT NULL,
	[ModifiedBy]			[int] NOT NULL, -- References the teacher user
	[LastModified]			[datetime] NOT NULL,
	CONSTRAINT [PK_PollAnswers] PRIMARY KEY CLUSTERED 
	(
 		[PollAnswerId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

CREATE NONCLUSTERED INDEX [IX_PollAnswers_PollId] ON [dbo].[PollAnswers] ([PollId])
GO

ALTER TABLE [dbo].[PollAnswers] ADD CONSTRAINT [DF_PollAnswers_IsDeleted] DEFAULT (0) FOR [IsDeleted]
GO

ALTER TABLE [dbo].[PollAnswers] WITH CHECK ADD CONSTRAINT [FK_PollAnswers_Polls] FOREIGN KEY([PollId])
REFERENCES [dbo].[Polls] ([PollId])
GO
ALTER TABLE [dbo].[PollAnswers] WITH CHECK ADD CONSTRAINT [FK_PollAnswers_Users_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[PollAnswers] WITH CHECK ADD CONSTRAINT [FK_PollAnswers_Users_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO


/********************/
/** POLL CLASSES **/
/********************/
-- Stores which classes can see the polls
CREATE TABLE [dbo].[PollClasses] (
	[PollId]				[int] NOT NULL, 
	[ClassId]				[int] NOT NULL, 
	CONSTRAINT [PK_PollClasses] PRIMARY KEY CLUSTERED 
	(
 		[PollId],
		[ClassId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

CREATE NONCLUSTERED INDEX [IX_PollClasses_ClassId] ON [dbo].[PollClasses] ([ClassId])
GO

ALTER TABLE [dbo].[PollClasses] WITH CHECK ADD CONSTRAINT [FK_PollClasses_Polls] FOREIGN KEY([PollId])
REFERENCES [dbo].[Polls] ([PollId])
GO
ALTER TABLE [dbo].[PollClasses] WITH CHECK ADD CONSTRAINT [FK_PollClasses_Classes] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Classes] ([ClassId])
GO


/*************************/
/** POLL USER ANSWERS **/
/*************************/
-- Stores the Users answers to the various polls on the site
CREATE TABLE [dbo].[PollUserAnswers] (
	[PollUserAnswerId]		[int] IDENTITY(1,1) NOT NULL,
	[IsUmbracoPoll]			[bit] NOT NULL, -- Defines whether the PollId and AnswerId references the database tables or the Umbraco nodes
	[UserId]				[int] NOT NULL, -- The user that has given this answer
	[AnswerId]				[int] NOT NULL,
	[PollId]				[int] NOT NULL,
	[CreationDate]			[datetime] NOT NULL,
	CONSTRAINT [PK_PollUserAnswers] PRIMARY KEY CLUSTERED 
	(
 		[PollUserAnswerId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

CREATE UNIQUE INDEX [IX_PollUserAnswers_UserId_AnswerId_IsUmbracoPoll] ON [dbo].[PollUserAnswers] ([UserId], [AnswerId], [IsUmbracoPoll])
GO
CREATE NONCLUSTERED INDEX [IX_PollUserAnswers_IsUmbracoPoll_PollId] ON [dbo].[PollUserAnswers] ([IsUmbracoPoll], [PollId])
GO

ALTER TABLE [dbo].[PollUserAnswers] ADD CONSTRAINT [DF_PollUserAnswers_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO

ALTER TABLE [dbo].[PollUserAnswers] WITH CHECK ADD CONSTRAINT [FK_PollUserAnswers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO


/****************************/
/** POLL ANSWER STATISTICS **/
/****************************/
CREATE TABLE [dbo].[PollAnswerStatistics] (
	[PollId]				[int] NOT NULL, 
	[AnswerId]				[int] NOT NULL,
	[IsUmbracoPoll]			[bit] NOT NULL,
	[NumberOfAnswers]		[int] NOT NULL,
	CONSTRAINT [PK_PollAnswerStatistics] PRIMARY KEY CLUSTERED 
	(
 		[PollId],
		[AnswerId],
		[IsUmbracoPoll]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

ALTER TABLE [dbo].[PollAnswerStatistics] ADD CONSTRAINT [DF_PollAnswerStatistics_NumberOfAnswers] DEFAULT (0) FOR [NumberOfAnswers]
GO


/********************/
/** BOARD MESSAGES **/
/********************/
CREATE TABLE [dbo].[BoardMessages] (
	[BoardMessageId]		[int] IDENTITY(1,1) NOT NULL,
	[Text]					[nvarchar](4000) NOT NULL,
	[MediaFileId]			[INT] NULL, -- References the Umbraco media file ID
	[IsPublished]			[bit] NOT NULL,
	[IsDeleted]				[bit] NOT NULL,
	[CreatedBy]				[int] NOT NULL, -- References the teacher user
	[CreationDate]			[datetime] NOT NULL,
	[ModifiedBy]			[int] NOT NULL, -- References the teacher user
	[LastModified]			[datetime] NOT NULL
	CONSTRAINT [PK_BoardMessages] PRIMARY KEY CLUSTERED 
	(
 		[BoardMessageId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

ALTER TABLE [dbo].[BoardMessages] ADD CONSTRAINT [DF_BoardMessages_IsDeleted] DEFAULT (0) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[BoardMessages] ADD CONSTRAINT [DF_BoardMessages_IsPublished] DEFAULT (1) FOR [IsPublished]
GO
ALTER TABLE [dbo].[BoardMessages] ADD CONSTRAINT [DF_BoardMessages_CreationDate] DEFAULT (getdate()) FOR [CreationDate]
GO
ALTER TABLE [dbo].[BoardMessages] ADD CONSTRAINT [DF_BoardMessages_LastModified] DEFAULT (getdate()) FOR [LastModified]
GO

ALTER TABLE [dbo].[BoardMessages] WITH CHECK ADD CONSTRAINT [FK_BoardMessages_Users_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BoardMessages] WITH CHECK ADD CONSTRAINT [FK_BoardMessages_Users_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Users] ([UserId])
GO

/***************************/
/** BOARD MESSAGE CLASSES **/
/***************************/
CREATE TABLE [dbo].[BoardMessageClasses] (
	[BoardMessageId]		[int] NOT NULL, 
	[ClassId]				[int] NOT NULL, 
	CONSTRAINT [PK_BoardMessageClasses] PRIMARY KEY CLUSTERED 
	(
 		[BoardMessageId],
		[ClassId]
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO	

CREATE NONCLUSTERED INDEX [IX_BoardMessageClasses_ClassId] ON [dbo].[BoardMessageClasses] ([ClassId])
GO

ALTER TABLE [dbo].[BoardMessageClasses] WITH CHECK ADD CONSTRAINT [FK_BoardMessageClasses_BoardMessages] FOREIGN KEY([BoardMessageId])
REFERENCES [dbo].[BoardMessages] ([BoardMessageId])
GO
ALTER TABLE [dbo].[BoardMessageClasses] WITH CHECK ADD CONSTRAINT [FK_BoardMessageClasses_Classes] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Classes] ([ClassId])
GO


/**********/
/** LOGS **/
/**********/
CREATE TABLE [dbo].[LogEntries](
	[LogEntryId] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[Severity] [nvarchar](32) NOT NULL,
	[Level] [tinyint] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[LogText] [ntext] NULL,	
	[Url] [varchar](500) NULL,
	[RequestMethod] [varchar](50) NULL,
 	[RequestLength] [bigint] NULL,
	[UserIP] [varchar](40) NULL,
	[UserHostName] [varchar](255) NULL,
	[UserAgent] [varchar](500) NULL,
	[FormData] [text] NULL,
 	[SessionId] [varchar](50) NULL,
 	[Referrer] [varchar](500) NULL,
 	[Cookies] [text] NULL,
 	[Sessions] [nvarchar](4000) NULL, 	
	[EventId] [int] NULL,
	[Priority] [int] NOT NULL,
	[MachineName] [nvarchar](32) NOT NULL,
	[AppDomainName] [nvarchar](512) NOT NULL,
	[ProcessId] [nvarchar](256) NOT NULL,
	[ProcessName] [nvarchar](512) NOT NULL,
	[ThreadName] [nvarchar](512) NULL,
	[Win32ThreadId] [nvarchar](128) NULL,	
	CONSTRAINT [PK_LogEntries] PRIMARY KEY CLUSTERED 
	(
 		[LogEntryId] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO	

ALTER TABLE [dbo].[LogEntries] ADD  CONSTRAINT [DF_LogEntries_LogDate] DEFAULT (getdate()) FOR [LogDate]
GO

CREATE TABLE [dbo].[LogEntryCategories](
	[CategoryLogId] [int] IDENTITY(1,1) NOT NULL,
	[LogEntryId] [int] NOT NULL,
	[CategoryName] [nvarchar](64) NOT NULL,
	CONSTRAINT [PK_LogEntryCategories] PRIMARY KEY CLUSTERED 
	(
		[CategoryLogId] ASC
	) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UX_LogEntryCategories_LogEntryId_CategoryName] ON [dbo].[LogEntryCategories] ([LogEntryId], [CategoryName])
GO

ALTER TABLE [dbo].[LogEntryCategories] WITH CHECK ADD CONSTRAINT [FK_LogEntryCategories_LogEntries] FOREIGN KEY([LogEntryId])
REFERENCES [dbo].[LogEntries] ([LogEntryId])
GO


/******************************/
/** BOARD MESSAGE USER READS **/
/******************************/
CREATE TABLE [dbo].[BoardMessageUserReads] (
	[BoardMessageUserReadId]			[int] IDENTITY(1,1) NOT NULL,
	[UserId]							[int] NOT NULL,
	[BoardMessageId]					[int] NOT NULL
	CONSTRAINT [PK_BoardMessageUserReads] PRIMARY KEY CLUSTERED 
	(
		[BoardMessageUserReadId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_BoardMessageUserReads_UserId ON [dbo].[BoardMessageUserReads] ([UserId])
GO
CREATE NONCLUSTERED INDEX IX_BoardMessageUserReads_BoardMessageId ON [dbo].[BoardMessageUserReads] ([BoardMessageId])
GO

ALTER TABLE [dbo].[BoardMessageUserReads] WITH CHECK ADD CONSTRAINT [FK_BoardMessageUserReads_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BoardMessageUserReads] WITH CHECK ADD CONSTRAINT [FK_BoardMessageUserReads_BoardMessages] FOREIGN KEY([BoardMessageId])
REFERENCES [dbo].[BoardMessages] ([BoardMessageId])
GO


/************************/
/** CRAWL BROKEN LINKS **/
/************************/
CREATE TABLE [dbo].[CrawlBrokenLinks](
	[CrawlBrokenUrlId] [int] IDENTITY(1,1) NOT NULL,
	[BrokenUrl] [varchar](max) NULL,
	[ReferrerUrl] [varchar](max) NULL,
	[RegisteredDate] [datetime] NULL
 CONSTRAINT [PK_CrawlBrokenLinks] PRIMARY KEY CLUSTERED 
(
	[CrawlBrokenUrlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO




/************************/
/** USER ARTICLES      **/
/************************/
CREATE TABLE [dbo].[UserArticles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](2048) NOT NULL,
	[Introduction] [ntext] NOT NULL,
	[Text1] [ntext] NOT NULL,
	[Text1SortOrder] [int] NOT NULL,
	[Text2] [ntext] NOT NULL,
	[Text2SortOrder] [int] NOT NULL,
	[Image1Text] [nvarchar](2048) NOT NULL,
	[Image1SortOrder] [int] NOT NULL,
	[Image2Text] [nvarchar](2048) NOT NULL,
	[Image2SortOrder] [int] NOT NULL,
	[Image3Text] [nvarchar](2048) NOT NULL,
	[Image3SortOrder] [int] NOT NULL,
	[Video1YouTubeUrl] [varchar](1024) NOT NULL,
	[Video1SortOrder] [int] NOT NULL,
	[Audio1Text] [nvarchar](2048) NOT NULL,
	[Audio1SortOrder] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsPublished] [bit] NOT NULL,
	[PublishedDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserArticles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO




/***********************************/
/** USER ARTICLES CO AUTHORS      **/
/***********************************/
CREATE TABLE [dbo].[UserArticleCoAuthors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserArticleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_UserArticleCoAuthors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO




/***********************************/
/** USER ARTICLES COMMENTS        **/
/***********************************/
CREATE TABLE [dbo].[UserArticleComments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ArticleId] [int] NOT NULL,
	[Comment] [ntext] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsTeacherFeedback] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserArticleComments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



/***********************************/
/** USER ARTICLES LAST SEEN       **/
/***********************************/
CREATE TABLE [dbo].[UserArticleLastSeen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserArticleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[LastSeenDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UserArticleLastSeen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/************************************/
/** SELECTED TEACHER CLASSES       **/
/************************************/
CREATE TABLE [dbo].[TeacherClasses](
	[TeacherClassId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClassId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TeacherClasses] PRIMARY KEY CLUSTERED 
(
	[TeacherClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[TeacherClasses]  WITH CHECK ADD  CONSTRAINT [FK_TeacherClasses_Classes] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Classes] ([ClassId])
GO

ALTER TABLE [dbo].[TeacherClasses] CHECK CONSTRAINT [FK_TeacherClasses_Classes]
GO

ALTER TABLE [dbo].[TeacherClasses]  WITH CHECK ADD  CONSTRAINT [FK_TeacherClasses_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO

ALTER TABLE [dbo].[TeacherClasses] CHECK CONSTRAINT [FK_TeacherClasses_Users]
GO