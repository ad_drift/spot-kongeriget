﻿--
-- JJohansen
-- 
-- CrawlBrokenLinks table for crawling the site for broken links
--
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CrawlBrokenLinks](
	[CrawlBrokenUrlId] [int] IDENTITY(1,1) NOT NULL,
	[BrokenUrl] [varchar](max) NULL,
	[ReferrerUrl] [varchar](max) NULL,
	[RegisteredDate] [datetime] NULL
 CONSTRAINT [PK_CrawlBrokenLinks] PRIMARY KEY CLUSTERED 
(
	[CrawlBrokenUrlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO