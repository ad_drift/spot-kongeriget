﻿--
-- JJohansen
-- Added read to BoardMessages
--
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[BoardMessages] ADD
	[IsRead]		[bit] NULL
GO

UPDATE [dbo].[BoardMessages] SET [IsRead] = 0
GO

ALTER TABLE [dbo].[BoardMessages] ALTER COLUMN
	[IsRead]		[bit] NOT NULL
GO

ALTER TABLE [dbo].[BoardMessages] ADD CONSTRAINT 
	[DF_BoardMessages_IsRead] DEFAULT (0) FOR [IsRead]
GO