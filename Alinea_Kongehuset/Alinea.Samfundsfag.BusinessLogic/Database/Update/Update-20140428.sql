﻿--
-- Updates all existing classes so that they match the new class creation,
-- which means that classes are no longer created based on class level but only the class name.
--
-- This script needs to be run on all three project databases on each environment
--
-- WARNING ! Because it would require to much work to code this in SQL you have to modify the scripts manually based on what the queries return!
-- JBunch
-- 

-- First load all classes that have more than one instance based on only class name
SELECT * FROM (
	SELECT [SchoolYearId], SchoolId,[Name], COUNT(*) as NumberOfClasses
	FROM [Classes]
	GROUP BY SchoolYearId, schoolid, Name
) SchoolClasses WHERE SchoolClasses.NumberOfClasses > 1

-- Load the classes
SELECT *
FROM [Classes]
WHERE [SchoolYearId] = '' AND [Name] IN ('', '')

-- Change user classes to the main class (pick one)
UPDATE [UserClasses]
SET ClassId = ''
WHERE ClassId = ''

-- Change the exams to the main class (use the one above)
UPDATE [Exams]
SET ClassId = ''
WHERE ClassId IS NOT NULL AND ClassId = ''

-- Change class exam tasks to the main class (pick one)
UPDATE [ClassExamTasks]
SET ClassId = ''
WHERE ClassId = ''

-- Delete the remaining classes
DELETE
FROM [Classes]
WHERE [SchoolYearId] = '' AND [ClassId] IN ('', '')




/*

select * from (
SELECT [SchoolYearId],
SchoolId
	  ,[Name]
	  ,COUNT(*) as antal
  FROM [Alinea_WebProever_Matematik].[dbo].[Classes]
  group by SchoolYearId, schoolid, Name
) a where a.antal > 1

select * from (
SELECT [SchoolYearId],
SchoolId
	  ,[Name]
	  ,COUNT(*) as antal
  FROM [Alinea_WebProever_biologi].[dbo].[Classes]
  group by SchoolYearId, schoolid, Name  
) a where a.antal > 1

select * from (
SELECT [SchoolYearId],
SchoolId
	  ,[Name]
	  ,COUNT(*) as antal
  FROM [Alinea_WebProever_geografi].[dbo].[Classes]
  group by SchoolYearId, schoolid, Name    
) a where a.antal > 1

*/