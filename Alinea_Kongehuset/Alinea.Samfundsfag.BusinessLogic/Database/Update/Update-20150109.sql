﻿--
-- Add LastSync column to schools in preparation for the unified sync experience
--
-- ORud
ALTER TABLE Schools ADD LastSync datetime null;
GO
UPDATE Schools SET LastSync = CreationDate;
GO
ALTER TABLE Schools ALTER COLUMN LastSync datetime NOT NULL
GO