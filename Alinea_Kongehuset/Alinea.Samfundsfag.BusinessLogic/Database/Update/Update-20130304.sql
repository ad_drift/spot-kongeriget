﻿--
-- JJohansen
-- Added read to BoardMessages on a UserLevel, removed read on class level
--
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[BoardMessages] DROP CONSTRAINT 
	[DF_BoardMessages_IsRead], COLUMN [IsRead]
GO
ALTER TABLE [dbo].[BoardMessages] DROP COLUMN
	[IsRead]
GO

CREATE TABLE [dbo].[BoardMessageUserReads] (
	[BoardMessageUserReadId]			[int] IDENTITY(1,1) NOT NULL,
	[UserId]							[int] NOT NULL,
	[BoardMessageId]					[int] NOT NULL
	CONSTRAINT [PK_BoardMessageUserReads] PRIMARY KEY CLUSTERED 
	(
		[BoardMessageUserReadId] ASC
	) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_BoardMessageUserReads_UserId ON [dbo].[BoardMessageUserReads] ([UserId])
GO
CREATE NONCLUSTERED INDEX IX_BoardMessageUserReads_BoardMessageId ON [dbo].[BoardMessageUserReads] ([BoardMessageId])
GO

ALTER TABLE [dbo].[BoardMessageUserReads] WITH CHECK ADD CONSTRAINT [FK_BoardMessageUserReads_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[BoardMessageUserReads] WITH CHECK ADD CONSTRAINT [FK_BoardMessageUserReads_BoardMessages] FOREIGN KEY([BoardMessageId])
REFERENCES [dbo].[BoardMessages] ([BoardMessageId])
GO