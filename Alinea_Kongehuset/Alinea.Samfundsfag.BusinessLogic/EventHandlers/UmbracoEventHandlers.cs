﻿using System.Web;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic;
using umbraco.cms.businesslogic.web;

namespace Alinea.Samfundsfag.BusinessLogic.EventHandlers
{
	public class UmbracoEventHandlers : ApplicationBase
	{
		public UmbracoEventHandlers()
		{
			Document.New += new Document.NewEventHandler(Document_New);
		}

		private void Document_New(Document sender, NewEventArgs e)
		{
			var dirty = false;

			if (HttpContext.Current.CurrentHandler.ToString() != "ASP.umbraco_dialogs_moveorcopy_aspx")
			{
				if (SetProperty(sender, "menuTitle", sender.Text))
					dirty = true;

				if (sender.ContentType.Alias == "Timeline")
				{
					if (SetProperty(sender, "headline", sender.Text))
						dirty = true;
				}
				else
				{
					if (SetProperty(sender, "title", sender.Text))
						dirty = true;
				}

				if (SetProperty(sender, "question", sender.Text))
					dirty = true;

				if (SetProperty(sender, "name", sender.Text))
					dirty = true;
			}

			if (dirty)
				sender.Save();
		}

		private bool SetProperty(Document doc, string alias, object value)
		{
			var prop = doc.getProperty(alias);
			if (prop != null)
			{
				prop.Value = value;
				return true;
			}
			return false;
		}
	}
}
