﻿using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Services;
using CoreM41.Diagnostics.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using umbraco.NodeFactory;

namespace Alinea.Samfundsfag.BusinessLogic.Utilities
{
	public class XsltHelper
	{
		/// <summary>
		/// Used for converting abbtrviation tags from umbraco nodes
		/// </summary>
		public static string ConvertAbbreviationTags(string text)
		{
			text = text.Replace("\n", " ").Replace("\r", " ");
			while (text.Contains("  "))
				text = text.Replace("  ", " ");

			var nodePath = Node.GetNodeByXpath(string.Format("/{0}/{1}/{2}",
																Constants.NodeDocumentTypes.Root,
																Constants.NodeDocumentTypes.Folder,
																Constants.NodeDocumentTypes.Abbreviation));

			if (nodePath != null)
			{
				var nodeList = nodePath.Parent.ChildrenAsList;

				if (nodeList != null)
				{
					foreach (Node node in nodeList)
					{
						text = text.Replace(node.GetProperty(Constants.NodePropertyNames.Identifier).ToString(),
											AbbreviationReplacement(node));
					}
				}
			}

			return text;
		}

		/// <summary>
		/// Used for converting profile tags from umbraco nodes
		/// </summary>
		public static string ConvertProfileTags(string text)
		{
			text = text.Replace("\n", " ").Replace("\r", " ");
			while (text.Contains("  "))
				text = text.Replace("  ", " ");

			var nodePath = Node.GetNodeByXpath(string.Format("/{0}/{1}/{2}/{3}/{4}",
			                                                 Constants.NodeDocumentTypes.Root,
			                                                 Constants.NodeDocumentTypes.Homepage,
			                                                 Constants.NodeDocumentTypes.StudentMaster,
			                                                 Constants.NodeDocumentTypes.Profiles,
			                                                 Constants.NodeDocumentTypes.Profile));
			if (nodePath != null)
			{
				var nodeList = nodePath.Parent.ChildrenAsList;

				foreach (Node node in nodeList)
				{
					if (node.GetProperty(Constants.NodePropertyNames.Identifier) != null) {
                        string profileRepl = ProfileReplacement(node);
                        string identifier = node.GetProperty(Constants.NodePropertyNames.Identifier).ToString();
                        if (!string.IsNullOrWhiteSpace(profileRepl) && !string.IsNullOrWhiteSpace(identifier))
                            text = text.Replace(identifier, profileRepl);
                    }
				}
			}

			return text;
		}

		/// <summary>
		/// Private method for replacing a abbreviation
		/// </summary>
		private static string AbbreviationReplacement(Node node)
		{
            try
            {
                return string.Format(Constants.Xslt.Html.Abbreviation, node.GetProperty(Constants.NodePropertyNames.Explanation), node.GetProperty(Constants.NodePropertyNames.Abbreviation));
            }
            catch (Exception ex)
            {
                return null;
            }

			
		}

		/// <summary>
		/// Private method for replacing a abbreviation
		/// </summary>
		private static string ProfileReplacement(Node node)
		{
            try
            {
                return string.Format(Constants.Xslt.Html.Profile, node.NiceUrl, node.GetProperty(Constants.NodePropertyNames.Name));
            }
            catch (Exception ex)
            {
                return null;
            }
			
		}

		/// <summary>
		/// Url resolver for issues with virtual paths
		/// </summary>
		public static string ResolveUrl(string path)
		{
			if (!path.StartsWith("~"))
			{
				path = "~" + path;
			}
			return VirtualPathUtility.ToAbsolute(path);
		}

		/// <summary>
		/// Converts a umbraco date to a html5 microformat
		/// </summary>
		public static string ToMicroFormat(DateTime date)
		{
			//Convert
			//2012-04-23T11:29:17
			// ->
			//2012-04-23T11:29+02:00
			return date.ToMicroFormat();
		}

		/// <summary>
		/// Converts a umbraco date to a readable date for the frontend
		/// </summary>
		public static string ToReadableDate(DateTime date)
		{
			//Convert
			//2012-04-23T11:29:17
			// ->
			//23. apr. 2012 kl. 11.29
			return date.ToString("dd. MMM. yyyy kl. HH:mm");
		}

		/// <summary>
		/// Returns a youtube id from a youtube url
		/// </summary>
		public static string CleanYoutubeId(string input)
		{
			string id = input;

			//Remove domain
			if (id.IndexOf("?v=") >= 0)
			{
				id = id.Substring(id.IndexOf("?v=") + 3);
			}

			//Remove domain if shortlink (http://youtu.be/9VWmZv871LE)
			if (id.IndexOf("/", 8) >= 0)
			{
				id = id.Substring(id.IndexOf("/", 8));
			}

			//Remove extra parameters
			Int32 andI = id.IndexOf("&");
			if (andI > -1)
				id = id.Substring(0, andI);

			// Send the Url of the site to fix youtube remote origin bug
			var fixRemoteOrigin = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;

			return Constants.Xslt.YouTubePrefix + id + "?rel=0&wmode=transparent&origin=" + fixRemoteOrigin;
		}

		/// <summary>
		/// Check if current user is a student
		/// </summary>
		public static bool IsLoggedIn()
		{
			if (UniLoginController.IsValidSsoSession)
			{
				return UniLoginController.IsLoggedInUserTeacherOrStudent;
			}
			return false;
		}

		/// <summary>
		/// Check if current user is a teacher
		/// </summary>
		public static bool IsLoggedInUserTeacher()
		{
			if (!UniLoginController.AccessDenied && UniLoginController.IsValidSsoSession && UniLoginController.IsLoggedInUserTeacherOrStudent)
			{
				return UniLoginController.IsLoggedInUserTeacher;
			}
			return false;
		}

		/// <summary>
		/// Check if current user is a student
		/// </summary>
		public static bool IsLoggedInUserStudent()
		{
			if (UniLoginController.IsValidSsoSession)
			{
				return UniLoginController.IsLoggedInUserStudent;
			}
			return false;
		}

		public static bool IsLoggedInUserStudentOrTeacherAsStudent()
		{
			if (UniLoginController.IsValidSsoSession && !UniLoginController.AccessDenied)
			{
				return UniLoginController.IsLoggedInUserStudent || (SessionController.IsCurrentUserTeacher && SessionController.HasUserClass);
			}
			return false;
		}

		/// <summary>
		/// Check if the current page is part of the teacher section or not
		/// </summary>
		public static bool IsInTeacherSection()
		{
			return HttpContext.Current.Request.RawUrl.StartsWith("/laerer");
		}

		/// <summary>
		/// Check if the current page is part of the student section or not
		/// </summary>
		public static bool IsInStudentSection()
		{
			return HttpContext.Current.Request.RawUrl.StartsWith("/elev");
		}

		/// <summary>
		/// Converts and given string to milliseconds, input must be mm:ss
		/// </summary>
		public static string ConvertToMilliseconds(string inputTime)
		{
			inputTime = "00:" + inputTime;
			TimeSpan timeSpan = TimeSpan.Parse(inputTime);
			return timeSpan.TotalMilliseconds.ToString();
		}

		public static string GetBannerImage(int currentPage)
		{
			Node node = new Node(currentPage);
			var prop = node.GetProperty("bannerImage");

			if (prop != null && prop.ToString() != string.Empty)
				return prop.ToString();
			else
				return GetBannerImage(node.Parent.Id);
		}

		public static string GetBannerTitle(int currentPage)
		{
			Node node = new Node(currentPage);
			var prop = node.GetProperty("bannerTitle");

			if (prop != null && prop.ToString() != string.Empty)
				return prop.ToString();
			else
				return GetBannerTitle(node.Parent.Id);
		}

		public static string GetIntroText(int currentPage)
		{
			Node node = new Node(currentPage);
			var prop = node.GetProperty("introText");

			if (prop != null && prop.ToString() != string.Empty)
				return prop.ToString();
			else
				return GetIntroText(node.Parent.Id);
		}

		public static XPathNodeIterator GetRelatedUserPolls(int categoryId)
		{
			int classId = SessionController.CurrentUserClass.ClassId;
			PollService pollService = new PollService();

			var polls = pollService.GetClassPolls(classId);
			if (polls.Count == 0)
			{
				//Due to a db issue on Alinea test we retreive the data again
				polls = pollService.GetClassPolls(classId);
			}

			polls = polls.Where(poll => poll.CategoryId == categoryId && poll.IsPublished).ToList();

			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.PreserveWhitespace = false;

			try
			{
      //          <headline>Quiz titel</headline>
      //<autoUpdate>4</autoUpdate>
      //<boolQuiz>1</boolQuiz>
      //<correctAnswer>
      //  <MultiNodePicker type="content">
      //    <nodeId>2640</nodeId>
      //  </MultiNodePicker>
      //</correctAnswer>
				xmlDocument.LoadXml(new XElement("Polls",
										from poll in polls
										select new XElement("Poll",
											new XAttribute("id", "db" + poll.PollId),
											new XElement("question", poll.Question),
											from pollAnswer in poll.PollAnswers
											select new XElement("Answer",
												new XAttribute("id", pollAnswer.PollAnswerId),
												new XAttribute("nodeName",
													pollAnswer.Answer)),
                                            new XElement("autoUpdate", 4),
                                            new XElement("boolQuiz", poll.IsQuiz ? "1" : "0"),
                                            new XElement("headline", poll.Headline),
                                            new XElement("correctAnswer",
                                                new XElement("MultiNodePicker",
                                                    new XAttribute("type", "content"),
                                                    new XElement("nodeId", poll.PollAnswers.Where(c => c.IsCorrect).Select(c => c.PollAnswerId).FirstOrDefault())
                                                ))
                                            )).ToString());
			}
			catch (Exception exception)
			{
				Logger.LogError(exception);
			}

			return xmlDocument.CreateNavigator().Select("/Polls/Poll");

			//Structure like umbraco polls.
			//Poll
			//question
			//Answer
			//@nodeName
			//Answer
			//@nodeName
		}

		public static string GetObjectiveTag(int objectivesId, int objectiveId)
		{
			Node objectivesNode = new Node(objectivesId);

			XDocument linksXml = XDocument.Parse(
				objectivesNode.GetProperty("completedObjectives").Value);

			List<int> completedGoals = (from node in linksXml.Descendants("link")
										select (int)node.Attribute("link")).ToList();

			string check = completedGoals.Contains(objectiveId)
				? Constants.Xslt.Html.ObjectiveCheckedAttribute : "";

			return string.Format(Constants.Xslt.Html.ObjectiveCheckbox, check);
		}

		public static bool ObjectiveIsCompleted(int objectivesId, int objectiveId)
		{
			Node objectivesNode = new Node(objectivesId);

			XDocument linksXml = XDocument.Parse(
				objectivesNode.GetProperty("completedObjectives").Value);

			List<int> completedGoals = (from node in linksXml.Descendants("link")
										select (int)node.Attribute("link")).ToList();

			return completedGoals.Contains(objectiveId);
		}

		public static string HasUserAlreadyAnswered(string _pollId)
		{
			_pollId = _pollId.TrimStart('d', 'b');
			int pollId = 0;
			int.TryParse(_pollId, out pollId);
			if (pollId > 0)
			{
				try
				{
					PollService pollService = new PollService();
					PollUserAnswer pollUserAnswer = pollService.GetPollUserAnswer(SessionController.CurrentUserId, pollId);
					if (pollUserAnswer != null)
					{
						return "true";
					}
				}
				catch (Exception exception)
				{
					Logger.LogError(exception);
				}
			}
			return string.Empty;
		}

		public static string GetUserAnsweredInputId(string _pollId)
		{
			_pollId = _pollId.TrimStart('d', 'b');
			int pollId = 0;
			int.TryParse(_pollId, out pollId);
			if (pollId > 0)
			{
				try
				{
					PollService pollService = new PollService();
					PollUserAnswer pollUserAnswer = pollService.GetPollUserAnswer(SessionController.CurrentUserId, pollId);
					if (pollUserAnswer != null)
					{
						return pollUserAnswer.AnswerId.ToString();
					}
				}
				catch (Exception exception)
				{
					Logger.LogError(exception);
				}
			}
			return string.Empty;
		}

		public static string GetAnsweredInputPercent(string _pollId, string inputId)
		{
			var isUmbracoPoll = !_pollId.StartsWith("db");
			var pollId = 0;
			int.TryParse(_pollId.TrimStart('d', 'b'), out pollId);
			if (pollId > 0)
			{
				try
				{
					var pollService = new PollService();
					var responses = pollService.GetPollResponses(pollId, isUmbracoPoll);
					var answer = responses.FirstOrDefault(response => response.InputId == string.Concat("answer", inputId));
					if (answer != null)
					{
						return answer.Percent;
					}
				}
				catch (Exception exception)
				{
					Logger.LogError(exception);
				}
			}
			return string.Empty;
		}
	}
}