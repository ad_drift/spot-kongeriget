﻿using System.Web;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.BusinessLogic.Utilities
{
	public class SamfundsfagInitModule : IHttpModule
	{
		#region Fields & Constants

		private static volatile bool applicationStarted = false;
		private static object applicationStartLock = new object();

		#endregion

		#region IHttpModule Members

		public void Dispose()
		{
		}

		public void Init(HttpApplication context)
		{
			if (!applicationStarted)
			{
				lock (applicationStartLock)
				{
					if (!applicationStarted)
					{
						// Hook into the UNI login events
						UniLoginController.UserUpdated += UserService.OnSynchronizeUser;
						UniLoginController.UserLoggedIn += UserService.OnUserLoggedIn;
						UniLoginController.RequireUniLoginUserId += UserService.OnRequireUniLoginUserId;

						applicationStarted = true;
					}
				}
			}
		}

		#endregion
	}
}
