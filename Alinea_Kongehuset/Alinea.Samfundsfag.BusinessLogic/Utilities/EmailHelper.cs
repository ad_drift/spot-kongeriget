﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.Utilities
{
	public static class EmailHelper
	{
		public const string EmailReqExValidation = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

		public static bool SendMail(string recipient, string sender, string emailSubject, string emailBody, Dictionary<string, string> values)
		{
			// Fill the tags
			foreach (var dict in values)
			{
				emailBody = emailBody.Replace("##" + dict.Key + "##", dict.Value);
			}

			// Remove all ##xxx## tags
			var regex = new System.Text.RegularExpressions.Regex(@"##.*?##");

			if (regex.IsMatch(emailBody))
			{
				emailBody = regex.Replace(emailBody, string.Empty);
			}

			MailMessage message = new MailMessage(sender, recipient);
			message.Subject = emailSubject;
			message.IsBodyHtml = false;
			message.Body = emailBody;

			return SendMail(message);
		}

		private static bool SendMail(MailMessage mailMessage)
		{
			try
			{
				SmtpClient client = new SmtpClient();
				client.Send(mailMessage);
				return true;
			}
			catch (SmtpException smtpEx)
			{
				if ((int)smtpEx.StatusCode != 442)
				{
					//					log.Error(string.Format("Unable to send email '{0}' to {1}. Smtp status code: {2}.",
					//						mailMessage.Subject,
					//						mailMessage.To.First().Address,
					//						smtpEx.StatusCode), smtpEx);
				}
				else
				{
					//					log.InfoFormat("Recipient mailbox unavailable '{0}'", mailMessage.To.First().Address);
				}

				return false;
			}
			catch (Exception ex)
			{
				//				log.Error(string.Format("Unable to send email '{0}' to {1}", mailMessage.Subject, mailMessage.To.First().Address), ex);
				return false;
			}
		}
	}
}
