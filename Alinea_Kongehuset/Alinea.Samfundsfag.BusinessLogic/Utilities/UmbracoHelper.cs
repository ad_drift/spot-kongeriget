﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using Alinea.Samfundsfag.BusinessLogic.Common;
using umbraco;
using umbraco.NodeFactory;

namespace Alinea.Samfundsfag.BusinessLogic.Utilities
{
	/// <summary>
	/// Umbraco helper methods.
	/// </summary>
	public static class UmbracoHelper
	{
		/// <summary>
		/// Gets the current Umbraco page node instance.
		/// </summary>
		public static Node CurrentPage
		{
			get
			{
				return Node.GetCurrent();
			}
		}

		/// <summary>
		/// Gets the parent Umbraco page node instance.
		/// </summary>
		public static Node ParentPage
		{
			get
			{
				return (Node)CurrentPage.Parent;
			}
		}

		/// <summary>
		/// Gets the Umbraco home page node.
		/// </summary>
		public static Node HomePage
		{
			get
			{
				return GetHomePageNode(CurrentPage);
			}
		}

		/// <summary>
		/// Gets the student page.
		/// </summary>
		public static Node StudentPage
		{
			get
			{
				return (Node)HomePage.ChildrenAsList.FirstOrDefault(node => node.NodeTypeAlias == Constants.NodeTypes.StudentMaster);
			}
		}

		/// <summary>
		/// Gets the teacher page.
		/// </summary>
		public static Node TeacherPage
		{
			get
			{
				return (Node)HomePage.ChildrenAsList.FirstOrDefault(node => node.NodeTypeAlias == Constants.NodeTypes.TeacherMaster);
			}
		}

		/// <summary>
		/// Gets the student search page.
		/// </summary>
		public static Node StudentSearchPage
		{
			get
			{
				return (Node)StudentPage.ChildrenAsList.FirstOrDefault(node => node.NodeTypeAlias == Constants.NodeTypes.SearchResults);
			}
		}

        /// <summary>
        /// Gets the teacher search page.
        /// </summary>
        public static Node TeacherSearchPage
        {
            get
            {
                return (Node)TeacherPage.ChildrenAsList.FirstOrDefault(node => node.NodeTypeAlias == Constants.NodeTypes.SearchResults);
            }
        }

		/// <summary>
		/// Gets the license required node.
		/// </summary>
		public static Node GetLicenseRequiredNode
		{
			get
			{
				return Node.GetNodeByXpath(string.Concat("/root/", Constants.NodeTypes.Homepage, "/", Constants.NodeTypes.LicenseRequired));
			}
		}

		/// <summary>
		/// Gets the Umbraco home page node based on the given node.
		/// </summary>
		/// <param name="currentNode"></param>
		/// <returns></returns>
		public static Node GetHomePageNode(Node currentNode)
		{
			if (currentNode.IsNull())
			{
				throw new ArgumentNullException("currentNode");
			}

			// See if the current page is the home page
			if (currentNode.NodeTypeAlias == Constants.NodeTypes.Homepage || currentNode.NodeTypeAlias == Constants.NodeTypes.CommonHomepage)
			{
				return currentNode;
			}

			// Loop the parent pages until we find the home page node
			Node node = (Node)currentNode.Parent;
			while (node != null)
			{
				if (node.NodeTypeAlias == Constants.NodeTypes.Homepage || node.NodeTypeAlias == Constants.NodeTypes.CommonHomepage)
				{
					return node;
				}
				node = (Node)node.Parent;
			}

			return node;
		}

		/// <summary>
		/// Returns the Umbraco node with the given ID.
		/// </summary>
		/// <param name="nodeId"></param>
		/// <returns></returns>
		public static Node GetNodeById(int nodeId)
		{
			return new Node(nodeId);
		}

		/// <summary>
		/// Returns the property from the given node.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public static Property GetProperty(Node node, string propertyName)
		{
			if (node.IsNull())
			{
				throw new ArgumentNullException("node");
			}

			return node.Properties[propertyName];
		}

		/// <summary>
		/// Returns the property for the current page.
		/// </summary>
		/// <param name="?"></param>
		/// <returns></returns>
		public static Property GetProperty(string propertyName)
		{
			return GetProperty(CurrentPage, propertyName);
		}

		/// <summary>
		/// Returns the dictionary value for the given key.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string GetDictionaryValue(string key)
		{
			return library.GetDictionaryItem(key);
		}

		/// <summary>
		/// Get List of Nodes by DocumentType alias.
		/// </summary>
		/// <param name="nodeTypeAlias">Array of DocumentType alias to match.</param>
		/// <param name="startNodeId">Select from this Nodes descendants.</param>
		public static List<Node> GetNodesByAlias(string[] nodeTypeAlias, int startNodeId = 0)
		{
			List<Node> nodes = new List<Node>();

			foreach (var alias in nodeTypeAlias)
			{
				var xpath = "";
				if (startNodeId > 0)
					xpath = string.Format("//*[@id={0}]//{1}[@isDoc]", startNodeId, alias);
				else
					xpath = string.Format("//{0}[@isDoc]", alias);

				XPathNodeIterator it = umbraco.library.GetXmlNodeByXPath(xpath);
				while (it.MoveNext())
				{
					int nodeId = Convert.ToInt32(it.Current.GetAttribute("id", it.Current.NamespaceURI));
					nodes.Add(new Node(nodeId));
				}
			}
			return nodes;
		}
	}
}
