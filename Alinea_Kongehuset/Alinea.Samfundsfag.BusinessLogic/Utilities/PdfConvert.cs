﻿using Pechkin;

namespace Alinea.Samfundsfag.BusinessLogic.Utilities
{
	public class PdfConvert
	{
		public static byte[] ConvertPageToPdf(string url, string cookieFile)
		{
			var globalConfig = new GlobalConfig();
			globalConfig.SetCookieTextFile(cookieFile);

			var pechkin = new Pechkin.Synchronized.SynchronizedPechkin(globalConfig);

			var objectConfig = new ObjectConfig();
			objectConfig.SetCreateExternalLinks(true)
				.SetLoadImages(true)
				.SetPageUri(url)
				.SetScreenMediaType(false);

			return pechkin.Convert(objectConfig);
		}
	}
}
