﻿namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	public partial class UserArticleTimelineItem
	{
		partial void OnCreated()
		{
		}

		public Models.UserArticleTimelineItem ToModel()
		{
			var model = new Models.UserArticleTimelineItem()
			{
				Id = this.Id,
				UserArticleId = this.UserArticleId,
				Label = this.Label,
				Title = this.Title,
				Description = this.Description,
				Position = this.Position,
				CreatedBy = this.CreatedBy,
				CreatedDate = this.CreatedDate,
				ModifiedBy = this.ModifiedBy,
				ModifiedDate = this.ModifiedDate,
				IsDeleted = this.IsDeleted
			};

			return model;
		}
	}
}
