﻿using System;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	partial class User
	{
		partial void OnCreated()
		{
			CreationDate = DateTime.Now;
		}
	}
}
