﻿using System;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	partial class School
	{
		partial void OnCreated()
		{
			CreationDate = DateTime.Now;
            LastSync = DateTime.Now;
		}
	}
}
