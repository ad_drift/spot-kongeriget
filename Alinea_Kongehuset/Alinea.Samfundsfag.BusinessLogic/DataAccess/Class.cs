﻿using System;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	partial class Class
	{
		partial void OnCreated()
		{
			CreationDate = DateTime.Now;
		}
	}
}
