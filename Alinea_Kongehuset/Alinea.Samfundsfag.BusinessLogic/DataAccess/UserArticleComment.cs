﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	public partial class UserArticleComment
	{
		partial void OnCreated()
		{
			CreatedDate = DateTime.Now;
		}

		public Models.UserArticleComment ToModel()
		{
			var model = new Models.UserArticleComment()
			{
				Id = this.Id,
				ArticleId = this.ArticleId,

				Comment = this.Comment,

				CreatedBy = this.CreatedBy,
				CreatedDate = this.CreatedDate,
				IsTeacherFeedback = this.IsTeacherFeedback,
				IsDeleted = this.IsDeleted
			};

			return model;
		}
	}
}
