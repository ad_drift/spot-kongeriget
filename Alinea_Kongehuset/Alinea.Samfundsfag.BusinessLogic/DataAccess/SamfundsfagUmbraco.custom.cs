﻿using System.Data.Linq;
using System.Linq;
using Futuristic.Web;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	partial class SamfundsfagUmbracoDataContext
	{
		public SamfundsfagUmbracoDataContext()
			: base(AppSettings.GetAppSetting("umbracoDbDSN"))
		{
			OnCreated();
		}

		partial void OnCreated()
		{
			var loadOptions = new DataLoadOptions();
			loadOptions.LoadWith<UserArticle>(c => c.UserArticleTimelineItems);
			loadOptions.AssociateWith<UserArticle>(c => c.UserArticleTimelineItems.Where(p => !p.IsDeleted).OrderBy(p => p.Position).ThenBy(p => p.Id));
			LoadOptions = loadOptions;
		}
	}
}
