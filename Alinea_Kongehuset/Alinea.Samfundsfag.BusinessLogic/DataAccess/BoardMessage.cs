﻿using System;
using System.Collections.Generic;
using CoreM41.Diagnostics.Logging;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
    partial class BoardMessage
    {
        partial void OnCreated()
        {
            CreationDate = DateTime.Now;
        }

		/// <summary>
		/// Returns a model instance of this board message.
		/// </summary>
		/// <returns></returns>
		public Models.BoardMessage ToModel(bool isRead, bool textAsHtml = true)
		{
            try
            {
                var classes = new List<Class>();
                foreach (var boardMessageClass in this.BoardMessageClasses)
                {
                    classes.Add(boardMessageClass.Class);
                }

                return new Models.BoardMessage(this, classes, textAsHtml, isRead);
            }
            catch (Exception exception)
            {
                Logger.LogError(exception);
            }

            return new Models.BoardMessage(this, new List<Class>(), textAsHtml, isRead);
		}
    }
}