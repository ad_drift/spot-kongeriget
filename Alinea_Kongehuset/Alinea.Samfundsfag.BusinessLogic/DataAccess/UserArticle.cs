﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alinea.Samfundsfag.BusinessLogic.Services;

namespace Alinea.Samfundsfag.BusinessLogic.DataAccess
{
	public partial class UserArticle
	{
		partial void OnCreated()
		{
			CreatedDate = DateTime.Now;
		}

		public Models.UserArticle ToModel()
		{
			var model = new Models.UserArticle()
			{
				Id = this.Id,

				Title = this.Title,
				Introduction = this.Introduction,
				Text1 = this.Text1,
				Text1SortOrder = this.Text1SortOrder,
				Text2 = this.Text2,
				Text2SortOrder = this.Text2SortOrder,
				Image1Text = this.Image1Text,
				Image1SortOrder = this.Image1SortOrder,
				Image2Text = this.Image2Text,
				Image2SortOrder = this.Image2SortOrder,
				Image3Text = this.Image3Text,
				Image3SortOrder = this.Image3SortOrder,
				Video1YouTubeUrl = this.Video1YouTubeUrl,
				Video1SortOrder = this.Video1SortOrder,
				Audio1Text = this.Audio1Text,
				Audio1SortOrder = this.Audio1SortOrder,
				TimelineHeadline = this.TimelineHeadline,
				TimelineActiveItem = this.TimelineActiveItem,
				TimelineItems = this.UserArticleTimelineItems.Select(c => c.ToModel()).ToList(),
				AssignmentId = this.AssignmentId,

				ClassId = this.ClassId,
				CreatedBy = this.CreatedBy,
				CreatedDate = this.CreatedDate,
				ModifiedBy = this.ModifiedBy,
				ModifiedDate = this.ModifiedDate,
				IsPublished = this.IsPublished,
				PublishedDate = this.PublishedDate,
				IsDeleted = this.IsDeleted
			};

			if (model.TimelineActiveItem == 0 && model.TimelineItems.Any())
			{
				model.TimelineActiveItem = model.TimelineItems.First().Id;
			}

			return model;
		}
	}
}
