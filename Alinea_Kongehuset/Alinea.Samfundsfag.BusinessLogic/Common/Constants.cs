﻿namespace Alinea.Samfundsfag.BusinessLogic.Common
{
    public static class Constants
    {
        public const string DateFormat = "d. MMMM yyyy";
        public const int PreviewLength = 12;
        public const string MessageId = "MessageId";

        public static class Crawler
        {
            public const string Token = "SudEPavukunaSpaB";
            public const string LatestCrawlText = "Seneste opdatering: ";
        }

        public static class Teacher
        {
            public const string SeeSiteAsStudent = "Se sitet som elev";
            public const string SelectTeacherClasses = "Vælg klasser";
        }

        public static class OrderTrialSubscription
        {
            public const string Headline = "headline";
            public const string Description = "description";
            public const string RecipientEmail = "alineaE-mail";
            public const string EmailSubject = "emneIMail";
            public const string EmailBody = "emailIndhold";
            public const string SuccessMessage = "successMessage";
            public const string ErrorMessage = "errorMessage";
        }

        public static class NodeTypes
        {
            public const string LicenseRequired = "LicenseRequired";
            public const string Homepage = "Homepage";
            public const string NewsArticle = "NewsArticle";
            public const string Angle = "Angle";
            public const string StudentMaster = "StudentMaster";
            public const string TeacherMaster = "TeacherMaster";
            public const string SearchResults = "SearchResults";

            public const string CommonHomepage = "CommonHomepage";

        }

        public static class NodeDocumentTypes
        {
            public const string Root = "root";
            public const string Homepage = "Homepage";
            public const string StudentMaster = "StudentMaster";
            public const string TeacherMaster = "TeacherMaster";
            public const string SingleColumn = "SingleCol";
            public const string Folder = "Folder";
            public const string Angle = "Angle";
            public const string Abbreviation = "Abbreviation";
            public const string Pictures = "Pictures";
            public const string Topic = "Topic";
            public const string Profiles = "Profiles";
            public const string Profile = "Profile";
            public const string NewsArticle = "NewsArticle";
            public const string Method = "Method";
            public const string Answer = "Answer";

            public const string UserArticle = "UserArticle";
            public const string UserArticleList = "UserArticleList";
            public const string UserArticleListMyPage = "UserArticleListMyPage";
            public const string CreateUserArticle = "CreateUserArticle";
            public const string AllArticles = "AllArticles";
            public const string PdfPage = "PdfPage";
        }

        public static class NodeNames
        {
            public const string News = "Spotlisten";
        }

        public static class NodePropertyNames
        {
            public const string Identifier = "identifier";
            public const string Explanation = "explanation";
            public const string Abbreviation = "abbreviation";
            public const string Title = "title";
            public const string Name = "name";

            public static class Crawler
            {
                public const string LatestCrawling = "latestCrawling";
            }

            public static class Timeline
            {
                public const string Title = "title";
                public const string Label = "label";
                public const string Description = "description";
                public const string Image = "image";
                public const string Position = "edit";
            }

            public static class FormPages
            {
                public const string SuccesMessage = "succesMessage";
                public const string SelectClassError = "selectClassError";

                public const string MessageError = "messageError";
                public const string CreateMessageTitle = "createMessageTitle";
                public const string EditMessageTitle = "editMessageTitle";

                public const string QuestionError = "questionError";
                public const string AnswerError = "answerError";
                public const string SelectTopicError = "selectTopicError";
                public const string CreatePollTitle = "createPollTitle";
                public const string EditPollTitle = "editPollTitle";
                public const string CorrectAnswerError = "correctAnswerError";
                public const string HeadlineError = "headlineError";
            }
        }

        public static class Fields
        {
            public const string Tags = "tags";
            public const string Content = "content";
            public const string Title = "title";
            public const string Abstract = "abstract";
            public const string Name = "name";
            public const string YouSearchedFor = "youSearchedFor";
            public const string Back = "back";
            public const string NoSearchResults = "noSearchResults";
            public const string ReadMore = "readMore";
            public const string NodeTypeAlias = "nodeTypeAlias";
            public const string LeftColumn = "leftColumn";
            public const string RightColumn = "rightColumn";
            public const string MenuTitle = "menuTitle";
            public const string Description = "description";
            public const string Experience = "experience";
        }

        public static class DictionaryKeys
        {
            public const string Search = "Search";
        }

        public static class Xslt
        {
            public const string YouTubePrefix = "http://www.youtube-nocookie.com/embed/";

            public static class Html
            {
                public const string Abbreviation = "<abbr title=\"{0}\">{1}</abbr>";
                public const string Profile = "<a class=\"lb-content\" href=\"{0}\">{1}</a>";
                public const string ObjectiveCheckbox = "<input type=\"checkbox\" disabled=\"disabled\" {0} />";
                public const string ObjectiveCheckedAttribute = "checked=\"checked\"";
            }
        }

        public static class States
        {
            public const string Published = "Publiceret";
            public const string WorkInProgress = "Under udarbejdelse";
        }

        public static class RequestParameters
        {
            public const string AssignmentNodeId = "AssignmentNodeId";
        }

    }
}
