﻿using CoreM41.Common.Enums;

namespace Alinea.Samfundsfag.BusinessLogic.Common.Enums
{
	/// <summary>
	/// Defines the supported query string variables.
	/// </summary>
	public enum QueryStringVariables
	{
		[QueryStringAttribute("s")]
		SearchString,
		Id
	}
}
