﻿namespace Alinea.Samfundsfag.BusinessLogic.Common.Enums
{
	/// <summary>
	/// Defines the supported user access settings for the frontend pages.
	/// </summary>
	public enum UserAccess
	{
		Everyone,
		StudentOrTeacher,
		Teacher,
        Student
	}
}
