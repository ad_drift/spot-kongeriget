﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco.NodeFactory;
using Alinea.Samfundsfag.BusinessLogic.Models;
using Alinea.Samfundsfag.BusinessLogic.Common;
using umbraco.cms.businesslogic.web;
using umbraco.cms.businesslogic;

namespace Alinea.Samfundsfag.BusinessLogic.Services.Umbraco
{
	public class TimelineService
	{
		public TimelineService()
		{

		}

		/// <summary>
		/// Gets a TimelineEntry object from the given umbracoId
		/// </summary>
		public static TimelineEntry GetTimelineEntry(int nodeId)
		{
			return GetTimelineEntry(new Node(nodeId));
		}

		/// <summary>
		/// Gets a TimelineEntry object from the given Umbraco node
		/// </summary>
		public static TimelineEntry GetTimelineEntry(Node node)
		{
			TimelineEntry timelineEntry = new TimelineEntry();

			if (node.GetProperty(Constants.NodePropertyNames.Timeline.Title) != null)
			{
				timelineEntry.Title = node.GetProperty(Constants.NodePropertyNames.Timeline.Title).Value;
			}
			if (node.GetProperty(Constants.NodePropertyNames.Timeline.Label) != null)
			{
				timelineEntry.Label = node.GetProperty(Constants.NodePropertyNames.Timeline.Label).Value;
			}
			if (node.GetProperty(Constants.NodePropertyNames.Timeline.Description) != null)
			{
				timelineEntry.Description = node.GetProperty(Constants.NodePropertyNames.Timeline.Description).Value;
			}
			if (node.GetProperty(Constants.NodePropertyNames.Timeline.Image) != null)
			{
				timelineEntry.Image = node.GetProperty(Constants.NodePropertyNames.Timeline.Image).Value;
			}
			if (node.GetProperty(Constants.NodePropertyNames.Timeline.Position) != null)
			{
				timelineEntry.Position = node.GetProperty(Constants.NodePropertyNames.Timeline.Position).Value;
			}
			if (timelineEntry.Position.IsNullOrEmpty())
			{
				timelineEntry.Position = "0";
			}

			return timelineEntry;
		}

		/// <summary>
		/// Gets the given nodes siblings
		/// </summary>
		public static List<TimelineEntry> GetSiblingTimelineEntries(int nodeId)
		{
			Document document = new Document(nodeId);
			Node node = new Node(nodeId);
			List<TimelineEntry> list = new List<TimelineEntry>();
			foreach (CMSNode childCmsNode in document.Parent.Children)
			{
				Document childDocument = new Document(childCmsNode.Id);
				if (childDocument.Published)
				{
					Node childNode = new Node(childDocument.Id);
					if (childNode.Id != node.Id)
					{
						list.Add(GetTimelineEntry(childNode));
					}
				}
			}
			return list;
		}

		/// <summary>
		/// Gets the given nodes children as TimelineItems
		/// </summary>
		public static List<TimelineEntry> GetChildTimelineEntries(int nodeId)
		{
			Node node = new Node(nodeId);
			List<TimelineEntry> list = new List<TimelineEntry>();
			foreach (Node childNode in node.ChildrenAsList)
			{
				Document childDocument = new Document(childNode.Id);
				if (childDocument.Published)
				{
					list.Add(GetTimelineEntry(childNode));
				}
			}
			return list;
		}
	}
}
