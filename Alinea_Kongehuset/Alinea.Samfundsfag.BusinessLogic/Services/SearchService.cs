﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Properties;
using Alinea.Samfundsfag.BusinessLogic.Utilities;
using CoreM41.Common;
using Examine;
using Examine.SearchCriteria;
using umbraco.NodeFactory;
using CoreM41.Diagnostics.Logging;
using umbraco.cms.businesslogic.Tags;
using Futuristic.Umbraco;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	public static class SearchService
	{
		#region Fields & Constants

		private static object cacheLock = new object();
		private const string AllTagsCacheKey = "Services_Tags_AllTags";

		#endregion

		#region Private Methods

		private static string GetTagsCacheKey(string searchString)
		{
			// Update this to support non null/empty search string 
			return AllTagsCacheKey;
		}

		private static string[] PerformSearchTags(string searchString = null)
		{
			var keywords = new List<string>();

			foreach (var tag in Tag.GetTags("default"))
			{
				if (tag.NodeCount > 0)
				{
					keywords.Add(tag.TagCaption);
				}
			}

			/* Removed by request from Alinea
			var nodes = UmbracoHelper.GetNodesByAlias(new string[] { Constants.NodeDocumentTypes.Angle, Constants.NodeDocumentTypes.NewsArticle, Constants.NodeDocumentTypes.Method });
			foreach (var node in nodes)
			{
				var prop = node.GetProperty("title");
				if (prop != null && !string.IsNullOrWhiteSpace(prop.Value))
				{
					keywords.Add(prop.Value);
				}
			}
			 * */

			/*
			var searcher = ExamineManager.Instance.SearchProviderCollection["TagsSearcher"];
			var searchCriteria = searcher.CreateSearchCriteria();

			ISearchCriteria query;

			if (searchString.IsNullOrEmpty())
			{
                query = searchCriteria.NodeTypeAlias(Constants.NodeTypes.NewsArticle).Or().NodeTypeAlias(Constants.NodeTypes.Angle).Compile();
			}
			else
			{
				// We do not support it since it would require us to do a search on each tag list as well but should be pretty simple to implement later.
				// But we will have to do something about the cache key as well
				//query = searchCriteria.Field(Constants.Fields.Tags, searchString.Trim().MultipleCharacterWildcard()).Compile();
				throw new NotImplementedException("Search string support is not implemented");
			}

			var searchResults = searcher.Search(query);

			foreach (var searchResult in searchResults)
			{
				if (searchResult.Fields.ContainsKey(Constants.Fields.Tags) && !string.IsNullOrEmpty(searchResult.Fields[Constants.Fields.Tags]))
				{
					foreach (string keyword in searchResult.Fields[Constants.Fields.Tags].Split(Const.Characters.Comma))
					{
						if (!keywords.Contains(keyword))
						{
							keywords.Add(keyword);
						}
					}
				}
			}
			*/

			keywords = keywords.Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();
			keywords.Sort();
			return keywords.ToArray();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Searches the tags and returns the result. If the search string is null or empty ALL tags are returned.
		/// NOTE: Does not support non null/empty search string.
		/// </summary>
		/// <param name="searchString"></param>
		/// <param name="skipCache"></param>
		/// <returns></returns>
		public static string[] SearchTags(string searchString = null, bool skipCache = false)
		{
			if (skipCache)
			{
				return PerformSearchTags(searchString);
			}
			else
			{
				return MemoryCache.Default.Get<string[]>(GetTagsCacheKey(searchString), cacheLock, Settings.Default.TagsCacheTimeout, () => PerformSearchTags(searchString));
			}
		}

		/// <summary>
		/// Searches the site with the given search string and returns the result.
		/// </summary>
		/// <param name="searchString"></param>
		/// <returns></returns>
		public static Models.SearchResult[] SearchSite(string searchString, bool useTeacherIndex)
		{
			var searcher = ExamineManager.Instance.SearchProviderCollection[useTeacherIndex ? "TeacherSearcher" : "DefaultSearcher"];
			var searchCriteria = searcher.CreateSearchCriteria(BooleanOperation.Or);

			var query = searchCriteria.
				Field(Constants.Fields.Tags, searchString).
				Or().
				Field(Constants.Fields.Title, searchString).
				Or().
				Field(Constants.Fields.Content, searchString).
				Or().
				Field(Constants.Fields.Abstract, searchString).
				Or().
				Field(Constants.Fields.Name, searchString).
				Or().
				Field(Constants.Fields.LeftColumn, searchString).
				Or().
				Field(Constants.Fields.RightColumn, searchString).
				Or().
				Field(Constants.Fields.Description, searchString).

				Compile();

			var searchResults = searcher.Search(query);

			return searchResults
				.Select(searchResult =>
				{
					var node = NodeHelper.GetNodeById(searchResult.Id);

					if(node != null)
					{
						string content = node.GetPropertyAsString(Constants.Fields.Abstract);
						if (string.IsNullOrEmpty(content))
							content = node.GetPropertyAsString(Constants.Fields.Experience);


						return new Models.SearchResult()
						{
							NodeId = searchResult.Id,
							Title = searchResult.Fields.ContainsKey(Constants.Fields.Title) ? searchResult.Fields[Constants.Fields.Title] : searchResult.Fields[Constants.Fields.Name],
							Content = content,
							NodeTypeAlias = searchResult.Fields[Constants.Fields.NodeTypeAlias],
							Date = node.CreateDate,
							Url = node.Url,

						};
					}
					else
					{
						return new Models.SearchResult() { NodeId = 0};
					}
				})
				.Where(sr => sr.NodeId != 0)
				.ToArray();
		}

		#endregion
	}
}
