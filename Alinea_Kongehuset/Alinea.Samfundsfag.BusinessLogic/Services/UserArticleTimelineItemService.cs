﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling User Article Timeline Items.
	/// </summary>
	public class UserArticleTimelineItemService : ServiceBase
	{
		#region Constructors

		public UserArticleTimelineItemService()
			: base()
		{
		}

		public UserArticleTimelineItemService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		public void DeleteUserArticleTimelineItem(int timelineItemId, int userId)
		{
			var us = new UserService();
			var user = us.GetUser(userId);

			var timelineItem = (from ua in this.DataContext.UserArticleTimelineItems
								where ua.Id == timelineItemId
								select ua).FirstOrDefault();

			if (timelineItem != null)
			{
				var article = (from ua in this.DataContext.UserArticles
							   where ua.Id == timelineItem.UserArticleId
							   select ua).FirstOrDefault();
				if (article != null && article.TimelineActiveItem == timelineItemId)
				{
					article.TimelineActiveItem = 0;
				}

				if (timelineItem.CreatedBy == user.UserId || user.IsTeacher)
				{
					timelineItem.IsDeleted = true;
					timelineItem.ModifiedBy = userId;
					timelineItem.ModifiedDate = DateTime.Now;
				}
				this.DataContext.SubmitChanges();
			}
		}
	}
}