﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Models;
using StackExchange.Profiling;
using umbraco.NodeFactory;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.Models.Json;
using CoreM41.Diagnostics.Logging;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	public class PollService : ServiceBase
	{
		#region Contructors

		public PollService()
			: base()
		{
		}

		public PollService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		#region Methods

		#region Public

		public List<Models.Poll> GetTeacherPolls(int userId)
		{
			return DataContext.Polls.Where(poll => poll.CreatedBy == userId)
									.AsEnumerable()
									.Select(poll => new Models.Poll(poll))
									.ToList();
		}

		public List<PollTopic> GetPollTopics()
		{
			return Node.GetNodeByXpath("/root/Homepage/StudentMaster").ChildrenAsList
				.Where(topic => topic.NodeTypeAlias == Constants.NodeDocumentTypes.Topic)
				.Select(topic => new Models.PollTopic(topic)).ToList();
		}

		public void UpsertPoll(int? pollId, List<int> selectedClasses, int selectedTopic, string question, List<Tuple<int, string, bool>> answers, bool isPublished, bool isQuiz, string headline)
		{
			DataAccess.Poll poll;
			int userId = SessionController.CurrentUserId;

			if (pollId.HasValue && pollId.Value > 0)
			{
				poll = GetPoll(pollId.Value);
			}
			else
			{
				//insert poll
				poll = new DataAccess.Poll()
				{
					CreatedBy = userId,
					CreationDate = DateTime.Now,
					IsDeleted = false,
				};
				DataContext.Polls.InsertOnSubmit(poll);
			}

			poll.IsPublished = isPublished;
			poll.LastModified = DateTime.Now;
			poll.ModifiedBy = userId;
			poll.Question = question;
			poll.CategoryId = selectedTopic;
			poll.IsQuiz = isQuiz;
			poll.Headline = headline;

			DataContext.SubmitChanges();

			//insert answers with the poll id
			UpsertPollAnswers(answers, poll, userId);

			//insert pollclasses
			UpsertPollClasses(selectedClasses, poll);

			DataContext.SubmitChanges();
		}

		public void DeletePoll(string _pollId)
		{
			int pollId = 0;
			int.TryParse(_pollId, out pollId);
			if (pollId > 0)
			{
				DataAccess.Poll poll = GetPoll(pollId);

				DeletePoll(poll);
			}
		}

		public DataAccess.Poll GetPoll(int pollId)
		{
			return DataContext.Polls.FirstOrDefault(poll => poll.PollId == pollId);
		}

		public PollClass GetPollClass(int classId, int pollId)
		{
			return DataContext.PollClasses.FirstOrDefault(pollClass =>
				pollClass.ClassId == classId &&
				pollClass.PollId == pollId);
		}

		#endregion

		#region Private

		private void DeletePoll(DataAccess.Poll poll)
		{
			DataContext.PollUserAnswers.DeleteAllOnSubmit(
				DataContext.PollUserAnswers
				.Where(pollUserAnswer => 
					pollUserAnswer.PollId == poll.PollId)
			);

			DataContext.PollAnswers.DeleteAllOnSubmit(poll.PollAnswers);
			DataContext.PollClasses.DeleteAllOnSubmit(poll.PollClasses);

			DataContext.Polls.DeleteOnSubmit(poll);
			DataContext.SubmitChanges();
		}

		private void UpsertPollClasses(List<int> selectedClasses, DataAccess.Poll poll)
		{
			DataContext.PollClasses.DeleteAllOnSubmit(poll.PollClasses);

			foreach (int classId in selectedClasses)
			{
				PollClass pollClass = new PollClass()
				{
					ClassId = classId,
					PollId = poll.PollId
				};
				DataContext.PollClasses.InsertOnSubmit(pollClass);
			}

			DataContext.SubmitChanges();
		}

		private void UpsertPollAnswers(List<Tuple<int, string, bool>> answers, DataAccess.Poll poll, int userId)
		{
			byte sortOrder = 1;
			foreach (Tuple<int, string, bool> answer in answers)
			{
				PollAnswer pollAnswer = GetPollAnswer(answer.Item1);

				if (pollAnswer == null)
				{
					pollAnswer = new PollAnswer()
					{
						CreatedBy = userId,
						CreationDate = DateTime.Now,
						IsDeleted = false,
						PollId = poll.PollId
					};
					DataContext.PollAnswers.InsertOnSubmit(pollAnswer);

					sortOrder++;
				}
				pollAnswer.SortOrder = sortOrder;
				pollAnswer.Answer = answer.Item2;
				pollAnswer.LastModified = DateTime.Now;
				pollAnswer.ModifiedBy = userId;
				pollAnswer.IsCorrect = answer.Item3;

				sortOrder++;

				DataContext.SubmitChanges();
			}
		}

		private PollAnswer GetPollAnswer(int answerId)
		{
			return DataContext.PollAnswers.FirstOrDefault(pollAnswer => pollAnswer.PollAnswerId == answerId);
		}

		#endregion

		#region Internal

		internal List<DataAccess.Poll> GetClassPolls(int classId)
		{
			return DataContext.Polls.Where(poll => poll.PollClasses.Any(pollClass => pollClass.ClassId == classId)).OrderByDescending(poll => poll.CreationDate).ToList();
		}

		#endregion

		#region Static

		public PollsResponse HandlePollVote(PollsRequest request)
		{
			using (MiniProfiler.Current.Step("Polls.Process.HandlePollVote"))
			{
				var pollsResponse = new PollsResponse();
				var userId = SessionController.CurrentUserId;

				var isUmbracoPoll = !request.Id.ToString().StartsWith("db");
				var pollId = 0;
				int.TryParse(request.Id.TrimStart('d', 'b'), out pollId);

				var pollUserAnswer = GetPollUserAnswer(userId, pollId);

				if (pollId > 0)
				{
					if (pollUserAnswer == null)
					{
						try
						{
							using (MiniProfiler.Current.Step("Polls.Process.HandlePollVote.SubmitChanges"))
							{
								pollUserAnswer = new PollUserAnswer()
									{
										CreationDate = DateTime.Now,
										IsUmbracoPoll = isUmbracoPoll,
										PollId = pollId,
										UserId = userId,
										AnswerId = request.AnswerId
									};
								DataContext.PollUserAnswers.InsertOnSubmit(pollUserAnswer);
								DataContext.SubmitChanges();
							}
						}
						catch (Exception exception)
						{
							Logger.LogError(exception);
						}
					}
				}

				pollsResponse.Responses = GetPollResponses(pollId, isUmbracoPoll);

				return pollsResponse;
			}
		}

		public PollUserAnswer GetPollUserAnswer(int userId, int pollId)
		{
			using (MiniProfiler.Current.Step("Polls.Process.HandlePollVote.GetPollUserAnswer"))
			{
				return DataContext.PollUserAnswers.FirstOrDefault(
					dbPollAnswer => dbPollAnswer.PollId == pollId &&
									dbPollAnswer.UserId == userId);
			}
		}

		public List<PollsResponse.Response> GetPollResponses(int pollId, bool isUmbracoPoll)
		{
			using (MiniProfiler.Current.Step("Polls.Process.HandlePollVote.GetPollResponses"))
			{
				List<PollsResponse.Response> list = new List<PollsResponse.Response>();
				List<Tuple<string, int>> countList = new List<Tuple<string, int>>();
				if (isUmbracoPoll)
				{
					Node node = new Node(pollId);
					if (node != null)
					{
						foreach (Node answer in node.Children)
						{
							countList.Add(GetPollAnswerTuple(answer.Id));
						}
					}
				}
				else
				{
					DataAccess.Poll poll = GetPoll(pollId);

					foreach (PollAnswer pollAnswer in poll.PollAnswers)
					{
						countList.Add(GetPollAnswerTuple(pollAnswer.PollAnswerId));
					}
				}
				int totalAnswers = countList.Sum(item => item.Item2);
				foreach (Tuple<string, int> pollAnswer in countList)
				{
					PollsResponse.Response response = new PollsResponse.Response();
					response.InputId = pollAnswer.Item1;
					response.Percent = ((pollAnswer.Item2*100)/totalAnswers).ToString();
					list.Add(response);
				}

				return list;
			}
		}

		private Tuple<string, int> GetPollAnswerTuple(int answerId)
		{
			Tuple<string, int> tuple = new Tuple<string, int>(
				"answer" + answerId,
				DataContext.PollUserAnswers.Where(pollUserAnswer => pollUserAnswer.AnswerId == answerId).Count());
			return tuple;
		}

		#endregion

		#endregion
	}
}
