﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Controller;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling User Article Comments.
	/// </summary>
	public class UserArticleCommentService : ServiceBase
	{
		#region Constructors

		public UserArticleCommentService()
			: base()
		{
		}

		public UserArticleCommentService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		public List<Models.UserArticleComment> GetComments(int userArticleId, bool teacherFeedback = false)
		{
			var userArticleComments = from uac in this.DataContext.UserArticleComments
									  where !uac.IsDeleted && uac.ArticleId == userArticleId && uac.IsTeacherFeedback == teacherFeedback
									  select uac;

			return userArticleComments.Select(a => a.ToModel()).ToList();
		}

		public int GetUnreadCommentsCount(int classId, int userId)
		{
			var articlesWithUnreadComments = from ua in this.DataContext.UserArticles
											 where !ua.IsDeleted && ua.IsPublished && ua.ClassId == classId
											 let userIsAuthor = userId == ua.CreatedBy
											 let userIsCoAuthor = ua.UserArticleCoAuthors.Count(uaca => uaca.UserId == userId) > 0
											 let userIsParticipating = ua.UserArticleComments.Count(uac => !uac.IsDeleted && uac.CreatedBy == userId) > 0
											 let lastSeen = ua.UserArticleLastSeens.Where(uals => uals.UserArticleId == ua.Id && uals.UserId == userId).Select(uals => uals.LastSeenDate).FirstOrDefault() as DateTime?
											 let unreadComments = ua.UserArticleComments.Where(uac => !uac.IsDeleted && !uac.IsTeacherFeedback && (lastSeen == null || uac.CreatedDate > lastSeen)).Count()
											 let unreadTeacherfeedback = ua.UserArticleComments.Where(uac => !uac.IsDeleted && uac.IsTeacherFeedback && (lastSeen == null || uac.CreatedDate > lastSeen)).Count()
											 select new
											 {
												 ArticleId = ua.Id,
												 IsAuthor = userIsAuthor,
												 IsCoAuthor = userIsCoAuthor,
												 IsParticipating = userIsParticipating,
												 UnreadComments = unreadComments,
												 UnreadTeacherFeedback = unreadTeacherfeedback
											 };

			if (articlesWithUnreadComments.Any())
			{
				var count1 = 0;
				if (articlesWithUnreadComments.Where(awuc => awuc.IsAuthor || awuc.IsCoAuthor || SessionController.IsCurrentUserTeacher).Any())
				{
					count1 = articlesWithUnreadComments.Where(awuc => awuc.IsAuthor || awuc.IsCoAuthor || SessionController.IsCurrentUserTeacher).Sum(awuc => awuc.UnreadTeacherFeedback);
				}

				var count2 = 0;
				if (articlesWithUnreadComments.Where(awuc => awuc.IsAuthor || awuc.IsCoAuthor || awuc.IsParticipating || SessionController.IsCurrentUserTeacher).Any())
				{
					count2 = articlesWithUnreadComments.Where(awuc => awuc.IsAuthor || awuc.IsCoAuthor || awuc.IsParticipating || SessionController.IsCurrentUserTeacher).Sum(awuc => awuc.UnreadComments);
				}

				return count1 + count2;
			}
			return 0;
		}

		public void CreateComment(int userArticleId, int userId, string comment, bool isTeacherFeedback = false)
		{
			if (!string.IsNullOrWhiteSpace(comment))
			{
				var userArticleComment = new UserArticleComment()
				{
					ArticleId = userArticleId,
					Comment = comment,
					IsTeacherFeedback = isTeacherFeedback,
					CreatedBy = userId,
					CreatedDate = DateTime.Now
				};

				this.DataContext.UserArticleComments.InsertOnSubmit(userArticleComment);
				this.DataContext.SubmitChanges();
			}
		}

		public void DeleteComment(int commentId, int userId)
		{
			var us = new UserService();
			var user = us.GetUser(userId);

			var comment = (from ua in this.DataContext.UserArticleComments
							where ua.Id == commentId
							select ua).FirstOrDefault();

			if (comment != null)
			{

				if (comment.CreatedBy == user.UserId || user.IsTeacher)
				{

					comment.IsDeleted = true;
					comment.ModifiedBy = userId;
					comment.ModifiedDate = DateTime.Now;
					this.DataContext.SubmitChanges();
				}
			}
		}
	}
}
