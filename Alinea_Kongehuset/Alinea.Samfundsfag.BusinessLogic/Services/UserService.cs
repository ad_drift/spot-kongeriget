﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Alinea.Core.Common.Enums;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Properties;
using Alinea.Samfundsfag.BusinessLogic.Utilities;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling users.
	/// </summary>
	public class UserService : ServiceBase
	{
		#region Constructors

		public UserService()
			: base()
		{
		}

		public UserService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets all the users.
		/// </summary>
		private IQueryable<User> Users
		{
			get
			{
				return DataContext.Users;
			}
		}

		/// <summary>
		/// Gets the non-teachers.
		/// </summary>
		private IQueryable<User> Students
		{
			get
			{
				return Users.
					Where(user => !user.IsTeacher);
			}
		}

		/// <summary>
		/// Gets the teachers.
		/// </summary>
		private IQueryable<User> Teachers
		{
			get
			{
				return Users.
					Where(user => user.IsTeacher);
			}
		}

		#endregion

		#region Methods

		private IQueryable<User> GetUsersForClass(int? classId = null)
		{
			if (!classId.HasValue)
			{
				classId = SessionController.CurrentUserClass.ClassId;
			}

			return DataContext.UserClasses.
				Where(userClass => userClass.ClassId == classId.Value).
				OrderBy(userClass => userClass.User.Name).
				Select(userClass => userClass.User);
		}

		/// <summary>
		/// Adds the user class mapping if it does not exist.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="classId"></param>
		private void UpsertUserClass(int userId, int classId)
		{
			if (DataContext.UserClasses.None(dbUserClass => dbUserClass.UserId == userId && dbUserClass.ClassId == classId))
			{
				UserClass userClassInstance = new UserClass()
				{
					UserId = userId,
					ClassId = classId
				};
				DataContext.UserClasses.InsertOnSubmit(userClassInstance);
				DataContext.SubmitChanges();
			}
		}

		private User UpsertUser(UniLoginUser uniLoginUser, out bool forceUpdate)
		{
			return UpsertUser((UniLoginUserSimple)uniLoginUser, out forceUpdate, primaryInstitutionId: uniLoginUser.PrimaryInstitutionId, primaryInstitutionName: uniLoginUser.PrimaryInstitutionName);
		}

		/// <summary>
		/// Inserts an new user if one does not exists and updates the existing one if the configured AlineaSsoUserSynchronizationTimeSpan time has passed since the last login.
		/// </summary>
		/// <param name="uniLoginUser"></param>
		/// <param name="forceUpdate"></param>
		/// <returns></returns>
		private User UpsertUser(UniLoginUserSimple uniLoginUser, out bool forceUpdate, string primaryInstitutionId = null, string primaryInstitutionName = null, bool isLogin = true)
		{
			User user = DataContext.Users.SingleOrDefault(dbUser => dbUser.UniLoginId == uniLoginUser.UserId);
			if (user.IsNull())
			{
				user = new User()
				{
					UniLoginId = uniLoginUser.UserId.Max(20)
				};
				DataContext.Users.InsertOnSubmit(user);
			}

			// Always update user type (demand from Alinea/UNI-c)
			user.IsTeacher = (uniLoginUser.UserType == UniLoginUserType.Teacher);
			if (isLogin)
			{
				user.LastLoginDate = DateTime.Now;
			}

			// Force an update if the configured last update time has passed since the last login
			forceUpdate = UniLoginController.RequiresUniLoginUpdate(user.LastModified);
			if (forceUpdate)
			{
				user.Name = uniLoginUser.Name.Max(100);

				if (!primaryInstitutionId.IsNullOrEmpty())
				{
					user.PrimaryInstitutionId = primaryInstitutionId.Max(50);
				}
				if (!primaryInstitutionName.IsNullOrEmpty())
				{
					user.PrimaryInstitutionName = primaryInstitutionName.Max(150);
				}
			}

			DataContext.SubmitChanges();

			return user;
		}

		/// <summary>
		/// Returns the user with the given ID.
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		internal User GetUser(int userId)
		{
			return Users.FirstOrDefault(user => user.UserId == userId);
		}

		/// <summary>
		/// Returns the user with the given UNI login user ID.
		/// </summary>
		/// <param name="uniLoginUserId"></param>
		/// <returns></returns>
		internal User GetUser(string uniLoginUserId)
		{
			return Users.FirstOrDefault(user => user.UniLoginId == uniLoginUserId);
		}

		/// <summary>
		/// Event method to be called when a user should be synchronized.
		/// Will only synchronize the user if the sender object is of the type UniLoginUser.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void OnSynchronizeUser(object sender, EventArgs e)
		{
			UniLoginUser uniLoginUser = sender as UniLoginUser;
			if (uniLoginUser.IsNull())
			{
				return;
			}

			using (var service = new UserService())
			{
				service.SynchronizeUser(uniLoginUser);
			}
		}

		/// <summary>
		/// Redirects the user to the license required page.
		/// </summary>
		public static void HandleAccessDenied()
		{
			if (UmbracoHelper.GetLicenseRequiredNode.Id != UmbracoHelper.CurrentPage.Id)
			{
				HttpContext.Current.Response.Redirect(UmbracoHelper.GetLicenseRequiredNode.NiceUrl);
			}
		}

		/// <summary>
		/// Event method called when a UNI login user ID is not present in the UNI login controller.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void OnRequireUniLoginUserId(object sender, EventArgs e)
		{
			if (!AuthenticationService.Instance.HasAccessToProduct(Settings.Default.AlineaProductKey, Settings.Default.AlineaProductHash))
			{
				HandleAccessDenied();
			}
		}

		/// <summary>
		/// Event method called when the UNI login user has logged in. Will populate the uni login user values if the user exists in the database.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void OnUserLoggedIn(object sender, EventArgs e)
		{
			UniLoginUser uniLoginUser = sender as UniLoginUser;
			if (uniLoginUser.IsNull())
			{
				return;
			}

			using (var userService = new UserService())
			{
				using (var schoolClassService = new SchoolClassService(userService.DataContext))
				{
					// Locate the user 
					var user = userService.GetUser(uniLoginUser.UserId);

					if (user != null)
					{
						bool forceUpdate;
						user = userService.UpsertUser(uniLoginUser, out forceUpdate);

						// Populate the UNI login user values
						uniLoginUser.Name = user.Name;
						uniLoginUser.LastUpdate = user.LastModified;
						uniLoginUser.UserType = (user.IsTeacher) ? UniLoginUserType.Teacher : UniLoginUserType.Student;
						uniLoginUser.PrimaryInstitutionId = user.PrimaryInstitutionId;
						uniLoginUser.PrimaryInstitutionName = user.PrimaryInstitutionName;
						var userClasses = schoolClassService.GetClassesForUser(user.UserId).ToList();

						uniLoginUser.Classes = userClasses.
							Select(userClass => new UniLoginClass(userClass.Name, userClass.ClassLevelId.HasValue ? userClass.ClassLevel.Name : null)).
							ToList();

						// Finish off by setting the current user ID
						SessionController.CurrentUserId = user.UserId;
						SessionController.IsCurrentUserTeacher = user.IsTeacher;

						// Also set the active class automatically if this is a student
						if (!user.IsTeacher && userClasses.Any())
						{
							SessionController.SetActiveUserClass(userClasses.First());
						}
					}
				}
			}
		}

		/// <summary>
		/// Synchronizes the given UNI login user with the user in the database.
		/// </summary>
		/// <param name="uniLoginUser"></param>
		public void SynchronizeUser(UniLoginUser uniLoginUser)
		{
			if (uniLoginUser.IsNull())
			{
				throw new ArgumentNullException("uniLoginUser");
			}

			bool forceUpdate;
			User user = UpsertUser(uniLoginUser, out forceUpdate);

			user.LastModified = DateTime.Now;

			if (forceUpdate)
			{
				// Delete any existing user class mappings
				DataContext.UserClasses.Where(dbUserClass => dbUserClass.UserId == user.UserId).
					ForEach(dbUserClass => DataContext.UserClasses.DeleteOnSubmit(dbUserClass));
			}

			DataContext.SubmitChanges();

			// Get the school for the user
			School school = SchoolClassServiceInstance.ForceGetSchool(uniLoginUser.PrimaryInstitutionId, uniLoginUser.PrimaryInstitutionName, forceUpdate);

			var currentSchoolYear = SchoolClassServiceInstance.CurrentSchoolYear;

			Class classInstance = null;

			// Loop the classes that the user is associated with
			foreach (var userClass in uniLoginUser.Classes)
			{
				// Get the class level for the user if any
				ClassLevel classLevel = userClass.Level.HasValue ? SchoolClassServiceInstance.GetClassLevel(userClass.Level.Value) : null;

				// Get the class for the user for the current school year
				classInstance = SchoolClassServiceInstance.ForceGetClass(userClass.Name, classLevel, school, currentSchoolYear, forceUpdate);

				// Check the user class mapping
				UpsertUserClass(user.UserId, classInstance.ClassId);

				if (user.IsTeacher && forceUpdate)
				{
					// Since this is a teacher also synchronize the students for the class
					foreach (var studentUniLoginUser in InfoService.Instance.GetUsersInClass(uniLoginUser.PrimaryInstitutionId, userClass.Name))
					{
						bool forceStudentUpdate;
						User student = UpsertUser(studentUniLoginUser, out forceStudentUpdate, primaryInstitutionId: user.PrimaryInstitutionId, primaryInstitutionName: user.PrimaryInstitutionName, isLogin: false);
						if (forceStudentUpdate)
						{
							UpsertUserClass(student.UserId, classInstance.ClassId);
						}
					}
				}
			}

			// Finish off by setting the current user ID
			SessionController.CurrentUserId = user.UserId;
			SessionController.IsCurrentUserTeacher = user.IsTeacher;

			// Also set the active class automatically if this is a student
			if (!user.IsTeacher)
			{
				SessionController.SetActiveUserClass(classInstance);
			}
		}

		/// <summary>
		/// Returns the students for the given class ID.
		/// If the class ID is null the CurrentUserClass will be used.
		/// </summary>
		/// <param name="classId"></param>
		/// <returns></returns>
		public IQueryable<User> GetStudentsForClass(int? classId = null)
		{
			if (!classId.HasValue)
			{
				classId = SessionController.CurrentUserClass.ClassId;
			}

			return GetUsersForClass(classId).
				Where(user => !user.IsTeacher);
		}

		/// <summary>
		/// Gets the student with the given ID within the given class.
		/// If the class ID is null the CurrentUserClass will be used.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="classId"></param>
		/// <returns></returns>
		public User GetStudent(int userId, int? classId = null)
		{
			return GetStudentsForClass(classId).
				Where(user => user.UserId == userId).
				FirstOrDefault();
		}

		/// <summary>
		/// Returns the teacher with the given ID and class.
		/// If the class ID is null the CurrentUserClass will be used.
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="classId"></param>
		/// <returns></returns>
		public User GetTeacher(int userId, int? classId = null)
		{
			return GetUsersForClass(classId).
				Where(user => user.IsTeacher).
				FirstOrDefault(user => user.UserId == userId);
		}
        public IQueryable<User> GetTeachersForSchool(string uniInstituionId)
        {
            return DataContext.Users.Where(w => w.IsTeacher && w.PrimaryInstitutionId == uniInstituionId);
        }

        #endregion
    }

}
