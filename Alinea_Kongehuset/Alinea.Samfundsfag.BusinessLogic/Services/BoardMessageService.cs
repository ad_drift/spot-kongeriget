﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Properties;
using umbraco.cms.businesslogic.media;
using System.IO;
using CoreM41.Web.Utilities;
using CoreM41.Diagnostics.Logging;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	public class BoardMessageService : ServiceBase
	{
		#region Constructors

		public BoardMessageService()
			: base()
		{
		}

		public BoardMessageService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		#region Methods

		#region Publics

		public int UpsertBoardMessage(int? boardMessageId, List<int> classes, string text, bool isPublished, FileUpload fileUpload)
		{
			BoardMessage boardMessage;
			if (boardMessageId.HasValue && boardMessageId.Value > 0)
			{
				boardMessage = GetBoardMessage(boardMessageId.Value);
			}
			else
			{
				boardMessage = new BoardMessage()
				{
					IsDeleted = false,
					CreatedBy = SessionController.CurrentUserId
				};
				DataContext.BoardMessages.InsertOnSubmit(boardMessage);
			}

			if (fileUpload.HasFile)
			{
				boardMessage.MediaFileId = HandleFile(fileUpload.PostedFile, boardMessage.MediaFileId);
			}

			boardMessage.Text = text;
			boardMessage.IsPublished = isPublished;
			boardMessage.ModifiedBy = SessionController.CurrentUserId;
			boardMessage.LastModified = DateTime.Now;

			DataContext.SubmitChanges();

			foreach (int classId in classes)
			{
				UpdateBoardMessageClassRelation(boardMessage, classId);
			}

			return boardMessage.BoardMessageId;
		}

		public void RelateBoardMessageToFile(int boardMessageId, int fileId)
		{
			var boardMessage = GetBoardMessage(boardMessageId);
			if (!boardMessage.IsNull())
			{
				boardMessage.MediaFileId = fileId;
				DataContext.SubmitChanges();	
			}
		}

		public List<Models.BoardMessage> GetTeachersBoardMessages(int userId)
		{			
			var dataBoardMessages = DataContext.BoardMessages.Where(boardMessage => boardMessage.CreatedBy == userId)
													.OrderByDescending(key => key.CreationDate);

			return GetBoardMessageModelsFromQuery(dataBoardMessages, userId);
		}

		public int CountGetStudentsBoardMessages(int classId)
		{
			return DataContext.BoardMessages.
						Where(boardMessage => boardMessage.BoardMessageClasses.Any(boardMessageClass => boardMessageClass.ClassId == classId) && boardMessage.IsPublished && !boardMessage.IsDeleted).
						Count();
		}

		public List<Models.BoardMessage> GetStudentsBoardMessages(int classId, int userId)
		{
			return GetStudentsBoardMessagesNoCache(classId, userId);
		}
				
		public Models.BoardMessage GetBoardMessageModel(int boardMessageId, int userId)
		{
			var boardMessage = this.GetBoardMessage(boardMessageId);

			return boardMessage.ToModel(IsMessageRead(userId, boardMessageId));
		}

		public BoardMessageClass GetBoardMessageClass(int classId, int boardMessageId)
		{
			return DataContext.BoardMessageClasses.FirstOrDefault(
													dcBoardMessageClass =>
													dcBoardMessageClass.BoardMessageId == boardMessageId &&
													dcBoardMessageClass.ClassId == classId);
		}

		public BoardMessage GetBoardMessage(int boardMessageId)
		{
			return DataContext.BoardMessages.FirstOrDefault(boardMessage => boardMessage.BoardMessageId == boardMessageId);
		}

		public void DeleteFile(int fileId, int boardMessageId)
		{
			BoardMessage boardMessage = GetBoardMessage(boardMessageId);
			if (boardMessage.MediaFileId == fileId)
			{
				try
				{
					Media media = new Media(boardMessage.MediaFileId.Value);
					media.delete(true);

					boardMessage.MediaFileId = null;
					DataContext.SubmitChanges();
				}
				catch (Exception exception)
				{
					Logger.LogError(exception);
				}
			}
		}

		public void DeleteMessage(string _messageId, bool includeMediaFile = true)
		{
			int messageId = 0;
			int.TryParse(_messageId, out messageId);
			if (messageId > 0)
			{
				BoardMessage boardMessage = GetBoardMessage(messageId);

				if (!boardMessage.IsNull())
				{
					if (includeMediaFile && boardMessage.MediaFileId.HasValue)
					{
						DeleteFile(boardMessage.MediaFileId.Value, boardMessage.BoardMessageId);
					}

					DataContext.BoardMessageUserReads.DeleteAllOnSubmit(boardMessage.BoardMessageUserReads);
					DataContext.BoardMessageClasses.DeleteAllOnSubmit(boardMessage.BoardMessageClasses);
					DataContext.BoardMessages.DeleteOnSubmit(boardMessage);
					DataContext.SubmitChanges();
				}
			}
		}

		public int MessageRead(int messageId, int userId, int classId)
		{
			var boardMessage = GetBoardMessage(messageId);
			if (!boardMessage.IsNull() && !IsMessageRead(userId, messageId))
			{
				DataContext.BoardMessageUserReads.InsertOnSubmit(new BoardMessageUserRead
					{
						UserId = userId,
						BoardMessageId = messageId
					});

				DataContext.SubmitChanges();
			}

			// messages marked as new
			var numberOfMessages = GetStudentsBoardMessages(classId, userId).Count();
			return numberOfMessages - GetBoardMessageUserReads(classId, userId);
		}

		public int GetBoardMessageUserReads(int classId, int userId)
		{
			return DataContext.BoardMessageUserReads.Count(boardMessageRead => 
						boardMessageRead.UserId == userId &&
						boardMessageRead.BoardMessage.BoardMessageClasses.Any(boardMessageClass => boardMessageClass.ClassId == classId) && 
						!boardMessageRead.BoardMessage.IsDeleted);
		}

		public bool IsMessageRead(int userId, int boardMessageId)
		{
			return DataContext.BoardMessageUserReads.FirstOrDefault(
				boardMessageRead => boardMessageRead.UserId == userId &&
				boardMessageRead.BoardMessageId == boardMessageId) != null;
		}

		#endregion

		#region Privates

		private List<Models.BoardMessage> GetStudentsBoardMessagesNoCache(int classId, int userId)
		{
			var dataBoardMessages = DataContext.BoardMessages.
				Where(boardMessage => boardMessage.BoardMessageClasses.Any(boardMessageClass => boardMessageClass.ClassId == classId) && boardMessage.IsPublished).
				OrderByDescending(key => key.CreationDate);

			return GetBoardMessageModelsFromQuery(dataBoardMessages, userId);
		}

		private List<Models.BoardMessage> GetBoardMessageModelsFromQuery(IQueryable<BoardMessage> boardMessagesQuery, int userId)
		{
			var boardMessagesFromDatabase = new List<BoardMessage>();

			try
			{
				boardMessagesFromDatabase = boardMessagesQuery.ToList();
			}
			catch (Exception exception)
			{
				Logger.LogError(exception);
			}

			var boardMessages = new List<Models.BoardMessage>();
			foreach (var boardMessage in boardMessagesFromDatabase)
			{
				boardMessages.Add(boardMessage.ToModel(IsMessageRead(userId, boardMessage.BoardMessageId)));
			}

			return boardMessages;
		}

		private void UpdateBoardMessageClassRelation(BoardMessage boardMessage, int classId)
		{
			BoardMessageClass boardMessageClass = GetBoardMessageClass(classId, boardMessage.BoardMessageId);

			if (boardMessageClass == null)
			{
				boardMessageClass = new BoardMessageClass()
				{
					BoardMessageId = boardMessage.BoardMessageId,
					ClassId = classId
				};

				boardMessage.BoardMessageClasses.Add(boardMessageClass);

				DataContext.SubmitChanges();
			}
		}

		private int HandleFile(HttpPostedFile postedFile, int? currentMediaId)
		{
			Media media;
			bool replaceExisting = false;
			if (currentMediaId.HasValue)
			{
				media = new Media(currentMediaId.Value);
				replaceExisting = true;
			}
			// Create a posted media file instance for handling of the media file
			PostedMediaFile postedMediaFile = new PostedMediaFile()
			{
				ContentLength = postedFile.ContentLength,
				ContentType = postedFile.ContentType,
				FileName = postedFile.FileName,
				InputStream = postedFile.InputStream
			};

			var mediaFactory = new UmbracoFileMediaFactory();

			int mediaFolderId = Settings.Default.TeacherMediaFolderId;

			umbraco.BusinessLogic.User umbracoUser = new umbraco.BusinessLogic.User(Settings.Default.TeacherUmbracoUserId);

			// Create a new media and handle the image
			media = mediaFactory.HandleMedia(mediaFolderId, postedMediaFile, umbracoUser, replaceExisting);
			//media.Text = postedMediaFile.FileName;
			media.Save();

			return media.Id;
		}

		#endregion

		#endregion
	}
}
