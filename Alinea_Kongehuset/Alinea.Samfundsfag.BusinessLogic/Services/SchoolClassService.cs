﻿using Alinea.Core.UniC.InfoTjeneste.V2;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using CoreM41.Diagnostics.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
    /// <summary>
    /// Service class for handling schools and classes.
    /// </summary>
    public class SchoolClassService : ServiceBase
    {
        #region Constructors

        public SchoolClassService()
            : base()
        {
        }

        public SchoolClassService(SamfundsfagUmbracoDataContext dataContext)
            : base(dataContext)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the active class levels.
        /// </summary>
        public IQueryable<ClassLevel> ClassLevels
        {
            get
            {
                return DataContext.ClassLevels.
                    Where(classLevel => classLevel.IsEnabled);
            }
        }

        /// <summary>
        /// Gets the classes.
        /// </summary>
        public IQueryable<Class> Classes
        {
            get
            {
                return DataContext.Classes;
            }
        }

        /// <summary>
        /// Gets the schools.
        /// </summary>
        public IQueryable<School> Schools
        {
            get
            {
                return DataContext.Schools;
            }
        }

        /// <summary>
        /// Gets the current school year.
        /// </summary>
        public SchoolYear CurrentSchoolYear
        {
            get
            {
                return DataContext.SchoolYears.
                    Where(schoolYear => schoolYear.StartDate < DateTime.Now).
                    OrderByDescending(schoolYear => schoolYear.StartDate).
                    First();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the class level instance for the given level.
        /// </summary>
        /// <param name="classLevelId"></param>
        /// <returns></returns>
        internal ClassLevel GetClassLevel(byte classLevelId)
        {
            return ClassLevels.SingleOrDefault(classLevel => classLevel.ClassLevelId == classLevelId);
        }

        /// <summary>
        /// Returns the class for the given level, name, school and school year.
        /// </summary>
        /// <param name="classLevel"></param>
        /// <param name="name"></param>
        /// <param name="school"></param>
        /// <param name="schoolYear"></param>
        /// <returns></returns>
        internal Class GetClass(string name, ClassLevel classLevel, School school, SchoolYear schoolYear)
        {
            var query = Classes.
                Where(dbClass => dbClass.Name == name && dbClass.SchoolId == school.SchoolId && dbClass.SchoolYearId == schoolYear.SchoolYearId);

            // JBunch @ Nov 25th 2013 - We no longer match classes by class level!
            // However there may be some existing classes with different class levels (or no class level).
            // Because of this we check to see if there is multiple classes created with the same class name,
            // and if there is we then fallback to the old code of using class level selection as well!
            var classes = query.ToList();

            if (classes.Count > 1 && classLevel != null)
            {
                classes = classes.
                    Where(dbClass => dbClass.ClassLevelId.HasValue && dbClass.ClassLevelId == classLevel.ClassLevelId).
                    ToList();
            }

            return classes.FirstOrDefault();
        }

        /// <summary>
        /// Returns the class with the given Id.
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        public Class GetClass(int classId)
        {
            return Classes.FirstOrDefault(dbClass => dbClass.ClassId == classId);
        }

        /// <summary>
        /// Returns the class for the given level, name, school and school year.
        /// If the class does not exist - one is created.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="classLevel"></param>
        /// <param name="school"></param>
        /// <param name="schoolYear"></param>
        /// <param name="forceUpdate"></param>
        /// <returns></returns>
        internal Class ForceGetClass(string name, ClassLevel classLevel, School school, SchoolYear schoolYear, bool forceUpdate)
        {
            // Get the class for the user for the current school year
            var classInstance = SchoolClassServiceInstance.GetClass(name, classLevel, school, schoolYear);

            // Create the class if is does not exist
            if (classInstance.IsNull())
            {
                forceUpdate = true;

                classInstance = new Class()
                {
                    // JBunch @ Nov 25th 2013 - We no longer match classes by class level and thus create new ones without a class level!
                    // Classes created before this date may have a class level associated but only one for the given name should exist
                    // Old code: ClassLevelId = classLevel.IsNull() ? null : (byte?)classLevel.ClassLevelId,
                    ClassLevelId = null,
                    SchoolId = school.SchoolId,
                    SchoolYearId = schoolYear.SchoolYearId
                };

                DataContext.Classes.InsertOnSubmit(classInstance);
            }

            if (forceUpdate)
            {
                classInstance.Name = name.Max(100);

                DataContext.SubmitChanges();
            }

            return classInstance;
        }

        /// <summary>
        /// Returns the school with the given institution ID.
        /// </summary>
        /// <param name="institutionId"></param>
        /// <returns></returns>
        public School GetSchool(string institutionId)
        {
            return Schools.FirstOrDefault(school => school.UniInstituionId == institutionId);
        }

        /// <summary>
        /// Returns the school with the given ID.
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        internal School GetSchool(int schoolId)
        {
            return Schools.FirstOrDefault(school => school.SchoolId == schoolId);
        }

        /// <summary>
        /// Returns the school with the given institution ID.
        /// If the school does not exist it will be created.
        /// </summary>
        /// <param name="institutionId"></param>
        /// <param name="name"></param>
        /// <param name="forceUpdate"></param>
        /// <returns></returns>
        internal School ForceGetSchool(string institutionId, string institutionName, bool forceUpdate)
        {
            School school = SchoolClassServiceInstance.GetSchool(institutionId);

            if (school.IsNull())
            {
                // Create a new school instance and force update
                forceUpdate = true;

                school = new School()
                {
                    UniInstituionId = institutionId.Max(50)
                };
                DataContext.Schools.InsertOnSubmit(school);
            }

            if (forceUpdate)
            {
                school.Name = institutionName.Max(250);

                DataContext.SubmitChanges();
            }

            return school;
        }

        public IQueryable<Class> GetClassesForSchool(int schoolId, SchoolYear schoolYear)
        {
            return from clss in DataContext.Classes
                   where clss.SchoolYearId == schoolYear.SchoolYearId
                   && clss.SchoolId == schoolId
                   select clss;
        }

        /// <summary>
        /// Returns the classes for the given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IQueryable<Class> GetClassesForUser(int userId, SchoolYear schoolYear = null)
        {
            if (schoolYear.IsNull())
            {
                schoolYear = CurrentSchoolYear;
            }

            try
            {
                return DataContext.UserClasses.
                    Where(userClass => userClass.UserId == userId && userClass.Class.SchoolYearId == schoolYear.SchoolYearId).
                    OrderBy(userClass => userClass.Class.Name).
                    Select(userClass => userClass.Class);
            }
            catch (Exception exception)
            {
                Logger.LogError(exception);
            }
            return null;
        }

        /// <summary>
        /// Returns the class for the given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="schoolYear"></param>
        /// <returns></returns>
        public Class GetClassForUser(int userId, int classId, SchoolYear schoolYear = null)
        {
            return GetClassesForUser(userId, schoolYear).
                FirstOrDefault(userClass => userClass.ClassId == classId);
        }

        #region Sync

        /// <summary>
        /// Syncronizes the database school with information from ALICE and STIL (UniLogin)
        /// </summary>
        /// <param name="institutionId">The id of the school to sync</param>
        /// <param name="thorough">If true the syncronization will update class levels, and update usertypes. But it will take much longer</param>
        /// <returns></returns>
        public bool SyncSchool(string institutionId, bool thorough = false)
        {
            SchoolYear schoolYear = CurrentSchoolYear;
            School school = GetSchool(institutionId);

            if (school == null)
            {
                return false;
            }

            try
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                // Get school and update info if thorough
                if (thorough)
                {
                    var serviceSchool = InfoService.Instance.GetInstitution(school.UniInstituionId);

                    if (!school.Name.Equals(serviceSchool.Navn))
                    {
                        school.Name = serviceSchool.Navn;
                    }
                }

                // Get classes
                //List<string> serviceClasses = new List<string>();
                // This is where you would use AuthenticationService.Instance.GetLicensedClasses if the project uses the class license system
                //serviceClasses = serviceClasses.Distinct().ToList();
                Klasse[] serviceClasses = InfoService.Instance.GetClasses(school.UniInstituionId);
                List<Class> databaseClasses = GetClassesForSchool(school.SchoolId, schoolYear).ToList();

                List<User> teachers = UserServiceInstance.GetTeachersForSchool(school.UniInstituionId).ToList();

                // Loop classes 
                List<string> classNames = serviceClasses.Select(s => s.Navn).ToList(); //.Distinct(StringComparer.OrdinalIgnoreCase).ToList();
                foreach (var cl in classNames.GroupBy(g => g.ToUpper()))
                {
                    string className = cl.First();
                    Class databaseClass = databaseClasses.FirstOrDefault(f => f.SchoolId == school.SchoolId && f.Name.Equals(className, StringComparison.OrdinalIgnoreCase));

                    if (databaseClass == null)
                    {
                        // If class is new or "new" for this schoolyear - insert to db
                        databaseClass = new Class()
                        {
                            // JBunch @ Nov 25th 2013 - We no longer match classes by class level and thus create new ones without a class level!
                            // Classes created before this date may have a class level associated but only one for the given name should exist
                            // Old code: ClassLevelId = classLevel.IsNull() ? null : (byte?)classLevel.ClassLevelId,
                            Name = className,
                            ClassLevelId = null,
                            SchoolId = school.SchoolId,
                            SchoolYearId = schoolYear.SchoolYearId
                        };

                        databaseClasses.Add(databaseClass);
                        DataContext.Classes.InsertOnSubmit(databaseClass);
                    }

                    //Get list of students in class from Uni-C (hentEleverPaaKlasse)
                    List<UniLoginUserSimple> serviceUsers = new List<UniLoginUserSimple>();

                    foreach (string cln in cl)
                    {
                        serviceUsers.AddRange(InfoService.Instance.GetUsersInClass(school.UniInstituionId, cln));
                    }

                    //Get list of existing students in class from db
                    var databaseUsers = UserServiceInstance.GetStudentsForClass(databaseClass.ClassId).ToList();

                    //Remove existing students that have been moved...
                    var wrongDatabaseUsers = databaseUsers.Where(w => !serviceUsers.Any(a => a.UserId == w.UniLoginId));

                    foreach (var wrongUser in wrongDatabaseUsers)
                    {
                        UserClass wrongUserClass = wrongUser.UserClasses.FirstOrDefault(f => f.Class.Name == className); //?
                        if (wrongUserClass != null)
                        {//Only delete it if it exists... 
                            DataContext.UserClasses.DeleteOnSubmit(wrongUserClass);
                        }
                    }

                    //No classlevels anymore?
                    //bool firstStudent = true;

                    //Get student and update info if thorough
                    //Loop students 
                    foreach (var serviceUser in serviceUsers)
                    {
                        //No classlevels anymore?
                        //Elev studentData = null;

                        var databaseUser = UserServiceInstance.GetUser(serviceUser.UserId);

                        #region No classlevels anymore?

                        //if (firstStudent || thorough) // || databaseUser == null
                        //{
                        //	studentData = InfoService.Instance.GetStudentData(serviceUser.UserId, school.UniInstituionId);
                        //}

                        //
                        //if (firstStudent && !String.IsNullOrEmpty(studentData.Klassetrin))
                        //{//Attempt to update classlevel
                        //	int classLevel = 0;
                        //	Int32.TryParse(studentData.Klassetrin, out classLevel);

                        //	if (classLevel > 0)
                        //	{
                        //		databaseClass.ClassLevelId = (byte)classLevel;
                        //		firstStudent = false;
                        //	}
                        //}
                        #endregion

                        //If student is new insert to db
                        if (databaseUser == null)
                        {
                            databaseUser = new User()
                            {
                                UniLoginId = serviceUser.UserId,
                                Name = serviceUser.Name,
                                IsTeacher = false,
                                PrimaryInstitutionId = institutionId,
                                PrimaryInstitutionName = school.Name
                            };

                            DataContext.Users.InsertOnSubmit(databaseUser);
                        }

                        if (!databaseUser.PrimaryInstitutionId.Equals(institutionId, StringComparison.OrdinalIgnoreCase))
                        {
                            Person uniData = InfoService.Instance.GetUserData(databaseUser.UniLoginId);

                            if (uniData.Instnr.Equals(institutionId, StringComparison.OrdinalIgnoreCase))
                            {//It looks like it is correct to move this student
                                databaseUser.PrimaryInstitutionId = institutionId;
                            }
                            else
                            {//This could indicate data inconsistencies in unilogin - or it could be a student that actually attends two different schools
                                Logger.LogWarning("Potential problem with student school relation: " + databaseUser.UniLoginId + " primary: " + uniData.Instnr + " but also: " + institutionId);
                            }
                        }

                        if (!databaseUser.Name.Equals(serviceUser.Name))
                        {
                            databaseUser.Name = serviceUser.Name;
                        }


                        UserClass databaseUserClass = databaseUser.UserClasses.FirstOrDefault(f => f.Class.Name.Equals(className, StringComparison.OrdinalIgnoreCase)
                            && f.Class.SchoolYearId == schoolYear.SchoolYearId); //NEW: OR: 2015-08-10 - This one seems important too...
                        if (databaseUserClass == null)
                        {
                            databaseClass.UserClasses.Add(new UserClass
                            {
                                User = databaseUser
                            });
                        }
                    }

                    //Ensure teachers have userclasses too (!)
                    foreach (User teacher in teachers)
                    {
                        UserClass databaseUserClass = teacher.UserClasses.FirstOrDefault(f => f.Class.Name.Equals(className, StringComparison.OrdinalIgnoreCase)
                            && f.Class.SchoolYearId == schoolYear.SchoolYearId); //NEW: TP: 2015-08-05 - Teacher clases year fix

                        if (databaseUserClass == null)
                        {
                            databaseClass.UserClasses.Add(new UserClass
                            {
                                User = teacher
                            });
                        }
                    }
                }

                //Teachers sometimes get stuck with classes from old schools, remove those too...
                foreach (User teacher in teachers)
                {
                    var wrongTeacherclasses = teacher.UserClasses.Where(w => w.Class.SchoolId != school.SchoolId);
                    DataContext.UserClasses.DeleteAllOnSubmit(wrongTeacherclasses);
                }

                // Remove classes that does not exist/have access anymore
                var wrongClasses = databaseClasses.Where(w => !serviceClasses.Any(a => a.Navn == w.Name));

                foreach (var wrongClass in wrongClasses)
                {
                    // If the class does not exist anymore, we probably don't need the students in it either...
                    DataContext.UserClasses.DeleteAllOnSubmit(wrongClass.UserClasses);

                    DataContext.Classes.DeleteOnSubmit(wrongClass);
                }

                school.LastSync = DateTime.Now;
                DataContext.SubmitChanges();

                stopwatch.Stop();
                Logger.LogInfo("School: " + school.UniInstituionId + " (" + school.Name + ") synced in " + stopwatch.Elapsed.TotalSeconds.ToString());

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }
        }

        /// <summary>
        /// Returns all active schools.
        /// </summary>
        /// <param name="hoursBeforeResynchronization"></param>
        /// <returns></returns>
        public List<School> GetAllActiveSchools(int hoursBeforeResynchronization, int monthsOfInactivityAllowed = 1)
        {
            //NEW: 2015-11-17: ALICE has a service with schools that are licensed to use this application.
            List<String> licensedSchools = new List<string>();

            licensedSchools.AddRange(AuthenticationService.Instance.GetLicensedSchools(Properties.Settings.Default.AlineaProductKey, Properties.Settings.Default.AlineaProductHash));

            // A user must have logged in less that a month ago.
            DateTime lastUserLoginDate = DateTime.Now.AddMonths(-monthsOfInactivityAllowed);
            var schools = DataContext.Schools.Where(school => DataContext.Users.Any(user => user.PrimaryInstitutionId == school.UniInstituionId && user.LastLoginDate > lastUserLoginDate));

            if (hoursBeforeResynchronization > 0)
            {
                // Only return schools that haven't been synchronized within the last X hours.
                DateTime lastSchoolSyncDate = DateTime.Now.AddHours(-hoursBeforeResynchronization);
                schools = schools.Where(school => school.LastSync < lastSchoolSyncDate);
            }

            //Only sync the school that have an active license
            List<School> schoolsToSync = schools.Where(w => licensedSchools.Contains(w.UniInstituionId)).ToList();

            return schoolsToSync;
        }

        #endregion

        #endregion
    }
}
