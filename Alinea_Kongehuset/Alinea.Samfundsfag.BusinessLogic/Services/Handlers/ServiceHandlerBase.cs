﻿using System;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using CoreM41.Web.Services.Generic;

namespace Alinea.Samfundsfag.BusinessLogic.Services.Handlers
{
	/// <summary>
	/// Base generic service handler for Daxas.
	/// </summary>
	/// <typeparam name="TRequest"></typeparam>
	/// <typeparam name="TResponse"></typeparam>
	public abstract class ServiceHandlerBase<TRequest, TResponse> : ServiceHandlerWithDataContext<SamfundsfagUmbracoDataContext, TRequest, TResponse>
		where TRequest : new()
		where TResponse : new()
	{
		//private Lazy<PollService> pollService;
		private Lazy<BoardMessageService> boardMessageService;

		public ServiceHandlerBase()
		{
			//pollService = new Lazy<PollService>(() => new PollService(DataContext));
			boardMessageService = new Lazy<BoardMessageService>(() => new BoardMessageService(DataContext));
		}

		protected  virtual  BoardMessageService BoardMessageServiceInstance
		{
			get
			{
				return boardMessageService.Value;
			}
		}

		/// <summary>
		/// Gets the PollService instance.
		/// </summary>
		//protected virtual PollService PollServiceInstance
		//{
		//    get
		//    {
		//        return pollService.Value;
		//    }
		//}
	}
}
