﻿using System;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using CoreM41.Core;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	public abstract class ServiceBase : ServiceBaseWithDataContext<SamfundsfagUmbracoDataContext>
	{
		#region Fields

		Lazy<SchoolClassService> schoolAndClassService;
		Lazy<UserService> userService;
		Lazy<BoardMessageService> boardMessageService;
		Lazy<UserArticleService> userArticleService;

		#endregion

		#region Constructors

		public ServiceBase()
			: this(null)
		{
		}

		public ServiceBase(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
			schoolAndClassService = new Lazy<SchoolClassService>(() => new SchoolClassService(DataContext));
			userService = new Lazy<UserService>(() => new UserService(DataContext));
			boardMessageService = new Lazy<BoardMessageService>(() => new BoardMessageService(DataContext));
			userArticleService = new Lazy<UserArticleService>(() => new UserArticleService(DataContext));
		}

		#endregion

		#region Properties


		/// <summary>
		/// Gets an instance of the class service class.
		/// </summary>
		protected SchoolClassService SchoolClassServiceInstance
		{
			get
			{
				return schoolAndClassService.Value;
			}
		}

		/// <summary>
		/// Gets an instance of the board message service class.
		/// </summary>
		protected BoardMessageService BoardMessageServiceInstance
		{
			get
			{
				return boardMessageService.Value;
			}
		}

		/// <summary>
		/// Gets an instance of the user service class.
		/// </summary>
		protected UserService UserServiceInstance
		{
			get
			{
				return userService.Value;
			}
		}

		/// <summary>
		/// Gets an instance of the user article service class.
		/// </summary>
		protected UserArticleService UserArticleServiceInstance
		{
			get
			{
				return userArticleService.Value;
			}
		}

		#endregion
	}
}
