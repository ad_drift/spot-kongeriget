﻿using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling Teacher Classes.
	/// </summary>
	public class TeacherClassService : ServiceBase
	{
		#region Constructors

		public TeacherClassService()
			: base()
		{
		}

		public TeacherClassService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		public List<int> GetSelectedClassIds(int teacherUserId)
		{
			var classIds = from tc in this.DataContext.TeacherClasses
						   where tc.UserId == teacherUserId
						   select tc.ClassId;

			return classIds.ToList();
		}

		public void SetSelectedClassIds(int teacherUserId, List<int> classIds)
		{
			this.DataContext.TeacherClasses.DeleteAllOnSubmit(from tc in this.DataContext.TeacherClasses
						   where tc.UserId == teacherUserId
						   select tc);

			this.DataContext.SubmitChanges();
			
			foreach(var classId in classIds)
			{
				this.DataContext.TeacherClasses.InsertOnSubmit(new TeacherClass()
				{
					UserId = teacherUserId,
					ClassId = classId,
					CreatedDate = DateTime.Now
				});
			}

			this.DataContext.SubmitChanges();
		}
	}
}
