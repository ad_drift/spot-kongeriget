﻿using System;
using System.Net;
using System.Web;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Alinea.Samfundsfag.BusinessLogic.Common;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;

using NCrawler;
using NCrawler.Interfaces;
using NCrawler.HtmlProcessor;
using umbraco.NodeFactory;
using System.Threading;
using System.Web.SessionState;
using umbraco.cms.businesslogic.web;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
    public class CrawlerService : ServiceBase
    {
        private static object crawlerRunLock = new object();

        #region Constructors

		    public CrawlerService()
			    : base()
		    {
		    }

			public CrawlerService(SamfundsfagUmbracoDataContext dataContext)
			    : base(dataContext)
		    {
		    }

		#endregion
        
        #region Public Methods

        public void ClearBrokenLinks()
        {
            DataContext.ExecuteCommand("DELETE FROM CrawlBrokenLinks");
            DataContext.SubmitChanges();
        }

        #endregion 

        #region Static Methods

        public static DateTime LatestCrawling
        {
            get
            {
                lock (crawlerRunLock)
                {
                    Document document = new Document(Node.GetNodeByXpath(string.Format("/{0}/{1}", 
                            Constants.NodeDocumentTypes.Root, 
                            Constants.NodeDocumentTypes.Homepage)).Id);

                    DateTime latestCrawling = new DateTime();
                    DateTime.TryParse(document.getProperty(Constants.NodePropertyNames.Crawler.LatestCrawling).Value.ToString(), out latestCrawling);

                    return latestCrawling;
                }
            }
            private set
            {
                lock (crawlerRunLock)
                {
                    Document document = new Document(Node.GetNodeByXpath(string.Format("/{0}/{1}",
                            Constants.NodeDocumentTypes.Root,
                            Constants.NodeDocumentTypes.Homepage)).Id);
                    document.getProperty(Constants.NodePropertyNames.Crawler.LatestCrawling).Value = value;
                }
            }
        }

        public static bool IsRequestCrawler()
        {
            if (HttpContext.Current.Request.QueryString[Constants.Crawler.Token] != null)
            {
                HttpContext.Current.Session[Constants.Crawler.Token] = HttpContext.Current.Request.QueryString[Constants.Crawler.Token];
            }
            return //HttpContext.Current.Request.IsLocal && 
                (HttpContext.Current.Session[Constants.Crawler.Token] != null || HttpContext.Current.Request.QueryString[Constants.Crawler.Token] != null);
        }

        public static IEnumerable<string> CrawlUrls()
        {
            List<string> list = new List<string>();

            /* Used when running a localserver on a specific port (http://localhost:65140/) */
            string host = HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;

            Node teacherNode = Node.GetNodeByXpath(string.Format("/{0}/{1}/{2}",
                        Constants.NodeDocumentTypes.Root,
                        Constants.NodeDocumentTypes.Homepage,
                        Constants.NodeDocumentTypes.TeacherMaster));
            Node studentNode = Node.GetNodeByXpath(string.Format("/{0}/{1}/{2}",
                        Constants.NodeDocumentTypes.Root,
                        Constants.NodeDocumentTypes.Homepage,
                        Constants.NodeDocumentTypes.StudentMaster));

            list.Add("http://" + host);
            list.Add(string.Format("http://" + host + "{0}?{1}=1", teacherNode.NiceUrl, Constants.Crawler.Token));
            list.Add(string.Format("http://" + host + "{0}?{1}=1", studentNode.NiceUrl, Constants.Crawler.Token));

            return list;
        }

        public static void RunCrawler(IEnumerable<string> crawlList)
        {
            LatestCrawling = DateTime.Now;
            Thread thread = new Thread(() => CrawlerThread(crawlList));
            thread.Start();
        }

        private static void CrawlerThread(IEnumerable<string> crawlList)
        {
            CrawlerService crawlerService = new CrawlerService();
            crawlerService.ClearBrokenLinks();
            CrawlerService.Run(crawlList);
        }

        #endregion

        #region Crawler

        private static void Run(IEnumerable<string> crawlList)
        {
            NCrawlerModule.Setup();
            // Demo 2 - Find broken links
            Console.Out.WriteLine("\nFind broken links");

            NCrawlerModule.Setup();

            foreach (var uri in crawlList)
            {
                using (Crawler c = new Crawler(new Uri(uri),
                new HtmlDocumentProcessor(), // Process html
                new DumpBrokenLinksStep()) // Custom pipeline Step
                {
                    MaximumThreadCount = 3,
                    MaximumCrawlDepth = 10,
                    AdhereToRobotRules = false,
                    AllowCrawlingExternalUrls = true,
                    UseCookies = true
                })
                {
                    // Begin crawl
                    c.Crawl();
                }
            }
        }

        /// <summary>
        /// 	Custom pipeline step, to dump url to console if status code is not ok
        /// </summary>
        internal class DumpBrokenLinksStep : ServiceBase, IPipelineStep
        {
            #region Constructors

                public DumpBrokenLinksStep()
			        : base()
		        {
		        }

				public DumpBrokenLinksStep(SamfundsfagUmbracoDataContext dataContext)
			        : base(dataContext)
		        {
		        }

                #endregion

            #region IPipelineStep Members

                public void Process(Crawler crawler, PropertyBag propertyBag)
                {
                    //Will print out every crawled url for debugging or understanding logic etc.
                    Console.Out.WriteLine(propertyBag.Step.Uri);
                    if (propertyBag.StatusCode != HttpStatusCode.OK)
                    {
                        CrawlBrokenLink brokenLink = new CrawlBrokenLink();

                        brokenLink.BrokenUrl = propertyBag.Step.Uri.ToString();
                        brokenLink.ReferrerUrl = propertyBag.OriginalReferrerUrl.ToString();
                        brokenLink.RegisteredDate = DateTime.Now;

                        DataContext.CrawlBrokenLinks.InsertOnSubmit(brokenLink);
                        DataContext.SubmitChanges();

                        Console.Out.WriteLine("Url '{0}' referenced from {1} returned with statuscode {2}",
                            propertyBag.Step.Uri, propertyBag.OriginalReferrerUrl, propertyBag.StatusCode);
                        Console.Out.WriteLine();
                    }

                    LatestCrawling = DateTime.Now;
                }

            #endregion
        }

        #endregion
    }
}
/*
Yes this is what the crawler looks like so watch out!


           ;               ,           
         ,;                 '.         
        ;:                   :;         
       ::                     ::       
       ::                     ::       
       ':                     :         
        :.                    :         
     ;' ::                   ::  '     
    .'  ';                   ;'  '.     
   ::    :;                 ;:    ::   
   ;      :;.             ,;:     ::   
   :;      :;:           ,;"      ::   
   ::.      ':;  ..,.;  ;:'     ,.;:   
    "'"...   '::,::::: ;:   .;.;""'     
        '"""....;:::::;,;.;"""         
    .:::.....'"':::::::'",...;::::;.   
   ;:' '""'"";.,;:::::;.'""""""  ':;   
  ::'         ;::;:::;::..         :;   
 ::         ,;:::::::::::;:..       :: 
 ;'     ,;;:;::::::::::::::;";..    ':. 
::     ;:"  ::::::"""'::::::  ":     :: 
 :.    ::   ::::::;  :::::::   :     ; 
  ;    ::   :::::::  :::::::   :    ;   
   '   ::   ::::::....:::::'  ,:   '   
    '  ::    :::::::::::::"   ::       
       ::     ':::::::::"'    ::       
       ':       """""""'      ::       
        ::                   ;:         
        ':;                 ;:"         
          ';              ,;'           
            "'           '"    

 
 */
