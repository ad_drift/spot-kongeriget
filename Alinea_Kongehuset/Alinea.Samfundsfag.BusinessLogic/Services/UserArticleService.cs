﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Futuristic.Common.Extensions;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling User Articles.
	/// </summary>
	public class UserArticleService : ServiceBase
	{
		#region Constructors

		public UserArticleService()
			: base()
		{
		}

		public UserArticleService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		/// <summary>
		/// Create a new user article or update an existing one.
		/// </summary>
		/// <param name="articleId">Optional. Inclide article id to edit an existing user article.</param>
		/// <returns>Id of the user article.</returns>
		public int SaveUserArticle(
			string title, string introduction,
			string text1, int text1SortOrder,
			string text2, int text2SortOrder,
			string image1Text, int image1SortOrder,
			string image2Text, int image2SortOrder,
			string image3Text, int image3SortOrder,
			string video1YouTubeUrl, int video1SortOrder,
			string audio1Text, int audio1SortOrder,
			string timelineHeadline, int timelineActiveItem,
			int assignmentId,
			List<int> coAuthorIds,
			List<int> subjectIds,
			int userId, int classId,
			List<Models.UserArticleTimelineItem> timelineItems,
			int articleId = 0)
		{
			// Get existing user article
			var userArticle = (from ua in this.DataContext.UserArticles
								where ua.Id == articleId && ua.CreatedBy == userId
								select ua).FirstOrDefault();

			// Init a new user article of existing user article was not found
			if (userArticle == null)
			{
				userArticle = new UserArticle();
				userArticle.IsDeleted = false;
				userArticle.IsPublished = false;
				userArticle.CreatedBy = userId;
				userArticle.CreatedDate = DateTime.Now;
				userArticle.ClassId = classId;
				userArticle.AssignmentId = assignmentId > 0 ? assignmentId : default(int?);
			}

			// Set values
			userArticle.Title = title.CropText(2048, false);
			userArticle.Introduction = introduction ?? "";

			userArticle.Text1 = text1;
			userArticle.Text1SortOrder = text1SortOrder;
			userArticle.Text2 = text2;
			userArticle.Text2SortOrder = text2SortOrder;

			userArticle.Image1Text = image1Text.CropText(2048, false);
			userArticle.Image1SortOrder = image1SortOrder;
			userArticle.Image2Text = image2Text.CropText(2048, false);
			userArticle.Image2SortOrder = image2SortOrder;
			userArticle.Image3Text = image3Text.CropText(2048, false);
			userArticle.Image3SortOrder = image3SortOrder;

			userArticle.Video1YouTubeUrl = video1YouTubeUrl;
			userArticle.Video1SortOrder = video1SortOrder;

			userArticle.Audio1Text = audio1Text.CropText(2048, false);
			userArticle.Audio1SortOrder = audio1SortOrder;

			userArticle.TimelineHeadline = timelineHeadline.CropText(2048, false);
			userArticle.TimelineActiveItem = timelineActiveItem;

			foreach (var item in userArticle.UserArticleTimelineItems)
			{
				var updateItem = timelineItems.First(c => c.Id == item.Id);
				item.Label = updateItem.Label.CropText(10, false);
				item.Title = updateItem.Title.CropText(50, false);
				item.Description = updateItem.Description.CropText(400, false);
				item.Position = updateItem.Position;
			}
			userArticle.UserArticleTimelineItems.AddRange(
				timelineItems.Where(c => c.Id == 0).Select(c => new UserArticleTimelineItem
				{
					Label = c.Label.CropText(10, false),
					Title = c.Title.CropText(50, false),
					Description = c.Description.CropText(400, false),
					Position = c.Position,
					CreatedBy = userId,
					CreatedDate = DateTime.Now
				}));
			
			// Insert into database
			if (userArticle.Id == 0)
			{
				this.DataContext.UserArticles.InsertOnSubmit(userArticle);
			}
			
			// Save
			this.DataContext.SubmitChanges();
				
			// Update co-authors
			this.DataContext.UserArticleCoAuthors.DeleteAllOnSubmit(this.DataContext.UserArticleCoAuthors.Where(uaca => uaca.UserArticleId == userArticle.Id));
			this.DataContext.SubmitChanges();
			
			foreach (var coAuthorId in coAuthorIds)
			{
				this.DataContext.UserArticleCoAuthors.InsertOnSubmit(new UserArticleCoAuthor() { UserArticleId = userArticle.Id, UserId = coAuthorId });
			}

			// Subjects
			this.DataContext.UserArticleSubjects.DeleteAllOnSubmit(this.DataContext.UserArticleSubjects.Where(uas => uas.UserArticleId == userArticle.Id));
			this.DataContext.SubmitChanges();

			foreach (var subjectId in subjectIds)
			{
				this.DataContext.UserArticleSubjects.InsertOnSubmit(new UserArticleSubject() { UserArticleId = userArticle.Id, SubjectId = subjectId });
			}

			this.DataContext.SubmitChanges();

			// Return
			return userArticle.Id;
		}

		/// <summary>
		/// Publish a user article. A user article can only be published once. Only the owner of the user article can publish it.
		/// </summary>
		public void PublishUserArticle(int articleId, int userId)
		{
			var userArticle = (from ua in this.DataContext.UserArticles
							   where ua.Id == articleId && ua.CreatedBy == userId && !ua.IsPublished
							   select ua).FirstOrDefault();

			if (userArticle != null)
			{
				userArticle.IsPublished = true;
				userArticle.PublishedDate = DateTime.Now;

				this.DataContext.SubmitChanges();
			}
		}

		public Models.UserArticle GetUserArticle(int userArticleId)
		{
			var userArticle = from ua in this.DataContext.UserArticles
							  where ua.Id == userArticleId
							  select ua;

			return userArticle.Select(ua => ua.ToModel()).FirstOrDefault();
		}

		public void DeleteUserArticle(int userArticleId, int userId)
		{
			var us = new UserService();
			var user = us.GetUser(userId);

			var userArticle = (from ua in this.DataContext.UserArticles
							   where ua.Id == userArticleId && (ua.CreatedBy == userId || user.IsTeacher)
							   select ua).FirstOrDefault();

			if(userArticle != null)
			{
				userArticle.IsDeleted = true;
				userArticle.ModifiedBy = userId;
				userArticle.ModifiedDate = DateTime.Now;
				this.DataContext.SubmitChanges();
			}
		}

		public List<Models.UserArticle> GetUserArticles(int classId, int? userId = null)
		{
			if (!userId.HasValue)
				userId = 0;

			var userArticles = from ua in this.DataContext.UserArticles
							   where ua.ClassId == classId && !ua.IsDeleted
							   let userIsAuthor = userId > 0 && ua.CreatedBy == userId.Value
							   let userIsCoAuthor = userId > 0 && ua.UserArticleCoAuthors.Count(ca => ca.UserId == userId.Value) > 0
							   where (userId == 0 && ua.IsPublished) || (userIsAuthor || userIsCoAuthor)
							   orderby ua.IsPublished, ua.PublishedDate descending, ua.CreatedDate descending
							   select ua;

			var articles = userArticles.Select(ua => ua.ToModel()).ToList();

			articles.RemoveAll(a => a.Title == string.Empty && a.Introduction == string.Empty && !a.HasCoverImage && a.Text1 == string.Empty && a.Text2 == string.Empty && !a.HasImage1 && a.Image1Text == string.Empty && !a.HasImage2 && a.Image2Text == string.Empty && !a.HasImage3 && a.Image3Text == string.Empty && !a.HasAudio1 && a.Audio1Text == string.Empty && a.Video1YouTubeUrl == string.Empty);

			return articles;
		}

		public List<Models.UserArticle> GetUserArticlesDebate(int classId, int userId)
		{
			var userArticles = from ua in this.DataContext.UserArticles
							   where ua.ClassId == classId && !ua.IsDeleted && ua.IsPublished
							   let userIsAuthor = ua.CreatedBy == userId
							   let userIsCoAuthor = ua.UserArticleCoAuthors.Count(ca => ca.UserId == userId) > 0
							   let userHasCommented = ua.UserArticleComments.Count(c => c.CreatedBy == userId && c.ArticleId == ua.Id) > 0
							   where !userIsAuthor && !userIsCoAuthor && userHasCommented
							   let latestCommentDate = ua.UserArticleComments.OrderByDescending(c => c.CreatedDate).First().CreatedDate
							   orderby latestCommentDate descending
							   select ua;

			return userArticles.Select(ua => ua.ToModel()).ToList();
		}

		public List<string> GetCoAuthors(int userArticleId)
		{
			var coAuthors = from ca in this.DataContext.UserArticleCoAuthors
							where ca.UserArticleId == userArticleId
							select ca.User.Name;

			return coAuthors.ToList();
		}

		public List<int> GetCoAuthorIds(int userArticleId)
		{
			var coAuthors = from ca in this.DataContext.UserArticleCoAuthors
							where ca.UserArticleId == userArticleId
							orderby ca.User.Name
							select ca.User.UserId;

			return coAuthors.ToList();
		}

		public DateTime? LastSeen(int userArticleId, int userId)
		{
			var lastSeen = from ls in this.DataContext.UserArticleLastSeens
						   where ls.UserArticleId == userArticleId && ls.UserId == userId
						   select ls.LastSeenDate;

			return lastSeen.FirstOrDefault();
		}

		public void IHaveSeenIt(int userArticleId, int userId)
		{
			if (this.DataContext.UserArticles.Exists(ua => ua.Id == userArticleId))
			{
				var lastSeen = (from ls in this.DataContext.UserArticleLastSeens
								where 
									ls.UserArticleId == userArticleId && 
									ls.UserId == userId
								select ls).FirstOrDefault();

				if (lastSeen == null)
				{
					lastSeen = new UserArticleLastSeen()
					{
						UserArticleId = userArticleId,
						UserId = userId,
						LastSeenDate = DateTime.Now
					};
					this.DataContext.UserArticleLastSeens.InsertOnSubmit(lastSeen);
					this.DataContext.SubmitChanges();
				}
				else
				{
					lastSeen.LastSeenDate = DateTime.Now;
					this.DataContext.SubmitChanges();
				}
			}
		}

		public int GetUnseenArticlesCount(int classId, int userId)
		{
			var unseenUserArticles = from ua in this.DataContext.UserArticles
									 where
										ua.ClassId == classId &&
										ua.IsPublished &&
										!ua.IsDeleted &&
										ua.Title != string.Empty &&
										ua.UserArticleLastSeens.Where(uals => uals.UserId == userId).Count() == 0
									 select ua;

			return unseenUserArticles.Count();
		}
	}
}
