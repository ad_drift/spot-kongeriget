﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;

namespace Alinea.Samfundsfag.BusinessLogic.Services
{
	/// <summary>
	/// Service class for handling subjects
	/// </summary>
	public class SubjectService : ServiceBase
	{
		#region Constructors

		public SubjectService()
			: base()
		{
		}

		public SubjectService(SamfundsfagUmbracoDataContext dataContext)
			: base(dataContext)
		{
		}

		#endregion

		public IQueryable<Subject> GetSubjects()
		{
			var q = DataContext.Subjects.OrderBy(s => s.Name);
			return q;
		}

		public List<string> GetSubjects(int userArticleId)
		{
			var subjects = from uas in this.DataContext.UserArticleSubjects
							where uas.UserArticleId == userArticleId
							orderby uas.Subject.Name
							select uas.Subject.Name;

			return subjects.ToList();
		}

		public List<int> GetSubjectIds(int userArticleId)
		{
			var subjects = from uas in this.DataContext.UserArticleSubjects
						   where uas.UserArticleId == userArticleId
						   orderby uas.Subject.Name
						   select uas.Subject.Id;

			return subjects.ToList();
		}
	}
}
