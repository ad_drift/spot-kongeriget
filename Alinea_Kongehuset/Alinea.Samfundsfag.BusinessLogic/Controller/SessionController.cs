﻿using System;
using Alinea.Core.Common.Enums;
using Alinea.Core.Session;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;

namespace Alinea.Samfundsfag.BusinessLogic.Controller
{
	/// <summary>
	/// Controller for accessing session variables.
	/// </summary>
	public class SessionController : BaseSessionController<Alinea.Samfundsfag.BusinessLogic.Controller.SessionController.SessionVariables>
	{
		private static SessionController instance = new SessionController();

		/// Sessions die so this class needs a backup of all variables in a cookie. 
		public enum SessionVariables
		{
			SessionControllerActiveUserClass,
			SessionControllerCurrentUserId,
			SessionControllerCurrentUserIsTeacher,
			[SupportCookie()]
			SessionControllerCurrentUserExamTestId
		}

		#region Public Properties & Methods

		/// <summary>
		/// Gets the ID of logged in user.
		/// </summary>
		public static int CurrentUserId
		{
			get
			{
				return instance.GetValue<int>(SessionVariables.SessionControllerCurrentUserId);
			}
			internal set
			{
				instance.SetValue(SessionVariables.SessionControllerCurrentUserId, value);
			}
		}

		/// <summary>
		/// Gets whether the current user is a student.
		/// </summary>
		public static bool IsCurrentUserStudent
		{
			get
			{
				return !IsCurrentUserTeacher;
			}
		}

		/// <summary>
		/// Gets whether the current user is a teacher.
		/// </summary>
		public static bool IsCurrentUserTeacher
		{
			get
			{
				return instance.GetValue<bool>(SessionVariables.SessionControllerCurrentUserIsTeacher);
			}
			internal set
			{
				instance.SetValue(SessionVariables.SessionControllerCurrentUserIsTeacher, value);
			}
		}

		/// <summary>
		/// Get and set the user exam currently being taken.
		/// </summary>
		public static int CurrentUserExamTestId
		{
			get
			{
				return instance.GetValue<int>(SessionVariables.SessionControllerCurrentUserExamTestId, defaultValue: -1);
			}
			set
			{
				instance.SetValue(SessionVariables.SessionControllerCurrentUserExamTestId, value);
			}
		}

		/// <summary>
		/// Gets the active user class.
		/// </summary>
		public static ActiveUserClass CurrentUserClass
		{
			get
			{
				if (!HasUserClass)
				{
					throw new ArgumentException("No active user class found");
				}

				return instance.GetValue<ActiveUserClass>(SessionVariables.SessionControllerActiveUserClass);
			}
		}

		/// <summary>
		/// Gets whether an active user class is set.
		/// </summary>
		public static bool HasUserClass
		{
			get
			{
				return instance.Exists(SessionVariables.SessionControllerActiveUserClass);
			}
		}

		/// <summary>
		/// Sets the active user class for the session.
		/// If classInstance is null the active user class will be removed.
		/// </summary>
		/// <param name="classInstance"></param>
		public static void SetActiveUserClass(Class classInstance)
		{
			if (classInstance.IsNull())
			{
				instance.Remove(SessionVariables.SessionControllerActiveUserClass);
			}
			else
			{
				instance.SetValue(SessionVariables.SessionControllerActiveUserClass, new ActiveUserClass()
				{
					ClassId = classInstance.ClassId,
					ClassName = classInstance.Name,
					SchoolId = classInstance.SchoolId,
					SchoolName = classInstance.School.Name,
					ClassLevelId = classInstance.ClassLevelId
				});
			}
		}

		#endregion
	}

}
