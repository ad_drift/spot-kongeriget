﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreM41.Web.UI;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using umbraco.NodeFactory;
using Alinea.Samfundsfag.BusinessLogic.Utilities;

namespace Alinea.Samfundsfag.BusinessLogic.UI
{
	public abstract class UserControlBase : UserControlBaseWithDataContext<SamfundsfagUmbracoDataContext>
    {
        #region Fields

        private Lazy<BoardMessageService> messageService;
        private Lazy<PollService> pollService;
        private Lazy<SchoolClassService> schoolClassService;
        private Node currentPage;

        #endregion

        #region Constructor

        public UserControlBase()
        {
            messageService = new Lazy<BoardMessageService>(() => new BoardMessageService(DataContext));
            pollService = new Lazy<PollService>(() => new PollService(DataContext));
            schoolClassService = new Lazy<SchoolClassService>(() => new SchoolClassService(DataContext));
            currentPage = Node.GetCurrent();
        }

        #endregion

        #region Properties

        protected Node CurrentPage
        {
            get
            {
                return currentPage;
            }
        }

        protected SchoolClassService SchoolClassService
        {
            get
            {
                return schoolClassService.Value;
            }
        }

        protected BoardMessageService BoardMessageService
        {
            get
            {
                return messageService.Value;
            }
        }

        protected PollService PollService
        {
            get
            {
                return pollService.Value;
            }
        }

        protected bool IsUserLoggedIn
        {
            get
            {
                return UniLoginController.IsValidSsoSession && UniLoginController.IsLoggedInUserTeacherOrStudent;
            }
        }

        protected int CurrentUserId
        {
            get
            {
                return SessionController.CurrentUserId;
            }
        }

        protected int CurrentClassId
        {
            get
            {
                return SessionController.CurrentUserClass.ClassId;
            }
        }

        protected bool IsUserLoggedInTeacher
        {
            get
            {
                return SessionController.IsCurrentUserTeacher;
            }
        }

        protected bool IsUserLoggedInStudent
        {
            get
            {
                return SessionController.IsCurrentUserStudent;
            }
        }

        protected bool HasUserClass
        {
            get
            {
                return SessionController.HasUserClass;
            }
        }

		protected bool IsStudentPage
		{
			get
			{
				return Request.Url.ToString().Contains(UmbracoHelper.StudentPage.NiceUrl);
			}
		}

		protected bool IsTeacherPage
		{
			get
			{
				return Request.Url.ToString().Contains(UmbracoHelper.TeacherPage.NiceUrl);
			}
		}

        #endregion
    }
}
