﻿using Alinea.Samfundsfag.BusinessLogic.Common.Enums;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using CoreM41.Web.UI;
using CoreM41.Web.Utilities;
using umbraco.editorControls.userControlGrapper;

namespace Alinea.Samfundsfag.BusinessLogic.UI
{
	public class DataTypeUserControlBase : UserControlBaseWithDataContext<SamfundsfagUmbracoDataContext>, IUsercontrolDataEditor
	{
		private string umbracoValue;

		/// <summary>
		/// Gets or sets the umbraco data type value.
		/// </summary>
		public object value
		{
			get
			{
				return umbracoValue;
			}
			set
			{
				umbracoValue = value.ToString();
			}
		}

		public int NodeId
		{
			get
			{
				return int.Parse(PageUtility.GetQueryStringVariable<QueryStringVariables>(QueryStringVariables.Id));
			}
		}
	}
}
