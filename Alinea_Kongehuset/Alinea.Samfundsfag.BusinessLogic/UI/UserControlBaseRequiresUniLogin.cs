﻿using System;
using System.Linq;
using Alinea.Core.UniLogin;
using Alinea.Samfundsfag.BusinessLogic.Common.Enums;
using Alinea.Samfundsfag.BusinessLogic.Controller;
using Alinea.Samfundsfag.BusinessLogic.Properties;
using Alinea.Samfundsfag.BusinessLogic.Services;
using Alinea.Samfundsfag.BusinessLogic.DataAccess;
using Alinea.Samfundsfag.BusinessLogic.Utilities;

namespace Alinea.Samfundsfag.BusinessLogic.UI
{
	/// <summary>
	/// User control base class with login check.
	/// If no user is logged in a redirect to the UNI login site is performed.
	/// </summary>
	public abstract class UserControlBaseRequiresUniLogin : UserControlBase
	{
		public UserControlBaseRequiresUniLogin()
		{
			UserAccess = UserAccess.StudentOrTeacher;
		}

		private UserAccess UserAccess
		{
			get;
			set;
		}

		/// <summary>
		/// Defines the required user access setting.
		/// </summary>
		public string RequiredUserAccess
		{
			set
			{
				UserAccess = Enums.Get<UserAccess>(value);
			}
		}

		/// <summary>
		/// Method called after the user access has been authenticated.
		/// </summary>
		private void ProcessAuthentication()
		{
			if (UniLoginController.AccessDenied)
			{
				UserService.HandleAccessDenied();
			}

			if (UserAccess == UserAccess.StudentOrTeacher)
			{
				if (!UniLoginController.IsLoggedInUserTeacherOrStudent)
				{
					UserService.HandleAccessDenied();
				}

				if (SessionController.IsCurrentUserTeacher && !SessionController.HasUserClass)
				{
					// We have a teacher login but no active user class
					// On samfundsfag that should be OK since we don't use the active class for anything
                    //  __      __
                    // ( _\    /_ )
                    //  \ _\  /_ / 
                    //   \ _\/_ /_ _
                    //   |_____/_/ /|
                    //   (  (_)__)J-)
                    //   (  /`.,   /
                    //    \/  ;   /
                    //     | === |
				}
			}
			else if (UserAccess == UserAccess.Teacher)
			{
				if (!UniLoginController.IsLoggedInUserTeacher)
				{
                    UserService.HandleAccessDenied();
				}
                else if (SessionController.HasUserClass)
                {
                    Response.Redirect(UmbracoHelper.StudentPage.NiceUrl);
                }
            }
            else if (UserAccess == UserAccess.Student)
            {
                if (!SessionController.HasUserClass && SessionController.IsCurrentUserTeacher)
                {
                    //Redirect teacher
                    Response.Redirect(UmbracoHelper.TeacherPage.NiceUrl);
                }
            }
		}

		protected void Page_Init(object sender, EventArgs e)
		{
            if (!CrawlerService.IsRequestCrawler())
            {
                if (!UniLoginController.IsValidSsoSession)
                {
                    AuthenticationService.RedirectToLoginPage(Request.Url.ToString());
                }
                else
                {
                    ProcessAuthentication();
                }
            }
            else
            {
                /* Crawler access - work around */
                /* Comment out the code below and use this to bypass the UNI Login */
                SessionController.CurrentUserId = 2;
                SessionController.IsCurrentUserTeacher = true;
                SessionController.SetActiveUserClass(DataContext.Classes.FirstOrDefault(c => c.ClassId == 1));
            }
		}
	}
}
